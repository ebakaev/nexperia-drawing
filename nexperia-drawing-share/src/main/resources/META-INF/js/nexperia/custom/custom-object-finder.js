(function () {
    var Dom = YAHOO.util.Dom, Event = YAHOO.util.Event;

    Alfresco.CustomObjectFinder = function Alfresco_CustomObjectFinder(htmlId,
                                                                       currentValueHtmlId) {
        Alfresco.CustomObjectFinder.superclass.constructor.call(this, htmlId,
            currentValueHtmlId);

        // Re-register with our own name
        this.name = "Alfresco.CustomObjectFinder";
        Alfresco.util.ComponentManager.reregister(this);

        return this;
    };

    YAHOO.extend(Alfresco.CustomObjectFinder, Alfresco.ObjectFinder, {
        _inAuthorityMode: function RTObjectSearcher__inAuthorityMode() {
            if (this.options.searchMode === "true") {
                //this.options.startLocation = "/app:company_home/st:sites/cm:tdm-drawing";
                return true;
            } else {
                return false;
            }
        },
        canItemBeSelected: function ObjectFinder_canItemBeSelected(id) {
            if (!this.options.multipleSelectMode && this.singleSelectedItem !== null) {
                return false;
            }
            if (this.options.currentItem === id) {
                return false;
            }
            var arr = Object.keys(this.selectedItems);
            // debugger;
            if(this.options.selectCount != undefined) {
                if (arr.length >= this.options.selectCount) {
                    return false;
                }
            }
            return (this.selectedItems[id] === undefined);
        }
    });
})();