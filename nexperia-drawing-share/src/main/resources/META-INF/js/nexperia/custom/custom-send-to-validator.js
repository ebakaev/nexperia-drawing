/**
 * Corporate Document Expiration and Publication date's validation handler
 *
 * @method corporateDatesValidation
 * @param field {object} The element representing the field the validation is for
 * @param args {object} Not used
 * @param event {object} The event that caused this handler to be called, maybe null
 * @param form {object} The forms runtime class instance the field is being managed by
 * @param silent {boolean} Determines whether the user should be informed upon failure
 * @param message {string} Message to display when validation fails, maybe null
 * @static
 */
Alfresco.forms.validation.checkEmail = function checkEmail(field, args, event, form, silent, message) {

    var send_to = field.value;
    if (!send_to) {
        field.value = form.ajaxSubmitHandlers.successCallback.scope.config.success.obj.jsNode.properties["draw:requester"];;
        return true;
    } else {
        var re = /(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))/;
        if (send_to.includes(";")) {
            var emails = send_to.split(";");
            for (var i=0; i<emails.length;i++) {
                var emailToCheck = emails[i].trim();
                if (!re.test(emailToCheck)) {
                   return false;
                }
            }
            return true;
        } else {
            return re.test(String(send_to).toLowerCase());
        }
    }

};