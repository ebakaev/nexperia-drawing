(function () {

//    getDefaultFolderIconForMC64 = function () {
//        return "nexperia-drawing-share/components/documentlibrary/images/drawingSpace-64.png";
//    };
//
//    getDefaultFolderIconForMC48 = function () {
//        return "nexperia-drawing-share/components/documentlibrary/images/drawingSpace-48.png";
//    };
//
//    getDefaultFolderIconForMC32 = function () {
//        return "nexperia-drawing-share/components/documentlibrary/images/drawingSpace-32.png";
//    };
//
//    if (Alfresco.DocumentListViewRenderer != null) {
//
//        Alfresco.DocumentListViewRenderer.prototype.getFolderIcon = function (node) {
//            var filterChain;
//            var size = this.getIconSize();
//            var s = size.substring(0, size.indexOf("x"));
//            var png;
//            if (node.type == "draw:drawingSpace") {
//                if (s == "64") {
//                    png = getDefaultFolderIconForMC64();
//                } else if (s == "48") {
//                    png = getDefaultFolderIconForMC48();
//                } else if (s == "32") {
//                    png = getDefaultFolderIconForMC32();
//                } else {
//                    png = getDefaultFolderIconForMC32();
//                }
//                filterChain = new Alfresco.CommonComponentIconFilterChain(node, this.folderIconConfig, png, this.getIconSize());
//            } else if (node.type == "cm:folder") {
//                filterChain = new Alfresco.CommonComponentIconFilterChain(node, this.folderIconConfig, this.getDefaultFolderIcon(), this.getIconSize());
//            } else {
//                filterChain = new Alfresco.CommonComponentIconFilterChain(node, this.folderIconConfig, this.getDefaultFolderIcon(), this.getIconSize());
//            }
//            var folderIconStr = filterChain.createIconResourceName();
//            return Alfresco.constants.URL_RESCONTEXT + folderIconStr;
//        };
//
//        Alfresco.DocumentList.generateThumbnailUrl = function (record, renditionName) {
//            var jsNode = record.jsNode,
//                nodeRef = jsNode.isLink ? jsNode.linkedNode.nodeRef : jsNode.nodeRef;
//
//            if (jsNode.type == "sg:UIItem") {
//                var png = 'components/documentlibrary/images/ui-item-64.png';
//                var filterChain = new Alfresco.CommonComponentIconFilterChain(jsNode, this.folderIconConfig, png, "64x64");
//                var folderIconStr = filterChain.createIconResourceName();
//                return Alfresco.constants.URL_RESCONTEXT + folderIconStr;
//            } else if(jsNode.type == "sg:mediaKit") {
//                var png = 'components/documentlibrary/images/mediaKit-64.png';
//                var filterChain = new Alfresco.CommonComponentIconFilterChain(jsNode, this.folderIconConfig, png, "64x64");
//                var folderIconStr = filterChain.createIconResourceName();
//                return Alfresco.constants.URL_RESCONTEXT + folderIconStr;
//            }
//            if (jsNode.isLink && !nodeRef) {
//                // broken link has no thumbnail
//                return "";
//            }
//            if (renditionName == null)
//                renditionName = "doclib";
//            return Alfresco.util.generateThumbnailUrl(jsNode, renditionName);
//        };
//    }

    YAHOO.Bubbling.fire("registerRenderer",
        {
            propertyName: "publishToWebCustom",
            renderer: function drawingPTW_renderer(record, label) {
                var jsNode = record.jsNode,
                    properties = jsNode.properties,
                    html = "";
                var drawingPtw = properties["draw:publishToWeb"] || "";
                if ("Yes" === drawingPtw) {
                    html = '<span style="color: #339966;"><strong>' + label + drawingPtw + '</strong></span>';
                } else {
                    html = '<span><strong>' + label + drawingPtw + '</strong></span>';
                }

                return html;
            }
        });
})();