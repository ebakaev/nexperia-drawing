/**
 * Copyright (C) 2005-2014 Alfresco Software Limited.
 *
 * This file is part of Alfresco
 *
 * Alfresco is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Alfresco is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Alfresco. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Dashboard SiteSearch component.
 *
 * @namespace Alfresco.dashlet
 * @class Alfresco.dashlet.SiteSearch
 */
(function()
{
   /**
    * YUI Library aliases
    */
   var Dom = YAHOO.util.Dom;

   /**
    * Alfresco Slingshot aliases
    */
   var $html = Alfresco.util.encodeHTML;

   /**
    * Dashboard SiteSearch constructor.
    *
    * @param {String} htmlId The HTML id of the parent element
    * @return {Alfresco.dashlet.SiteSearch} The new component instance
    * @constructor
    */
   Alfresco.dashlet.DrawingAdvancedSearch = function DrawingAdvancedSearch_constructor(htmlId)
   {
      Alfresco.dashlet.DrawingAdvancedSearch.superclass.constructor.call(this, "Alfresco.dashlet.DrawingAdvancedSearch", htmlId, ["container", "datasource", "datatable"]);

      Alfresco.service.Preferences.prototype.combine = function(values) {

          function deepMerge(target, source) {
              if(typeof target !== 'object' || typeof source !== 'object') return false;
              for(var prop in source) {
                  if(!source.hasOwnProperty(prop)) continue;
                  if(prop in target) {
                          if(typeof target[prop] !== 'object') {
                              target[prop] = source[prop];
                          } else {
                              if(typeof source[prop] !== 'object') {
                                  target[prop] = source[prop];
                              } else {
                                  if(target[prop].concat && source[prop].concat) {
                                     target[prop] = target[prop].concat(source[prop]);
                                  } else {
                                     target[prop] = deepMerge(target[prop], source[prop]);
                                  }
                              }
                          }
                  } else {
                    target[prop] = source[prop];
                  }
              }
              return target;
          }

        var resultObject = {};
        Object.keys(values).forEach(function(key){
            resultObject = deepMerge(resultObject, Alfresco.util.dotNotationToObject(key, values[key]));
        })
//        console.log(resultObject);
        //Saving all entered properties
        this._jsonCall(Alfresco.util.Ajax.POST, this._url(), resultObject,{});
      }

      // Services
      this.services.preferences = new Alfresco.service.Preferences();

      return this;
   };

   /**
    * Extend from Alfresco.component.SearchBase
    */
   YAHOO.extend(Alfresco.dashlet.DrawingAdvancedSearch, Alfresco.component.SearchBase,
   {
      /**
       * Object container for initialization options
       *
       * @property options
       * @type object
       */
      options:
      {
         /**
          * Search root node
          *
          * @property searchRootNode
          * @type string
          * @default ""
          */
         searchRootNode: "",

         /**
          * Search term
          *
          * @property searchTerm
          * @type string
          * @default ""
          */
         searchTerm: "",

         /**
          * Number of results to display in the search dashlet
          *
          * @property resultSize
          * @type string
          * @default "10"
          */
         resultSize: "10"
      },

      PREFERENCES_SITE_SEARCH_DASHLET_TERM: "",
      PREFERENCES_SITE_SEARCH_DASHLET_PRIMARY_FORMAT_TERM: "",
      PREFERENCES_SITE_SEARCH_DASHLET_MAG_CODE_TERM: "",
      PREFERENCES_SITE_SEARCH_DASHLET_DESCRIPTIVE_TITLE_TERM: "",
      PREFERENCES_SITE_SEARCH_DASHLET_DRAWING_TYPE_TERM: "",
      PREFERENCES_SITE_SEARCH_DASHLET_VERSION_TERM: "",
      PREFERENCES_SITE_SEARCH_DASHLET_CREATED_FOR_TERM: "",
      PREFERENCES_SITE_SEARCH_DASHLET_SECURITY_STATUS_TERM: "",
      PREFERENCES_SITE_SEARCH_DASHLET_REQUESTER_TERM: "",
      PREFERENCES_SITE_SEARCH_DASHLET_DELIVERY_DATE_TERM: "",
      PREFERENCES_SITE_SEARCH_DASHLET_REQUAIRED_RENDITIONS_TERM: "",
      PREFERENCES_SITE_SEARCH_DASHLET_RESULTSIZE: "",
      PREFERENCES_SITE_SEARCH_DASHLET_MODIFICATION_FROM: "",
      PREFERENCES_SITE_SEARCH_DASHLET_MODIFICATION_TO: "",
      PREFERENCES_SITE_SEARCH_DASHLET_DRAWING_STATUS: "",
      PREFERENCES_SITE_SEARCH_DASHLET_CREATOR: "",
      // PREFERENCES_SITE_SEARCH_DASHLET_LAST_STATUS_CHANGE_FROM: "",
      // PREFERENCES_SITE_SEARCH_DASHLET_LAST_STATUS_CHANGE_TO: "",

      /**
       * Fired by YUI when parent element is available for scripting
       *
       * @method onReady
       */
      onReady: function SiteSearch_onReady()
      {
         var me = this,
            id = this.id;

         // Preferences
         var PREFERENCES_SITE_SEARCH_DASHLET = this.services.preferences.getDashletId(this, "site.search");
         this.PREFERENCES_SITE_SEARCH_DASHLET_TERM = PREFERENCES_SITE_SEARCH_DASHLET + ".term";
         this.PREFERENCES_SITE_SEARCH_DASHLET_RESULTSIZE = PREFERENCES_SITE_SEARCH_DASHLET + ".resultSize";

         this.PREFERENCES_SITE_SEARCH_DASHLET_MAG_CODE_TERM = PREFERENCES_SITE_SEARCH_DASHLET + ".magCodeTerm";
         this.PREFERENCES_SITE_SEARCH_DASHLET_PRIMARY_FORMAT_TERM = PREFERENCES_SITE_SEARCH_DASHLET + ".primaryFormatTerm";
         this.PREFERENCES_SITE_SEARCH_DASHLET_DESCRIPTIVE_TITLE_TERM = PREFERENCES_SITE_SEARCH_DASHLET + ".descriptiveTitleTerm";
         this.PREFERENCES_SITE_SEARCH_DASHLET_DRAWING_TYPE_TERM = PREFERENCES_SITE_SEARCH_DASHLET + ".drawingTypeTerm";
         this.PREFERENCES_SITE_SEARCH_DASHLET_VERSION_TERM = PREFERENCES_SITE_SEARCH_DASHLET + ".versionTerm";
         this.PREFERENCES_SITE_SEARCH_DASHLET_CREATED_FOR_TERM = PREFERENCES_SITE_SEARCH_DASHLET + ".createdForTerm";
         this.PREFERENCES_SITE_SEARCH_DASHLET_SECURITY_STATUS_TERM = PREFERENCES_SITE_SEARCH_DASHLET + ".securityStatusTerm";
         this.PREFERENCES_SITE_SEARCH_DASHLET_REQUESTER_TERM = PREFERENCES_SITE_SEARCH_DASHLET + ".requesterTerm";
         this.PREFERENCES_SITE_SEARCH_DASHLET_DELIVERY_DATE_TERM = PREFERENCES_SITE_SEARCH_DASHLET + ".deliveryDateTerm";
         this.PREFERENCES_SITE_SEARCH_DASHLET_REQUAIRED_RENDITIONS_TERM = PREFERENCES_SITE_SEARCH_DASHLET + ".requiredRenditionsTerm";
         this.PREFERENCES_SITE_SEARCH_DASHLET_MODIFICATION_FROM = PREFERENCES_SITE_SEARCH_DASHLET + ".modificationDateFrom";
         this.PREFERENCES_SITE_SEARCH_DASHLET_MODIFICATION_TO = PREFERENCES_SITE_SEARCH_DASHLET + ".modificationDateTo";
         this.PREFERENCES_SITE_SEARCH_DASHLET_DRAWING_STATUS = PREFERENCES_SITE_SEARCH_DASHLET + ".drawStatus";
         this.PREFERENCES_SITE_SEARCH_DASHLET_CREATOR = PREFERENCES_SITE_SEARCH_DASHLET + ".creator";
         // this.PREFERENCES_SITE_SEARCH_DASHLET_LAST_STATUS_CHANGE_FROM = PREFERENCES_SITE_SEARCH_DASHLET + ".lastStatusDateFrom";
         // this.PREFERENCES_SITE_SEARCH_DASHLET_LAST_STATUS_CHANGE_TO = PREFERENCES_SITE_SEARCH_DASHLET + ".lastStatusDateTo";

         // Create result size menu
         this.widgets.resultSizeMenuButton = Alfresco.util.createYUIButton(this, "resultSize", this.onResultSizeSelected,
         {
            type: "menu",
            menu: "resultSize-menu",
            lazyloadmenu: false
         });
         this.widgets.resetSearchFormButton = Alfresco.util.createYUIButton(this, "advSearchResetButton", this.onResetSearchForm);
//         this.widgets.downloadButton = Alfresco.util.createYUIButton(this, "download-button", this.onDownloadButton);

         // Load preferences
         var prefs = this.services.preferences.get();

         var term = Alfresco.util.findValueByDotNotation(prefs, this.PREFERENCES_SITE_SEARCH_DASHLET_TERM),
             resultSize = Alfresco.util.findValueByDotNotation(prefs, this.PREFERENCES_SITE_SEARCH_DASHLET_RESULTSIZE),
             magCode = Alfresco.util.findValueByDotNotation(prefs, this.PREFERENCES_SITE_SEARCH_DASHLET_MAG_CODE_TERM),
             primaryFormat = Alfresco.util.findValueByDotNotation(prefs, this.PREFERENCES_SITE_SEARCH_DASHLET_PRIMARY_FORMAT_TERM),
             descriptiveTitle = Alfresco.util.findValueByDotNotation(prefs, this.PREFERENCES_SITE_SEARCH_DASHLET_DESCRIPTIVE_TITLE_TERM),
             drawingType = Alfresco.util.findValueByDotNotation(prefs, this.PREFERENCES_SITE_SEARCH_DASHLET_DRAWING_TYPE_TERM),
             createdFor = Alfresco.util.findValueByDotNotation(prefs, this.PREFERENCES_SITE_SEARCH_DASHLET_CREATED_FOR_TERM),
             securityStatus = Alfresco.util.findValueByDotNotation(prefs, this.PREFERENCES_SITE_SEARCH_DASHLET_SECURITY_STATUS_TERM),
             requester = Alfresco.util.findValueByDotNotation(prefs, this.PREFERENCES_SITE_SEARCH_DASHLET_REQUESTER_TERM),
             deliveryDate = Alfresco.util.findValueByDotNotation(prefs, this.PREFERENCES_SITE_SEARCH_DASHLET_DELIVERY_DATE_TERM),
             requiredRenditions = Alfresco.util.findValueByDotNotation(prefs, this.PREFERENCES_SITE_SEARCH_DASHLET_REQUAIRED_RENDITIONS_TERM),
             version = Alfresco.util.findValueByDotNotation(prefs, this.PREFERENCES_SITE_SEARCH_DASHLET_VERSION_TERM),
             modificationDateFrom = Alfresco.util.findValueByDotNotation(prefs, this.PREFERENCES_SITE_SEARCH_DASHLET_MODIFICATION_FROM),
             modificationDateTo = Alfresco.util.findValueByDotNotation(prefs, this.PREFERENCES_SITE_SEARCH_DASHLET_MODIFICATION_TO);
             drawStatus = Alfresco.util.findValueByDotNotation(prefs, this.PREFERENCES_SITE_SEARCH_DASHLET_DRAWING_STATUS);
             creator = Alfresco.util.findValueByDotNotation(prefs, this.PREFERENCES_SITE_SEARCH_DASHLET_CREATOR);
             // lastStatusDateFrom = Alfresco.util.findValueByDotNotation(prefs, this.PREFERENCES_SITE_SEARCH_DASHLET_LAST_STATUS_CHANGE_FROM),
             // lastStatusDateTo = Alfresco.util.findValueByDotNotation(prefs, this.PREFERENCES_SITE_SEARCH_DASHLET_LAST_STATUS_CHANGE_TO);

         if (term != null || resultSize != null)
         {
            this.options.searchTerm = Dom.get(this.id + "-search-text").value = (term ? term : "");
            if (magCode != null) {
                this.options.magCodeTerm = Dom.get(this.id + "-mag-search-text").value = (magCode ? magCode : "");
            }
            if (primaryFormat != null) {
                this.options.primaryFormatTerm = Dom.get(this.id + "-primaryFormat-search-text").value = (primaryFormat ? primaryFormat : "");
            }
            if (descriptiveTitle != null) {
                this.options.descriptiveTitleTerm = Dom.get(this.id + "-descriptiveTitle-search-text").value = (descriptiveTitle ? descriptiveTitle : "");
            }
            if (drawingType != null) {
                this.options.drawingTypeTerm = Dom.get(this.id + "-drawingType-search-text").value = (drawingType ? drawingType : "");
            }
            if (createdFor != null) {
                this.options.createdForTerm = Dom.get(this.id + "-createdFor-search-text").value = (createdFor ? createdFor : "");
            }
            if (securityStatus != null) {
                this.options.securityStatusTerm = Dom.get(this.id + "-securityStatus-search-text").value = (securityStatus ? securityStatus : "");
            }
            if (requester != null) {
                this.options.requesterTerm = Dom.get(this.id + "-requester-search-text").value = (requester ? requester : "");
            }
            if (deliveryDate != null) {
                this.options.deliveryDateTerm = Dom.get(this.id + "-deliveryDate-search-text").value = (deliveryDate ? deliveryDate : "");
            }
            if (requiredRenditions != null) {
                this.options.requiredRenditionsTerm = Dom.get(this.id + "-requiredRenditions-search-text").value = (requiredRenditions ? requiredRenditions : "");
            }
            if (version != null) {
                this.options.versionTerm = Dom.get(this.id + "-version-search-text").value = (version ? version : "");
            }
            if (modificationDateFrom != null) {
                this.options.modificationDateFromTerm = Dom.get(this.id + "-modificationDateFrom-search-text").value = (modificationDateFrom ? modificationDateFrom : "");
            }
            if (modificationDateTo != null) {
                this.options.modificationDateToTerm = Dom.get(this.id + "-modificationDateTo-search-text").value = (modificationDateTo ? modificationDateTo : "");
            }
            if (drawStatus != null) {
                this.options.drawStatusTerm = Dom.get(this.id + "-drawStatus-search-text").value = (drawStatus ? drawStatus : "");
            }
            if (creator != null) {
                this.options.creatorTerm = Dom.get(this.id + "-creator-search-text").value = (creator ? creator : "");
            }
            // if (lastStatusDateFrom != null) {
            //     this.options.lastStatusDateFrom = Dom.get(this.id + "-lastStatusDateFrom-search-text").value = (lastStatusDateFrom ? lastStatusDateFrom : "");
            // }
            // if (lastStatusDateTo != null) {
            //     this.options.lastStatusDateTo = Dom.get(this.id + "-lastStatusDateTo-search-text").value = (lastStatusDateTo ? lastStatusDateTo : "");
            // }
            this.options.resultSize = (resultSize ? resultSize : "10");
            this.doRequest();
         }

         var resultSize = this.getResultSize();
         this.widgets.resultSizeMenuButton.set("label", resultSize + " " + Alfresco.constants.MENU_ARROW_SYMBOL);
         this.widgets.resultSizeMenuButton.value = resultSize;

         // Display the toolbar now that we have selected the filter
         Dom.removeClass(Selector.query(".toolbar div", id, true), "hidden");

         // Enter key press
         var me = this;
         Dom.get(id + "-search-text").onkeypress = function(e)
         {
            if (e.keyCode == YAHOO.util.KeyListener.KEY.ENTER)
            {
               me.setSearchTerm(YAHOO.lang.trim(Dom.get(me.id + "-search-text").value));
               me.setMagCodeTerm(YAHOO.lang.trim(Dom.get(me.id + "-mag-search-text").value));
               me.setPrimaryFormatTerm(YAHOO.lang.trim(Dom.get(me.id + "-primaryFormat-search-text").value));
               me.setDescriptiveTitleTerm(YAHOO.lang.trim(Dom.get(me.id + "-descriptiveTitle-search-text").value));
               me.setDrawingTypeTerm(YAHOO.lang.trim(Dom.get(me.id + "-drawingType-search-text").value));
               me.setCreatedForTerm(YAHOO.lang.trim(Dom.get(me.id + "-createdFor-search-text").value));
               me.setSecurityStatusTerm(YAHOO.lang.trim(Dom.get(me.id + "-securityStatus-search-text").value));
               me.setRequesterTerm(YAHOO.lang.trim(Dom.get(me.id + "-requester-search-text").value));
               me.setDeliveryDateTerm(YAHOO.lang.trim(Dom.get(me.id + "-deliveryDate-search-text").value));
               me.setRequiredRenditionsTerm(YAHOO.lang.trim(Dom.get(me.id + "-requiredRenditions-search-text").value));
               me.setVersionTerm(YAHOO.lang.trim(Dom.get(me.id + "-version-search-text").value));
               me.setModificationDateFromTerm(YAHOO.lang.trim(Dom.get(me.id + "-modificationDateFrom-search-text").value));
               me.setModificationDateToTerm(YAHOO.lang.trim(Dom.get(me.id + "-modificationDateTo-search-text").value));
               me.setDrawStatusTerm(YAHOO.lang.trim(Dom.get(me.id + "-drawStatus-search-text").value));
               me.setCreatorTerm(YAHOO.lang.trim(Dom.get(me.id + "-creator-search-text").value));
               // me.setLastStatusChangedFromTerm(YAHOO.lang.trim(Dom.get(me.id + "-lastStatusDateFrom-search-text").value));
               // me.setLastStatusChangedToTerm(YAHOO.lang.trim(Dom.get(me.id + "-lastStatusDateTo-search-text").value));
               me.doRequest();
            }
         };

         // Search button click
         Dom.get(id + "-search-button").onclick = function(e)
         {
            me.setSearchTerm(YAHOO.lang.trim(Dom.get(me.id + "-search-text").value));
            me.setMagCodeTerm(YAHOO.lang.trim(Dom.get(me.id + "-mag-search-text").value));
            me.setPrimaryFormatTerm(YAHOO.lang.trim(Dom.get(me.id + "-primaryFormat-search-text").value));
            me.setDescriptiveTitleTerm(YAHOO.lang.trim(Dom.get(me.id + "-descriptiveTitle-search-text").value));
            me.setDrawingTypeTerm(YAHOO.lang.trim(Dom.get(me.id + "-drawingType-search-text").value));
            me.setCreatedForTerm(YAHOO.lang.trim(Dom.get(me.id + "-createdFor-search-text").value));
            me.setSecurityStatusTerm(YAHOO.lang.trim(Dom.get(me.id + "-securityStatus-search-text").value));
            me.setRequesterTerm(YAHOO.lang.trim(Dom.get(me.id + "-requester-search-text").value));
            me.setDeliveryDateTerm(YAHOO.lang.trim(Dom.get(me.id + "-deliveryDate-search-text").value));
            me.setRequiredRenditionsTerm(YAHOO.lang.trim(Dom.get(me.id + "-requiredRenditions-search-text").value));
            me.setVersionTerm(YAHOO.lang.trim(Dom.get(me.id + "-version-search-text").value));
            me.setModificationDateFromTerm(YAHOO.lang.trim(Dom.get(me.id + "-modificationDateFrom-search-text").value));
            me.setModificationDateToTerm(YAHOO.lang.trim(Dom.get(me.id + "-modificationDateTo-search-text").value));
            me.setDrawStatusTerm(YAHOO.lang.trim(Dom.get(me.id + "-drawStatus-search-text").value));
            me.setCreatorTerm(YAHOO.lang.trim(Dom.get(me.id + "-creator-search-text").value));
            // me.setLastStatusChangedFromTerm(YAHOO.lang.trim(Dom.get(me.id + "-lastStatusDateFrom-search-text").value));
            // me.setLastStatusChangedToTerm(YAHOO.lang.trim(Dom.get(me.id + "-lastStatusDateTo-search-text").value));
            me.doRequest();
         };

         // Search button click
         Dom.get(id + "-print-view-button").onclick = function(e)
         {
             me.doPrintViewRequest();
         };

         // Update button click
         Dom.get(id + "-update-button").onclick = function(e)
         {
           me.doBulkUpdateRequest();
         };

         // Download button click
         Dom.get(id + "-download-button").onclick = function(e)
         {
             me.downloadButtonRequest();
         };

        Dom.get(id + "-report-button").onclick = function(e) {
            var url = Alfresco.constants.PROXY_URI + "drawing/reports?term={term}&magCode={magCode}&primaryFormat={primaryFormat}&descriptiveTitle={descriptiveTitle}&drawingType={drawingType}&createdFor={createdFor}&securityStatus={securityStatus}&requester={requester}&deliveryDate={deliveryDate}&requiredRenditions={requiredRenditions}&version={version}&modificationFromDate={modificationFromDate}&modificationToDate={modificationToDate}&drawStatus={drawStatus}&creator={creator}";
            url =  YAHOO.lang.substitute(url,
                {
                   term: encodeURIComponent(YAHOO.lang.trim(Dom.get(me.id + "-search-text").value)),
                   magCode: encodeURIComponent(YAHOO.lang.trim(Dom.get(me.id + "-mag-search-text").value)),
                   primaryFormat: encodeURIComponent(YAHOO.lang.trim(Dom.get(me.id + "-primaryFormat-search-text").value)),
                   descriptiveTitle: encodeURIComponent(YAHOO.lang.trim(Dom.get(me.id + "-descriptiveTitle-search-text").value)),
                   drawingType: encodeURIComponent(YAHOO.lang.trim(Dom.get(me.id + "-drawingType-search-text").value)),
                   createdFor: encodeURIComponent(YAHOO.lang.trim(Dom.get(me.id + "-createdFor-search-text").value)),
                   securityStatus: encodeURIComponent(YAHOO.lang.trim(Dom.get(me.id + "-securityStatus-search-text").value)),
                   requester: encodeURIComponent(YAHOO.lang.trim(Dom.get(me.id + "-requester-search-text").value)),
                   deliveryDate: encodeURIComponent(YAHOO.lang.trim(Dom.get(me.id + "-deliveryDate-search-text").value)),
                   requiredRenditions: encodeURIComponent(YAHOO.lang.trim(Dom.get(me.id + "-requiredRenditions-search-text").value)),
                   version: encodeURIComponent(YAHOO.lang.trim(Dom.get(me.id + "-version-search-text").value)),
                   modificationFromDate: encodeURIComponent(YAHOO.lang.trim(Dom.get(me.id + "-modificationDateFrom-search-text").value)),
                   modificationToDate: encodeURIComponent(YAHOO.lang.trim(Dom.get(me.id + "-modificationDateTo-search-text").value)),
                   drawStatus: encodeURIComponent(YAHOO.lang.trim(Dom.get(me.id + "-drawStatus-search-text").value)),
                   creator: encodeURIComponent(YAHOO.lang.trim(Dom.get(me.id + "-creator-search-text").value))
                   // lastStatusDateFrom: encodeURIComponent(YAHOO.lang.trim(Dom.get(me.id + "-lastStatusDateFrom-search-text").value)),
                   // astStatusDateTo: encodeURIComponent(YAHOO.lang.trim(Dom.get(me.id + "-lastStatusDateTo-search-text").value))
                });
            window.open(url, "_blank");
         };
      },

      onResetSearchForm: function DrawingClone_onResetCloneFormButtonClick(e) {
        var searchForm = Dom.get(this.id + "-adv-search-form");
        searchForm.reset();
      },

      /**
       * Gets the search term
       *
       * @method getSearchTerm
       * @return {string} The trimmed and encoded search term
       */
      getSearchTerm: function SiteSearch_getSearchTerm()
      {
         return this.options.searchTerm;
      },

      getArchive: function SiteSearch_getArchive()
      {
           var archiveCheckbox = Dom.get(this.id + "-archive").checked;
           if (archiveCheckbox) {
              return "yes";
           } else {
              return "no";
           }
      },

      /**
       * Sets the search term
       *
       * @method setSearchTerm
       * @param {string} searchTerm The search term to set
       */
      setSearchTerm: function SiteSearch_setSearchTerm(searchTerm)
      {
         if (this.options.searchTerm != searchTerm)
         {
            this.options.searchTerm = searchTerm;
         }
      },

      /**
       * Gets the search Mag Code term
       *
       * @method getMagCodeTerm
       * @return {string} The trimmed and encoded search term
       */
      getMagCodeTerm: function SiteSearch_getMagCodeTerm()
      {
         return this.options.magCodeTerm;
      },
      getPrimaryFormatTerm: function SiteSearch_getPrimaryFormatTerm()
      {
         return this.options.primaryFormatTerm;
      },
      getDescriptiveTitleTerm: function SiteSearch_getDescriptiveTitleTerm()
      {
         return this.options.descriptiveTitleTerm;
      },
      getDrawingTypeTerm: function SiteSearch_getDrawingTypeTerm()
      {
         return this.options.drawingTypeTerm;
      },
      getCreatedForTerm: function SiteSearch_getCreatedForTerm()
      {
         return this.options.createdForTerm;
      },
      getSecurityStatusTerm: function SiteSearch_getSecurityStatusTerm()
      {
         return this.options.securityStatusTerm;
      },
      getRequesterTerm: function SiteSearch_getRequesterTerm()
      {
         return this.options.requesterTerm;
      },
      getDeliveryDateTerm: function SiteSearch_getDeliveryDateTerm()
      {
         return this.options.deliveryDateTerm;
      },
      getRequiredRenditionsTerm: function SiteSearch_getRequiredRenditionsTerm()
      {
         return this.options.requiredRenditionsTerm;
      },
      getVersionTerm: function SiteSearch_getVersionTerm()
      {
         return this.options.versionTerm;
      },
      getModificationFromTerm: function SiteSearch_getModificationFromTerm()
      {
         return this.options.modificationDateFromTerm;
      },
      getModificationToTerm: function SiteSearch_getModificationToTerm()
      {
         return this.options.modificationDateToTerm;
      },
      getDrawStatusTerm: function SiteSearch_getDrawStatusTerm()
      {
         return this.options.drawStatusTerm;
      },
      getCreatorTerm: function SiteSearch_getCreatorTerm()
      {
         return this.options.creatorTerm;
      },
      // getLastStatusChangedFromTerm: function SiteSearch_getLastStatusChangedFromTerm()
      // {
      //    return this.options.lastStatusChangedFromTerm;
      // },
      // getLastStatusChangedToTerm: function SiteSearch_getLastStatusChangedToTerm()
      // {
      //    return this.options.lastStatusChangedToTerm;
      // },

      /**
       * Sets the search Mag Code term
       *
       * @method setMagCodeTerm
       * @param {string} searchTerm The search term to set
       */
      setMagCodeTerm: function SiteSearch_setMagCodeTerm(magCodeTerm)
      {
         if (this.options.magCodeTerm != magCodeTerm)
         {
            this.options.magCodeTerm = magCodeTerm;
         }
      },
      setPrimaryFormatTerm: function SiteSearch_setPrimaryFormatTerm(primaryFormatTerm)
      {
         if (this.options.primaryFormatTerm != primaryFormatTerm)
         {
            this.options.primaryFormatTerm = primaryFormatTerm;
         }
      },
      setDescriptiveTitleTerm: function SiteSearch_setDescriptiveTitleTerm(descriptiveTitleTerm)
      {
         if (this.options.descriptiveTitleTerm != descriptiveTitleTerm)
         {
            this.options.descriptiveTitleTerm = descriptiveTitleTerm;
         }
      },
      setDrawingTypeTerm: function SiteSearch_setDrawingTypeTerm(drawingTypeTerm)
      {
         if (this.options.drawingTypeTerm != drawingTypeTerm)
         {
            this.options.drawingTypeTerm = drawingTypeTerm;
         }
      },
      setCreatedForTerm: function SiteSearch_setCreatedForTerm(createdForTerm)
      {
         if (this.options.createdForTerm != createdForTerm)
         {
            this.options.createdForTerm = createdForTerm;
         }
      },
      setSecurityStatusTerm: function SiteSearch_setSecurityStatusTerm(securityStatusTerm)
      {
         if (this.options.securityStatusTerm != securityStatusTerm)
         {
            this.options.securityStatusTerm = securityStatusTerm;
         }
      },
      setRequesterTerm: function SiteSearch_setRequesterTerm(requesterTerm)
      {
         if (this.options.requesterTerm != requesterTerm)
         {
            this.options.requesterTerm = requesterTerm;
         }
      },
      setDeliveryDateTerm: function SiteSearch_setDeliveryDateTerm(deliveryDateTerm)
      {
         if (this.options.deliveryDateTerm != deliveryDateTerm)
         {
            this.options.deliveryDateTerm = deliveryDateTerm;
         }
      },
      setRequiredRenditionsTerm: function SiteSearch_setRequiredRenditionsTerm(requiredRenditionsTerm)
      {
         if (this.options.requiredRenditionsTerm != requiredRenditionsTerm)
         {
            this.options.requiredRenditionsTerm = requiredRenditionsTerm;
         }
      },
      setVersionTerm: function SiteSearch_setVersionTerm(versionTerm)
      {
         if (this.options.versionTerm != versionTerm)
         {
            this.options.versionTerm = versionTerm;
         }
      },
      setModificationDateFromTerm: function SiteSearch_setModificationDateFromTerm(modificationDateFromTerm)
      {
         if (this.options.modificationDateFromTerm != modificationDateFromTerm)
         {
            this.options.modificationDateFromTerm = modificationDateFromTerm;
         }
      },
      setModificationDateToTerm: function SiteSearch_setModificationDateToTerm(modificationDateToTerm)
      {
         if (this.options.modificationDateToTerm != modificationDateToTerm)
         {
            this.options.modificationDateToTerm = modificationDateToTerm;
         }
      },
      setDrawStatusTerm: function SiteSearch_setDrawStatusTerm(drawStatusTerm)
      {
         if (this.options.drawStatusTerm != drawStatusTerm)
         {
            this.options.drawStatusTerm = drawStatusTerm;
         }
      },
      setCreatorTerm: function SiteSearch_setCreatorTerm(creatorTerm)
      {
         if (this.options.creatorTerm != creatorTerm)
         {
            this.options.creatorTerm = creatorTerm;
         }
      },
      // setLastStatusChangedFromTerm: function SiteSearch_setLastStatusChangedFromTerm(lastStatusChangedFromTerm)
      // {
      //    if (this.options.lastStatusChangedFromTerm != lastStatusChangedFromTerm)
      //    {
      //       this.options.lastStatusChangedFromTerm = lastStatusChangedFromTerm;
      //    }
      // },
      // setLastStatusChangedToTerm: function SiteSearch_setLastStatusChangedToTerm(lastStatusChangedToTerm)
      // {
      //    if (this.options.lastStatusChangedToTerm != lastStatusChangedToTerm)
      //    {
      //       this.options.lastStatusChangedToTerm = lastStatusChangedToTerm;
      //    }
      // },



      /**
       * Gets the result size
       *
       * @method getResultSize
       * @return {string} The number of results to display in the search dashlet
       */
      getResultSize: function SiteSearch_getResultSize()
      {
         return YAHOO.lang.trim(this.options.resultSize);
      },

      /**
       * Sets the result size
       *
       * @method setResultSize
       * @param {string} resultSize The result size to set
       */
      setResultSize: function SiteSearch_setResultSize(resultSize)
      {
         if (this.options.resultSize != resultSize)
         {
            this.options.resultSize = resultSize;

            // Save preferences
//            this.services.preferences.set(this.PREFERENCES_SITE_SEARCH_DASHLET_RESULTSIZE, resultSize);
         }
      },

      /**
       * Gets the root node
       *
       * @method getRootNode
       * @return {string} The search root node
       */
      getRootNode: function SiteSearch_getRootNode()
      {
         return this.options.searchRootNode;
      },

      /**
       * Helper method called by "renderDescription" method
       *
       * @method buildNameWithHref
       * @param {string} href
       * @param {string} name
       * @return {string} The name (with href) for the found result
       */
      buildNameWithHref: function SiteSearch_buildNameWithHref(href, name)
      {
         return '<h3 class="itemname"> <a class="theme-color-1" href=' + href + '>' + $html(name) + '</a></h3>';
      },

      /**
       * Helper method called by "renderDescription" method
       *
       * @method buildDescription
       * @param {string} resultType
       * @param {string} siteShortName
       * @param {string} siteTitle
       * @return {string} The description for the found result
       */
      buildDescription: function SiteSearch_buildDescription(resultType, siteShortName, siteTitle)
      {
         var desc = '';

         var siteId = this.options.siteId;
         if (!(siteId && siteId != null))
         {
            desc = resultType + ' ' + this.msg("message.insite") + ' <a href="' + Alfresco.constants.URL_PAGECONTEXT + 'site/' + siteShortName + '/dashboard">' + $html(siteTitle) + '</a>';
         }

         return desc;
      },

      /**
       * Builds the url for the data table
       *
       * @method buildUrl
       * @retrun {string} The url for the data table
       */
      buildUrl: function SiteSearch_buildUrl()
      {
         var url = Alfresco.constants.PROXY_URI + "slingshot/search?term={term}&magCode={magCode}&primaryFormat={primaryFormat}&descriptiveTitle={descriptiveTitle}&drawingType={drawingType}&createdFor={createdFor}&securityStatus={securityStatus}&requester={requester}&deliveryDate={deliveryDate}&requiredRenditions={requiredRenditions}&version={version}&archive={archive}&maxResults={maxResults}&rootNode={rootNode}&modificationFromDate={modificationFromDate}&modificationToDate={modificationToDate}&drawStatus={drawStatus}&creator={creator}&sort=name";

         var siteId = this.options.siteId;
         if (siteId && siteId != null)
         {
            url += "&site=" + siteId;
         }

         return YAHOO.lang.substitute(url,
         {
            term: encodeURIComponent(this.getSearchTerm()),
            magCode: encodeURIComponent(this.getMagCodeTerm()),
            primaryFormat: encodeURIComponent(this.getPrimaryFormatTerm()),
            descriptiveTitle: encodeURIComponent(this.getDescriptiveTitleTerm()),
            drawingType: encodeURIComponent(this.getDrawingTypeTerm()),
            createdFor: encodeURIComponent(this.getCreatedForTerm()),
            securityStatus: encodeURIComponent(this.getSecurityStatusTerm()),
            requester: encodeURIComponent(this.getRequesterTerm()),
            deliveryDate: encodeURIComponent(this.getDeliveryDateTerm()),
            requiredRenditions: encodeURIComponent(this.getRequiredRenditionsTerm()),
            version: encodeURIComponent(this.getVersionTerm()),
            archive: this.getArchive(),
            maxResults: this.getResultSize(),
            rootNode: encodeURIComponent(this.getRootNode()),
            modificationFromDate: encodeURIComponent(this.getModificationFromTerm()),
            modificationToDate: encodeURIComponent(this.getModificationToTerm()),
            drawStatus: encodeURIComponent(this.getDrawStatusTerm()),
            creator: encodeURIComponent(this.getCreatorTerm())
            // lastStatusChangedFromDate: encodeURIComponent(this.getLastStatusChangedFromTerm()),
            // lastStatusChangedToDate: encodeURIComponent(this.getLastStatusChangedToTerm())
         });
      },

      /**
       * Init the data table
       *
       * @method doRequest
       */
      doRequest: function SiteSearch_doRequest()
      {
          // Save preferences
          var data = {};
          data[this.PREFERENCES_SITE_SEARCH_DASHLET_TERM] = this.getSearchTerm();
          data[this.PREFERENCES_SITE_SEARCH_DASHLET_MAG_CODE_TERM] = this.getMagCodeTerm();
          data[this.PREFERENCES_SITE_SEARCH_DASHLET_DESCRIPTIVE_TITLE_TERM] = this.getDescriptiveTitleTerm();
          data[this.PREFERENCES_SITE_SEARCH_DASHLET_PRIMARY_FORMAT_TERM] = this.getPrimaryFormatTerm();
          data[this.PREFERENCES_SITE_SEARCH_DASHLET_DRAWING_TYPE_TERM] = this.getDrawingTypeTerm();
          data[this.PREFERENCES_SITE_SEARCH_DASHLET_VERSION_TERM] = this.getVersionTerm();
          data[this.PREFERENCES_SITE_SEARCH_DASHLET_CREATED_FOR_TERM] = this.getCreatedForTerm();
          data[this.PREFERENCES_SITE_SEARCH_DASHLET_SECURITY_STATUS_TERM] = this.getSecurityStatusTerm();
          data[this.PREFERENCES_SITE_SEARCH_DASHLET_REQUESTER_TERM] = this.getRequesterTerm();
          data[this.PREFERENCES_SITE_SEARCH_DASHLET_DELIVERY_DATE_TERM] = this.getDeliveryDateTerm();
          data[this.PREFERENCES_SITE_SEARCH_DASHLET_REQUAIRED_RENDITIONS_TERM] = this.getRequiredRenditionsTerm();
          data[this.PREFERENCES_SITE_SEARCH_DASHLET_RESULTSIZE] = this.getResultSize();
          data[this.PREFERENCES_SITE_SEARCH_DASHLET_MODIFICATION_FROM] = this.getModificationFromTerm();
          data[this.PREFERENCES_SITE_SEARCH_DASHLET_MODIFICATION_TO] = this.getModificationToTerm;
          data[this.PREFERENCES_SITE_SEARCH_DASHLET_DRAWING_STATUS] = this.getDrawStatusTerm;
          data[this.PREFERENCES_SITE_SEARCH_DASHLET_CREATOR] = this.getCreatorTerm;
          // data[this.PREFERENCES_SITE_SEARCH_DASHLET_LAST_STATUS_CHANGE_FROM] = this.getLastStatusChangedFromTerm();
          // data[this.PREFERENCES_SITE_SEARCH_DASHLET_LAST_STATUS_CHANGE_TO] = this.getLastStatusChangedToTerm();
          this.services.preferences.combine(data);

         this.widgets.alfrescoDataTable = new Alfresco.util.DataTable(
         {
            dataSource:
            {
               url: this.buildUrl(),
               config:
               {
                  responseSchema:
                  {
                     resultsList: 'items'
                  }
               }
            },
            dataTable:
            {
               container: this.id + "-search-results",
               columnDefinitions:
               [
                  {key: "site", formatter: this.bind(this.renderThumbnail), width: 66},
                  {key: "path", formatter: this.bind(this.renderDescription)}
               ],
               config:
               {
                  MSG_EMPTY: this.msg("no.result")
               }
            }
         });
      },

      downloadButtonRequest: function AdvSearch_downloadButtonRequest()
      {
            var records = this.widgets.alfrescoDataTable.widgets.dataTable.getRecordSet()._records, record;
            var maxCount = 50;
            if (records.length < maxCount) {
                maxCount = records.length;
            }
            var drawings = [];
            for(i = 0; i < maxCount; i++) {
                record = records[i];
                drawings.push(record._oData.nodeRef);
            }

            Alfresco.util.Ajax.jsonGet({
                url: Alfresco.constants.PROXY_URI + "drawing/download.json?nodeRefs=" + drawings,
                successCallback: {
                    fn: function onDrawingsDownload_Success(response) {
                        var renditionsToDownload = response.json.renditions;
                        var rendition;
                        var count = 0;
                        renditionsToDownload.forEach(function(rendition) {
                            var a = document.createElement('a');
                            a.setAttribute('download','download');
                            a.href = Alfresco.constants.PROXY_URI + "slingshot/node/content/workspace/SpacesStore/" + rendition + "?a=true";
                            a.click();
                            delete a;
                            if (++count >= 10) {
                                var timeStart = new Date().getTime();
                                 while (true) {
                                     var elapsedTime = new Date().getTime() - timeStart;
                                     if (elapsedTime > 1000) {
                                         break;
                                     }
                                 }
                                count = 0;
                            }
                        })
                    },
                    scope: this
                },
                failureCallback: {
                    fn: function (response) {
                        Alfresco.util.PopupManager.displayMessage(
                        {
                            text: "Drawing/s download failed! Please contact system administrator!"
                        });
                    },
                    scope: this
                }
            });

      },

      pause: function SiteSearch_pause(milliseconds) {

      },

      doPrintViewRequest: function SiteSearch_printViewRequest()
      {
             var records = this.widgets.alfrescoDataTable.widgets.dataTable.getRecordSet()._records, record;
             var nodeRefs = "";
             for(i = 0; i < records.length; i++) {
                 record = records[i];
                 var nodeRef = record._oData.nodeRef;
                 nodeRef = nodeRef.replace("workspace://SpacesStore/","");
                 nodeRefs += nodeRef;
                 if (i < records.length) {
                    nodeRefs += ",";
                 }
             }

             var url = Alfresco.constants.PROXY_URI + "drawing/printview?nodeRefs=" + nodeRefs;
             window.open(url, '_blank').focus();
      },

      doBulkUpdateRequest: function SiteSearch_bulkUpdateRequest()
      {
          var records = this.widgets.alfrescoDataTable.widgets.dataTable.getRecordSet()._records, record;
          var recordsCount = 0;
          if (records.length <= 50) {
             recordsCount = records.length;
          } else {
             recordsCount = 50;
          }

          var nodeRefs = "";
          for(i = 0; i < recordsCount; i++) {
              record = records[i];
              var nodeRef = record._oData.nodeRef;
              nodeRefs += nodeRef;
              if (i < recordsCount) {
                 nodeRefs += ",";
              }
          }

          var url = Alfresco.constants.PROXY_URI + "drawing/metadataupdate?nodeRefs=" + nodeRefs;
          window.open(url, '_blank').focus();
      },

      /**
       * Called by the DataTable to render the 'thumbnail' cell
       *
       * @method renderThumbnail
       * @param elCell {object}
       * @param oRecord {object}
       * @param oColumn {object}
       * @param oData {object|string}
       */
      renderThumbnail: function SiteSearch_renderThumbnail(elCell, oRecord, oColumn, oData)
      {
         if (oRecord.getData("type") === "document")
         {
            Dom.addClass(elCell.parentNode, "thumbnail");
         }
         elCell.innerHTML = this.buildThumbnailHtml(oRecord, 48, 48);
      },

      /**
       * Called by the DataTable to render the 'description' cell
       *
       * @method renderThumbnail
       * @param elCell {object}
       * @param oRecord {object}
       * @param oColumn {object}
       * @param oData {object|string}
       */
      renderDescription: function SiteSearch_renderDescription(elCell, oRecord, oColumn, oData)
      {
         var type = oRecord.getData("type"),
            name = oRecord.getData("name"),
            displayName = oRecord.getData("displayName"),
            site = oRecord.getData("site"),
            path = oRecord.getData("path"),
            nodeRef = oRecord.getData("nodeRef"),
            container = oRecord.getData("container"),
            modifiedOn = oRecord.getData("modifiedOn"),
            siteShortName = site.shortName,
            siteTitle = site.title,
            modified = Alfresco.util.formatDate(Alfresco.util.fromISO8601(modifiedOn)),
            resultType = this.buildTextForType(type),
            href = this.getBrowseUrl(name, type, site, path, nodeRef, container, modified);

         elCell.innerHTML = this.buildNameWithHref(href, displayName) + this.buildDescription(resultType, siteShortName, siteTitle) + this.buildPath(type, path, site);
      },

      /**
       * Changes the number of result to show in the search dashlet.
       * If a search has been done before a new search with the result number will be done.
       *
       * @param {string} p_sType The event
       * @param {array} p_aArgs Event arguments
       */
      onResultSizeSelected: function SiteSearch_onResultSizeSelected(p_sType, p_aArgs)
      {
         var menuItem = p_aArgs[1];

         if (menuItem)
         {
            this.widgets.resultSizeMenuButton.set("label", menuItem.cfg.getProperty("text") + " " + Alfresco.constants.MENU_ARROW_SYMBOL);
            this.widgets.resultSizeMenuButton.value = menuItem.value;

            this.setResultSize(menuItem.value);

            var dataTable = this.widgets.alfrescoDataTable;
            if (dataTable)
            {
               // change the inital value of "maxResults"
               var dataSource = dataTable.getDataTable().getDataSource(),
                  url = dataSource.liveData,
                  urlSplit = url.split("?"),
                  params = urlSplit[1].split("&");
               for (var i = 0; i < params.length; i++)
               {
                  if (params[i].split("=")[0] === "maxResults")
                  {
                     params[i] = "maxResults=" + menuItem.value;
                     url = urlSplit[0] + "?" + params.join("&");
                     break;
                  }
               }

               // change the inital url to the new one
               dataSource.liveData = url;

               // load data table
               dataTable.loadDataTable();
            }
         }
      }
   });
})();