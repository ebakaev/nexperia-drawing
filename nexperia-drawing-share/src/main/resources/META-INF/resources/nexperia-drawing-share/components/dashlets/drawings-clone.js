/**
 * Sample Hello World dashboard component.
 *
 * @namespace MyCompany.dashlet
 * @class MyCompany.dashlet.HelloWorld
 * @author
 */
(function () {
    /**
     * YUI Library aliases
     */
    var Dom = YAHOO.util.Dom,
        Event = YAHOO.util.Event;

    /**
     * Alfresco Slingshot aliases
     */
    var $html = Alfresco.util.encodeHTML,
        $combine = Alfresco.util.combinePaths;


    /**
     * Dashboard HelloWorld constructor.
     *
     * @param {String} htmlId The HTML id of the parent element
     * @return {MyCompany.dashlet.HelloWorld} The new component instance
     * @constructor
     */
    Alfresco.dashlet.DrawingClone = function DrawingClone_constructor(htmlId) {
        return Alfresco.dashlet.DrawingClone.superclass.constructor.call(this, "Alfresco.dashlet.DrawingClone", htmlId);
    };

    /**
     * Extend from Alfresco.component.Base and add class implementation
     */
    YAHOO.extend(Alfresco.dashlet.DrawingClone, Alfresco.component.Base,
        {
            /**
             * Object container for initialization options
             *
             * @property options
             * @type object
             */
            options: {},

            /**
             * Fired by YUI when parent element is available for scripting
             *
             * @method onReady
             */
            onReady: function DrawingClone_onReady() {
                this.widgets.createDrawingButton = Alfresco.util.createYUIButton(this, "createDrawingButton", this.onCreateDrawingButtonClick);
                this.widgets.loadMetadataButton = Alfresco.util.createYUIButton(this, "loadMetadataButton", this.onLoadMetadataButtonButtonClick);
                this.widgets.resetCloneFormButton = Alfresco.util.createYUIButton(this, "resetCloneFormButton", this.onResetCloneForm);

                var primaryFormat = Dom.get(this.id + "-primary-format");
                var requiredRenditions = Dom.get(this.id + "-required-renditions");
                var requester = Dom.get(this.id + "-requester");

                if (primaryFormat.value === 'EPS') {
                    for (var i = 0; i < requiredRenditions.options.length; i++) {
                        for (var i = 0; i < requiredRenditions.options.length; i++) {
                            requiredRenditions.options[i].selected = requiredRenditions.options[i].value === 'SVG';
                        }
                    }
                }

                // We set current user to this property
                requester.value = Alfresco.constants.USERNAME;

                function callback(e) {
                    if (primaryFormat.value === 'EPS') {
                        for (var i = 0; i < requiredRenditions.options.length; i++) {
                            if (requiredRenditions.options[i].value === 'SVG') {
                                requiredRenditions.options[i].selected = true;
                            }
                        }
                    }
                }
                Event.addListener(primaryFormat, "change", callback);
            },

            /**
             * Button click event handler
             *
             * @method onCreateDrawingButtonClick
             */
            onCreateDrawingButtonClick: function DrawingClone_onCreateDrawingButtonClick(e) {
                var drawingIds = Dom.get(this.id + "-drawing-id").value.length > 0 ? Dom.get(this.id + "-drawing-id").value : null;
                var howManyToCreate = Dom.get(this.id + "-count").value.length > 0 ? Dom.get(this.id + "-count").value : null;
                var descriptiveTitle = Dom.get(this.id + "-descriptive-title").value.length > 0 ? Dom.get(this.id + "-descriptive-title").value : null;
                var drawingType = Dom.get(this.id + "-drawing-type").value.length > 0 ? Dom.get(this.id + "-drawing-type").value : null;
                var primaryFormat = Dom.get(this.id + "-primary-format").value.length > 0 ? Dom.get(this.id + "-primary-format").value : null;
                var drawingVersion = Dom.get(this.id + "-drawing-version").value.length > 0 ? Dom.get(this.id + "-drawing-version").value : null;
                var createdFor = Dom.get(this.id + "-created-for").value.length > 0 ? Dom.get(this.id + "-created-for").value : null;
                var securityStatus = Dom.get(this.id + "-security-status").value.length > 0 ? Dom.get(this.id + "-security-status").value : null;
                var magCode = Dom.get(this.id + "-mag-code").value.length > 0 ? Dom.get(this.id + "-mag-code").value : null;
                var requester = Dom.get(this.id + "-requester").value.length > 0 ? Dom.get(this.id + "-requester").value : null;
                var deliveryDate = Dom.get(this.id + "-delivery-date").value.length > 0 ? Dom.get(this.id + "-delivery-date").value : null;
                var requiredRenditionsMultiSelect = Dom.get(this.id + "-required-renditions");

                var requiredRenditions = "";

                for (var i = 0; i < requiredRenditionsMultiSelect.options.length; i++) {
                    if (requiredRenditionsMultiSelect.options[i].selected) {
                        requiredRenditions += requiredRenditionsMultiSelect.options[i].value + ",";
                    }
                }

                if (requiredRenditions.length > 0) {
                    requiredRenditions = requiredRenditions.substring(0, requiredRenditions.length - 1);
                }

                if (drawingType == null || primaryFormat == null || drawingVersion == null
                    || securityStatus == null || magCode == null || descriptiveTitle == null) {
                    Alfresco.util.PopupManager.displayPrompt({
                                title: "Drawing/s creation warning!",
                                text: "Please fill in all mandatory values!"
                            });
                    return;
                }

                if (drawingVersion.split(".").length != 2) {
                    Alfresco.util.PopupManager.displayPrompt({
                                title: "Drawing/s creation warning!",
                                text: "Please check 'Drawing version' value, it should be in '1.0' format!"
                            });
                    return;
                }

                if (requester != null && !validateEmail(requester)) {
                    Alfresco.util.PopupManager.displayPrompt({
                                title: "Drawing/s creation warning!",
                                text: "Requester value isn't valid! Please fill the correct email address."
                            });
                    return;
                }

                if (descriptiveTitle != null && descriptiveTitle.includes("|")) {
                    var descCount = descriptiveTitle.split("|");
                    if (drawingIds != null && drawingIds.includes(",")) {
                        var drawingCount = drawingIds.split(",");
                        if (drawingCount != null && drawingCount.length != descCount.length) {
                            Alfresco.util.PopupManager.displayPrompt({
                                    title: "Drawing/s creation warning!",
                                    text: "Count of 'Descriptive title' and 'Drawing ID' are not equal, please check. Descriptive titles should be separated by '|' and equals to commaseparated 'Drawing ID' field."
                                });
                            return;
                        }
                    } else if (howManyToCreate != null && howManyToCreate != 1 && descCount.length != howManyToCreate) {
                        Alfresco.util.PopupManager.displayPrompt({
                                title: "Drawing/s creation warning!",
                                text: "Count of 'Descriptive title' and 'Number of drawings to create' are not equal, please check. Descriptive titles should be separated by '|' and equals to 'Number of drawings to create' field if Drawing ID field is not specified."
                            });
                        return;
                    }
                    descriptiveTitle = encodeURI(descriptiveTitle);
                }

                var url = "";
                    if (drawingIds) {
                        url += "drawingIds=" + drawingIds + "&"
                    }
                    if (descriptiveTitle) {
                        url += "descriptiveTitle=" + descriptiveTitle + "&"
                    }
                    if (drawingType) {
                        url += "drawingType=" + drawingType + "&"
                    }
                    if (primaryFormat) {
                        url += "primaryFormat=" + primaryFormat + "&"
                    }
                    if (drawingVersion) {
                        url += "drawingVersion=" + drawingVersion + "&"
                    }
                    if (createdFor) {
                        url += "createdFor=" + createdFor + "&"
                    }
                    if (securityStatus) {
                        url += "securityStatus=" + securityStatus + "&"
                    }
                    if (magCode) {
                        url += "magCode=" + magCode + "&"
                    }
                    if (requester) {
                        url += "requester=" + requester + "&"
                    }
                    if (deliveryDate) {
                        url += "deliveryDate=" + deliveryDate + "&"
                    }
                    if (requiredRenditions) {
                        url += "requiredRenditions=" + requiredRenditions  + "&";
                    }
                    if (howManyToCreate) {
                        url += "howManyToCreate=" + howManyToCreate;
                    }

                Alfresco.util.Ajax.jsonGet({
                    url: Alfresco.constants.PROXY_URI + "drawing/clone.json?" + url,
                    successCallback: {
                        fn: function onDrawingCreate_Success(response) {
                            if (response.json.msg.includes("already exists")) {
                                Alfresco.util.PopupManager.displayPrompt({
                                    title: "Drawing/s creation failed!",
                                    noEscape: false,
                                    text: response.json.msg
                                });
                            } else {
                                Alfresco.util.PopupManager.displayPrompt({
                                    title: "Drawing/s successfully created!",
                                    noEscape: false,
                                    text: response.json.msg + " \nPlease note that Drawings will appear in Search and Site activities dashlets in 10-15sec."
                                });
                            }
                        },
                        scope: this
                    },
                    failureCallback: {
                        fn: function (response) {
                            Alfresco.util.PopupManager.displayMessage(
                            {
                                text: "Drawing/s is/are creation failed! Please contact system administrator!"
                            });
                        },
                        scope: this
                    }
                });

                function validateEmail(email) {
                                const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                                return re.test(String(email).toLowerCase());
                  }

            },

            onResetCloneForm: function DrawingClone_onResetCloneFormButtonClick(e) {
                var cloneForm = Dom.get(this.id + "-clone-form");
                cloneForm.reset();

                var primaryFormat = Dom.get(this.id + "-primary-format");
                var requiredRenditions = Dom.get(this.id + "-required-renditions");

                if (primaryFormat.value === 'EPS') {
                    for (var i = 0; i < requiredRenditions.options.length; i++) {
                        for (var i = 0; i < requiredRenditions.options.length; i++) {
                            requiredRenditions.options[i].selected = requiredRenditions.options[i].value === 'SVG';
                        }
                    }
                }
            },

            /**
             * Button click event handler
             *
             * @method onLoadMetadataButtonButtonClick
             */
            onLoadMetadataButtonButtonClick: function DrawingClone_onLoadMetadataButtonButtonClick(e) {



            function load_rendition(drawingIds, extension, element) {

                                 Alfresco.util.Ajax.jsonGet({
                                                        url: Alfresco.constants.PROXY_URI + "slingshot/live-search-docs?t=" + drawingIds + "." + extension + "&maxResults=1&site=tdm-drawing",
                                                        successCallback: {
                                                            fn: function onSearchRendition_Success(response2) {
                                                                if (response2.json.items.length > 0) {
                                                                    var url = Alfresco.constants.PROXY_URI + "/api/node/workspace/SpacesStore/" + response2.json.items[0].nodeRef.replace("workspace://SpacesStore/","") + "/content/thumbnails/imgpreview?c=force";
                                                                    Alfresco.util.Ajax.jsonGet({
                                                                        url: url,
                                                                        successCallback: {
                                                                            fn: function onSearchRendition_Success(response3) {
                                                                                element.src = url;
                                                                            },
                                                                            scope: this
                                                                        },
                                                                        failureCallback: {
                                                                            fn: function (response3) {
                                                                                element.src = "https://assets.nexperia.com/documents/logo/Nexperia_X_RGB.svg";
                                                                            },
                                                                            scope: this
                                                                        }
                                                                    });
                                                                } else {
                                                                    element.src = "https://assets.nexperia.com/documents/logo/Nexperia_X_RGB.svg";
                                                                }
                                                            },
                                                            scope: this
                                                        },
                                                        failureCallback: {
                                                            fn: function (response) {
                                                                element.src = "https://assets.nexperia.com/documents/logo/Nexperia_X_RGB.svg";
                                                                /* Set no preview picture  https://assets.nexperia.com/documents/logo/Nexperia_X_RGB.svg  */
                                                            },
                                                            scope: this
                                                        }
                                                    });

                        }





                var drawingIds = Dom.get(this.id + "-drawing-id").value;

                Alfresco.util.Ajax.jsonGet({
                    url: Alfresco.constants.PROXY_URI + "slingshot/search?term=" + drawingIds + "&maxResults=1&site=tdm-drawing",
                    successCallback: {
                        fn: function onGetMetadata_Success(response) {
                            var primaryFormat = response.json.items[0].node.properties["draw:primaryFormat"];
                            Dom.get(this.id + "-descriptive-title").value = response.json.items[0].node.properties["draw:descriptiveTitle"];
                            Dom.get(this.id + "-drawing-type").value = response.json.items[0].node.properties["draw:drawingType"];
                            Dom.get(this.id + "-primary-format").value = primaryFormat;
                            Dom.get(this.id + "-drawing-version").value = response.json.items[0].node.properties["draw:drawingVersion"];
                            Dom.get(this.id + "-created-for").value = response.json.items[0].node.properties["draw:createdFor"];
                            Dom.get(this.id + "-security-status").value = response.json.items[0].node.properties["draw:securityStatus"];
                            Dom.get(this.id + "-mag-code").value = response.json.items[0].node.properties["draw:magCode"];
                            Dom.get(this.id + "-requester").value = requester = Alfresco.constants.USERNAME;
//                            Dom.get(this.id + "-requester").value = response.json.items[0].node.properties["draw:requester"];

//                            var deliveryDate = response.json.items[0].node.properties["draw:deliveryDate"];
//
//                            var now = new Date();
//                            if (deliveryDate) {
//                               var delDate = new Date(deliveryDate.iso8601);
//                               if (now.getTime() > delDate.getTime()) {
//                                   Dom.get(this.id + "-delivery-date").value = now.toISOString().split("T")[0];
//                               } else {
//                                   Dom.get(this.id + "-delivery-date").value = deliveryDate.iso8601.split("T")[0];
//                               }
//                            } else {
//                               Dom.get(this.id + "-delivery-date").value = now.toISOString().split("T")[0];
//                            }

                            /* Required renditions */
                            var rrs = Dom.get(this.id + "-required-renditions").options, rr;
                            for(i = 0; i < rrs.length; i++) {
                               rr = rrs[i];
                               var responseRRs = response.json.items[0].node.properties["draw:requiredRenditions"], responseRR
                               if (responseRRs) {
                                   for(j = 0; j < responseRRs.length; j++) {
                                       responseRR = responseRRs[j];
                                       if (rr.innerText === responseRR) {
                                           rr.selected = true;
                                       }
                                   }
                               }
                            }

                            var element = Dom.get(this.id + "-preview");
                            element.src = Alfresco.constants.PROXY_URI + "drawing/preview?drawingId=" + drawingIds;
                        },
                        scope: this
                    },
                    failureCallback: {
                        fn: function (response) {
                            Alfresco.util.PopupManager.displayMessage(
                            {
                                text: "Drawing not found or incorrect value is set!"
                            });
                        },
                        scope: this
                    }
                });

            },

            /**
            * Convert an ISO8601 date string into a native JavaScript Date object
            *
            * @instance
            * @param {String} date ISO8601 formatted date string
            * @param {Boolean} [ignoreTime] Ignores any time (and therefore timezone) components.
            * @return {Date} JavaScript native Date object or null on errors
            */
            fromISO8601: function alfresco_core_TemporalUtils__fromISO8601(dateString, ignoreTime) {
             if (ignoreTime)
                 {
                    dateString = dateString.split("T")[0];
                 }
             try
                 {
                    return stamp.fromISOString(dateString);
                 }
             catch(e)
                 {
                    return null;
                 }
            }


        });
})();