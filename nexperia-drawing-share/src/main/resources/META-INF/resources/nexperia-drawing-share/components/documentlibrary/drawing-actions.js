(function()
{

    var preloaderInstance;

    var preloaderStart = function (){
        preloaderInstance = Alfresco.util.PopupManager.displayMessage({
            text: "Please wait...",
            modal: true,
            displayTime: 0,
            spanClass: "wait",
            effect: null
        });
    };

    var preloaderStop = function(){
        try{
            if (preloaderInstance) {
                preloaderInstance.destroy();
            }
        }catch(e) {}
    };

    var displayMessage = function(response) {
        Alfresco.util.PopupManager.displayMessage({
            displayTime: 3,
            text: response
        });
    };

    YAHOO.Bubbling.fire("registerAction",{ actionName: "onDrawingCreate",
        fn: function onDrawingCreate(record) {
            preloaderStart();

            Alfresco.util.Ajax.jsonGet({
                url: Alfresco.constants.PROXY_URI + "drawing/folder/convert.json?nodeRef=" + record.nodeRef,
                successCallback: {
                    fn: function onFolderTypeChange_Success(response) {
                        preloaderStop();
                        YAHOO.Bubbling.fire("metadataRefresh");
                        if (response.serverResponse.responseText) {
                            var message = JSON.parse(response.serverResponse.responseText)["msg"];
                            displayMessage(message);
                        }
                    },
                    scope: this
                },
                failureCallback: {
                    fn: function (response) {
                        preloaderStop();
                        if (response.serverResponse.responseText) {
                            var message = JSON.parse(response.serverResponse.responseText)["message"];
                            message = message.replace(/[0-9]/g, '');
                            displayMessage(message);
                        }
                    },
                    scope: this
                }
            });

        }
    });

    YAHOO.Bubbling.fire("registerAction",{ actionName: "onSelectedDrawingCreate",
        fn: function onSelectedDrawingCreate(assets) {
            preloaderStart();
            assets.forEach(function(item, i, arr) {
                if (item.jsNode.type === "cm:folder") {
//                    alert( i + " ::::: type => " + item.jsNode.type + " ::::: nodeRef =>" + item.nodeRef + ")" );
                    Alfresco.util.Ajax.jsonGet({
                        url: Alfresco.constants.PROXY_URI + "drawing/folder/convert.json?nodeRef=" + item.nodeRef,
                        successCallback: {
                            fn: function onFolderTypeChange_Success(response) {
                                preloaderStop();
                                YAHOO.Bubbling.fire("metadataRefresh");
                                if (response.serverResponse.responseText) {
                                    var message = JSON.parse(response.serverResponse.responseText)["msg"];
                                    displayMessage(message);
                                }
                            },
                            scope: this
                        },
                        failureCallback: {
                            fn: function (response) {
                                preloaderStop();
                                if (response.serverResponse.responseText) {
                                    var message = JSON.parse(response.serverResponse.responseText)["message"];
                                    message = message.replace(/[0-9]/g, '');
                                    displayMessage(message);
                                }
                            },
                            scope: this
                        }
                    });
                }
            });
        }
    });

    YAHOO.Bubbling.fire("registerAction",{ actionName: "onDrawingArchive",
        fn: function onDrawingArchive(record) {
            preloaderStart();

            Alfresco.util.Ajax.jsonGet({
                url: Alfresco.constants.PROXY_URI + "drawing/archive.json?nodeRef=" + record.nodeRef,
                successCallback: {
                    fn: function onDrawingArchive_Success(response) {
                        preloaderStop();
                        YAHOO.Bubbling.fire("metadataRefresh");
                        if (response.serverResponse.responseText) {
                            var message = JSON.parse(response.serverResponse.responseText)["msg"];
                            displayMessage(message);
                        }
                    },
                    scope: this
                },
                failureCallback: {
                    fn: function (response) {
                        preloaderStop();
                        if (response.serverResponse.responseText) {
                            var message = JSON.parse(response.serverResponse.responseText)["message"];
                            message = message.replace(/[0-9]/g, '');
                            displayMessage(message);
                        }
                    },
                    scope: this
                }
            });

        }
    });

    YAHOO.Bubbling.fire("registerAction",{ actionName: "onDrawingActivate",
        fn: function onDrawingActivate(record) {
            preloaderStart();

            Alfresco.util.Ajax.jsonGet({
                url: Alfresco.constants.PROXY_URI + "drawing/activate.json?nodeRef=" + record.nodeRef,
                successCallback: {
                    fn: function onDrawingActivate_Success(response) {
                        preloaderStop();
                        YAHOO.Bubbling.fire("metadataRefresh");
                        if (response.serverResponse.responseText) {
                            var message = JSON.parse(response.serverResponse.responseText)["msg"];
                            displayMessage(message);
                        }
                    },
                    scope: this
                },
                failureCallback: {
                    fn: function (response) {
                        preloaderStop();
                        if (response.serverResponse.responseText) {
                            var message = JSON.parse(response.serverResponse.responseText)["message"];
                            message = message.replace(/[0-9]/g, '');
                            displayMessage(message);
                        }
                    },
                    scope: this
                }
            });

        }
    });

        YAHOO.Bubbling.fire("registerAction",{ actionName: "onInputDocumentCreate",
            fn: function onDrawingCreate(record) {
                preloaderStart();

                Alfresco.util.Ajax.jsonGet({
                    url: Alfresco.constants.PROXY_URI + "drawing/folder/create.json?nodeRef=" + record.nodeRef,
                    successCallback: {
                        fn: function onDrawingActivate_Success(response) {
                            preloaderStop();
                            YAHOO.Bubbling.fire("metadataRefresh");
                            displayMessage("Done!");
                        },
                        scope: this
                    },
                    failureCallback: {
                        fn: function (response) {
                            preloaderStop();
                            displayMessage("Failed!");
                        },
                        scope: this
                    }
                });

            }
        });

})();