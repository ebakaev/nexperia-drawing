<@markup id="css" >
   <#-- CSS Dependencies -->
   <@link rel="stylesheet" type="text/css" href="${url.context}/res/nexperia-drawing-share/components/dashlets/site-search.css" group="dashlets"  />
</@>

<@markup id="js">
   <#-- JavaScript Dependencies -->
   <@script type="text/javascript" src="${url.context}/res/nexperia-drawing-share/components/search/search-lib.js" group="dashlets"/>
   <@script type="text/javascript" src="${url.context}/res/nexperia-drawing-share/components/dashlets/site-search.js" group="dashlets"/>
</@>

<@markup id="widgets">
   <@createWidgets group="dashlets"/>
</@>

<@markup id="html">
   <@uniqueIdDiv>
      <#assign el=args.htmlid?html>
            <style>
                #${el}-search-button { background-color: #E64D1F!important;color: white; }
                #${el}-qSearchResetButton-button { background-color: #E64D1F!important;color: white; }
            </style>
      <div class="dashlet sitesearch">
         <div class="title" style="background-color: #036F7B;color: white;">Fast search</div>
         <div class="toolbar body" style="overflow-x: auto;background-color: #f3f6f8;">
            <form id="${el}-quick-search-form">
            <table style="width: 100%;table-layout: auto;">
                <tbody>
                <tr>
                    <td><span style="display: inline-block; min-width: 130px;"> Drawing ID: </span></td>
                    <td><input id="${el}-fast-search-text" maxlength="1024" type="text" style="width:193px"/></td>
                    <td rowspan="2">
                        <div>
                            <span class="yui-button yui-menu-button" id="${el}-resultSize">
                                 <span class="first-child">
                                     <button type="button" tabindex="0"></button>
                                 </span>
                            </span>
                            <select id="${el}-resultSize-menu">
                                <option value="10">10</option>
                                <option value="25">25</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                                <option value="1000">1000</option>
                                <option value="5000">5000</option>
                            </select>
                        </div>
                        <div style="width: 105px;padding-top: 3px">
                            <input type="checkbox" id="${el}-archive"/>
                            <label for="${el}-archive">Archive filter</label>
                        </div>
                    </td>
                </tr>
                <tr style="margin-bottom: 5px;">
                    <td><span style="display: inline-block; min-width: 130px;"> MAG code: </span></td>
                    <td><select id="${el}-mag-search-text" style="width:198px">
                        <option value=""></option>
                        <option value="RE1">RE1</option>
                        <option value="RE2">RE2</option>
                        <option value="RP3">RP3</option>
                        <option value="RP8">RP8</option>
                        <option value="RY7">RY7</option>
                        <option value="R03">R03</option>
                        <option value="R04">R04</option>
                        <option value="R07">R07</option>
                        <option value="R17">R17</option>
                        <option value="R18">R18</option>
                        <option value="R19">R19</option>
                        <option value="R33">R33</option>
                        <option value="R34">R34</option>
                        <option value="R71">R71</option>
                        <option value="R73">R73</option>
                    </select>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <table>
                            <tr>
                                <td>
                                    <span class="yui-button yui-push-button search-icon">
                                        <span class="first-child">
                                            <button id="${el}-search-button" type="button">${msg("searchButton.text")}</button>

                                        </span>
                                    </span>
                                </td>
                                <td>
                                    <button id="${el}-qSearchResetButton" type="button">Reset</button>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
            </form>
         </div>
         <div id="${el}-list" class="body scrollableList" <#if args.height??>style="height: ${args.height?html}px;"</#if>>
            <div id="${el}-search-results"></div>
         </div>
      </div>
   </@>
</@>