<@markup id="css" >
   <#-- CSS Dependencies -->
   <@link rel="stylesheet" type="text/css" href="${url.context}/res/nexperia-drawing-share/components/dashlets/site-search.css" group="dashlets"  />
</@>

<@markup id="js">
   <#-- JavaScript Dependencies -->
   <@script type="text/javascript" src="${url.context}/res/nexperia-drawing-share/components/search/drawing-advanced-search-lib.js" group="dashlets"/>
   <@script type="text/javascript" src="${url.context}/res/nexperia-drawing-share/components/dashlets/drawing-advanced-search.js" group="dashlets"/>
</@>

<@markup id="widgets">
   <@createWidgets group="dashlets"/>
</@>

<@markup id="html">
   <@uniqueIdDiv>
      <#assign el=args.htmlid?html>
      <style>
          #${el}-advSearchResetButton-button { background-color: #E64D1F!important;color: white; }
          #${el}-print-view-button { background-color: #E64D1F!important;color: white; }
          #${el}-update-button { background-color: #E64D1F!important;color: white; }
          #${el}-report-button { background-color: #E64D1F!important;color: white; }
          #${el}-search-button { background-color: #E64D1F!important;color: white; }
          #${el}-download-button { background-color: #E64D1F!important;color: white; }
      </style>
      <div class="dashlet advancedsearch">
         <div class="title" style="background-color: #036F7B;color: white;">${msg("header.title")}</div>

<div class="toolbar body" style="overflow-x: auto;background-color: #f3f6f8;">
    <form id="${el}-adv-search-form">
        <table style="width: 100%;table-layout: auto;">
            <tbody>
            <tr>
                <td><span style="display: inline-block; min-width: 130px;"> Drawing ID: </span></td>
                <td><input id="${el}-search-text" maxlength="1024" type="text" style="width:193px"/></td>
                <td rowspan="2">
                    <div>
                            <span class="yui-button yui-menu-button" id="${el}-resultSize">
                                 <span class="first-child">
                                     <button type="button" tabindex="0"></button>
                                 </span>
                            </span>
                        <select id="${el}-resultSize-menu">
                            <option value="10">10</option>
                            <option value="25">25</option>
                            <option value="50">50</option>
                            <option value="100">100</option>
                            <option value="1000">1000</option>
                            <option value="5000">5000</option>
                        </select>
                    </div>
                    <div style="width: 105px;padding-top: 3px">
                        <input type="checkbox" id="${el}-archive"/>
                        <label for="${el}-archive">Archive filter</label>
                    </div>
                </td>
            </tr>
            <tr style="margin-bottom: 5px;">
                <td><span style="display: inline-block; min-width: 130px;"> MAG code: </span></td>
                <td><select id="${el}-mag-search-text" style="width:198px">
                    <option value=""></option>
                    <option value="R03">R03</option>
                    <option value="R04">R04</option>
                    <option value="R07">R07</option>
                    <option value="R17">R17</option>
                    <option value="R18">R18</option>
                    <option value="R19">R19</option>
                    <option value="R33">R33</option>
                    <option value="R34">R34</option>
                    <option value="R71">R71</option>
                    <option value="R73">R73</option>
                    <option value="RE1">RE1</option>
                    <option value="RE2">RE2</option>
                    <option value="RP3">RP3</option>
                    <option value="RP8">RP8</option>
                    <option value="RTC">RTC</option>
                    <option value="RY7">RY7</option>
                    </select>
                </td>
            </tr>

            <tr>
                <td style="width: 132px;"><span
                        style="display: inline-block; min-width: 130px;"> Primary format: </span></td>
                <td style="width: 200px;"><select id="${el}-primaryFormat-search-text" style="width:198px">
                    <option value=""></option>
                    <option value="EPS">EPS</option>
                    <option value="SVG">SVG</option>
                    <option value="JPG">JPG</option>
                </select></td>
            </tr>
            <tr>
                <td style="width: 132px;"><span
                        style="display: inline-block; min-width: 130px;"> Descriptive title: </span></td>
                <td style="width: 200px;"><input id="${el}-descriptiveTitle-search-text" maxlength="1024" type="text"
                                                 style="width:193px"/></td>
            </tr>
            <tr>
                <td style="width: 132px;"><span style="display: inline-block; min-width: 130px;"> Drawing type: </span>
                </td>
                <td style="width: 200px;"><select id="${el}-drawingType-search-text" style="width:198px">
                    <option value=""></option>
                    <option value="Application diagram">Application diagram</option>
                    <option value="Blank pinning diagram">Blank pinning diagram</option>
                    <option value="Block diagram">Block diagram</option>
                    <option value="Equation (MathML)">Equation (MathML)</option>
                    <option value="Equivalent circuit">Equivalent circuit</option>
                    <option value="Footprint">Footprint</option>
                    <option value="Flowchart">Flowchart</option>
                    <option value="Functional diagram">Functional diagram</option>
                    <option value="Graph">Graph</option>
                    <option value="Generated graph">Generated graph</option>
                    <option value="Generated pin config diagram">Generated pin config diagram</option>
                    <option value="Graphic symbol">Graphic symbol</option>
                    <option value="Icons">Icons</option>
                    <option value="Internal circuitry">Internal circuitry</option>
                    <option value="Intellectual Property Department (IPD)">Intellectual Property Department (IPD)
                    </option>
                    <option value="Labeling">Labeling</option>
                    <option value="Logo">Logo</option>
                    <option value="Logic diagram">Logic diagram</option>
                    <option value="Logic symbol">Logic symbol</option>
                    <option value="Memory organization chart">Memory organization chart</option>
                    <option value="Mechanical drawing">Mechanical drawing</option>
                    <option value="Minimized outline">Minimized outline</option>
                    <option value="On screen display">On screen display</option>
                    <option value="Outline 3d">Outline 3d</option>
                    <option value="Other graphic">Other graphic</option>
                    <option value="Package outline">Package outline</option>
                    <option value="Packing">Packing</option>
                    <option value="Port configuration diagram">Port configuration diagram</option>
                    <option value="Pin configuration diagram">Pin configuration diagram</option>
                    <option value="Programming model chart">Programming model chart</option>
                    <option value="Printed circuit">Printed circuit</option>
                    <option value="Simplified outline">Simplified outline</option>
                    <option value="Schematic diagram">Schematic diagram</option>
                    <option value="State diagram">State diagram</option>
                    <option value="Teletext character set">Teletext character set</option>
                    <option value="Test circuit">Test circuit</option>
                    <option value="Timing diagram">Timing diagram</option>
                    <option value="Waveform">Waveform</option>
                </select></td>
            </tr>
            <tr>
                <td style="width: 132px;"><span style="display: inline-block; min-width: 130px;"> Created for: </span>
                </td>
                <td style="width: 200px;"><input id="${el}-createdFor-search-text" maxlength="1024" type="text"
                                                 style="width:193px"/></td>
            </tr>
            <tr>
                <td style="width: 132px;"><span
                        style="display: inline-block; min-width: 130px;"> Security status: </span></td>
                <td style="width: 200px;"><select id="${el}-securityStatus-search-text" style="width:198px">
                    <option value=""></option>
                    <option value="Company Public">Company Public</option>
                    <option value="Company Secret">Company Secret</option>
                    <option value="Company Internal">Company Internal</option>
                    <option value="Company Confidential">Company Confidential</option>
                </select></td>
            </tr>
            <tr>
                <td style="width: 132px;"><span style="display: inline-block; min-width: 130px;"> Requester: </span>
                </td>
                <td style="width: 200px;"><input id="${el}-requester-search-text" maxlength="1024" type="text"
                                                 style="width:193px"/></td>
            </tr>
            <tr>
                <td style="width: 132px;"><span style="display: inline-block; min-width: 130px;"> Delivery date: </span>
                </td>
                <td style="width: 200px;"><input id="${el}-deliveryDate-search-text" maxlength="1024" type="date"
                                                 style="width:192px"/></td>
            </tr>
            <tr>
                <td style="width: 132px;" valign="top"><span
                        style="display: inline-block; min-width: 130px;text-align:top"> Modification date: </span></td>
                <td style="width: 200px;padding:0;">
                    <table style="border-spacing: 0px;">
                        <tr>
                            <td>
                                <label for="${el}-modificationDateFrom-search-text" style="display: inline-block; width: 40px;">From:</label>
                                <input id="${el}-modificationDateFrom-search-text" type="date" style="width:143px"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="${el}-modificationDateTo-search-text" style="display: inline-block; width: 40px;">To:</label>
                                <input id="${el}-modificationDateTo-search-text" type="date" style="width:143px"/>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="vertical-align: top; width: 132px;"><span style="display: inline-block; min-width: 130px;"> Required renditions: </span>
                </td>
                <td style="width: 200px;"><select id="${el}-requiredRenditions-search-text" style="width:198px"
                                                  multiple="multiple">
                    <option value="SVG">SVG</option>
                    <option value="PNG">PNG</option>
                    <option value="PDF">PDF</option>
                    <option value="JPG">JPG</option>
                    <option value="GIF">GIF</option>
                </select></td>
            </tr>
            <tr>
                <td style="width: 132px;"><span style="display: inline-block; min-width: 130px;"> Version: </span></td>
                <td style="width: 200px;"><input id="${el}-version-search-text" maxlength="1024" type="text"
                                                 style="width:193px"/></td>
            </tr>
            <tr>
                <td style="width: 132px;"><span style="display: inline-block; min-width: 130px;"> Drawing status: </span></td>
                <td style="width: 200px;">
                    <select id="${el}-drawStatus-search-text" style="width:198px">
                        <option value=""></option>
                        <option value="Draft">Draft</option>
                        <option value="Pending approval">Pending approval</option>
                        <option value="Approved">Approved</option>
                        <option value="Released">Released</option>
                    </select>
<#--                    <input id="${el}-drawStatus-search-text" maxlength="1024" type="text"-->
<#--                                                 style="width:193px"/></td>-->
            </tr>
            <tr>
                <td style="width: 132px;"><span style="display: inline-block; min-width: 130px;"> Creator: </span></td>
                <td style="width: 200px;"><input id="${el}-creator-search-text" maxlength="1024" type="text"
                                                 style="width:193px"/></td>
            </tr>
            <tr>
                <td colspan="3">
                    <table>
                        <tr>
                            <td>
                                <span>
                                    <button class="search-icon-drawing" id="${el}-search-button" type="button" style="height: 28px;">
                                        ${msg("searchButton.text")}
                                    </button>
                                </span>
                            </td>
                            <td>
                                <span class="yui-button yui-push-button">
                                    <span class="first-child">
                                        <button id="${el}-report-button" type="button">${msg("printReportButton.text")}</button>
                                     </span>
                                </span>
                            </td>
                            <td>
                                <span class="yui-button yui-push-button">
                                     <span class="first-child">
                                        <button id="${el}-print-view-button" type="button">${msg("printViewButton.text")}</button>
                                     </span>
                                </span>
                            </td>
                            <td>
                                <span class="yui-button yui-push-button">
                                     <span class="first-child">
                                        <button id="${el}-update-button" type="button">Update metadata</button>
                                     </span>
                                </span>
                            </td>
                            <td>
                                <span class="yui-button yui-push-button">
                                     <span class="first-child">
                                        <button id="${el}-download-button" type="button">Download</button>
                                     </span>
                                </span>
                            </td>
                            <td>
                                <button id="${el}-advSearchResetButton" type="button">${msg("advSearchResetButton.text")}</button>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            </tbody>
        </table>
    </form>
</div>


         <div id="${el}-list" class="body scrollableList" <#if args.height??>style="height: ${args.height?html}px;"</#if>>
            <div id="${el}-search-results"></div>
         </div>
      </div>
   </@>
</@>