<div class="form-field">
   <#if form.mode == "view">
      <div class="viewmode-field">
      </div>
   <#else>
        <div id="template_x002e_dnd-upload_x002e_documentlibrary_x0023_default-versionSection-div" class="">
            <div class="yui-gd" style="width: 200px;">
               <div class="yui-u first" style="width: 90px;">
                  <label for="${fieldHtmlId}-minor" id="${fieldHtmlId}-minorVersion">Minor changes</label>
               </div>
               <div class="yui-u" style="width: 20px;">
                  <input id="${fieldHtmlId}" type="hidden" name="${field.name}" value="minor" />
                  <input id="id="${fieldHtmlId}-minor" type="radio" name="-" checked="checked" tabindex="0" style="width: 25px;" onchange='javascript:YAHOO.util.Dom.get("${fieldHtmlId}").value="minor";' checked="checked">
               </div>
            </div>
            <div class="yui-gd" style="width: 200px;">
               <div class="yui-u first" style="width: 90px;">
                  <label for="${fieldHtmlId}-major" id="${fieldHtmlId}-majorVersion">Major changes</label>
               </div>
               <div class="yui-u" style="width: 20px;">
                  <input id="${fieldHtmlId}-major" type="radio" name="-" tabindex="0" style="width: 25px;" onchange='javascript:YAHOO.util.Dom.get("${fieldHtmlId}").value="major";'>
               </div>
            </div>
        </div>
   </#if>
</div>