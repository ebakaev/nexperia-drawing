<@markup id="css" >
<#-- CSS Dependencies -->
</@>

<@markup id="js">
<#-- JavaScript Dependencies -->
<@script type="text/javascript" src="${url.context}/res/nexperia-drawing-share/components/dashlets/drawings-clone.js" group="dashlets"/>
</@>

<@markup id="widgets">
<@createWidgets group="dashlets"/>
</@>

<@markup id="html">
<@uniqueIdDiv>
<#assign el=args.htmlid?html>
<#assign dashboardconfig=config.scoped['Dashboard']['dashboard']>
<style>
    #${el}-loadMetadataButton-button { background-color: #E64D1F!important;color: white; }
    #${el}-createDrawingButton-button { background-color: #E64D1F!important;color: white; }
    #${el}-resetCloneFormButton-button { background-color: #E64D1F!important;color: white; }
</style>
<div class="dashlet clone">
    <div class="title" style="background-color: #036F7B;color: white;">
        ${msg("header.title")}
    </div>
    <div class="body">

        <form id="${el}-clone-form">
            <table style="width: 100%;table-layout: auto;background-color: #f3f6f8;">
                <tbody>
                <tr>
                    <td>Drawing ID:</td>
                    <td>
                        <textarea id="${el}-drawing-id" rows=2></textarea>
                        <!--                    <input id="${el}-drawing-id" maxlength="1024" type="text" />-->
                    </td>
                    <td rowspan="12" style="text-align: center;"><img id="${el}-preview"
                                                                      src="https://assets.nexperia.com/documents/logo/Nexperia_X_RGB.svg"
                                                                      width="400" height="400"/></td>
                </tr>
                <tr>
                    <td>
                        <button id="${el}-loadMetadataButton">Retrieve metadata</button>
                    </td>
                </tr>
                <tr>
                    <td style="width: 160px;">Number of drawings to create:</td>
                    <td style="width: 187px;"><input id="${el}-count" maxlength="2" type="text" value="1" style="width: 25px"/></td>
                </tr>
                <tr>
                    <td style="width: 160px;">Descriptive title <span style="color:red;font-weight: bold">*</span>:</td>
                    <td style="width: 187px;">
                        <textarea id="${el}-descriptive-title" rows=2></textarea>
                    </td>
                </tr>
                <tr>
                    <td style="width: 160px;">Drawing type <span style="color:red;font-weight: bold">*</span>:</td>
                    <td style="width: 187px;">
                        <select id="${el}-drawing-type" style="width: 199px;">
                            <option value="Application diagram">Application diagram</option>
                            <option value="Blank pinning diagram">Blank pinning diagram</option>
                            <option value="Block diagram">Block diagram</option>
                            <option value="Equation (MathML)">Equation (MathML)</option>
                            <option value="Equivalent circuit">Equivalent circuit</option>
                            <option value="Footprint">Footprint</option>
                            <option value="Flowchart">Flowchart</option>
                            <option value="Functional diagram">Functional diagram</option>
                            <option value="Graph">Graph</option>
                            <option value="Generated graph">Generated graph</option>
                            <option value="Generated pin config diagram">Generated pin config diagram</option>
                            <option value="Graphic symbol">Graphic symbol</option>
                            <option value="Icons">Icons</option>
                            <option value="Internal circuitry">Internal circuitry</option>
                            <option value="Intellectual Property Department (IPD)">Intellectual Property Department
                                (IPD)
                            </option>
                            <option value="Labeling">Labeling</option>
                            <option value="Logo">Logo</option>
                            <option value="Logic diagram">Logic diagram</option>
                            <option value="Logic symbol">Logic symbol</option>
                            <option value="Memory organization chart">Memory organization chart</option>
                            <option value="Mechanical drawing">Mechanical drawing</option>
                            <option value="Minimized outline">Minimized outline</option>
                            <option value="On screen display">On screen display</option>
                            <option value="Outline 3d">Outline 3d</option>
                            <option value="Other graphic">Other graphic</option>
                            <option value="Package outline">Package outline</option>
                            <option value="Packing">Packing</option>
                            <option value="Port configuration diagram">Port configuration diagram</option>
                            <option value="Pin configuration diagram">Pin configuration diagram</option>
                            <option value="Programming model chart">Programming model chart</option>
                            <option value="Printed circuit">Printed circuit</option>
                            <option value="Simplified outline">Simplified outline</option>
                            <option value="Schematic diagram">Schematic diagram</option>
                            <option value="State diagram">State diagram</option>
                            <option value="Teletext character set">Teletext character set</option>
                            <option value="Test circuit">Test circuit</option>
                            <option value="Timing diagram">Timing diagram</option>
                            <option value="Waveform">Waveform</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td style="width: 160px;">Primary Format <span style="color:red;font-weight: bold">*</span>:</td>
                    <td style="width: 187px;">
                        <select id="${el}-primary-format" style="width: 199px;">
                            <option value="EPS">EPS</option>
                            <option value="SVG">SVG</option>
                            <option value="JPG">JPG</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td style="width: 160px;">Drawing Version <span style="color:red;font-weight: bold">*</span>:</td>
                    <td style="width: 187px;"><input id="${el}-drawing-version" maxlength="1024" type="text"/></td>
                </tr>
                <tr>
                    <td style="width: 160px;">Created for:</td>
                    <td style="width: 187px;"><input id="${el}-created-for" maxlength="1024" type="text"/></td>
                </tr>
                <tr>
                    <td style="width: 160px;">Security Status <span style="color:red;font-weight: bold">*</span>:</td>
                    <td style="width: 187px;">
                        <select id="${el}-security-status" style="width: 199px;">
                            <option value="Company Public">Company Public</option>
                            <option value="Company Secret">Company Secret</option>
                            <option value="Company Internal">Company Internal</option>
                            <option value="Company Confidential">Company Confidential</option>
                        </select></td>
                </tr>
                <tr>
                    <td style="width: 160px;">MAG code <span style="color:red;font-weight: bold">*</span>:</td>
                    <td style="width: 187px;">
                        <select id="${el}-mag-code" style="width: 199px;">
                            <option value="R03">R03</option>
                            <option value="R04">R04</option>
                            <option value="R07">R07</option>
                            <option value="R17">R17</option>
                            <option value="R18">R18</option>
                            <option value="R19">R19</option>
                            <option value="R33">R33</option>
                            <option value="R34">R34</option>
                            <option value="R71">R71</option>
                            <option value="R73">R73</option>
                            <option value="RE1">RE1</option>
                            <option value="RE2">RE2</option>
                            <option value="RP3">RP3</option>
                            <option value="RP8">RP8</option>
                            <option value="RTC">RTC</option>
                            <option value="RY7">RY7</option>
                        </select></td>
                </tr>
                <tr>
                    <td style="width: 160px;">Requester:</td>
                    <td style="width: 187px;"><input id="${el}-requester" maxlength="1024" type="text"/></td>
                </tr>
                <tr>
                    <td style="width: 160px;">Delivery date:</td>
                    <td style="width: 187px;"><input id="${el}-delivery-date" maxlength="1024" type="date" style="width: 194px;"/></td>
                </tr>
                <tr>
                    <td style="width: 160px;">Required renditions:</td>
                    <td style="width: 187px;">
                        <select id="${el}-required-renditions" multiple="multiple" style="width: 199px;">
                            <option value="SVG">SVG</option>
                            <option value="PNG">PNG</option>
                            <option value="JPG">JPG</option>
                            <option value="GIF">GIF</option>
                        </select></td>
                </tr>
                <tr>
                    <td colspan=3>
                        <table>
                            <tr>
                                <td>
                                    <button id="${el}-createDrawingButton">Create drawing job</button>
                                </td>
                                <td>
                                    <button id="${el}-resetCloneFormButton">Reset</button>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
        </form>

    </div>
</div>
</@>
</@>