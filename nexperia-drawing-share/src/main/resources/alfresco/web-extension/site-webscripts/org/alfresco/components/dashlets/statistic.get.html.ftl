<@markup id="css" >
<#-- CSS Dependencies -->
    <@link rel="stylesheet" type="text/css" href="${url.context}/res/nexperia-drawing-share/components/dashlets/statistic/css/visualize.css" group="dashlets"  />
    <@link rel="stylesheet" type="text/css" href="${url.context}/res/nexperia-drawing-share/components/dashlets/statistic/css/visualize-light.css" group="dashlets"  />
</@>

<@markup id="js">
<#-- JavaScript Dependencies -->
    <@script type="text/javascript" src="${url.context}/res/nexperia-drawing-share/components/dashlets/statistic/js/visualize.jQuery.js" group="dashlets"/>
</@>

<@markup id="widgets">
    <@createWidgets group="dashlets"/>
</@>

<@markup id="html">
    <@uniqueIdDiv>
        <#assign el=args.htmlid?html>
        <div class="dashlet statistic">
            <div class="title" style="background-color: #036F7B;color: white;">Drawing creation statistic</div>
            <div class="toolbar body" style="overflow-x: auto;background-color: #f3f6f8;">
            </div>
        </div>
    </@>
</@>