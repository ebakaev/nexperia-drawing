var searchResultPage = widgetUtils.findObject(model.jsonModel.widgets, "id", "FCTSRCH_SEARCH_RESULT");

if(searchResultPage != null) {
    searchResultPage.config = {
        enableContextMenu : false,
        mergeActions : true,
        additionalDocumentAndFolderActions : ["release-drawing-job-action"]
    }
}

model.jsonModel.widgets.push({
    name: "search/action"
});