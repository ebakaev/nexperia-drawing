package com.nexperia.alfresco.evaluator;

import org.alfresco.web.evaluator.BaseEvaluator;
import org.json.simple.JSONObject;

public class IsPrimaryFormat extends BaseEvaluator {

    @Override
    public boolean evaluate(JSONObject jsonObject) {
        String type = this.getNodeType(jsonObject);
        if ("draw:drawingSpace".equals(type)) {
            return false;
        } else {
            return checkParent(jsonObject);
        }
    }

    private boolean checkParent(JSONObject jsonObject) {
        JSONObject parent = (JSONObject) jsonObject.get("parent");
        String renditionName = (String) this.getProperty(jsonObject, "cm:name");
        JSONObject drawingProperties = (JSONObject) parent.get("properties");
        String primaryFormat = (String) drawingProperties.get("draw:primaryFormat");
        return renditionName.contains(primaryFormat.toLowerCase()) || renditionName.contains(primaryFormat.toUpperCase());
    }
}
