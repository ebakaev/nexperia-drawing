package com.nexperia.alfresco.evaluator;

import org.alfresco.web.evaluator.BaseEvaluator;
import org.json.simple.JSONObject;

public class HasDrawingStatusEvaluator extends BaseEvaluator {

	private final String status;

	protected HasDrawingStatusEvaluator(String status) {
		this.status = status;
	}

	@Override
	public boolean evaluate(JSONObject jsonObject) {
		JSONObject node = (JSONObject) jsonObject.get("node");
		if (node != null) {
			JSONObject properties = (JSONObject) node.get("properties");
			String currentStatus = (String) properties.get("draw:drawingStatus");
			return status.equalsIgnoreCase(currentStatus);
		}
		return false;
	}
}
