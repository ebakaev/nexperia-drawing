package com.nexperia.alfresco.evaluator;

import org.alfresco.web.evaluator.BaseEvaluator;
import org.json.simple.JSONObject;

import java.util.List;

public class IsAllowedToDelete extends BaseEvaluator {

    List permittedUsers;

    @Override
    public boolean evaluate(JSONObject jsonObject) {
        JSONObject node = (JSONObject) jsonObject.get("node");
        if (node != null) {
            String currentUser = this.getUserId();
            return permittedUsers.contains(currentUser);
        }
        return false;
    }

    public void setPermittedUsers(List permittedUsers) {
        this.permittedUsers = permittedUsers;
    }
}