package com.nexperia.alfresco.evaluator;

import org.alfresco.web.evaluator.BaseEvaluator;
import org.json.simple.JSONObject;

// We cannot pass through all chain of parents here.

public class IsFolderConvertableEvaluator extends BaseEvaluator {

    @Override
    public boolean evaluate(JSONObject jsonObject) {
        String type = this.getNodeType(jsonObject);
        if ("draw:drawingSpace".equals(type)) {
            return false;
        } else {
            return checkParents(jsonObject);
        }
    }

    private boolean checkParents(JSONObject jsonObject) {
        JSONObject parent = (JSONObject) jsonObject.get("parent");
        String name = (String) this.getProperty(parent, "cm:name");
        String type = this.getNodeType(parent);
        if ("draw:drawingSpace".equals(type)) {
            return false;
        } else if (name.equals("documentLibrary")) {
            return true;
        } else {
            return checkParents(parent);
        }
    }

}
