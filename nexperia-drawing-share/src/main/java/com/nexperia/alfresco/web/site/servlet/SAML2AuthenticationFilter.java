package com.nexperia.alfresco.web.site.servlet;

import org.alfresco.error.AlfrescoRuntimeException;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.IOException;
import java.util.*;
import java.util.function.Consumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.*;
import javax.servlet.http.*;

public class SAML2AuthenticationFilter implements Filter {

    public static final Logger logger = LoggerFactory.getLogger(SAML2AuthenticationFilter.class);

    // SAML2 SSO module context name
    private static final String SAML_CONTEXT = "alfresco-saml2-sso";
    private static final String PROXY_HEADER = "proxy_s";
    private static final String SAML_DATE_PREFIX = "sdt";
    private static final String FIRST_NAME_PREFIX = "fn";
    private static final String LAST_NAME_PREFIX = "ln";
    private static final String NICK_NAME_PREFIX = "nn";
    private static final String GROUPS_PREFIX = "gs";

    private String userCookieName;
    private String cookiePath;
    // Used for IDP initiated SSO
    private String idpLoginURL;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        this.userCookieName = filterConfig.getInitParameter("userCookieName");
        this.cookiePath = filterConfig.getInitParameter("cookiePath");
        this.idpLoginURL = filterConfig.getInitParameter("idpLoginURL");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        HttpServletResponse httpServletResponse = (HttpServletResponse) response;

        HttpUtils.MutableHttpServletRequest requestWrapper = HttpUtils.wrapHttpRequest(httpServletRequest);

        if (requestedUriIsUsedForWebScript(requestWrapper)) {
            chainDoFilterWithRequestWrapper(chain, httpServletResponse, requestWrapper, mutableHttpServletRequest -> {});
            return;
        }

        if (userAlreadyHasSession(requestWrapper)) {
            chainDoFilterWithRequestWrapper(chain, httpServletResponse, requestWrapper, mutableHttpServletRequest ->
                    mutableHttpServletRequest.putHeader(PROXY_HEADER, (String) mutableHttpServletRequest.getSession().getAttribute(UserCredentials.EMAIL.attribute)));
            return;
        }

        if (userNameIsValid(CookieUtils.getValue(userCookieName, requestWrapper)) &&
                samlDateIsValid(CookieUtils.getValue(userCookieName + SAML_DATE_PREFIX, requestWrapper))) {
            populateSessionAttributes(requestWrapper);
            chainDoFilterWithRequestWrapper(chain, httpServletResponse, requestWrapper,
                    mutableHttpServletRequest -> {
                        mutableHttpServletRequest.putHeader(PROXY_HEADER, Decryptor.decrypt(CookieUtils.getValue(userCookieName, mutableHttpServletRequest)));
                        removeUnnecessaryCookies(httpServletResponse, requestWrapper);
                    }
            );
        } else {
            if (StringUtils.isNotEmpty(idpLoginURL)) {
                redirect(httpServletResponse, () -> idpLoginURL);
            } else {
                redirect(httpServletResponse, () -> {
                    StringBuffer redirectUrl = requestWrapper.getRequestURL();
                    if (StringUtils.isNotEmpty(requestWrapper.getQueryString())) {
                        redirectUrl.append('?').append(requestWrapper.getQueryString());
                    }
                    return HttpUtils.getApplicationURL(requestWrapper, SAML_CONTEXT) + "?redirectTo=" + redirectUrl.toString();
                });
            }
        }
    }

    private void chainDoFilterWithRequestWrapper(FilterChain chain, HttpServletResponse httpServletResponse, HttpUtils.MutableHttpServletRequest requestWrapper,
                                                 Consumer<HttpUtils.MutableHttpServletRequest> consumer) throws IOException, ServletException {
        consumer.accept(requestWrapper);
        chain.doFilter(requestWrapper, httpServletResponse);
    }

    private void populateSessionAttributes(HttpServletRequest httpServletRequest) {
        HttpSession session = httpServletRequest.getSession();
        session.setAttribute(UserCredentials.EMAIL.attribute, Decryptor.decrypt(CookieUtils.getValue(userCookieName, httpServletRequest)));
        session.setAttribute(UserCredentials.FIRST_LOGIN.attribute, "true");
        session.setAttribute(UserCredentials.FIRST_NAME.attribute, Decryptor.decrypt(CookieUtils.getValue(userCookieName + FIRST_NAME_PREFIX, httpServletRequest)));
        session.setAttribute(UserCredentials.LAST_NAME.attribute, Decryptor.decrypt(CookieUtils.getValue(userCookieName + LAST_NAME_PREFIX, httpServletRequest)));
        session.setAttribute(UserCredentials.NICK_NAME.attribute, Decryptor.decrypt(CookieUtils.getValue(userCookieName + NICK_NAME_PREFIX, httpServletRequest)));
        session.setAttribute(UserCredentials.GROUPS.attribute, Decryptor.decrypt(CookieUtils.getValue(userCookieName + GROUPS_PREFIX, httpServletRequest)));
    }

    private void removeUnnecessaryCookies(HttpServletResponse httpServletResponse, HttpServletRequest httpServletRequest) {
        CookieUtils.removeAll(httpServletResponse, httpServletRequest, cookiePath,
                Arrays.asList(userCookieName,
                        userCookieName + FIRST_NAME_PREFIX,
                        userCookieName + LAST_NAME_PREFIX,
                        userCookieName + NICK_NAME_PREFIX,
                        userCookieName + GROUPS_PREFIX,
                        userCookieName + SAML_DATE_PREFIX));
    }

    private boolean requestedUriIsUsedForWebScript(HttpServletRequest httpServletRequest) {
        String requestURI = httpServletRequest.getRequestURI();
        return requestURI.contains("/s/") || requestURI.contains("/wcs/");
    }

    private boolean userAlreadyHasSession(HttpServletRequest httpServletRequest) {
        return StringUtils.isNotEmpty((String) httpServletRequest.getSession().getAttribute(UserCredentials.EMAIL.attribute));
    }

    private boolean userNameIsValid(String name) {
        if (StringUtils.isEmpty(name)) {
            return false;
        }
        if (!(new SimpleEmailValidator()).isValidEmail(Decryptor.decrypt(name))) {
            return false;
        }
        return true;
    }

    /**
     * Checks whether the SAML date is valid
     *
     * @param value - encrypted value of the SAML timestamp in reverse order
     * @return true if the value is valid date and the difference between SAML date and current date less than 1 second
     */
    private boolean samlDateIsValid(String value) {

        if (StringUtils.isEmpty(value)) {
            return false;
        }

        String samlDateString = new StringBuilder(Decryptor.decrypt(value)).reverse().toString();

        Long samlDate;
        try {
            samlDate = Long.valueOf(samlDateString);
        } catch (NumberFormatException e) {
            return false;
        }

        long diff = Math.abs(new Date().getTime() - samlDate) / 1000;

        if (diff > 1) {
            return false;
        }

        return true;
    }

    @FunctionalInterface
    interface Redirectable {
        String redirectTo();
    }

    private static void redirect(HttpServletResponse response, Redirectable r) throws IOException {
        response.sendRedirect(r.redirectTo());
    }

    private static class HttpUtils {
        static final class MutableHttpServletRequest extends HttpServletRequestWrapper {
            private final Map<String, String> customHeaders;

            public MutableHttpServletRequest(HttpServletRequest request) {
                super(request);
                this.customHeaders = new HashMap<>();
            }

            public void putHeader(String name, String value) {
                this.customHeaders.put(name, value);
            }

            public String getHeader(String name) {
                String headerValue = customHeaders.get(name);

                if (headerValue != null) {
                    return headerValue;
                }

                return ((HttpServletRequest) getRequest()).getHeader(name);
            }

            public Enumeration<String> getHeaderNames() {
                Set<String> set = new HashSet<String>(customHeaders.keySet());

                @SuppressWarnings("unchecked")
                Enumeration<String> e = ((HttpServletRequest) getRequest()).getHeaderNames();
                while (e.hasMoreElements()) {
                    String n = e.nextElement();
                    set.add(n);
                }

                return Collections.enumeration(set);
            }

        }

        static MutableHttpServletRequest wrapHttpRequest(HttpServletRequest request) {
            return new MutableHttpServletRequest(request);
        }

        static String getApplicationURL(HttpServletRequest request, String context) {
            StringBuffer url = new StringBuffer();
            int port = request.getServerPort();
            if (port < 0) {
                port = 80;
            }
            String scheme = request.getScheme();
            url.append(scheme);
            url.append("://");
            url.append(request.getServerName());
            if (("http".equals(scheme) && (port != 80)) || ("https".equals(scheme) && (port != 443))) {
                url.append(':');
                url.append(port);
            }
            return url.toString() + "/" + context;
        }

    }

    private static class CookieUtils {

        static String getValue(String name, HttpServletRequest request) {
            Cookie[] cookies = request.getCookies();
            if (cookies != null) {
                for (Cookie cookie : cookies) {
                    if (StringUtils.equals(cookie.getName(), name)) {
                        return cookie.getValue();
                    }
                }
            }
            return StringUtils.EMPTY;
        }

        static void removeAll(HttpServletResponse response, HttpServletRequest request, String cookiePath, List<String> names) {
            if (names == null || names.isEmpty()) {
                return;
            }
            Cookie[] cookies = request.getCookies();
            if (cookies != null) {
                for (Cookie cookie : cookies) {
                    if (cookie != null && names.contains(cookie.getName()))
                        remove(cookie, response, cookiePath);
                }
            }
        }

        private static void remove(Cookie cookie, HttpServletResponse response, String cookiePath) {
            cookie.setPath(cookiePath);
            cookie.setMaxAge(0);
            cookie.setValue(null);
            response.addCookie(cookie);
        }
    }

    private static class Decryptor {

        /**
         * Uses hardcoded key and vector for decryption
         *
         * @param toDecrypt - string to decrypt
         * @return decrypted and Base64 decoded string value
         */
        static String decrypt(String toDecrypt) {
            try {
                byte[] key = {2, 0, 1, 7, 0, 3, 1, 7, 2, 0, 1, 7, 0, 3, 1, 7};
                byte[] vector = {0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1};

                IvParameterSpec iv = new IvParameterSpec(vector);
                SecretKeySpec skeySpec = new SecretKeySpec(key, "AES");

                Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
                cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);

                byte[] original = cipher.doFinal(Base64.decodeBase64(toDecrypt.getBytes("UTF-8")));
                return new String(original);
            } catch (Exception e) {
                throw new AlfrescoRuntimeException(e.getLocalizedMessage());
            }
        }
    }

    private static class SimpleEmailValidator {

        private Pattern pattern;
        private Matcher matcher;

        private static final String EMAIL_PATTERN =
                "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                        + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        public SimpleEmailValidator() {
            pattern = Pattern.compile(EMAIL_PATTERN);
        }

        /**
         * Validate hex with regular expression
         *
         * @param hex hex for validation
         * @return true valid hex, false invalid hex
         */
        public boolean isValidEmail(final String hex) {
            matcher = pattern.matcher(hex);
            return matcher.matches();
        }
    }

    @Override
    public void destroy() {

    }

}
