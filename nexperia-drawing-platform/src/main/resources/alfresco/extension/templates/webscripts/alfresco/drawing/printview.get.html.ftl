<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Print View</title>
    <style>
        table.iksweb{text-decoration: none;border-collapse:collapse;width:100%;text-align:center;}
        table.iksweb th{font-weight:500;font-size:15px; color:#036F7B;background-color:#e9ecee;}
        table.iksweb td{font-size:14px;color:black;}
        table.iksweb td,table.iksweb th{padding:5px 1px;line-height:14px;vertical-align: middle;border: 1px solid #354251;}	table.iksweb tr:hover{background-color:#f9fafb}
        table.iksweb tr:hover td{color:black;cursor:default;}
    </style>
    <link rel="icon" href="/share/res/favicon.ico" type="image/vnd.microsoft.icon" />
    <script src=https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js></script>
    <script src=https://www.kryogenix.org/code/browser/sorttable/sorttable.js></script>
</head>
<body>

    <table class="iksweb sortable" >
        <thead>
            <tr>
                <th>Preview</th>
                <th>DrawingId (${drawings?size})</th>
                <th>Descriptive title</th>
                <th>Drawing type</th>
                <th>Primary format</th>
                <th>Drawing version</th>
                <th>Created for</th>
                <th>Security status</th>
                <th>MAG code</th>
                <th>Requester</th>
                <th>Last status changed date</th>
                <th>Last modified date</th>
                <th>Delivery date</th>
                <th>Required renditions</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody>
            <#list drawings as drawing >
                <tr>
                    <td><a href="${drawing.baseUrl + "/share/proxy/alfresco/drawing/preview?drawingId=" + drawing.drawingId?js_string}" target="_blank"><img src="${drawing.baseUrl + "/share/proxy/alfresco/drawing/preview?drawingId=" + drawing.drawingId?js_string}" width="200" height="200"/></a></td>
                    <td>${(drawing.drawingId?js_string)!}</td>
                    <td>${(drawing.descriptiveTitle?js_string)!}</td>
                    <td>${(drawing.drawingType?js_string)!}</td>
                    <td>${(drawing.primaryFormat?js_string)!}</td>
                    <td>${(drawing.drawingVersion?js_string)!}</td>
                    <td>${(drawing.createdFor?js_string)!}</td>
                    <td>${(drawing.securityStatus?js_string)!}</td>
                    <td>${(drawing.magCode?js_string)!}</td>
                    <td>${(drawing.requester?js_string)!}</td>
                    <td>${(drawing.lastStatusChangedDate?js_string)!}</td>
                    <td>${(drawing.modified?js_string)!}</td>
                    <td>${(drawing.deliveryDate?js_string)!}</td>
                    <td>${(drawing.requiredRenditions?js_string)!}</td>
                    <td>${(drawing.drawingStatus?js_string)!}</td>
                </tr>
            </#list>
        </tbody>
    </table>

</body>
</html>