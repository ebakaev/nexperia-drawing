<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Metadata update result</title>
    <style>
        table.iksweb{text-decoration: none;border-collapse:collapse;width:100%;text-align:center;}
        table.iksweb th{font-weight:500;font-size:15px; color:#036F7B;background-color:#e9ecee;}
        table.iksweb td{font-size:14px;color:black;}
        table.iksweb td,table.iksweb th{padding:8px 5px;line-height:14px;vertical-align: middle;border: 1px solid #354251;}	table.iksweb tr:hover{background-color:#f9fafb}
        table.iksweb tr:hover td{color:black;cursor:default;}
    </style>
    <link rel="icon" href="/share/res/favicon.ico" type="image/vnd.microsoft.icon" />
</head>
<body>
    <table class="iksweb">
        <thead>
            <tr>
                <th>DrawingId</th>
                <th>Descriptive title</th>
                <th>Drawing type</th>
                <th>Primary format</th>
                <th>Drawing version</th>
                <th>Created for</th>
                <th>Security status</th>
                <th>MAG code</th>
                <th>Requester</th>
                <th>Delivery date</th>
                <th>Required renditions</th>
            </tr>
        </thead>
        <tbody>
            <#list drawings as drawing>
                <tr <#if drawing.success == 'false'>bgcolor="#e0370f"</#if>>
                    <td>${(drawing.drawingId?js_string)!}</td>
                    <td>${(drawing.descriptiveTitle?js_string)!}</td>
                    <td>${(drawing.drawingType?js_string)!}</td>
                    <td>${(drawing.primaryFormat?js_string)!}</td>
                    <td>${(drawing.drawingVersion?js_string)!}</td>
                    <td>${(drawing.createdFor?js_string)!}</td>
                    <td>${(drawing.securityStatus?js_string)!}</td>
                    <td>${(drawing.magCode?js_string)!}</td>
                    <td>${(drawing.requester?js_string)!}</td>
                    <td>${(drawing.deliveryDate?js_string)!}</td>
                    <td>${(drawing.requiredRenditions?js_string)!}</td>
                </tr>
            </#list>
        </tbody>
    </table>
    </br>
    <button type="button" onclick="javascript:history.back()">Back update page</button>
    </br>
    </br>
    </br>
    <#if failed?has_content >
        <b>Failed</b> Drawing IDs to search: <b>${(failed?js_string)!}</b>
        <br/>
    </#if>
    -------------------------------------
    </br>
    <#if success?has_content >
        <b>Successfully</b> updated Drawing IDs to search: <b>${(success?js_string)!}</b>
    </#if>

</body>
</html>