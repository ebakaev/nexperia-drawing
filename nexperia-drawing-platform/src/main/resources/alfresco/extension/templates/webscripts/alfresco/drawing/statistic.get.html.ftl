<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8"/>
    <title>Charts!</title>
    <link href="http://localhost:8180/share/res/nexperia-drawing-share/components/dashlets/statistic/css/basic.css" type="text/css" rel="stylesheet"/>
    <link href="http://localhost:8180/share/res/nexperia-drawing-share/components/dashlets/statistic/css/visualize.css" type="text/css" rel="stylesheet"/>
    <link href="http://localhost:8180/share/res/nexperia-drawing-share/components/dashlets/statistic/css/visualize-light.css" type="text/css" rel="stylesheet"/>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
    <script type="text/javascript" src="http://localhost:8180/share/res/nexperia-drawing-share/components/dashlets/statistic/js/visualize.jQuery.js"></script>
    <script type="text/javascript">
        $(function(){
            $('table').visualize({type: 'bar'});
        });
    </script>
</head>
<body>

<table <#--style="display:none"-->>
    <caption>Visits from August 16 to August 21</caption>
    <thead>
    <tr>
        <td></td>
        <th scope="col">DDD</th>
        <th scope="col">Tuesday</th>
        <th scope="col">Wednesday</th>
        <th scope="col">Thursday</th>
        <th scope="col">Friday</th>
        <th scope="col">Saturday</th>
        <th scope="col">Sunday</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <th scope="row">CatsWhoCode.com</th>
        <td>12541</td>
        <td>11204</td>
        <td>11354</td>
        <td>10058</td>
        <td>9871</td>
        <td>8254</td>
        <td>5477</td>
    </tr>
    <tr>
        <th scope="row">WpRecipes.com</th>
        <td>9855</td>
        <td>8870</td>
        <td>8731</td>
        <td>7488</td>
        <td>8159</td>
        <td>6547</td>
        <td>4512</td>
    </tr>
    <tr>
        <th scope="row">CatsWhoBlog.com</th>
        <td>3241</td>
        <td>2544</td>
        <td>2597</td>
        <td>3108</td>
        <td>2114</td>
        <td>2045</td>
        <td>950</td>
    </tr>
    </tbody>
</table>

</body>
</html>