<import resource="classpath:/alfresco/templates/webscripts/org/alfresco/slingshot/search/search.lib.js">
function main()
{
    var searchTerm, i;
    var searchTerms = args.term.split(",")
    var searchTerm = "(";
    for (i = 0; i < searchTerms.length; i++) {
      if (i!=0) {
      	searchTerm += " OR ";
      }
      searchTerm += "@cm\\:name:'" + searchTerms[i] + "'";
    }
    searchTerm += ") AND TYPE:'draw:drawingSpace'";

    var magCode = args.magCode
    if (magCode) {
        searchTerm += " AND @draw\\:magCode:'" + magCode + "'";
    }

    var primaryFormat = args.primaryFormat
    if (primaryFormat) {
        searchTerm += " AND @draw\\:primaryFormat:'" + primaryFormat + "'";
    }

    var descriptiveTitle = args.descriptiveTitle
    if (descriptiveTitle) {
        searchTerm += " AND @draw\\:descriptiveTitle:'" + descriptiveTitle + "'";
    }

    var drawingType = args.drawingType
    if (drawingType) {
        searchTerm += " AND @draw\\:drawingType:'" + drawingType + "'";
    }

    var createdFor = args.createdFor
    if (createdFor) {
        searchTerm += " AND @draw\\:createdFor:'" + createdFor + "'";
    }

    var securityStatus = args.securityStatus
    if (securityStatus) {
        searchTerm += " AND @draw\\:securityStatus:'" + securityStatus + "'";
    }

    var requester = args.requester
    if (requester) {
        searchTerm += " AND @draw\\:requester:'" + requester + "'";
    }

    var deliveryDate = args.deliveryDate
    if (deliveryDate) {
        searchTerm += " AND @draw\\:deliveryDate:['" + deliveryDate + "' TO '" + deliveryDate + "']";
    }

    var modificationFromDate = args.modificationFromDate;
    var modificationToDate = args.modificationToDate;
    logger.log("modificationFromDate: " + modificationFromDate);
    logger.log("modificationToDate: " + modificationToDate);

    if ((modificationFromDate || modificationToDate) && modificationFromDate !== "undefined" && modificationToDate !== "undefined") {
        if (modificationFromDate != null && (modificationToDate == null || modificationToDate == '' || modificationToDate.length < 8) ) {
            searchTerm += " AND @cm\\:modified:['" + modificationFromDate + "' TO NOW]";
            logger.log("Appending only from ...");
        } else if ((modificationFromDate == null  || modificationFromDate == '' || modificationFromDate.length < 8) && modificationToDate != null) {
            searchTerm += " AND @cm\\:modified:['1970-01-01' TO '" + modificationToDate + "']";
            logger.log("Appending only to ...");
        } else {
            searchTerm += " AND @cm\\:modified:['" + modificationFromDate + "' TO '" + modificationToDate + "']";
            logger.log("Appending both ...");
        }
    }

    var requiredRenditions = args.requiredRenditions
    if (requiredRenditions) {
        searchTerm += " AND @draw\\:requiredRenditions:'" + requiredRenditions + "'";
    }

    var version = args.version
    if (version) {
        searchTerm += " AND @draw\\:version:'" + version + "'";
    }

    var drawingStatus = args.drawStatus
    if (drawingStatus) {
        searchTerm += " AND @draw\\:drawingStatus:'" + drawingStatus + "'";
    }

    var creator = args.creator
    if (creator) {
        searchTerm += " AND @cm\\:creator:'" + creator + "'";
    }

    var isArchive = args.archive;
    if (isArchive) {
        if (isArchive === "no") {
            searchTerm += " AND NOT PATH:'/app:company_home/st:sites/cm:tdm-drawing/cm:documentLibrary/cm:Zzz_Archive/*'"
        }
    }

    logger.log("SEARCH TERM: " + searchTerm);

    var params =
    {
        siteId: args.site,
        containerId: args.container,
        repo: (args.repo !== null) ? (args.repo == "true") : false,
        term: searchTerm,
        tag: args.tag,
        query: args.query,
//        query: query,
        rootNode: args.rootNode,
        sort: args.sort,
        maxResults: (args.maxResults !== null) ? parseInt(args.maxResults, 10) : DEFAULT_MAX_RESULTS,
        pageSize: (args.pageSize !== null) ? parseInt(args.pageSize, 10) : DEFAULT_PAGE_SIZE,
        startIndex: (args.startIndex !== null) ? parseInt(args.startIndex, 10) : 0,
        facetFields: args.facetFields,
        filters: args.filters,
        encodedFilters: args.encodedFilters,
        spell: (args.spellcheck !== null) ? (args.spellcheck == "true") : false
    };

    model.data = getSearchResults(params);
}

main();