<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Metadata update</title>
    <style>
        table.iksweb{text-decoration: none;border-collapse:collapse;width:100%;text-align:center;}
        table.iksweb th{font-weight:500;font-size:15px; color:#036F7B;background-color:#e9ecee;}
        table.iksweb td{font-size:14px;color:black;}
        table.iksweb td,table.iksweb th{padding:5px 1px;line-height:14px;vertical-align: middle;border: 1px solid #354251;}	table.iksweb tr:hover{background-color:#f9fafb}
        table.iksweb tr:hover td{color:black;cursor:default;}
    </style>
    <link rel="icon" href="/share/res/favicon.ico" type="image/vnd.microsoft.icon" />
    <script src=https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js></script>
    <script src=https://www.kryogenix.org/code/browser/sorttable/sorttable.js></script>

</head>
<body>

<form method="post" action="metadataupdate.html" accept-charset="utf-8">
    <table class="iksweb sortable">
        <thead>
            <tr>
                <th>Preview</th>
                <th>DrawingId (${drawings?size})</th>
                <th>Descriptive title</th>
                <th>Drawing type</th>
                <th>Primary format</th>
                <th>Drawing version</th>
                <th>Created for</th>
                <th>Security status</th>
                <th>MAG code</th>
                <th>Requester</th>
                <th>Delivery date</th>
                <th>Required renditions</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody>
            <#list drawings as drawing>
                <tr>
                    <td><a href="${drawing.baseUrl + "/share/proxy/alfresco/drawing/preview?drawingId=" + drawing.drawingId?js_string}" target="_blank"><img src="${drawing.baseUrl + "/share/proxy/alfresco/drawing/preview?drawingId=" + drawing.drawingId?js_string}" width="50" height="50"/></a></td>
                    <td>${(drawing.drawingId?js_string)!}</td>
                    <td style="width: 600px;">
                        <textarea id="${(drawing.nodeRef?js_string)!}_desctitle" name="${(drawing.nodeRef?js_string)!}_desctitle" style="width: fit-content;width: 550px;">${(drawing.descriptiveTitle?js_string)!}</textarea>
                    </td>
                    <td>${(drawing.drawingType?js_string)!}</td>
                    <td>${(drawing.primaryFormat?js_string)!}</td>
                    <td>${(drawing.drawingVersion?js_string)!}</td>
                    <td><input type="text" id="${(drawing.nodeRef?js_string)!}_cf" name="${(drawing.nodeRef?js_string)!}_cf" value="${(drawing.createdFor?js_string)!}"></td>
                    <td>
                        <select id="${(drawing.nodeRef?js_string)!}_ss" name="${(drawing.nodeRef?js_string)!}_ss">
                            <option value="Company Public" <#if drawing.securityStatus == 'Company Public'>selected</#if>>Company Public</option>
                            <option value="Company Secret" <#if drawing.securityStatus == 'Company Secret'>selected</#if>>Company Secret</option>
                            <option value="Company Internal" <#if drawing.securityStatus == 'Company Internal'>selected</#if>>Company Internal</option>
                            <option value="Company Confidential" <#if drawing.securityStatus == 'Company Confidential'>selected</#if>>Company Confidential</option>
                        </select>
                    <td>
                        <select id="${(drawing.nodeRef?js_string)!}_mc" name="${(drawing.nodeRef?js_string)!}_mc">
                            <option value="R03" <#if drawing.magCode == 'R03'>selected</#if>>R03</option>
                            <option value="R04" <#if drawing.magCode == 'R04'>selected</#if>>R04</option>
                            <option value="R07" <#if drawing.magCode == 'R07'>selected</#if>>R07</option>
                            <option value="R17" <#if drawing.magCode == 'R17'>selected</#if>>R17</option>
                            <option value="R18" <#if drawing.magCode == 'R18'>selected</#if>>R18</option>
                            <option value="R19" <#if drawing.magCode == 'R19'>selected</#if>>R19</option>
                            <option value="R33" <#if drawing.magCode == 'R33'>selected</#if>>R33</option>
                            <option value="R34" <#if drawing.magCode == 'R34'>selected</#if>>R34</option>
                            <option value="R71" <#if drawing.magCode == 'R71'>selected</#if>>R71</option>
                            <option value="R73" <#if drawing.magCode == 'R73'>selected</#if>>R73</option>
                            <option value="RE1" <#if drawing.magCode == 'RE1'>selected</#if>>RE1</option>
                            <option value="RE2" <#if drawing.magCode == 'RE2'>selected</#if>>RE2</option>
                            <option value="RP3" <#if drawing.magCode == 'RP3'>selected</#if>>RP3</option>
                            <option value="RP8" <#if drawing.magCode == 'RP8'>selected</#if>>RP8</option>
                            <option value="RTC" <#if drawing.magCode == 'RTC'>selected</#if>>RTC</option>
                            <option value="RY7" <#if drawing.magCode == 'RY7'>selected</#if>>RY7</option>
                        </select>
                    </td>
                    <td><input type="text" id="${(drawing.nodeRef?js_string)!}_req" name="${(drawing.nodeRef?js_string)!}_req" value="${(drawing.requester?js_string)!}"></td>
                    <td>${(drawing.deliveryDate?js_string)!}</td>
                    <td><input type="text" id="${(drawing.nodeRef?js_string)!}_rr" name="${(drawing.nodeRef?js_string)!}_rr" value="${(drawing.requiredRenditions?js_string)!}"></td>
                    <td>${(drawing.drawingStatus?js_string)!}</td>
                </tr>
            </#list>
        </tbody>
    </table>
    <br/>
    <button name="name" value="value" type="submit" onClick="this.form.submit(); this.disabled=true; this.innerHtml='Saving…';">Apply changes</button>
    <br/>
</form>

</body>
</html>