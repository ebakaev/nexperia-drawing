<span style="font-family: Tahoma, Arial, Helvetica, sans-serif;font-size: 12px;">
Dear ${userFullName}, <br/><br/>

 The report has been successfully generated. Please find it in the attachment.<br/>
 Also you can find it in Alfresco Drawing <a target="_blank" href="https://www.drawing-dam.pim.nexperia.com/share/page/site/tdm-drawing/documentlibrary#filter=path%7C%2FReports%7C&page=1">Reports folder</a>
    <br/><br/>

With kind regards<br/>
Alfresco Drawing team.