<span style="font-family: Tahoma, Arial, Helvetica, sans-serif;font-size: 12px;">
Dear ${userFullNames}, <br/><br/>

The drawing job <a target="_blank" href="${drawingLink}">${drawingName}</a> is pending approval.<br/><br/>

Task Performer: ${performer}
<br/>
<#if comment?has_content && comment?trim?has_content>
The last task performer added the following comment for you:
<br/>
 - ${comment!""}
<br/><br/>
</#if>