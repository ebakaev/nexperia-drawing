package com.nexperia.alfresco.drawing.remote.impl;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import com.nexperia.alfresco.drawing.remote.client.AuthenticatingClient;
import com.nexperia.alfresco.drawing.remote.client.Response;
import com.nexperia.alfresco.drawing.remote.exception.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.springframework.beans.BeansException;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.extensions.surf.exception.ConnectorServiceException;
import org.springframework.extensions.surf.util.URLEncoder;
import org.springframework.extensions.webscripts.connector.*;

import javax.servlet.http.HttpServletResponse;

/**
 * Implementation of authenticating webscript client.
 * 
 * Connector configurations are made within Remote configuration section
 * of a configuration file. This class instantiates a Spring-configured
 * {@link ConnectorService} to create connectors.
 * 
 * Custom configuration of connectors can be put to a file
 * alfresco/extension/remote-publication-config.xml file in the classpath.
 * 
 * @author byaminov
 *
 */
public abstract class AuthenticatingClientImpl implements AuthenticatingClient {
	
	private static final Log logger = LogFactory.getLog(AuthenticatingClientImpl.class);

	public static final String DEFAULT_APPLICATION_CONTEXT_LOCATION = 
		"classpath:META-INF/remote-publication-context.xml";
	public static final String DEFAULT_ENDPOINT_ID = "alfresco";

	protected Connector connector;
	protected HttpMethod httpMethod = HttpMethod.POST;
	
	private List<String> contentTypesToDebug = Arrays.asList(new String[] {
			"text/plain", "text/html", "application/json"});
	private int numberOfCharactersToDebug = 1100;

	/**
	 * Creates webscript client which connects to
	 * remote webscript using given connector.
	 * 
	 * This is the most inner constructor.
	 * @param connector connector to use.
	 * @param username username to use. Set to null if a declared endpoint is used.
	 * @param password password to use. Set to null if a declared endpoint is used.
	 */
	public AuthenticatingClientImpl(Connector connector, String username, String password) {
		this.connector = connector;
		setCredentials(username, password);
		init();
	}

	/**
	 * Creates webscript client using default location of
	 * spring context XML and endpoint id.
	 * @throws BeansException thrown in case ConnectorService could not
	 * be instantiated using the spring config.
	 * @throws ConnectorServiceException thrown in case connector could not
	 * be created by ConnectorService.
	 */
	public AuthenticatingClientImpl() throws BeansException, ConnectorServiceException {
		this(DEFAULT_ENDPOINT_ID, null, null);
	}

	/**
	 * Creates webscript client which will use given endpoint id to
	 * get connector from ConnectorService.
	 * @param endpointId endpoint id to use.
	 * @param username username to use. Set to null if a declared endpoint is used.
	 * @param password password to use. Set to null if a declared endpoint is used.
	 * @throws BeansException thrown in case ConnectorService could not
	 * be instantiated using the spring config.
	 * @throws ConnectorServiceException thrown in case connector could not
	 * be created by ConnectorService.
	 */
	public AuthenticatingClientImpl(String endpointId, String username, String password) 
			throws BeansException, ConnectorServiceException {
		this(new String[] {DEFAULT_APPLICATION_CONTEXT_LOCATION}, endpointId, username, password);
	}

	/**
	 * Creates webscript client by given path to spring configuration
	 * files and endpoint id to use to get connector.
	 * @param springContextPaths paths to spring configuration files where
	 * ConnectorService can be retrieved (bean id="connector.service").
	 * @param endpointId endpoint id to use to get connector from ConnectorService.
	 * @param username username to use. Set to null if a declared endpoint is used.
	 * @param password password to use. Set to null if a declared endpoint is used.
	 * @throws BeansException thrown in case ConnectorService could not
	 * be instantiated using the spring config.
	 * @throws ConnectorServiceException thrown in case connector could not
	 * be created by ConnectorService.
	 */
	public AuthenticatingClientImpl(String[] springContextPaths, String endpointId, 
			String username, String password) throws BeansException, ConnectorServiceException {
		this((ConnectorService) new ClassPathXmlApplicationContext(springContextPaths).
				getBean("connector.service"), endpointId, username, password);
	}

	/**
	 * Creates webscript client by given ConnectorService and endpoint
	 * id. This constructor just gets Connector from connector service and
	 * invokes {@link #AuthenticatingClientImpl(Connector, String, String)}.
	 * @param connectorService connector service.
	 * @param endpointId endpoint id to use.
	 * @param username username to use. Set to null if a declared endpoint is used.
	 * @param password password to use. Set to null if a declared endpoint is used.
	 * @throws ConnectorServiceException thrown in case Connector could not
	 * be retrieved from ConnectorService.
	 */
	public AuthenticatingClientImpl(ConnectorService connectorService, String endpointId,
									String username, String password) throws ConnectorServiceException {
		this(connectorService.getConnector(endpointId), username, password);
	}

	/**
	 * @see AuthenticatingClient#setCredentials(String, String)
	 */
	public void setCredentials(String username, String password) {
		if (username != null) {
			Credentials credentials = getConnector().getCredentials();
			if (credentials == null) {
				credentials = new CredentialsImpl(connector.getEndpoint());
			}
			credentials.setProperty(Credentials.CREDENTIAL_USERNAME, username);
			if (password != null) {
				credentials.setProperty(Credentials.CREDENTIAL_PASSWORD, password);
			} else {
				credentials.removeProperty(Credentials.CREDENTIAL_PASSWORD);
			}
			getConnector().setCredentials(credentials);
		}
	}
	
	/**
	 * Method called by every constructor of this class after basic initialization.
	 */
	protected void init() {
		// do nothing by default, can be extended in children classes
	}

	/**
	 * Make a request to remote webscript and parse JSON response.
	 * 
	 * @param url base webscript URL.
	 * @param content content of the request. Can be null.
	 * @param params request parameters.
	 * @return response object.
	 * @throws WebScriptClientException thrown in case connection is unavailable, authentication failed,
	 * response status was not 200, etc.
	 */
	protected Response callWebScript(String url, InputStream content, Map<String, String> params)
			throws WebScriptClientException {
		return callWebScript(url, content, params, false);
	}

    protected Response callWebScript(HttpMethod httpMethod, String url, InputStream content, Map<String, String> params,
									 boolean acceptOnlySuccessfulStatus) throws WebScriptClientException {
        try {
            if (logger.isDebugEnabled()) {
                logger.debug("Executing request to remote webscript. " +
                        "Request is: " + dumpRequest(url, params));
            }

            // Execute the request
            Response response = doRequest(httpMethod, url, content, params);

            if (logger.isDebugEnabled()) {
                StringBuffer sb = new StringBuffer("Got response, status [");
                sb.append(response.getStatus());
                if (StringUtils.isNotEmpty(response.getStatus().getMessage())) {
                    sb.append("], status message [");
                    sb.append(response.getStatus().getMessage());
                }
                if (response.getStatus().getException() != null) {
                    sb.append("], exception [");
                    sb.append(response.getStatus().getException());
                }
                sb.append("]");

                // Debug content if needed
                String contentType = response.getStatus().getHeaders().get("Content-Type");
                if (contentType != null) {
                    for (String type : contentTypesToDebug) {
                        if (contentType.startsWith(type)) {

                            // Output the content
                            String responseContent = response.getResponse();
                            if (responseContent != null) {
                                sb.append(", content:\n");
                                if (responseContent.length() <= numberOfCharactersToDebug) {
                                    sb.append(responseContent);
                                } else {
                                    sb.append(responseContent.substring(0, numberOfCharactersToDebug));
                                }
                            }

                            break;
                        }
                    }

                }

                logger.debug(sb.toString());
            }

            // Check that response status is one of parseable
            checkResponseStatus(response, acceptOnlySuccessfulStatus);

            return response;
        } catch (WebScriptClientException e) {
            logger.error("Remote webscript invocation failed. Request was: " + dumpRequest(url, params), e);
            throw e;
        }
    }
    
    protected Response doRequest(HttpMethod httpMethod, String url, InputStream content, 
    		Map<String, String> params) {
        ConnectorContext context = new ConnectorContext(httpMethod);
        org.springframework.extensions.webscripts.connector.Response res = connector.call(buildQuery(url, params),
                context, content);
        String data = res.getResponse();
        InputStream is = null;
        try {
        	is = res.getResponseStream();
        } catch (NullPointerException e) {
        	// If both "data" and "is" are null in Response, then NPE is thrown.
        	// We can avoid this only by catching the NPE.
        	logger.warn("Empty response received");
        }
        Response result = new Response(data, is, res.getStatus(), res.getEncoding());
        return result;
    }

	
	protected Response callWebScript(String url, InputStream content, Map<String, String> params,
			boolean acceptOnlySuccessfulStatus) throws WebScriptClientException {
        return callWebScript(httpMethod, url, content, params, acceptOnlySuccessfulStatus);
    }

	protected String dumpRequest(String url, Map<String, String> params) {
		StringBuffer sb = new StringBuffer();
		sb.append("\nURL: ").append(url);
		sb.append("\nParameters: ").append(params);
		return sb.toString();
	}

	/**
	 * Appends parameters to the request URL.
	 * @param basePath base request URL.
	 * @param args parameters to append.
	 * @return base URL with parameters appended.
	 */
	public String buildQuery(String basePath, Map<String, String> args) throws WebScriptClientException {
		return buildQuery(basePath, args, true);
	}
	
	/**
	 * Appends parameters to the request URL.
	 * @param basePath base request URL.
	 * @param args parameters to append.
	 * @param escapeArgs true if arguments should be escaped to represent a valid URL, e.g. 
	 * ':' replaced by '%3A' etc.
	 * @return base URL with parameters appended.
	 */
	protected String buildQuery(String basePath, Map<String, String> args, boolean escapeArgs) 
				throws WebScriptClientException {
		StringBuffer sb = new StringBuffer(basePath);
		if (args != null && args.size() != 0) {
			if (sb.indexOf("?") == -1) {
				sb.append('?');
			} else {
				sb.append('&');
			}
			Iterator<Map.Entry<String, String>> i = args.entrySet().iterator();
			while (i.hasNext()) {
				Map.Entry<String, String> entry = i.next();
				if (entry.getValue() == null) {
					throw new IllegalParametersException("Null value provided for property '" +
							entry.getKey() + "'");
				}
				sb.append(escapeArgs ? URLEncoder.encode(entry.getKey()) : entry.getKey());
				sb.append('=');
				sb.append(escapeArgs ? URLEncoder.encode(entry.getValue()) : entry.getValue());
				if (i.hasNext()) {
					sb.append('&');
				}
			}
		}
		return sb.toString();
	}

	private void checkResponseStatus(Response response, boolean acceptOnlySuccessfulStatus) 
			throws WebScriptClientException {
		int code = response.getStatus().getCode();
		
		Integer[] authenticationProblemCodes = new Integer[] {
				HttpServletResponse.SC_FORBIDDEN,
				HttpServletResponse.SC_UNAUTHORIZED
		};
		
		if (Arrays.asList(authenticationProblemCodes).contains(code)) {
			throw new AuthenticationFailedException(
					"Failed to invoke remote webscript:\n" + response.getResponse());
		}
		
		Integer[] connectionProblemCodes = new Integer[] {
				499,
				498
		};
		
		if (Arrays.asList(connectionProblemCodes).contains(code)) {
			ResponseStatus status = response.getStatus();
			throw new ConnectionException(
					"Connection problem occured with code " + code + 
					" and message: " + status.getMessage(), status.getException());
		}
		
		if (acceptOnlySuccessfulStatus) {
            checkSpecificStatuses(response);
			// Fail if status is not one of 2xx
			if (code / 100 != 2) {
                String message = response.getStatus().getMessage();
                if (message == null || message.isEmpty()) {
                    message = "HTTP Status code: " + response.getStatus().getCode() + " without message";
                }
				throw new WebScriptClientException("Error occured while invoking webscript: " +
                        message, response.getStatus().getException());
			}
		}
	}

    @SuppressWarnings("unused")
    protected void checkSpecificStatuses(Response response) {
        //nothing specific check in base class
    }
	/**
	 * Parses response as a JSON array of JSON objects where each field
	 * is a String or String array.
	 * @param response web script response
	 * @return list of maps from resulting JSON field names to their values.
	 * Each value is String or String[].
	 * @throws InvalidResponseException thrown in case if response
	 * could not be parsed as expected JSON array.
	 */
	protected List<Map<String, Object>> parseJSONArray(Response response) throws InvalidResponseException {
		JSONArray responseObj = (JSONArray) parseJSON(response, false);
		
		List<Map<String, Object>> result = new ArrayList<Map<String, Object>>(responseObj.length());
		try {
			for (int i = 0; i < responseObj.length(); i++) {
				JSONObject object = responseObj.getJSONObject(i);
				result.add(parseJSONObject(object));
			}
		} catch (JSONException e) {
			throw new InvalidResponseException("Could not parse response string as JSON. " +
					"Response string is:\n" + response.getResponse(), e);
		}
		
		return result;
	}
	
	/**
	 * Parses response as a JSON object where each field
	 * is a String or String array.
	 * @param response web script response
	 * @return map from resulting JSON field names to their values.
	 * Each value is String or String[].
	 * @throws InvalidResponseException thrown in case if response
	 * could not be parsed as expected JSON object.
	 */
	protected Map<String, Object> parseJSONObject(Response response) throws InvalidResponseException {
		Object responseObj = parseJSON(response, true);
		try {
			return parseJSONObject((JSONObject) responseObj);
		} catch (JSONException e) {
			throw new InvalidResponseException("Could not parse response string as JSON. " +
					"Response string is:\n" + response.getResponse(), e);
		}
	}
	
	private Object parseJSON(Response response, boolean expectJSONObject) throws InvalidResponseException {
		String responseString = response.getResponse();
		if (StringUtils.isEmpty(responseString)) {
			throw new InvalidResponseException("Response returned from remote webscript " +
					"webscript is empty");
		}
		Object responseObj;
		try {
			JSONTokener tokener = new JSONTokener(responseString);
			responseObj =  tokener.nextValue();
		} catch (JSONException e) {
			throw new InvalidResponseException("Could not parse response string as JSON. " +
					"Response string is:\n" + responseString, e);
		}
		if (responseObj == null) {
			throw new InvalidResponseException("Could not parse response string as JSON. " +
					"Response string is:\n" + responseString);
		}
		if (expectJSONObject) {
			if (!(responseObj instanceof JSONObject)) {
				throw new InvalidResponseException("Expected JSONObject as a root element of JSON, but found " + 
						responseObj.getClass().getName() + ". Response string is:\n" + responseString);
			}
		} else {
			if (!(responseObj instanceof JSONArray)) {
				throw new InvalidResponseException("Expected JSONArray as a root element of JSON, but found " + 
						responseObj.getClass().getName() + ". Response string is:\n" + responseString);
			}
		}
		return responseObj;
	}

	@SuppressWarnings("unchecked")
	private Map<String, Object> parseJSONObject(JSONObject json) throws JSONException {
		Map<String, Object> result = new HashMap<String, Object>();
		Iterator keysIterator = json.keys();
		while (keysIterator.hasNext()) {
			String key = (String) keysIterator.next();
			if (json.get(key) instanceof JSONArray) {
				JSONArray array = (JSONArray) json.get(key);
				String[] values = new String[array.length()];
				for (int i = 0; i < array.length(); i++) {
					values[i] = array.getString(i);
				}
				result.put(key, values);
			} else {
				// Consider it's a string
				result.put(key, json.getString(key));
			}
		}
		return result;
	}

	public Connector getConnector() {
		return this.connector;
	}
	
	public void setConnector(Connector connector) {
		this.connector = connector;
	}

	public void setHttpMethod(String httpMethod) {
		this.httpMethod = HttpMethod.valueOf(httpMethod);
	}
	
	public void setContentTypesToDebug(List<String> contentTypesToDebug) {
		this.contentTypesToDebug = contentTypesToDebug;
	}
	
	public void setNumberOfCharactersToDebug(int numberOfCharactersToDebug) {
		this.numberOfCharactersToDebug = numberOfCharactersToDebug;
	}
	
}
