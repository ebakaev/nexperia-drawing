package com.nexperia.alfresco.drawing.behaviour;

import com.nexperia.S3Client;
import com.nexperia.alfresco.drawing.model.DrawingModel;
import com.nexperia.alfresco.drawing.service.CreateRenditionService;
import com.nexperia.alfresco.drawing.service.PublishToWebService;
import com.nexperia.alfresco.drawing.utils.DrawingUtil;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.node.NodeServicePolicies;
import org.alfresco.repo.policy.Behaviour;
import org.alfresco.repo.policy.JavaBehaviour;
import org.alfresco.repo.policy.PolicyComponent;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class DrawingBehaviour implements NodeServicePolicies.OnUpdatePropertiesPolicy {

    public static final Logger logger = LoggerFactory.getLogger(DrawingBehaviour.class);

    CreateRenditionService createRenditionService;
    DrawingUtil drawingUtil;
    S3Client s3Client;
    PublishToWebService publishService;

    private final PolicyComponent policyComponent;
    private final ServiceRegistry serviceRegistry;

    public DrawingBehaviour(PolicyComponent policyComponent, ServiceRegistry serviceRegistry) {
        this.policyComponent = policyComponent;
        this.serviceRegistry = serviceRegistry;
    }

    public void init() {
        this.policyComponent.bindClassBehaviour(
                NodeServicePolicies.OnUpdatePropertiesPolicy.QNAME,
                DrawingModel.TYPE_DRAWING,
                new JavaBehaviour(this, NodeServicePolicies.OnUpdatePropertiesPolicy.QNAME.getLocalName(), Behaviour.NotificationFrequency.TRANSACTION_COMMIT));
    }

    @Override
    public void onUpdateProperties(NodeRef drawing, Map<QName, Serializable> before, Map<QName, Serializable> after) {
        if (notExists(drawing)) {
            logger.debug("Drawing onUpdateProperties: Node {} does not exist.", drawing);
            return;
        }
        if (isWorkingCopy(drawing)) {
            logger.debug("Drawing onUpdateProperties: Node {} is working copy.", drawing);
            return;
        }
        List<String> oldRequiredRenditions = (List<String>) before.get(DrawingModel.PROP_REQUIRED_RENDITIONS);
        List<String> newRequiredRenditions = (List<String>) after.get(DrawingModel.PROP_REQUIRED_RENDITIONS);

        if (oldRequiredRenditions != null && newRequiredRenditions != null
                && !Arrays.equals(oldRequiredRenditions.toArray(), newRequiredRenditions.toArray())) {
            List<String> addedFormats = newRequiredRenditions.stream()
                    .filter(element -> !oldRequiredRenditions.contains(element))
                    .collect(Collectors.toList());
            //To generate new renditions
            addedFormats.forEach(ext -> {
                        NodeRef existingRendition = drawingUtil.findExistingRendition(drawing, ext);
                        if (existingRendition == null) {
                            String primaryFormat = (String) serviceRegistry.getNodeService().getProperty(drawing, DrawingModel.PROP_PRIMARY_FORMAT);
                            NodeRef primaryFormatRendition = drawingUtil.findExistingRendition(drawing, primaryFormat.toLowerCase());
                            if (primaryFormatRendition != null) {
                                createRenditionService.createRendition(primaryFormatRendition, ext.toLowerCase());
                            }
                        }
                    }
            );
        }

        String oldSecurityStatus = (String) before.get(DrawingModel.PROP_SECURITY_STATUS);
        String newSecurityStatus = (String) after.get(DrawingModel.PROP_SECURITY_STATUS);

        if (DrawingModel.SECURITY_STATUS_COMPANY_PUBLIC.equals(oldSecurityStatus)
                && !DrawingModel.SECURITY_STATUS_COMPANY_PUBLIC.equals(newSecurityStatus)) {
            //Make all renditions obsolete.
            List<NodeRef> releasedRenditions = drawingUtil.getReadyToPublishRenditionsFromDrawing(drawing);
            releasedRenditions.forEach(rendition -> {
                s3Client.makeDocumentObsolete(drawingUtil.getName(rendition), drawingUtil.getType(rendition));
                publishService.makeRenditionObsolete(rendition);
            });
        }
    }

    private boolean isWorkingCopy(NodeRef nodeRef) {
        return serviceRegistry.getNodeService().hasAspect(nodeRef, ContentModel.ASPECT_WORKING_COPY);
    }

    private boolean notExists(NodeRef nodeRef) {
        return !serviceRegistry.getNodeService().exists(nodeRef);
    }

    public void setCreateRenditionService(CreateRenditionService createRenditionService) {
        this.createRenditionService = createRenditionService;
    }

    public void setDrawingUtil(DrawingUtil drawingUtil) {
        this.drawingUtil = drawingUtil;
    }

    public void setS3Client(S3Client s3Client) {
        this.s3Client = s3Client;
    }

    public void setPublishService(PublishToWebService publishService) {
        this.publishService = publishService;
    }
}
