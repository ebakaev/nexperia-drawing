package com.nexperia.alfresco.drawing.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class DrawingSearchDto {
	private String name;
	private String drawingType;
	private String primaryFormat;
	private String drawingVersion;
	private String createdFor;
	private String securityStatus;
	private String magCode;
	private String requester;
	private String deliveryDate;
	private String requiredRenditions;
	private String descriptiveTitle;
}
