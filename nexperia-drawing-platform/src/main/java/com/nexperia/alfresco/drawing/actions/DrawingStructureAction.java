package com.nexperia.alfresco.drawing.actions;

import org.alfresco.model.ContentModel;
import org.alfresco.repo.action.executer.ActionExecuterAbstractBase;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.action.Action;
import org.alfresco.service.cmr.action.ParameterDefinition;
import org.alfresco.service.cmr.model.FileFolderService;
import org.alfresco.service.cmr.model.FileNotFoundException;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.StoreRef;
import org.alfresco.service.cmr.search.ResultSet;
import org.alfresco.service.cmr.search.SearchService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

public class DrawingStructureAction extends ActionExecuterAbstractBase {

    public static final Logger logger = LoggerFactory.getLogger(DrawingStructureAction.class);

    ServiceRegistry serviceRegistry;
    String structureDocument;
    String rootNode;
    String queryPath = "PATH:\"/app:company_home/cm:TDM_x0020_Drawing/cm:Drawings_x0020_Root//*\" and TYPE:\"drawing:drawingSpace\" and @cm\\:name:%1$s";

    @Override
    protected void executeImpl(Action action, NodeRef nodeRef) {
        List<String> allLines = null;
        try {
            allLines = Files.readAllLines(Paths.get(structureDocument));
        } catch (IOException e) {
            e.printStackTrace();
        }
        List<DrawingsInfo> drawings = new ArrayList<>();
        for (String drawingInfo : allLines) {
            String[] line = drawingInfo.split("\t");
            drawings.add(new DrawingsInfo(line[0], line[1]));
        }

        Map<String, Set<String>> groupedDrawings = drawings.stream().collect(Collectors.groupingBy(DrawingsInfo::getPath, Collectors.mapping(DrawingsInfo::getName, Collectors.toSet())));

        groupedDrawings.keySet().forEach(drawingPath -> {
            NodeRef folderNr = findOrCreateFolder(drawingPath);
            if (folderNr != null) {
                groupedDrawings.get(drawingPath).forEach(drawingName -> {
                    NodeRef drawingSpaceNr = findDrawingSpace(drawingName);
                    logger.debug(drawingName + " will be moved to " + folderNr);
                    if (drawingSpaceNr != null) {
                        try {
                            serviceRegistry.getFileFolderService().move(drawingSpaceNr, folderNr, null);
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });

    }

    private NodeRef findDrawingSpace(String drawingName) {
        SearchService searchService = serviceRegistry.getSearchService();
        String requestString = String.format(queryPath, drawingName);
        logger.debug("Request string: " + requestString);
        ResultSet resultSet = searchService.query(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE, SearchService.LANGUAGE_FTS_ALFRESCO, requestString);
        if (resultSet.getNodeRefs().size() == 1) {
            logger.debug("moving...");
            return resultSet.getNodeRefs().get(0);
        }
        logger.debug("There're " + resultSet.getNodeRefs().size() + " results found for " + drawingName + ". Skipping...");
        return null;
    }

    private NodeRef findOrCreateFolder(String drawingPath) {
        logger.debug("Searching creating the path: " + drawingPath);
        FileFolderService fileFolderService = serviceRegistry.getFileFolderService();
        NodeRef rootNodeNr = new NodeRef(rootNode);
        List<String> paths = Arrays.asList(drawingPath.replace(";", "").split(">"))
                .stream()
                .map(String::trim)
                .collect(Collectors.toList());
        NodeRef resultFolderNr = rootNodeNr;
        for (String path : paths) {
            logger.debug("searching: " + path);
            NodeRef nr = fileFolderService.searchSimple(resultFolderNr, path);
            if (nr == null) {
                resultFolderNr = fileFolderService.create(resultFolderNr, path, ContentModel.TYPE_FOLDER).getNodeRef();
                logger.debug("created the new path: " + path + " with NodeRef: " + resultFolderNr);
            } else {
                resultFolderNr = nr;
                logger.debug("found: " + path + " with NodeRef: " + resultFolderNr);
            }
        }

        return resultFolderNr;
    }

    @Override
    protected void addParameterDefinitions(List<ParameterDefinition> list) {
        // nothing
    }

    public void setServiceRegistry(ServiceRegistry serviceRegistry) {
        this.serviceRegistry = serviceRegistry;
    }

    class DrawingsInfo {

        public DrawingsInfo(String name, String path) {
            this.name = name;
            this.path = path;
        }

        String name;
        String path;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPath() {
            return path;
        }

        public void setPath(String path) {
            this.path = path;
        }
    }

    public void setStructureDocument(String structureDocument) {
        this.structureDocument = structureDocument;
    }

    public void setQueryPath(String queryPath) {
        this.queryPath = queryPath;
    }

    public void setRootNode(String rootNode) {
        this.rootNode = rootNode;
    }
}