package com.nexperia.alfresco.drawing.remote.exception;




/**
 * Exception thrown when parameters specified in
 * request to remote publication webscript were incorrect.
 * 
 * @author byaminov
 *
 */
public class IllegalParametersException extends WebScriptClientException {

	private static final long serialVersionUID = -7529819787074767788L;

	public IllegalParametersException(String message) {
		super(message);
	}

}
