package com.nexperia.alfresco.drawing.filter;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CodingErrorAction;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;
import javax.servlet.FilterChain;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.alfresco.repo.SessionUser;
import org.alfresco.repo.security.authentication.AuthenticationException;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.repo.security.authentication.Authorization;
import org.alfresco.repo.web.auth.BasicAuthCredentials;
import org.alfresco.repo.web.auth.TicketCredentials;
import org.alfresco.repo.web.filter.beans.DependencyInjectedFilter;
import org.alfresco.repo.webdav.auth.BaseAuthenticationFilter;
import org.alfresco.service.cmr.security.NoSuchPersonException;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class NexperiaWebDavAuthenticationFilter extends BaseAuthenticationFilter implements DependencyInjectedFilter {
        private static Log logger = LogFactory.getLog(NexperiaWebDavAuthenticationFilter.class);
        private static final String PPT_EXTN = ".ppt";
        private static final String[] ENCODINGS;

        public NexperiaWebDavAuthenticationFilter() {
        }

        public void doFilter(ServletContext context, ServletRequest req, ServletResponse resp, FilterChain chain) throws IOException, ServletException {
            try {
                this.doFilterInternal(context, req, resp, chain);
            } finally {
                if (logger.isTraceEnabled()) {
                    logger.debug("About to clear the security context");
                }

                AuthenticationUtil.clearCurrentSecurityContext();
            }

        }

        protected void doFilterInternal(ServletContext context, ServletRequest req, ServletResponse resp, FilterChain chain) throws IOException, ServletException {
            if (logger.isTraceEnabled()) {
                logger.trace("Entering AuthenticationFilter.");
            }

            HttpServletRequest httpReq = (HttpServletRequest)req;
            HttpServletResponse httpResp = (HttpServletResponse)resp;
            SessionUser user = this.getSessionUser(context, httpReq, httpResp, false);
            if (user == null) {
                if (logger.isDebugEnabled()) {
                    logger.debug("There is no user in the session.");
                }

                String authHdr = httpReq.getHeader("Authorization");
                if (authHdr != null && authHdr.length() > 5 && authHdr.substring(0, 5).equalsIgnoreCase("BASIC")) {
                    if (logger.isDebugEnabled()) {
                        logger.debug("Basic authentication details present in the header.");
                    }

                    byte[] encodedString = Base64.decodeBase64(authHdr.substring(5).getBytes());
                    Set<String> attemptedAuths = new HashSet(ENCODINGS.length * 2);
                    String[] var11 = ENCODINGS;
                    int var12 = var11.length;

                    for(int var13 = 0; var13 < var12; ++var13) {
                        String encoding = var11[var13];
                        CharsetDecoder decoder = Charset.forName(encoding).newDecoder().onMalformedInput(CodingErrorAction.REPORT);

                        try {
                            String basicAuth = decoder.decode(ByteBuffer.wrap(encodedString)).toString();
                            if (attemptedAuths.add(basicAuth)) {
                                String username = null;
                                String password = null;
                                int pos = basicAuth.indexOf(":");
                                if (pos != -1) {
                                    username = basicAuth.substring(0, pos);
                                    password = basicAuth.substring(pos + 1);
                                } else {
                                    username = basicAuth;
                                    password = "";
                                }

                                Authorization auth = new Authorization(username, password);
                                if (auth.isTicket()) {
                                    this.authenticationService.validate(auth.getTicket());
                                }  else {
                                    if (!username.contains("@")) {
                                        this.authenticationService.authenticate(username, password.toCharArray());
                                        if (this.authenticationListener != null) {
                                            this.authenticationListener.userAuthenticated(new BasicAuthCredentials(username, password));
                                        }
                                    } else {
                                        MutableHttpServletRequest mutableRequest = new MutableHttpServletRequest(httpReq);
                                        mutableRequest.putHeader("proxy_s", username);
                                        // Get the user details object from the session
                                        SessionUser ssoUser = getSessionUser(context, mutableRequest, httpResp, false);
                                        if (ssoUser != null) {
                                            authenticationListener.userAuthenticated(new TicketCredentials(ssoUser.getTicket()));
                                        } else {
                                            authenticationService.authenticate(username, password.toCharArray());
                                        }
                                    }
                                }

                                user = this.createUserEnvironment(httpReq.getSession(), this.authenticationService.getCurrentUserName(), this.authenticationService.getCurrentTicket(), false);
                                if (logger.isTraceEnabled()) {
                                    logger.trace("Successfully created user environment, login using basic auth or ROLE_TICKET for user: " + AuthenticationUtil.maskUsername(user.getUserName()));
                                }
                                break;
                            }
                        } catch (CharacterCodingException var21) {
                            if (logger.isDebugEnabled()) {
                                logger.debug("Didn't decode using " + decoder.getClass().getName(), var21);
                            }
                        } catch (AuthenticationException var22) {
                            if (logger.isDebugEnabled()) {
                                logger.debug("Authentication error ", var22);
                            }
                        } catch (NoSuchPersonException var23) {
                            if (logger.isDebugEnabled()) {
                                logger.debug("There is no such person error ", var23);
                            }
                        }
                    }
                } else {
                    String ticket = req.getParameter("ticket");
                    if (ticket != null && ticket.length() > 0) {
                        if (ticket.endsWith(".ppt")) {
                            ticket = ticket.substring(0, ticket.length() - ".ppt".length());
                        }

                        if (logger.isTraceEnabled()) {
                            Log var10000 = logger;
                            String var10001 = req.getRemoteHost();
                            var10000.trace("Logon via ticket from " + var10001 + " (" + req.getRemoteAddr() + ":" + req.getRemotePort() + ") ticket=" + ticket);
                        }

                        this.authenticationService.validate(ticket);
                        if (this.authenticationListener != null) {
                            this.authenticationListener.userAuthenticated(new TicketCredentials(ticket));
                        }

                        String currentUsername = this.authenticationService.getCurrentUserName();
                        user = this.createUserEnvironment(httpReq.getSession(), currentUsername, ticket, false);
                        if (logger.isTraceEnabled()) {
                            logger.trace("Successfully created user environment, login using TICKET for user: " + AuthenticationUtil.maskUsername(user.getUserName()));
                        }
                    }
                }

                if (user == null) {
                    if (logger.isDebugEnabled()) {
                        logger.debug("No user/ticket, force the client to prompt for logon details.");
                    }

                    httpResp.setHeader("WWW-Authenticate", "BASIC realm=\"Alfresco DAV Server\"");
                    httpResp.setStatus(401);
                    httpResp.flushBuffer();
                    return;
                }
            } else {
                if (this.authenticationListener != null) {
                    this.authenticationListener.userAuthenticated(new TicketCredentials(user.getTicket()));
                }

                if (logger.isTraceEnabled()) {
                    logger.trace("User already set to: " + AuthenticationUtil.maskUsername(user.getUserName()));
                }
            }

            chain.doFilter(req, resp);
        }

        public void destroy() {
        }

        protected Log getLogger() {
            return logger;
        }

        static {
            String[] encodings = new String[]{"UTF-8", System.getProperty("file.encoding"), "ISO-8859-1"};
            Set<String> encodingsSet = new LinkedHashSet();
            String[] var2 = encodings;
            int var3 = encodings.length;

            for(int var4 = 0; var4 < var3; ++var4) {
                String encoding = var2[var4];
                encodingsSet.add(encoding);
            }

            ENCODINGS = new String[encodingsSet.size()];
            encodingsSet.toArray(ENCODINGS);
        }
}
