package com.nexperia.alfresco.drawing.webscripts;

import org.alfresco.model.ContentModel;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.repository.NodeRef;
import org.springframework.extensions.webscripts.AbstractWebScript;
import org.springframework.extensions.webscripts.WebScriptRequest;
import org.springframework.extensions.webscripts.WebScriptResponse;

import java.io.IOException;

public class CreateInputDocumentsFolder extends AbstractWebScript {

    ServiceRegistry serviceRegistry;

    @Override
    public void execute(WebScriptRequest webScriptRequest, WebScriptResponse webScriptResponse) throws IOException {
        String nodeRef = webScriptRequest.getParameter("nodeRef");
        serviceRegistry.getFileFolderService().create(new NodeRef(nodeRef), "Input documents", ContentModel.TYPE_FOLDER);
    }

    public void setServiceRegistry(ServiceRegistry serviceRegistry) {
        this.serviceRegistry = serviceRegistry;
    }
}
