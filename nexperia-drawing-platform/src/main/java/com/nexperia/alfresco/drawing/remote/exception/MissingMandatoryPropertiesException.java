package com.nexperia.alfresco.drawing.remote.exception;

import java.util.List;




/**
 * Exception which is thrown if not all mandatory properties were specified in 
 * publication or update request.
 * 
 * @author byaminov
 *
 */
public class MissingMandatoryPropertiesException extends WebScriptClientException {

	private static final long serialVersionUID = 5391194336234070639L;
	
	private List<String> missingProperties;

	public MissingMandatoryPropertiesException(String message, List<String> missingProperties) {
		super(message);
		this.missingProperties = missingProperties;
	}

	public List<String> getMissingProperties() {
		return missingProperties;
	}
	
	@Override
	public String getMessage() {
		if (missingProperties == null || missingProperties.size() == 0) {
			return super.getMessage();
		} else {
			StringBuffer sb = new StringBuffer(super.getMessage());
			sb.append(" Missing properties: [");
			for (String prop : missingProperties) {
				sb.append(prop).append(", ");
			}
			sb.replace(sb.length() - 2, sb.length() - 1, "]");
			return sb.toString();
		}
	}

}
