package com.nexperia.alfresco.drawing.migration;

import com.nexperia.alfresco.drawing.model.DrawingModel;
import com.nexperia.alfresco.drawing.model.legacy.LegacyDrawingModel;
import com.nexperia.alfresco.drawing.service.StructureService;
import com.nexperia.alfresco.drawing.utils.DrawingUtil;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.action.executer.ActionExecuterAbstractBase;
import org.alfresco.repo.transaction.RetryingTransactionHelper;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.action.Action;
import org.alfresco.service.cmr.action.ParameterDefinition;
import org.alfresco.service.cmr.model.FileFolderService;
import org.alfresco.service.cmr.model.FileInfo;
import org.alfresco.service.cmr.model.FileNotFoundException;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.cmr.repository.StoreRef;
import org.alfresco.service.cmr.search.ResultSet;
import org.alfresco.service.cmr.search.SearchService;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.namespace.QName;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class MigrateAction extends ActionExecuterAbstractBase {

    public static final Logger logger = LoggerFactory.getLogger(MigrateAction.class);

    ServiceRegistry serviceRegistry;
    DrawingUtil drawingUtil;
    StructureService structureService;

    private final String DRAWING_SPACE = "drawing:drawingSpace";
    private final String DRAWING_CONTAINER = "drawing:drawingContainer";
    private final String DRAWING_INPUT_DOCUMENTS_CONTAINER = "drawing:inputDocumentsContainer";
    private final String SPACES_STORE_PREFIX = "workspace://SpacesStore/";
    //    String sourceFolder;
    String targetFolder;
    String migratedFolder;
    String drawingsToMigrate;
    String queryPath = "PATH:\"/app:company_home/cm:TDM_x0020_Drawing/cm:Drawings_x0020_Root//*\" and TYPE:\"drawing:drawingSpace\" and @cm\\:name:%1$s";

    @Override
    protected void executeImpl(Action action, NodeRef nodeRefOfAction) {
        NodeRef targetSpaceNr = new NodeRef(targetFolder);
        NodeRef migratedSpaceNr = new NodeRef(migratedFolder);
//        NodeRef sourceSpaceNr = new NodeRef(sourceFolder);

        logger.debug("STARTING...");

        processDrawingsFromFileWithNodeRefs(targetSpaceNr, migratedSpaceNr);

        logger.debug("Migration FINISHED!");
    }

    private void processDrawingsFromFileWithNames(NodeRef targetSpaceNr, NodeRef migratedSpaceNr) {
        try {
            List<String> allLines = Files.readAllLines(Paths.get(drawingsToMigrate));
            int size = allLines.size();
            int i = 1;
            int failed = 0;
            logger.debug("There're '" + size + "' Drawings to process.");
            for (String drawingName : allLines) {
                logger.debug("[ " + i++ + " of  " + size + " ] Processing " + drawingName);
                NodeRef sourceDrawingNodeRef = findDrawingByName(drawingName.trim());
                if (sourceDrawingNodeRef != null) {
                    logger.debug("Drawing was found!");
                    processDrawing(sourceDrawingNodeRef, targetSpaceNr, migratedSpaceNr, false);
                } else {
                    failed++;
                    logger.debug("NOT_FOUND\t" + drawingName);
                }
            }
            logger.debug(failed + " count of Drawings were not found.");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void processDrawingsFromFileWithNodeRefs(NodeRef targetSpaceNr, NodeRef migratedSpaceNr) {
        NodeService nodeService = serviceRegistry.getNodeService();
        try {
            List<String> allLines = Files.readAllLines(Paths.get(drawingsToMigrate));
            int size = allLines.size();
            int i = 1;
            int failed = 0;
            logger.debug("There're '" + size + "' Drawings to process.");
            for (String drawingUUID : allLines) {
                NodeRef sourceDrawingNodeRef = new NodeRef(SPACES_STORE_PREFIX + drawingUUID.trim());
                if (serviceRegistry.getNodeService().exists(sourceDrawingNodeRef)) {
                    String drawingName = drawingUtil.getName(sourceDrawingNodeRef);
                    logger.debug("[ " + i++ + " of  " + size + " ] Processing " + drawingName);
                    String magCode = (String) nodeService.getProperty(sourceDrawingNodeRef, LegacyDrawingModel.PROP_MAG_CODE);
                    String createdFor = (String) nodeService.getProperty(sourceDrawingNodeRef, LegacyDrawingModel.PROP_CREATED_FOR);
                    String drawingType = (String) nodeService.getProperty(sourceDrawingNodeRef, LegacyDrawingModel.PROP_DRAWING_TYPE);
                    NodeRef targetStructuredSpace = structureService.getTargetSpace(drawingName, drawingType, magCode, createdFor);
                    if (targetStructuredSpace != null) {
                        logger.debug("SS was FOUND for {} to {}", sourceDrawingNodeRef, targetStructuredSpace);
                        processDrawing(sourceDrawingNodeRef, targetStructuredSpace, migratedSpaceNr, false);
                    } else {
                        logger.debug("SS NOT FOUND, so the Drawing {} goes to Zzz_Archive space {}", sourceDrawingNodeRef, targetSpaceNr);
                        processDrawing(sourceDrawingNodeRef, targetSpaceNr, migratedSpaceNr, true);
                    }
                } else {
                    failed++;
                    logger.debug(sourceDrawingNodeRef + " doesn't exists!");
                }
            }
            logger.debug(failed + " count of Drawings were not found.");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private NodeRef findDrawingByName(String drawingName) {
        SearchService searchService = serviceRegistry.getSearchService();
        String requestString = String.format(queryPath, drawingName);
        logger.debug("Request string: " + requestString);
        ResultSet resultSet = searchService.query(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE, SearchService.LANGUAGE_FTS_ALFRESCO, requestString);
        if (resultSet.getNodeRefs().size() == 1) {
            return resultSet.getNodeRefs().get(0);
        } else {
            if (resultSet.getNodeRefs().size() != 0) {
                logger.debug("There're " + resultSet.getNodeRefs().size() + " results found for " + drawingName + ". Skipping");
            }
        }
        return null;
    }

    private void processDrawingsFromSourceSpace(NodeRef sourceSpaceNr, NodeRef targetSpaceNr, NodeRef migratedSpaceNr) {
        FileFolderService fileFolderService = serviceRegistry.getFileFolderService();
        List<FileInfo> drawings = fileFolderService.listFolders(sourceSpaceNr);
        logger.debug("Got Drawings from source: " + drawings.size());
        int size = drawings.size();
        int i = 1;
        for (FileInfo fileInfo : drawings) {
            logger.debug("[ " + i++ + " of  " + size + " ] Processing " + name);
            processDrawing(fileInfo.getNodeRef(), targetSpaceNr, migratedSpaceNr, false);
        }
    }

    private void processDrawing(NodeRef sourceDrawingNodeRef, NodeRef targetSpaceNr, NodeRef migratedSpaceNr, boolean isArchived) {
        FileFolderService fileFolderService = serviceRegistry.getFileFolderService();
        String sourceDrawingName = drawingUtil.getName(sourceDrawingNodeRef);
        logger.debug("Migration is processing for " + sourceDrawingName + " [" + sourceDrawingNodeRef + "]");
        if (serviceRegistry.getNodeService().hasAspect(sourceDrawingNodeRef, DrawingModel.ASPECT_MIGRATED)) {
            //We're skipping already migrated Drawings
            logger.debug("SKIPPING Drawing as already Migrated!: " + sourceDrawingName);
//            try {
//                serviceRegistry.getFileFolderService().move(drawingNodeRef, migratedSpaceNr, null);
//            } catch (FileNotFoundException e) {
//                e.printStackTrace();
//            }
        } else {
            NodeService nodeService = serviceRegistry.getNodeService();
            String type = nodeService.getType(sourceDrawingNodeRef).toPrefixString(serviceRegistry.getNamespaceService());
            if (DRAWING_SPACE.equals(type)) {
                try {
                    NodeRef newNodeRef = serviceRegistry.getRetryingTransactionHelper().doInTransaction(new RetryingTransactionHelper.RetryingTransactionCallback<NodeRef>() {
                        public NodeRef execute() throws Throwable {
                            NodeRef migratedDrawingSpace = migrateDrawingSpace(targetSpaceNr, sourceDrawingNodeRef, isArchived);
                            if (isArchived) {
                                //Mark that Job in archived space and archived.
                                nodeService.addAspect(migratedDrawingSpace, DrawingModel.ASPECT_ARCHIVED, null);
                                logger.debug("Marked as Archived");
                            }
                            logger.debug("drawing space created with nr: " + migratedDrawingSpace);
                            List<FileInfo> folders = fileFolderService.listFolders(sourceDrawingNodeRef);
                            for (FileInfo fi : folders) {
                                String folderType = nodeService.getType(fi.getNodeRef()).toPrefixString(serviceRegistry.getNamespaceService());
                                if (DRAWING_CONTAINER.equals(folderType)) {
                                    List<FileInfo> renditions = fileFolderService.listFiles(fi.getNodeRef());
                                    logger.debug("there're " + renditions.size() + " renditions to migrate...");
                                    migrateRenditions(migratedDrawingSpace, renditions, sourceDrawingNodeRef);
                                }
                                if (DRAWING_INPUT_DOCUMENTS_CONTAINER.equals(folderType)) {
                                    serviceRegistry.getFileFolderService().move(fi.getNodeRef(), migratedDrawingSpace, null);
                                }
                            }
                            //Mark that this Job was moved to the new env
                            nodeService.addAspect(sourceDrawingNodeRef, DrawingModel.ASPECT_MIGRATED, null);
                            //Move to result space
                            serviceRegistry.getFileFolderService().move(sourceDrawingNodeRef, migratedSpaceNr, null);
                            return migratedDrawingSpace;
                        }
                    }, false, true);
                } catch (Exception e) {
                    logger.debug(sourceDrawingName + " migration FAILED with exception: " + e);
                }
            }
        }
    }

    private void migrateRenditions(NodeRef migratedDrawingSpace, List<FileInfo> renditions, NodeRef sourceDrawingNodeRef) throws FileNotFoundException {
        String status = (String) serviceRegistry.getNodeService().getProperty(migratedDrawingSpace, DrawingModel.PROP_STATUS);
        String sourceStatus = (String) serviceRegistry.getNodeService().getProperty(sourceDrawingNodeRef, LegacyDrawingModel.PROP_STATUS);
        for (FileInfo fi : renditions) {
            serviceRegistry.getFileFolderService().move(fi.getNodeRef(), migratedDrawingSpace, null);
            serviceRegistry.getNodeService().setType(fi.getNodeRef(), DrawingModel.TYPE_RENDITION);
            serviceRegistry.getNodeService().setProperty(fi.getNodeRef(), DrawingModel.PROP_STATUS, status);
            if (DrawingModel.STATUS_RELEASED.equals(sourceStatus) && !fi.getName().contains("EPS")) {
                serviceRegistry.getNodeService().setProperty(fi.getNodeRef(), DrawingModel.PROP_PUBLISH_TO_WEB, "Yes");
            }
            logger.debug(fi.getName() + " rendition moved to: " +
                    serviceRegistry.getNodeService().getProperty(migratedDrawingSpace, ContentModel.PROP_NAME) + " (" + migratedDrawingSpace + ")");
        }
    }

    private NodeRef migrateDrawingSpace(NodeRef targetSpaceNr, NodeRef nodeRef, boolean isArchived) throws Exception {
        Map<QName, Serializable> properties = serviceRegistry.getNodeService().getProperties(nodeRef);
        Map<QName, Serializable> newProps = new HashMap<>();
        newProps.put(ContentModel.PROP_NAME, properties.get(ContentModel.PROP_NAME));
        newProps.put(DrawingModel.PROP_DRAWING_ID, properties.get(ContentModel.PROP_NAME));
        newProps.put(DrawingModel.PROP_MAG_CODE, properties.get(LegacyDrawingModel.PROP_MAG_CODE));
        newProps.put(DrawingModel.PROP_CREATED_FOR, properties.get(LegacyDrawingModel.PROP_CREATED_FOR));
        newProps.put(DrawingModel.PROP_DELIVERY_DATE, properties.get(LegacyDrawingModel.PROP_DELIVERY_DATE));
        newProps.put(DrawingModel.PROP_DESCRIPTIVE_TITLE, properties.get(LegacyDrawingModel.PROP_DESCRIPTIVE_TITLE));
        newProps.put(DrawingModel.PROP_DRAWING_TYPE, properties.get(LegacyDrawingModel.PROP_DRAWING_TYPE));
        newProps.put(DrawingModel.PROP_DRAWING_VERSION, properties.get(LegacyDrawingModel.PROP_DRAWING_VERSION));
        newProps.put(DrawingModel.PROP_LAST_STATUS_CHANGED_DATE, properties.get(LegacyDrawingModel.PROP_LAST_STATUS_CHANGE));
        newProps.put(DrawingModel.PROP_REQUESTER, properties.get(LegacyDrawingModel.PROP_REQUESTER));
        newProps.put(DrawingModel.PROP_SECURITY_STATUS, properties.get(LegacyDrawingModel.PROP_SECURITY_STATUS));

        String primaryFormat = (String) properties.get(LegacyDrawingModel.PROP_PRIMARY_FORMAT);
        newProps.put(DrawingModel.PROP_PRIMARY_FORMAT,
                (primaryFormat.equalsIgnoreCase("ai")
                        || primaryFormat.equalsIgnoreCase("gif")
                        || primaryFormat.equalsIgnoreCase("vsd"))
                        ? "EPS" : ((String) properties.get(LegacyDrawingModel.PROP_PRIMARY_FORMAT)).toUpperCase());

        // Required renditions
        ArrayList<String> requiredRenditions = (ArrayList<String>) properties.get(LegacyDrawingModel.PROP_REQUIRED_RENDITIONS);
        requiredRenditions.remove("eps");
        requiredRenditions.remove("pdf");
        requiredRenditions.remove("PDF");
        requiredRenditions.remove("ai");
        requiredRenditions.remove("AI");
        requiredRenditions.remove("vsd");
        requiredRenditions.remove("VSD");
        requiredRenditions.remove("svg_1");
        requiredRenditions.remove("SVG_1");
        newProps.put(DrawingModel.PROP_REQUIRED_RENDITIONS, (Serializable) requiredRenditions.stream().map(StringUtils::upperCase).collect(Collectors.toList()));

        // drawingStatus
        if (isArchived) {
            newProps.put(DrawingModel.PROP_STATUS, DrawingModel.STATUS_ARCHIVED);
        } else {
            String status = (String) properties.get(LegacyDrawingModel.PROP_STATUS);
            newProps.put(DrawingModel.PROP_STATUS,
                    (status.equals("Not started") || status.equals("Deleted")
                            || status.equals("Other") || status.equals("Waiting for client")) ? "Draft" : properties.get(LegacyDrawingModel.PROP_STATUS));
        }

        logger.debug("MIGRATING " + properties.get(ContentModel.PROP_NAME) + " with props: " + newProps);
        return serviceRegistry.getNodeService().createNode(targetSpaceNr, ContentModel.ASSOC_CONTAINS,
                QName.createQName(NamespaceService.CONTENT_MODEL_1_0_URI, (String) properties.get(ContentModel.PROP_NAME)), DrawingModel.TYPE_DRAWING, newProps)
                .getChildRef();
    }

    @Override
    protected void addParameterDefinitions(List<ParameterDefinition> list) {
        // there is no params
    }

    public void setServiceRegistry(ServiceRegistry serviceRegistry) {
        this.serviceRegistry = serviceRegistry;
    }

    public void setTargetFolder(String targetFolder) {
        this.targetFolder = targetFolder;
    }

    public void setMigratedFolder(String migratedFolder) {
        this.migratedFolder = migratedFolder;
    }

//    public void setSourceFolder(String sourceFolder) {
//        this.sourceFolder = sourceFolder;
//    }

    public void setDrawingsToMigrate(String drawingsToMigrate) {
        this.drawingsToMigrate = drawingsToMigrate;
    }

    public void setQueryPath(String queryPath) {
        this.queryPath = queryPath;
    }

    public void setDrawingUtil(DrawingUtil drawingUtil) {
        this.drawingUtil = drawingUtil;
    }

    public void setStructureService(StructureService structureService) {
        this.structureService = structureService;
    }
}
