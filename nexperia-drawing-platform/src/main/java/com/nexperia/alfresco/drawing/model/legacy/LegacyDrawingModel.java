package com.nexperia.alfresco.drawing.model.legacy;

import org.alfresco.service.namespace.QName;

public class LegacyDrawingModel {

	public static final String DRAWING_MODEL_1_0_URI = "http://www.nxp.com/drawing/content-model";

	public static final QName TYPE_DRAWING_APPLICATION_HOME = QName.createQName(DRAWING_MODEL_1_0_URI, "applicationHome");

	public static final QName TYPE_TEMPLATES_SPACE = QName.createQName(DRAWING_MODEL_1_0_URI, "templatesSpace");
	public static final QName TYPE_DRAWINGS_SPACE = QName.createQName(DRAWING_MODEL_1_0_URI, "drawingSpace");
    public static final QName TYPE_VA_HOLDER = QName.createQName(DRAWING_MODEL_1_0_URI, "valueAssistanceHolder");
	public static final QName TYPE_INPUT_DOCUMENTS_CONTAINER = QName.createQName(DRAWING_MODEL_1_0_URI,
			"inputDocumentsContainer");

	public static final QName TYPE_EVENT = QName.createQName(DRAWING_MODEL_1_0_URI, "event");
	public static final QName TYPE_WHERE_USED_RECORD = QName.createQName(DRAWING_MODEL_1_0_URI, "whereUsedRecord");
	public static final QName PROP_EVENT_DATE = QName.createQName(DRAWING_MODEL_1_0_URI, "eventDate");
    public static final QName PROP_LONG_EVENT_DATE = QName.createQName(DRAWING_MODEL_1_0_URI, "eventLongDate");
    public static final QName PROP_EVENT_TASK = QName.createQName(DRAWING_MODEL_1_0_URI, "eventTask");
	public static final QName PROP_EVENT_ACTOR = QName.createQName(DRAWING_MODEL_1_0_URI, "eventActor");
	public static final QName PROP_EVENT_STATUS = QName.createQName(DRAWING_MODEL_1_0_URI, "eventStatus");
	public static final QName PROP_EVENT_VERSION_LABEL = QName.createQName(DRAWING_MODEL_1_0_URI, "eventVersionLabel");


	public static final QName TASK_DRAWING = QName.createQName(DRAWING_MODEL_1_0_URI, "drawing");
	public static final QName TASK_QUALITY_CHECK = QName.createQName(DRAWING_MODEL_1_0_URI, "qualityCheck");
	public static final QName TASK_APPROVAL = QName.createQName(DRAWING_MODEL_1_0_URI, "approval");
	public static final QName TASK_NONE = QName.createQName(DRAWING_MODEL_1_0_URI, "none");
	public static final QName TASK_INITIATE_DRAWING = QName.createQName(DRAWING_MODEL_1_0_URI, "initiateDrawing");

	public static final QName PROP_REMARK_HANDLED = QName.createQName(DRAWING_MODEL_1_0_URI, "remarkHandled");

	public static final String ROOT_GROUP = "GROUP_TDM_Draw Users";
	public static final String GROUP_DRAW_COORDINATOR = "GROUP_P_TDM_Draw_Coordinator";
	public static final String GROUP_DRAW_REQUESTOR = "GROUP_P_TDM_Draw_Requestor";
	public static final String GROUP_DRAW_ILLUSTRATOR = "GROUP_P_TDM_Draw_Illustrator";
	public static final String GROUP_DRAW_QUALIFIER = "GROUP_P_TDM_Draw_Qualifier";
	public static final String GROUP_O_TDM_CDS = "GROUP_O_TDM_CDS";

    public static final QName PROP_VA_HOLDER_NAME = QName.createQName(DRAWING_MODEL_1_0_URI, "valueAssistance");
    public static final QName PROP_VA_HOLDER_VALUES = QName.createQName(DRAWING_MODEL_1_0_URI, "valueAssistanceValues");

	public static final QName PROP_LAST_STATUS_CHANGE = QName.createQName(DRAWING_MODEL_1_0_URI, "lastStatusChange");
	public static final QName PROP_STATUS = QName.createQName(DRAWING_MODEL_1_0_URI, "status");
	public static final QName PROP_REVISED = QName.createQName(DRAWING_MODEL_1_0_URI, "revised");
	public static final QName PROP_IS_RELEASED_ONCE = QName.createQName(DRAWING_MODEL_1_0_URI, "isReleasedOnce");
	public static final QName PROP_WAS_IN_APPROVE_TASK_ONCE = QName.createQName(DRAWING_MODEL_1_0_URI, "wasInApproveTaskOnce");
	public static final QName PROP_DRAWING_STATUS = QName.createQName(DRAWING_MODEL_1_0_URI, "drawingStatus");

	public static final QName PROP_CURR_PERFORMER = QName.createQName(DRAWING_MODEL_1_0_URI, "currentPerformer");
	public static final QName PROP_CURR_TASK_TYPE = QName.createQName(DRAWING_MODEL_1_0_URI, "currentTaskType");
	public static final QName PROP_PREV_TASK_TYPE = QName.createQName(DRAWING_MODEL_1_0_URI, "prevTaskType");
	public static final QName PROP_PREV_STATUS = QName.createQName(DRAWING_MODEL_1_0_URI, "prevStatus");
	public static final QName PROP_PREV_ALLOW_DOWNLOAD_ALF_VERSION = QName.createQName(DRAWING_MODEL_1_0_URI, "prevAllowDownloadAlfVersion");
	public static final QName PROP_CURR_TASK_ID = QName.createQName(DRAWING_MODEL_1_0_URI, "currentTaskId");
	public static final QName PROP_CURR_POOLED_PERFORMERS = QName.createQName(DRAWING_MODEL_1_0_URI, "pooledPerformers");
	public static final QName PROP_CURR_TASK_OUTCOME = QName.createQName(DRAWING_MODEL_1_0_URI, "currentTaskOutcome");
	public static final QName PROP_CURR_TASK_TRANSITION = QName.createQName(DRAWING_MODEL_1_0_URI, "currentTaskTransition");
	public static final QName PROP_COMMENT = QName.createQName(DRAWING_MODEL_1_0_URI, "transitionComment");
	public static final QName PROP_ALLOW_TASK_COMPLETE = QName.createQName(DRAWING_MODEL_1_0_URI, "allowTaskComplete");
	public static final QName PROP_ALLOW_DOWNLOAD_ALF_VERSION = QName.createQName(DRAWING_MODEL_1_0_URI, "allowDownloadAlfVersion");
	public static final QName PROP_ALLOW_EMAIL_CONTROL = QName.createQName(DRAWING_MODEL_1_0_URI, "allowEmailControl");
	public static final QName PROP_LB_AVAIL_CHECK_STATUS = QName.createQName(DRAWING_MODEL_1_0_URI, "lbAvailCheckStatus");
	public static final QName PROP_GUID = QName.createQName(DRAWING_MODEL_1_0_URI, "drawGUID");
	public static final QName PROP_STATE_ID = QName.createQName(DRAWING_MODEL_1_0_URI, "stateId");
	public static final QName PROP_DRAWING_ID = QName.createQName(DRAWING_MODEL_1_0_URI, "drawingId");
	public static final QName PROP_AUTOGENERATED_DRAWING_ID = QName.createQName(DRAWING_MODEL_1_0_URI, "autoGeneratedDrawingId");
	public static final QName PROP_REWORK = QName.createQName(DRAWING_MODEL_1_0_URI, "rework");
	public static final QName PROP_FORMAT = QName.createQName(DRAWING_MODEL_1_0_URI, "format");
	public static final QName PROP_MULTIPLE_RENDITIONS = QName.createQName(DRAWING_MODEL_1_0_URI, "multipleRenditions");
	public static final QName PROP_DRAWING_VERSION = QName.createQName(DRAWING_MODEL_1_0_URI, "drawingVersion");
	public static final QName PROP_PREV_DRAWING_VERSION = QName.createQName(DRAWING_MODEL_1_0_URI, "prevDrawingVersion");
	public static final QName PROP_DESCRIPTIVE_TITLE = QName.createQName(DRAWING_MODEL_1_0_URI, "descriptiveTitle");
	public static final QName PROP_DRAWING_TYPE = QName.createQName(DRAWING_MODEL_1_0_URI, "drawingType");
	public static final QName PROP_PRIMARY_FORMAT = QName.createQName(DRAWING_MODEL_1_0_URI, "primaryFormat");
	public static final QName PROP_CREATED_FOR = QName.createQName(DRAWING_MODEL_1_0_URI, "createdFor");
	public static final QName PROP_QUALITY_LEVEL = QName.createQName(DRAWING_MODEL_1_0_URI, "qualityLevel");
	public static final QName PROP_SECURITY_STATUS = QName.createQName(DRAWING_MODEL_1_0_URI, "securityStatus");
	public static final QName PROP_REQUESTER = QName.createQName(DRAWING_MODEL_1_0_URI, "requester");
	public static final QName PROP_MAG_CODE = QName.createQName(DRAWING_MODEL_1_0_URI, "magCode");
	public static final QName PROP_PUBLISH_TO_WEB = QName.createQName(DRAWING_MODEL_1_0_URI, "publishToWeb");
	public static final QName PROP_REQUIRED_RENDITIONS = QName.createQName(DRAWING_MODEL_1_0_URI, "requiredRenditions");
	public static final QName PROP_ADDITIONAL_INFO = QName.createQName(DRAWING_MODEL_1_0_URI, "additionalInfo");
	public static final QName PROP_PUBLICATION_ID = QName.createQName(DRAWING_MODEL_1_0_URI, "publicationId");
	public static final QName PROP_CREATION_DATE = QName.createQName(DRAWING_MODEL_1_0_URI, "creationDate");
	public static final QName PROP_MAJOR_VERSION_2B_USED = QName.createQName(DRAWING_MODEL_1_0_URI, "majorVersion2BUsed");
	public static final QName PROP_DELIVERY_DATE = QName.createQName(DRAWING_MODEL_1_0_URI, "deliveryDate");
	public static final QName PROP_WEBDAV_URL = QName.createQName(DRAWING_MODEL_1_0_URI, "webDavUrl");
		

	//Where used props
	public static final QName PROP_MODULE_NAME = QName.createQName(DRAWING_MODEL_1_0_URI, "moduleName");
	public static final QName PROP_DOCUMENT_ID = QName.createQName(DRAWING_MODEL_1_0_URI, "documentId");
	public static final QName PROP_DOCUMENT_VERSION = QName.createQName(DRAWING_MODEL_1_0_URI, "documentVersion");
	public static final QName PROP_DOCUMENT_STATUS = QName.createQName(DRAWING_MODEL_1_0_URI, "documentStatus");
	public static final QName PROP_ALFRESCO_VERSION = QName.createQName(DRAWING_MODEL_1_0_URI, "alfrescoVersion");
	public static final QName PROP_FILE_NAME = QName.createQName(DRAWING_MODEL_1_0_URI, "fileName");
	public static final QName PROP_OWNER = QName.createQName(DRAWING_MODEL_1_0_URI, "owner");
	public static final QName PROP_WHERE_USED_ID = QName.createQName(DRAWING_MODEL_1_0_URI, "whereUsedIdentifier");

	public static final QName TYPE_DRAWINGS_ROOT_SPACE = QName.createQName(DRAWING_MODEL_1_0_URI, "drawingsRootSpace");

	// Performers
	public static final QName PROP_ILLUSTRATOR = QName.createQName(DRAWING_MODEL_1_0_URI, "illustrator");
	public static final QName PROP_QUALITY_CHECKER = QName.createQName(DRAWING_MODEL_1_0_URI, "qualityChecker");
	public static final QName PROP_APPROVER = QName.createQName(DRAWING_MODEL_1_0_URI, "approver");
	public static final QName PROP_DRAW_COORDINATOR = QName.createQName(DRAWING_MODEL_1_0_URI, "drawCoordinator");

	// History
	public static final QName PROP_ILLUSTRATORS = QName.createQName(DRAWING_MODEL_1_0_URI, "illustrators");
	public static final QName PROP_QUALITY_CHECKERS = QName.createQName(DRAWING_MODEL_1_0_URI, "qualityCheckers");
	public static final QName PROP_APPROVERS = QName.createQName(DRAWING_MODEL_1_0_URI, "approvers");
	public static final QName PROP_DRAW_COORDINATORS = QName.createQName(DRAWING_MODEL_1_0_URI, "drawCoordinators");

	// Containers
	public static final QName TYPE_REMARKS_CONTAINER = QName.createQName(DRAWING_MODEL_1_0_URI, "remarksContainer");
	public static final QName TYPE_EVENTS_CONTAINER = QName.createQName(DRAWING_MODEL_1_0_URI, "eventsContainer");
	public static final QName TYPE_RELEASES_CONTAINER = QName.createQName(DRAWING_MODEL_1_0_URI, "releasesContainer");
	public static final QName TYPE_DRAWING_CONTAINER = QName.createQName(DRAWING_MODEL_1_0_URI, "drawingContainer");
	public static final QName TYPE_WHERE_USED_CONTAINER = QName.createQName(DRAWING_MODEL_1_0_URI, "whereUsedContainer");

	public static final QName TYPE_MAIN_DRAWING_FORMAT = QName.createQName(DRAWING_MODEL_1_0_URI, "mainDrawing");
	public static final QName TYPE_RENDITION = QName.createQName(DRAWING_MODEL_1_0_URI, "rendition");

	//Statuses
	public static final String STATUS_IN_PROGRESS = "In progress";
	public static final String STATUS_NOT_STARTED = "Not started";
	public static final String STATUS_COMPLETED = "Completed";
	public static final String STATUS_STOPPED = "Stopped";
	public static final String STATUS_DELETED = "Deleted";
	public static final String STATUS_WAITING_FOR_CLIENT = "Waiting for client";
	public static final String STATUS_ACCEPTED = "Accepted";
	public static final String STATUS_RELEASED = "Released";
	public static final String STATUS_OBSOLETE = "Obsolete";
	public static final String STATUS_ON_HOLD = "On hold";
	public static final String STATUS_REASSIGNMENT = "Reassignment";
	public static final String STATUS_DRAFT = "Draft";
	public static final String STATUS_ADDITIONAL_WORK_REQUESTED = "Additional rework requested";
	public static final String STATUS_RESTORED = "Deleted drawing restored";
	public static final String STATUS_RESTARTED = "Stopped drawing restarted";
	public static final String STATUS_REVISION_INITIATED = "Revision initiated";
	public static final String STATUS_REVISION_DECLINED = "Revision declined";
	public static final String STATUS_MODIFIED = "Modified";
	public static final String TASK_EDIT_METADATA = "Edit metadata";

    public static final String PROP_QUALITY_LEVELS_NAME = "qualityLevel";
	public static final String PROP_GROUP_AUTHORIZATIONS_NAME = "groupAuthorization";
    public static final String PROP_PRIMARY_FORMATS_NAME = "primaryFormat";
    public static final String PROP_FORMAT_NAME = "drawingFormatName";
    public static final String PROP_FORMAT_LABEL = "drawingFormatLabel";
    public static final String PROP_FORMAT_SUFFIX = "drawingFormatSuffix";
    public static final String PROP_FORMAT_AUTOMATED = "drawingFormatAutomated";
    public static final String PROP_FORMAT_PARAMETERS = "drawingFormatParameters";
    public static final String PROP_SUPERSEDED_DOC_IDS = "supersededDocIds";

    public static final QName ASPECT_PRIMARY_FORMAT = QName.createQName(DRAWING_MODEL_1_0_URI, "drawingPrimaryFormatAspect");
    public static final QName PROP_PRIMARY_FORMAT_NAME = QName.createQName(DRAWING_MODEL_1_0_URI, "drawingFormatName");
    public static final QName PROP_PRIMARY_FORMAT_LABEL = QName.createQName(DRAWING_MODEL_1_0_URI, "drawingFormatLabel");
    public static final QName PROP_PRIMARY_FORMAT_SUFFIX = QName.createQName(DRAWING_MODEL_1_0_URI, "drawingFormatSuffix");
    public static final QName PROP_PRIMARY_FORMAT_AUTOMATED = QName.createQName(DRAWING_MODEL_1_0_URI, "drawingFormatAutomated");
    public static final QName PROP_PRIMARY_FORMAT_PARAMETERS = QName.createQName(DRAWING_MODEL_1_0_URI, "drawingFormatParameters");

	public static final QName ASPECT_REMARK = QName.createQName(DRAWING_MODEL_1_0_URI, "remarkAspect");

	public static final QName ASPECT_RELEASED_VERSIONS = QName.createQName(DRAWING_MODEL_1_0_URI, "releasedVersionsAspect");
	public static final QName PROP_RELEASED_DRAWINGS = QName.createQName(DRAWING_MODEL_1_0_URI, "releasedDrawingsInfo");

	public static final String DRAWING_SECURITY_STATUS_COMPANY_PUBLIC = "Company Public";
    public static final String DRAWING_SECURITY_STATUS_COMPANY_CONFIDENTIAL = "Company Confidential";
    public static final String DRAWING_SECURITY_STATUS_COMPANY_INTERNAL = "Company Internal";
    public static final String DRAWING_SECURITY_STATUS_COMPANY_SECRET = "Company Secret";

    public static final QName ASPECT_DRAWING_DATA_ASPECT = QName.createQName(DRAWING_MODEL_1_0_URI, "drawingDataAspect");

    public static final QName PROP_EXT_REFERENCE_ID = QName.createQName(DRAWING_MODEL_1_0_URI, "referenceId");

	public static final String ROLE_COORDINATOR = "Coordinator";
	public static final String ROLE_REQUESTER = "Requester";
	public static final String ROLE_PERFORMER = "Performer";

	public static final String CLONED_FROM_DRAWING_ID = "clonedFromDrawingID";
	public static final String NAME_OF_INITIATE_DRAWING_TASK = "Initiate Job";
}
