package com.nexperia.alfresco.drawing.webscripts;

import com.nexperia.alfresco.drawing.dto.DrawingSearchDto;
import com.nexperia.alfresco.drawing.service.DrawingReportService;
import com.nexperia.alfresco.drawing.service.EmailService;
import com.nexperia.alfresco.drawing.utils.DrawingUtil;
import org.alfresco.error.AlfrescoRuntimeException;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.repository.NodeRef;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.extensions.webscripts.AbstractWebScript;
import org.springframework.extensions.webscripts.WebScriptException;
import org.springframework.extensions.webscripts.WebScriptRequest;
import org.springframework.extensions.webscripts.WebScriptResponse;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;

public class DrawingReportWebScript extends AbstractWebScript {

    private static final Logger logger = LoggerFactory.getLogger(DrawingReportWebScript.class);

    private DrawingReportService drawingReportService;
    private ServiceRegistry serviceRegistry;
    private DrawingUtil drawingUtil;
    private EmailService emailService;
    static final String NO_REPLY = "no-reply@nexperia.com";

    @Override
    public void execute(WebScriptRequest webScriptRequest, WebScriptResponse webScriptResponse) {
        NodeRef report = drawingReportService.createReport(DrawingSearchDto.builder()
                .name(webScriptRequest.getParameter("term"))
                .drawingType(webScriptRequest.getParameter("drawingType"))
                .primaryFormat(webScriptRequest.getParameter("primaryFormat"))
                .drawingVersion(webScriptRequest.getParameter("drawingVersion"))
                .createdFor(webScriptRequest.getParameter("createdFor"))
                .securityStatus(webScriptRequest.getParameter("securityStatus"))
                .magCode(webScriptRequest.getParameter("magCode"))
                .requester(webScriptRequest.getParameter("requester"))
                .deliveryDate(webScriptRequest.getParameter("deliveryDate"))
                .requiredRenditions(webScriptRequest.getParameter("requiredRenditions"))
                .descriptiveTitle(webScriptRequest.getParameter("descriptiveTitle"))
                .build());
        webScriptResponse.setHeader("Content-Transfer-Encoding", "binary");
        webScriptResponse.setHeader("Content-Disposition", "attachment; filename=" + serviceRegistry.getNodeService().getProperty(report, ContentModel.PROP_NAME));
        try (InputStream inputStream = serviceRegistry.getContentService().getReader(report, ContentModel.PROP_CONTENT).getContentInputStream()) {
            IOUtils.copy(inputStream, webScriptResponse.getOutputStream());
			sendEmailToInitiator(report);
        } catch (IOException e) {
            throw new WebScriptException(e.getMessage());
        }
    }

    private void sendEmailToInitiator(NodeRef report) {
        Map<String, Object> model = new HashMap<>();

        try {
            String username = AuthenticationUtil.getFullyAuthenticatedUser();
            NodeRef currentUser = serviceRegistry.getPersonService().getPerson(username);
            String to = drawingUtil.getEmail(currentUser);
            List<String> toEmails = new ArrayList<>();
			List<NodeRef> reportToSend = Arrays.asList(report);
			toEmails.add(to);
            String fullName = drawingUtil.getFullUserName(currentUser);
			model.put("userFullName", fullName);
            String subject = "Alfresco Drawing: Report generated.";
            logger.debug("noReply: {},toMails: {}, emailSubject: {}", NO_REPLY, toEmails.toArray(), subject);
            emailService.send(NO_REPLY, toEmails, null, subject, "report", model, reportToSend);
        } catch (Exception e) {
            e.printStackTrace();
            throw new AlfrescoRuntimeException(e.getMessage());
        }
    }

	public DrawingReportService getDrawingReportService() {
		return drawingReportService;
	}

	public void setDrawingReportService(DrawingReportService drawingReportService) {
		this.drawingReportService = drawingReportService;
	}

	public ServiceRegistry getServiceRegistry() {
		return serviceRegistry;
	}

	public void setServiceRegistry(ServiceRegistry serviceRegistry) {
		this.serviceRegistry = serviceRegistry;
	}

	public DrawingUtil getDrawingUtil() {
		return drawingUtil;
	}

	public void setDrawingUtil(DrawingUtil drawingUtil) {
		this.drawingUtil = drawingUtil;
	}

	public EmailService getEmailService() {
		return emailService;
	}

	public void setEmailService(EmailService emailService) {
		this.emailService = emailService;
	}
}
