package com.nexperia.alfresco.drawing.service;

import org.alfresco.error.AlfrescoRuntimeException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.io.ClassPathResource;

import java.util.List;
import java.util.Map;

public class TemplateService {

    private final org.alfresco.service.cmr.repository.TemplateService freemarkerTemplateService;
    private final List<String> templatePaths;

    public TemplateService(org.alfresco.service.cmr.repository.TemplateService freemarkerTemplateService, List<String> templatePaths) {
        this.freemarkerTemplateService = freemarkerTemplateService;
        this.templatePaths = templatePaths;
    }

    public String processFreeMarkerTemplate(String templateId, Map<String, Object> map) {
        String fullPath = getPath(templateId);
        return freemarkerTemplateService.processTemplate(fullPath, map);
    }

    public String getPath(String templateId) {
        String template = templateId.replace('.', '/') + ".ftl";
        String fullPath = null;
        for (String templatePath : templatePaths) {
            fullPath = templatePath + "/" + template;
            ClassPathResource cp = new ClassPathResource(fullPath);
            if (cp.exists()) {
                break;
            }
        }
        if (fullPath == null) {
            throw new AlfrescoRuntimeException("Not able to build template path for: " + templateId);
        }
        return fullPath;
    }

    public org.alfresco.service.cmr.repository.TemplateService getFreemarkerTemplateService() {
        return freemarkerTemplateService;
    }

}
