package com.nexperia.alfresco.drawing.actions;

import com.nexperia.alfresco.drawing.model.DrawingModel;
import com.nexperia.alfresco.drawing.utils.DrawingUtil;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.action.executer.ActionExecuterAbstractBase;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.action.Action;
import org.alfresco.service.cmr.action.ParameterDefinition;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.stream.Collectors;

public class DirectReviseAction extends ActionExecuterAbstractBase {

    public static final Logger logger = LoggerFactory.getLogger(DirectReviseAction.class);

    ServiceRegistry serviceRegistry;
    DrawingUtil drawingUtil;

    @Override
    protected void executeImpl(Action action, NodeRef nodeRef) {
        logger.debug("Direct revise action started!");
        NodeRef drawing = drawingUtil.getDrawingOfRendition(nodeRef);
        if (drawing == null) {
            logger.debug("RenditionTypeSetAction: Parent Node of {} is not a drawing [{}].", drawingUtil.getName(nodeRef), nodeRef);
            return;
        }
        String drawingName = drawingUtil.getName(drawing);
        String renditionName = drawingUtil.getName(nodeRef);
        String drawingStatus = drawingUtil.getStatus(drawing);
        String primaryFormat = drawingUtil.getPrimaryFormat(drawing);
        String renditionExtension = FilenameUtils.getExtension(renditionName);
        if (DrawingModel.STATUS_RELEASED.equals(drawingStatus)
                && StringUtils.isNotEmpty(primaryFormat)
                && primaryFormat.equalsIgnoreCase(renditionExtension)) {
            logger.debug("Uploaded the Primary format on Released Drawing '{}'. Running revise action with minor version incrementation.", drawingName);
            List<NodeRef> renditions = serviceRegistry.getNodeService().getChildAssocs(drawing).stream().map(ChildAssociationRef::getChildRef).collect(Collectors.toList());
            serviceRegistry.getNodeService().setProperty(drawing, DrawingModel.PROP_STATUS, DrawingModel.STATUS_DRAFT);
            logger.debug("{} drawing job status updated to {}", drawingName, DrawingModel.STATUS_DRAFT);
            for (NodeRef rendition : renditions) {
                serviceRegistry.getNodeService().setProperty(rendition, DrawingModel.PROP_STATUS, DrawingModel.STATUS_DRAFT);
                String rendName = (String) serviceRegistry.getNodeService().getProperty(rendition, ContentModel.PROP_NAME);
                logger.debug("{} rendition status updated to {}", rendName, DrawingModel.STATUS_DRAFT);
            }

            NodeService nodeService = serviceRegistry.getNodeService();
            String drawingVersion = (String) nodeService.getProperty(drawing, DrawingModel.PROP_DRAWING_VERSION);
            String newDrawingVersion = DrawingUtil.incrementVersion(drawingVersion, false);
            nodeService.setProperty(drawing, DrawingModel.PROP_DRAWING_VERSION, newDrawingVersion);

            logger.debug("{} drawing job is revised.", drawingName);
        }
    }

    @Override
    protected void addParameterDefinitions(List<ParameterDefinition> list) {

    }

    public void setDrawingUtil(DrawingUtil drawingUtil) {
        this.drawingUtil = drawingUtil;
    }

    public void setServiceRegistry(ServiceRegistry serviceRegistry) {
        this.serviceRegistry = serviceRegistry;
    }
}
