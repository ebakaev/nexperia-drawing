package com.nexperia.alfresco.drawing.webscripts;

import com.nexperia.alfresco.drawing.model.DrawingModel;
import org.alfresco.error.AlfrescoRuntimeException;
import org.alfresco.model.ContentModel;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.model.FileInfo;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.extensions.webscripts.AbstractWebScript;
import org.springframework.extensions.webscripts.WebScriptException;
import org.springframework.extensions.webscripts.WebScriptRequest;
import org.springframework.extensions.webscripts.WebScriptResponse;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ChangeTypeWebScript extends AbstractWebScript {

    protected final Logger logger = LoggerFactory.getLogger(getClass());
    private ServiceRegistry serviceRegistry;

    @Override
    public void execute(WebScriptRequest webScriptRequest, WebScriptResponse webScriptResponse) throws IOException {
        String nodeRefString = webScriptRequest.getParameter("nodeRef");
        if (nodeRefString == null) {
            throw new WebScriptException(400, "nodeRef is mandatory parameter");
        }
        NodeRef nodeRef = new NodeRef(nodeRefString);
        Map<String, Object> result = new HashMap<>();
        QName type = serviceRegistry.getNodeService().getType(nodeRef);
        if (ContentModel.TYPE_FOLDER.equals(type)) {
            if (checkParents(nodeRef) && checkChildrens(nodeRef)) {
                serviceRegistry.getNodeService().setType(nodeRef, DrawingModel.TYPE_DRAWING);
                logger.debug("Folder successfully converted to Drawing space");
                result.put("msg", "Success");
            }
        } else {
            logger.debug("Only Folder can be converted to Drawing space");
            result.put("msg", "Only Folder can be converted to Drawing space");
        }

        try {
            JSONObject tomJsonObj = new JSONObject();
            tomJsonObj.put("msg", result.get("msg"));
            String jsonString = tomJsonObj.toString();
            webScriptResponse.getWriter().write(jsonString);
        } catch (JSONException e) {
            throw new WebScriptException("Unable to serialize JSON response");
        }
    }

    private boolean checkChildrens(NodeRef nodeRef) {
        List<FileInfo> children = serviceRegistry.getFileFolderService().listFolders(nodeRef);
        for (FileInfo fileInfo : children) {
            QName type = serviceRegistry.getNodeService().getType(fileInfo.getNodeRef());
            if (DrawingModel.TYPE_DRAWING.equals(type)) {
                throw new AlfrescoRuntimeException("There is a Drawing space in this folder!");
            } else {
                checkChildrens(fileInfo.getNodeRef());
            }
        }
        return true;
    }

    private boolean checkParents(NodeRef nodeRef) {
        NodeRef parent = serviceRegistry.getNodeService().getPrimaryParent(nodeRef).getParentRef();
        QName type = serviceRegistry.getNodeService().getType(parent);
        String name = (String) serviceRegistry.getNodeService().getProperty(parent, ContentModel.PROP_NAME);
        if (DrawingModel.TYPE_DRAWING.equals(type)) {
            throw new AlfrescoRuntimeException("Drawing space cannot be created under another Drawing space!");
        } else if (name.equals("documentLibrary")) {
            return true;
        } else {
            return checkParents(parent);
        }
    }

    public void setServiceRegistry(ServiceRegistry serviceRegistry) {
        this.serviceRegistry = serviceRegistry;
    }
}
