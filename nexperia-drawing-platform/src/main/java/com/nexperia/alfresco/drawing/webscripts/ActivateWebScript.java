package com.nexperia.alfresco.drawing.webscripts;

import com.nexperia.alfresco.drawing.model.DrawingModel;
import com.nexperia.alfresco.drawing.service.StructureService;
import com.nexperia.alfresco.drawing.utils.DrawingUtil;
import com.nexperia.alfresco.drawing.utils.SearchUtils;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.apache.http.HttpStatus;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.extensions.webscripts.AbstractWebScript;
import org.springframework.extensions.webscripts.WebScriptException;
import org.springframework.extensions.webscripts.WebScriptRequest;
import org.springframework.extensions.webscripts.WebScriptResponse;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class ActivateWebScript extends AbstractWebScript {

    protected final Logger logger = LoggerFactory.getLogger(getClass());

    private ServiceRegistry serviceRegistry;
    private DrawingUtil drawingUtil;
    private String createdSpacePath;
    private StructureService structureService;

    @Override
    public void execute(WebScriptRequest webScriptRequest, WebScriptResponse webScriptResponse) throws IOException {
        NodeService nodeService = serviceRegistry.getNodeService();
        String nrStr = webScriptRequest.getParameter("nodeRef");
        NodeRef drawingNodeRef = new NodeRef(nrStr);
        if (!drawingUtil.isDrawing(drawingNodeRef)) {
//            throw new WebScriptException(HttpStatus.SC_BAD_REQUEST, "Drawing type object should be provided!");
            logger.debug("Node with {} nodeRef is not Drawing. Skipping...", drawingNodeRef);
            return;
        }
        if (!nodeService.hasAspect(drawingNodeRef, DrawingModel.ASPECT_ARCHIVED)) {
            logger.debug("Drawing with {} nodeRef is not archived to be activated. Skipping...", drawingNodeRef);
            return;
        }
        String drawingName = drawingUtil.getName(drawingNodeRef);
        logger.debug(drawingName + " is Activating");

        //moving node to Archive space
        NodeRef targetSpace = structureService.getTargetSpace(drawingName, drawingUtil.getType(drawingNodeRef),
                drawingUtil.getMAGCode(drawingNodeRef), drawingUtil.getCreatedFor(drawingNodeRef));
        nodeService.moveNode(drawingNodeRef, targetSpace != null ? targetSpace : getCreatedClonedSpaceLocation(), null, null);
        //marking by this aspect to indicate in Share UI
        nodeService.removeAspect(drawingNodeRef, DrawingModel.ASPECT_ARCHIVED);
        nodeService.setProperty(drawingNodeRef, DrawingModel.PROP_STATUS, DrawingModel.STATUS_DRAFT);
        logger.debug("{} status is updated to {}", drawingName, DrawingModel.STATUS_DRAFT);
        Date now = new Date();
        nodeService.setProperty(drawingNodeRef, DrawingModel.PROP_LAST_STATUS_CHANGED_DATE, now);
        logger.debug("{} last status changed is updated to {}", drawingName, now);

        /* Rendition status update */
        List<NodeRef> renditions = nodeService.getChildAssocs(drawingNodeRef).stream().map(ChildAssociationRef::getChildRef).collect(Collectors.toList());
        for (NodeRef rendition : renditions) {
            serviceRegistry.getNodeService().setProperty(rendition, DrawingModel.PROP_STATUS, DrawingModel.STATUS_DRAFT);
            logger.debug("{} status updated to {}", drawingUtil.getName(rendition), DrawingModel.STATUS_DRAFT);
        }

        try {
            JSONObject tomJsonObj = new JSONObject();
            tomJsonObj.put("msg", "Drawing '" + drawingName + "' was successfully activated.");
            String jsonString = tomJsonObj.toString();
            webScriptResponse.getWriter().write(jsonString);
        } catch (JSONException e) {
            throw new WebScriptException("Unable to serialize JSON response");
        }
    }

    private NodeRef getCreatedClonedSpaceLocation() {
        return SearchUtils.getSpaceLocation(createdSpacePath, serviceRegistry);
    }

    public void setServiceRegistry(ServiceRegistry serviceRegistry) {
        this.serviceRegistry = serviceRegistry;
    }

    public void setCreatedSpacePath(String createdSpacePath) {
        this.createdSpacePath = createdSpacePath;
    }

    public void setDrawingUtil(DrawingUtil drawingUtil) {
        this.drawingUtil = drawingUtil;
    }

    public void setStructureService(StructureService structureService) {
        this.structureService = structureService;
    }
}