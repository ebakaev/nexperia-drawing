package com.nexperia.alfresco.drawing.service.exceptions;

public class DrawingExistsException extends Exception {
    public DrawingExistsException(String message) {
        super(message);
    }
}
