package com.nexperia.alfresco.drawing.webscripts;

import com.nexperia.alfresco.drawing.dto.PreviewResponse;
import com.nexperia.alfresco.drawing.service.DrawingService;
import org.springframework.extensions.webscripts.AbstractWebScript;
import org.springframework.extensions.webscripts.WebScriptRequest;
import org.springframework.extensions.webscripts.WebScriptResponse;

import java.io.IOException;
import java.io.OutputStream;

public class PreviewByRenditionWebScript extends AbstractWebScript {

	private DrawingService drawingService;

	@Override
	public void execute(WebScriptRequest req, WebScriptResponse res) throws IOException {
		String drawingId = req.getParameter("drawingId");
		if (drawingId == null) {
			String nodeId = req.getParameter("nodeId");
			drawingId = drawingService.getDrawingId(nodeId);
		}
		PreviewResponse previewResponse = drawingService.getPreviewByRendition(drawingId);
		res.setContentType(previewResponse.getContentType());
		OutputStream out = res.getOutputStream();
		out.write(previewResponse.getContent());
		out.flush();
		out.close();
	}

	public void setDrawingService(DrawingService drawingService) {
		this.drawingService = drawingService;
	}
}
