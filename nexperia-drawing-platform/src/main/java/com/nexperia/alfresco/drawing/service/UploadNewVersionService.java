package com.nexperia.alfresco.drawing.service;

import org.alfresco.model.ContentModel;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.repository.ContentWriter;
import org.alfresco.service.cmr.repository.NodeRef;

import java.io.IOException;
import java.io.InputStream;

public class UploadNewVersionService {

    ServiceRegistry serviceRegistry;
//	private TicketComponent ticketComponent;
//	private String uploadEndpoint;

    public void upload(InputStream contentStream, NodeRef target, String mimeType) throws IOException {
        ContentWriter writer = serviceRegistry.getContentService().getWriter(target, ContentModel.PROP_CONTENT, true);
        writer.setMimetype(mimeType);
        writer.setEncoding("UTF-8");
        writer.putContent(contentStream);
//		String ticket = ticketComponent.getCurrentTicket(AuthenticationUtil.getFullyAuthenticatedUser(), true);
//		String fullUrl = uploadEndpoint + nodeId + "/content?alf_ticket=" + ticket;
//		File tempFile = DrawingFileUtils.writeToFile(contentStream);
//		HttpHeaders headers = new HttpHeaders();
//		headers.setContentType(MediaType.valueOf(mimeType));
//		byte[] binary = Files.readAllBytes(tempFile.toPath());
//		RestTemplate restTemplate = new RestTemplate();
//		restTemplate.exchange(fullUrl, HttpMethod.PUT, new HttpEntity<>(binary, headers), String.class);
    }

    //	public void setTicketComponent(TicketComponent ticketComponent) {
//		this.ticketComponent = ticketComponent;
//	}
//
//	public void setUploadEndpoint(String uploadEndpoint) {
//		this.uploadEndpoint = uploadEndpoint;
//	}
//
    public void setServiceRegistry(ServiceRegistry serviceRegistry) {
        this.serviceRegistry = serviceRegistry;
    }
}
