package com.nexperia.alfresco.drawing.remote.impl;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import com.nexperia.alfresco.drawing.remote.client.PublicationClient;
import com.nexperia.alfresco.drawing.remote.client.Response;
import com.nexperia.alfresco.drawing.remote.exception.InvalidResponseException;
import com.nexperia.alfresco.drawing.remote.exception.RemotePublicationError;
import com.nexperia.alfresco.drawing.remote.exception.WebScriptClientException;
import com.nexperia.alfresco.drawing.remote.publication.InternalParameterConstants;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.BeansException;

import org.springframework.extensions.surf.exception.ConnectorServiceException;
import org.springframework.extensions.webscripts.connector.Connector;
import org.springframework.extensions.webscripts.connector.ConnectorService;

/**
 * Publication client which invokes remote Publication 
 * WebScript. This client uses alfresco-webscript-framework
 * to invoke the WebScript.
 * 
 * @author byaminov
 *
 */
public class PublicationClientImpl extends AuthenticatingClientImpl
			implements PublicationClient, InternalParameterConstants {

	private static final Log logger = LogFactory.getLog(PublicationClientImpl.class);
	
	protected Map<Integer, RemotePublicationError> code2error;
	protected String webscriptUrl = "/nxp/spider/publication";
	
	protected PublishRequest publish = new PublishRequest();
	protected UpdateRequest update = new UpdateRequest();
	
	public PublicationClientImpl() throws BeansException, ConnectorServiceException {
		super();
	}
	
	public PublicationClientImpl(Connector connector, String username, String password) {
		super(connector, username, password);
	}

	public PublicationClientImpl(ConnectorService connectorService, String endpointId, String username, String password)
			throws ConnectorServiceException {
		super(connectorService, endpointId, username, password);
	}

	public PublicationClientImpl(String endpointId, String username, String password) 
			throws BeansException, ConnectorServiceException {
		super(endpointId, username, password);
	}

	public PublicationClientImpl(String[] springContextPaths, String endpointId, String username, 
			String password) throws BeansException, ConnectorServiceException {
		super(springContextPaths, endpointId, username, password);
	}

	protected void init() {
		code2error = new HashMap<Integer, RemotePublicationError>();
		for (RemotePublicationError error : RemotePublicationError.values()) {
			code2error.put(error.getCode(), error);
		}
	};

	/**
	 * @see {@link PublicationClient#publish(InputStream, Map)}
	 */
	public String publish(InputStream content, Map<String, String> metadata)
				throws WebScriptClientException {
		return publish(content, metadata, null);
	}

	/**
	 * @see {@link PublicationClient#publish(InputStream, Map, boolean))}
	 */
	public String publish(InputStream content, Map<String, String> metadata, 
			Map<String, String> publicationParameters) throws WebScriptClientException {
		return publish.doRequest(content, metadata, publicationParameters);
	}

	/**
	 * @see {@link PublicationClient#update(String, InputStream, Map)}
	 */
	public boolean update(String id, InputStream content, Map<String, String> metadata) 
				throws WebScriptClientException {
		return update(id, content, metadata, null);
	}

	/**
	 * @see {@link PublicationClient#update(String, InputStream, Map, boolean)}
	 */
	public boolean update(String id, InputStream content, Map<String, String> metadata, 
			Map<String, String> publicationParameters) throws WebScriptClientException {
		// Check parameters
		if (StringUtils.isEmpty(id)) {
			throw new IllegalArgumentException("ID provided is empty");
		}
		
		// Prepare request
		metadata.put(PARAM_ID, id);
		
		return update.doRequest(content, metadata, publicationParameters);
	}

	/**
	 * Gets response result code.
	 * @param res parsed response result.
	 * @return result code
	 * @throws InvalidResponseException thrown in case if code is not
	 * present in response or cannot be parsed as a number.
	 */
	protected int getCode(Map<String, Object> res) throws InvalidResponseException {
		String codeString = (String) res.get(KEY_CODE_ID);
		try {
			return Integer.parseInt(codeString);
		} catch (NumberFormatException e) {
			throw new InvalidResponseException("Could not parse code: " + codeString, e);
		}
	}
	
	public void setWebscriptUrl(String webscriptUrl) {
		this.webscriptUrl = webscriptUrl;
	}

	
	/** Class to aggregate commonalities in publish and update requests */
	private abstract class RemoteRequest<T> {
		public T doRequest(InputStream content, Map<String, String> metadata, 
				Map<String, String> publicationParameters) throws WebScriptClientException {
			// Check parameters
			if (metadata == null) {
				throw new IllegalArgumentException("Metadata must not be null");
			}
			
			// Prepare request
			Map<String, String> params = new HashMap<String, String>();
			params.putAll(metadata);
			params.put(PARAM_COMMAND, getCommandName());
			if (content == null) {
				params.put(PARAM_NO_CONTENT, String.valueOf(true));
			}
			if (publicationParameters != null) {
				params.putAll(publicationParameters);
			}
			
			Response response = PublicationClientImpl.this.callWebScript(webscriptUrl, content, params);
			Map<String, Object> res = PublicationClientImpl.this.parseJSONObject(response);

			try {
				int code = getCode(res);
				if (code == 0) {
					// Success
					String key = getResultKey();
					String stringValue = (String) res.get(key);
					if (StringUtils.isEmpty(stringValue)) {
						throw new InvalidResponseException("Response is successful, but has no '" + 
								key + "' parameter or its value is empty");
					} else {
						if (logger.isDebugEnabled()) {
							logger.debug("Got successful response, result: " + stringValue);
						}
						return getResult(stringValue);
					}
				} else {
					// Failure
					RemotePublicationError error = code2error.get(code);
					if (error == null) {
						if (logger.isWarnEnabled()) {
							logger.warn("Got unknown error code: " + code);
						}
						error = RemotePublicationError.OTHER;
					}
					throw error.getPublicationException(res);
				}
			} catch (WebScriptClientException e) {
				if (logger.isErrorEnabled()) {
					logger.error("Remote publication failed. Request was: " + dumpRequest(
							webscriptUrl, params));
				}
				throw e;
			}
		}

		protected abstract String getCommandName();
		protected abstract String getResultKey();
		protected abstract T getResult(String stringValue);
		
	}
	
	private class PublishRequest extends RemoteRequest<String> {
		@Override
		protected String getCommandName() {
			return PARAM_COMMAND_VALUE_PUBLISH;
		}
		@Override
		protected String getResult(String stringValue) {
			return stringValue;
		}
		@Override
		protected String getResultKey() {
			return KEY_PUBLISHED_COPY_ID;
		}
	}

	private class UpdateRequest extends RemoteRequest<Boolean> {
		@Override
		protected String getCommandName() {
			return PARAM_COMMAND_VALUE_UPDATE;
		}
		@Override
		protected Boolean getResult(String stringValue) {
			return Boolean.parseBoolean(stringValue);
		}
		@Override
		protected String getResultKey() {
			return KEY_NEW_VERSION_WAS_PUBLISHED;
		}
	}
}
