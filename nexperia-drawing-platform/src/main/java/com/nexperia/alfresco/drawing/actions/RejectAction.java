package com.nexperia.alfresco.drawing.actions;

import com.nexperia.alfresco.drawing.model.DrawingModel;
import com.nexperia.alfresco.drawing.service.EmailService;
import com.nexperia.alfresco.drawing.utils.DrawingUtil;
import org.alfresco.repo.forum.CommentService;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.action.Action;
import org.alfresco.service.cmr.repository.NodeRef;

import java.util.List;

public class RejectAction extends BaseFlowAction {

	protected RejectAction(EmailService emailService, ServiceRegistry serviceRegistry, CommentService commentService, String emailSubject, String emailTemplate, String baseUrl, DrawingUtil drawingUtil) {
		super(emailService, serviceRegistry, commentService, emailSubject, emailTemplate, baseUrl, drawingUtil);
	}

	@Override
	protected boolean isAllowed(NodeRef drawing) {
		return serviceRegistry.getNodeService().getProperty(drawing, DrawingModel.PROP_STATUS).equals(DrawingModel.STATUS_PENDING_APPROVAL);
	}

	@Override
	protected void executeImpl(Action action, NodeRef drawing) {
		super.executeAction(action, drawing, super.getRenditions(drawing));
	}

	@Override
	protected void updateStatus(NodeRef drawing, List<NodeRef> renditions) {
		serviceRegistry.getNodeService().setProperty(drawing, DrawingModel.PROP_STATUS, DrawingModel.STATUS_DRAFT);
	}

	@Override
	protected void updateVersion(Action action, NodeRef drawing) {
		//Nothing
	}
}
