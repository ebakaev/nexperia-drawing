package com.nexperia.alfresco.drawing.remote.exception;



/**
 * Exception thrown connection to remote publication webscript 
 * failed because of timeout, unknown host, etc.
 * 
 * @author byaminov
 *
 */
public class ConnectionException extends WebScriptClientException {

	private static final long serialVersionUID = -714882968966976663L;
	
	public ConnectionException(String message, Throwable cause) {
		super(message, cause);
	}

}
