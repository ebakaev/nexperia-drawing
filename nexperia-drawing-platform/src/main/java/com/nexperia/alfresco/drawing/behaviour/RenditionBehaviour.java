package com.nexperia.alfresco.drawing.behaviour;

import com.nexperia.S3Client;
import com.nexperia.alfresco.drawing.model.DrawingModel;
import com.nexperia.alfresco.drawing.service.PublishToWebService;
import com.nexperia.alfresco.drawing.utils.DrawingUtil;
import org.alfresco.error.AlfrescoRuntimeException;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.node.NodeServicePolicies;
import org.alfresco.repo.policy.Behaviour;
import org.alfresco.repo.policy.JavaBehaviour;
import org.alfresco.repo.policy.PolicyComponent;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.repository.ContentReader;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.Map;

public class RenditionBehaviour implements NodeServicePolicies.OnUpdatePropertiesPolicy/*, VersionServicePolicies.OnCreateVersionPolicy*/ {

    public static final Logger logger = LoggerFactory.getLogger(RenditionBehaviour.class);

    private final PolicyComponent policyComponent;
    private final ServiceRegistry serviceRegistry;
    DrawingUtil drawingUtil;
    S3Client s3Client;
    PublishToWebService publishService;
    static final String VERSION_PARAM = "revision-version";

    public RenditionBehaviour(PolicyComponent policyComponent, ServiceRegistry serviceRegistry, DrawingUtil drawingUtil) {
        this.policyComponent = policyComponent;
        this.serviceRegistry = serviceRegistry;
        this.drawingUtil = drawingUtil;
    }

    public void init() {
        this.policyComponent.bindClassBehaviour(
                NodeServicePolicies.OnUpdatePropertiesPolicy.QNAME,
                DrawingModel.TYPE_RENDITION,
                new JavaBehaviour(this, "onUpdateProperties", Behaviour.NotificationFrequency.EVERY_EVENT)
        );
    }

    @Override
    public void onUpdateProperties(NodeRef nodeRef, Map<QName, Serializable> before, Map<QName, Serializable> after) {
        String oldName = (String) before.get(ContentModel.PROP_NAME);
        String newName = (String) after.get(ContentModel.PROP_NAME);
        String oldPtW = (String) before.get(DrawingModel.PROP_PUBLISH_TO_WEB);
        String newPtW = (String) after.get(DrawingModel.PROP_PUBLISH_TO_WEB);

        if (oldName == null || newName == null) {
            return;
        }
        if (notExists(nodeRef)) {
            logger.debug("onUpdateProperties: Node {} does not exist.", nodeRef);
            return;
        }
        if (isWorkingCopy(nodeRef)) {
            logger.debug("onUpdateProperties: Node {} is working copy.", nodeRef);
            return;
        }
        if (!StringUtils.equals(oldName, newName)) {
            throw new AlfrescoRuntimeException("Different file name!");
        }
        if (!StringUtils.equals(oldPtW, newPtW)) {
            NodeRef parentNr = serviceRegistry.getNodeService().getPrimaryParent(nodeRef).getParentRef();
            boolean isDrawingReleased = StringUtils.equals(DrawingModel.STATUS_RELEASED, (String) serviceRegistry.getNodeService().getProperty(parentNr, DrawingModel.PROP_STATUS));
            boolean isNotSecret = drawingUtil.isNotSecret(parentNr);
            /* Obsolete action */
            if (!StringUtils.equals(oldPtW, newPtW) && isDrawingReleased) {
                if (StringUtils.equals(newPtW, "No")) {
                    logger.debug("Performing obsolete of {} rendition", newName);
                    s3Client.makeDocumentObsolete(drawingUtil.getName(nodeRef), drawingUtil.getType(nodeRef));
                    publishService.makeRenditionObsolete(nodeRef);
                } else if (StringUtils.equals(newPtW, "Yes")) {
                    if (isNotSecret) {
                        logger.debug("Performing release of {} rendition", newName);
                        ContentReader contentReader = serviceRegistry.getContentService().getReader(nodeRef, ContentModel.PROP_CONTENT);
                        s3Client.releaseDocument(drawingUtil.getName(nodeRef), contentReader.getContentInputStream(), drawingUtil.getType(nodeRef));
                        publishService.publishRendition(nodeRef);
                    } else {
                        logger.debug("Drawing security status is Secret, skipping {} rendition release", newName);
                    }
                }
            }
        }
    }

    private boolean isWorkingCopy(NodeRef nodeRef) {
        return serviceRegistry.getNodeService().hasAspect(nodeRef, ContentModel.ASPECT_WORKING_COPY);
    }

    private boolean notExists(NodeRef nodeRef) {
        return !serviceRegistry.getNodeService().exists(nodeRef);
    }

    private boolean isNotContent(NodeRef nodeRef) {
        return !serviceRegistry.getNodeService().getType(nodeRef).equals(ContentModel.TYPE_CONTENT);
    }

    public void setS3Client(S3Client s3Client) {
        this.s3Client = s3Client;
    }

    public void setPublishService(PublishToWebService publishService) {
        this.publishService = publishService;
    }
}
