package com.nexperia.alfresco.drawing.dto;

import lombok.Builder;
import lombok.Data;
import lombok.NonNull;

@Data
@Builder
public class DrawingRepresentation {
    @NonNull
    private String nodeRef;
    private String drawingId;
    private String drawingType;
    private String primaryFormat;
    private String drawingVersion;
    private String createdFor;
    private String securityStatus;
    private String magCode;
    private String requester;
    private String deliveryDate;
    private String requiredRenditions;
    private String descriptiveTitle;
    private String success;
    private String modified;
    private String lastStatusChangedDate;
    private String baseUrl;
    private String drawingStatus;
}
