package com.nexperia.alfresco.drawing.service;

import com.nexperia.alfresco.drawing.model.DrawingModel;
import com.nexperia.alfresco.drawing.transformer.CustomTransformService;
import com.nexperia.alfresco.drawing.utils.DrawingUtil;
import org.alfresco.error.AlfrescoRuntimeException;
import org.alfresco.model.ContentModel;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.repository.ContentReader;
import org.alfresco.service.cmr.repository.ContentService;
import org.alfresco.service.cmr.repository.ContentWriter;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.namespace.QName;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Stream;

public class CreateRenditionService {

    public static final Logger logger = LoggerFactory.getLogger(CreateRenditionService.class);

    private final UploadNewVersionService uploadNewVersionService;
    private final ServiceRegistry serviceRegistry;
    private final CustomTransformService imageToImage;
    private final CustomTransformService imageToPdf;
    private final CustomTransformService pdfToImage;
    private final boolean isUsePDFFLYForRenditions;
    DrawingUtil drawingUtil;
    PDFFLYService pdfflyService;

    public CreateRenditionService(UploadNewVersionService uploadNewVersionService,
                                  ServiceRegistry serviceRegistry,
                                  CustomTransformService imageToImage,
                                  CustomTransformService imageToPdf,
                                  CustomTransformService pdfToImage,
                                  DrawingUtil drawingUtil,
                                  boolean isUsePDFFLYForRenditions,
                                  PDFFLYService pdfflyService) {
        this.uploadNewVersionService = uploadNewVersionService;
        this.serviceRegistry = serviceRegistry;
        this.imageToImage = imageToImage;
        this.imageToPdf = imageToPdf;
        this.pdfToImage = pdfToImage;
        this.drawingUtil = drawingUtil;
        this.isUsePDFFLYForRenditions = isUsePDFFLYForRenditions;
        this.pdfflyService = pdfflyService;
    }

    public void create(NodeRef renditionSource, NodeRef drawing) {
        String drawingName = drawingUtil.getName(drawing);
        String renditionExtension = drawingUtil.getFileExtension(renditionSource);
        NodeRef existingRendition = drawingUtil.findExistingRendition(drawing, renditionExtension);
        try {
            if (existingRendition == null) {
                changeRenditionSourceName(renditionSource, renditionExtension);
                NodeRef newRendition = createRendition(renditionSource, drawing, drawingName + "." + renditionExtension, false);
                createOrUpdateRequiredRenditions(drawing, newRendition);
            } else {
                uploadNewVersion(renditionSource, existingRendition, renditionExtension);
                createOrUpdateRequiredRenditions(drawing, existingRendition);
            }
        } finally {
            deleteRenditionSource(renditionSource);
        }
        serviceRegistry.getNodeService().setProperty(drawing, DrawingModel.PROP_IS_ALLOWED_NEXT_STEP, true);
    }

    private void setRenditionType(NodeRef nodeRef) {
        serviceRegistry.getNodeService().setType(nodeRef, DrawingModel.TYPE_RENDITION);
    }

    private void changeRenditionSourceName(NodeRef renditionSource, String sourceExtension) {
        String newName = UUID.randomUUID().toString() + "." + sourceExtension;
        serviceRegistry.getNodeService().setProperty(renditionSource, ContentModel.PROP_NAME, newName);
    }

    private void deleteRenditionSource(NodeRef renditionSource) {
        serviceRegistry.getNodeService().deleteNode(renditionSource);
    }

    @SuppressWarnings("unchecked")
    private void createOrUpdateRequiredRenditions(NodeRef drawing, NodeRef source) {
        String primaryFormat = (String) serviceRegistry.getNodeService().getProperty(drawing, DrawingModel.PROP_PRIMARY_FORMAT);
        String fileExtension = drawingUtil.getFileExtension(source);

        if (!StringUtils.equalsIgnoreCase(primaryFormat, fileExtension)) return;

        List<String> requiredRenditions = (List<String>) serviceRegistry.getNodeService().getProperty(drawing, DrawingModel.PROP_REQUIRED_RENDITIONS);
        if (requiredRenditions == null || requiredRenditions.isEmpty()) return;
        logger.debug("There're {} required renditions", requiredRenditions.size());

        String drawingName = drawingUtil.getName(drawing);
        requiredRenditions.stream()
                .map(String::toLowerCase)
                .flatMap(ext -> Stream.of(ext.split(",")))
                .filter(ext -> !StringUtils.equalsIgnoreCase(ext, primaryFormat))
                .forEach(ext -> {
                    NodeRef existingRendition = drawingUtil.findExistingRendition(drawing, ext);
                    if (existingRendition == null) {
                        logger.debug("Create new '{}' rendition", ext);
                        createRendition(source, drawing, drawingName + "." + ext, true);
                    } else {
                        logger.debug("Updating content for '{}' rendition", ext);
                        InputStream content = getTransformedContent(source, ext);
                        uploadNewVersion(content, existingRendition, ext);
                    }
                });
    }

    private void uploadNewVersion(NodeRef source, NodeRef existingRendition, String fileExtension) {
        ContentService contentService = serviceRegistry.getContentService();
        ContentReader reader = contentService.getReader(source, ContentModel.PROP_CONTENT);
        try (InputStream inputStream = reader.getContentInputStream()) {
            String mimeType = serviceRegistry.getMimetypeService().getMimetype(fileExtension);
            uploadNewVersionService.upload(inputStream, existingRendition, mimeType);
        } catch (Exception e) {
            throw new AlfrescoRuntimeException("Upload new version failed: " + e.getMessage());
        }
    }

    private void uploadNewVersion(InputStream source, NodeRef existingRendition, String fileExtension) {
        try (InputStream inputStream = source) {
            String mimeType = serviceRegistry.getMimetypeService().getMimetype(fileExtension);
            uploadNewVersionService.upload(inputStream, existingRendition, mimeType);
        } catch (Exception e) {
            throw new AlfrescoRuntimeException("Upload new version failed: " + e.getMessage());
        }
    }

    private NodeRef createRendition(NodeRef sourceRendition, NodeRef drawing, String targetName, boolean withTransform) {
        NodeRef targetRendition = createRenditionNodeRef(drawing, targetName);
        if (withTransform) {
            copyNodeContent(sourceRendition, targetRendition);
        } else {
            copyContent(sourceRendition, targetRendition);
        }
        setRenditionType(targetRendition);
        return targetRendition;
    }

    public NodeRef createRendition(NodeRef primaryFormat, String renditionExtension) {
        NodeRef drawing = drawingUtil.getDrawingOfRendition(primaryFormat);
        String drawingName = drawingUtil.getName(drawing);
        return createRendition(primaryFormat, drawing, drawingName + "." + renditionExtension, true);
    }

    private NodeRef createRenditionNodeRef(NodeRef drawing, String targetName) {
        Map<QName, Serializable> targetProps = new HashMap<>();
        targetProps.put(ContentModel.PROP_NAME, targetName);
        return serviceRegistry.getNodeService()
                .createNode(drawing,
                        ContentModel.ASSOC_CONTAINS,
                        QName.createQName(NamespaceService.CONTENT_MODEL_1_0_URI, targetName),
                        DrawingModel.TYPE_RENDITION,
                        targetProps).getChildRef();
    }

    private void copyContent(NodeRef sourceRendition, NodeRef targetRendition) {
        String targetName = drawingUtil.getName(targetRendition);
        ContentReader contentReader = serviceRegistry.getContentService().getReader(sourceRendition, ContentModel.PROP_CONTENT);
        try (InputStream sourceStream = contentReader.getContentInputStream()) {
            ContentWriter contentWriter = serviceRegistry.getContentService().getWriter(targetRendition, ContentModel.PROP_CONTENT, true);
            contentWriter.guessMimetype(targetName);
            contentWriter.putContent(sourceStream);
        } catch (IOException e) {
            throw new AlfrescoRuntimeException("Copy content failed: " + e.getMessage());
        }
    }

    private void copyNodeContent(NodeRef sourceRendition, NodeRef targetRendition) {
        try {
            String targetName = drawingUtil.getName(targetRendition);
            String targetExtension = drawingUtil.getFileExtension(targetName);
            ContentWriter contentWriter = serviceRegistry.getContentService().getWriter(targetRendition, ContentModel.PROP_CONTENT, true);
            contentWriter.guessMimetype(targetName);
            contentWriter.putContent(getTransformedContent(sourceRendition, targetExtension));
        } catch (Exception e) {
            // passes the failed transformations, to don't interrupt the upload of EPS.
            // For now we have a problem to generate SVG for some EPS files
            logger.error("Transform content failed: " + e.getMessage(), e);
//            throw new AlfrescoRuntimeException("Transform content failed: " + e.getMessage());
        }
    }

    private ByteArrayInputStream getTransformedContent(NodeRef source, String targetExtension) {
        String srcExtension = drawingUtil.getFileExtension(source);
        byte[] binary;
        if (StringUtils.equalsIgnoreCase(srcExtension, "pdf")) {
            binary = pdfToImage.transform(source, targetExtension);
        } else if (StringUtils.equalsIgnoreCase(targetExtension, "pdf")) {
            binary = imageToPdf.transform(source, targetExtension);
        } else {
            logger.debug("PDFFLY enabled => {}", isUsePDFFLYForRenditions);
            if (isUsePDFFLYForRenditions && srcExtension.equalsIgnoreCase(DrawingModel.FORMAT_EPS)) {
                logger.debug("Using PDFFLY");
                try {
                    binary = FileUtils.readFileToByteArray(pdfflyService.getConvertedFile(source, targetExtension));
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            } else {
                logger.debug("Using ImageMagic");
                binary = imageToImage.transform(source, targetExtension);
            }
        }
        return new ByteArrayInputStream(binary);
    }

}

