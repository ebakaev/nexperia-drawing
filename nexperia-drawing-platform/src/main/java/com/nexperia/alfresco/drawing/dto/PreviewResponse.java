package com.nexperia.alfresco.drawing.dto;

public class PreviewResponse {
	private final byte[] content;
	private final String contentType;

	public PreviewResponse(byte[] content, String contentType) {
		this.content = content;
		this.contentType = contentType;
	}

	public byte[] getContent() {
		return content;
	}

	public String getContentType() {
		return contentType;
	}
}
