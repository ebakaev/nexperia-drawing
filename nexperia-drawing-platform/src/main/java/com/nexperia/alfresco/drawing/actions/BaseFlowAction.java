package com.nexperia.alfresco.drawing.actions;

import com.nexperia.alfresco.drawing.model.DrawingModel;
import com.nexperia.alfresco.drawing.service.EmailService;
import com.nexperia.alfresco.drawing.utils.DrawingUtil;
import org.alfresco.error.AlfrescoRuntimeException;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.action.ParameterDefinitionImpl;
import org.alfresco.repo.action.executer.ActionExecuterAbstractBase;
import org.alfresco.repo.forum.CommentService;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.action.Action;
import org.alfresco.service.cmr.action.ParameterDefinition;
import org.alfresco.service.cmr.dictionary.DataTypeDefinition;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.stream.Collectors;

public abstract class BaseFlowAction extends ActionExecuterAbstractBase {

    private static final Logger logger = LoggerFactory.getLogger(BaseFlowAction.class);

    static final String SEND_TO_PARAM = "send-to";
    static final String COMMENT_PARAM = "comment";
    static final String VERSION_PARAM = "revision-version";
    static final String NO_REPLY = "no-reply@nexperia.com";

    protected final EmailService emailService;
    protected final ServiceRegistry serviceRegistry;
    protected final CommentService commentService;
    protected final String emailSubject;
    protected final String emailTemplate;
    protected final String baseUrl;
    protected final DrawingUtil drawingUtil;

    protected BaseFlowAction(EmailService emailService, ServiceRegistry serviceRegistry, CommentService commentService, String emailSubject, String emailTemplate, String baseUrl, DrawingUtil drawingUtil) {
        this.emailService = emailService;
        this.serviceRegistry = serviceRegistry;
        this.commentService = commentService;
        this.emailSubject = emailSubject;
        this.emailTemplate = emailTemplate;
        this.baseUrl = baseUrl;
        this.drawingUtil = drawingUtil;
    }

    protected void executeAction(Action action, NodeRef drawing, List<NodeRef> renditions) {
        if (isAllowed(drawing)) {
            updateStatus(drawing, renditions);
            updateVersion(action, drawing);
            serviceRegistry.getNodeService().setProperty(drawing, DrawingModel.PROP_LAST_STATUS_CHANGED_DATE, new Date());
            commentAndEmail(action, drawing, renditions);
        }
    }

    @SuppressWarnings("unchecked")
    private void commentAndEmail(Action action, NodeRef drawing, List<NodeRef> renditions) {
        String comment = (String) action.getParameterValue(COMMENT_PARAM);
        List<String> sendToList =  drawingUtil.getEmails(action.getParameterValue(SEND_TO_PARAM));
        createComment(comment, drawing);
        if (this.name.equals(DrawingModel.ACTION_RELEASE)
                || this.name.equals(DrawingModel.ACTION_APPROVE_AND_RELEASE)
                || this.name.equals(DrawingModel.ACTION_REJECT)) {
            renditions = null;
        } else if (this.name.equals(DrawingModel.ACTION_APPROVE)) {
            renditions = drawingUtil.getRequiredRenditions(drawing);
        }
        sendEmail(sendToList, drawing, renditions, comment);
    }

    protected List<NodeRef> getRenditions(NodeRef drawing) {
        NodeService nodeService = serviceRegistry.getNodeService();
        return nodeService.getChildAssocs(drawing).stream().filter(childAssociationRef -> DrawingModel.TYPE_RENDITION.equals(nodeService.getType(childAssociationRef.getChildRef())))
                .map(ChildAssociationRef::getChildRef)
                .collect(Collectors.toList());
    }

    private void createComment(String comment, NodeRef nodeRef) {
        if (StringUtils.isNotBlank(comment)) {
            commentService.createComment(nodeRef, String.valueOf(new Date()), comment, false);
        }
    }

    private void sendEmail(List<String> sendToList, NodeRef drawing, List<NodeRef> renditions, String comment) {
        if (sendToList == null || sendToList.isEmpty()) {
            logger.debug("SendToList is empty...");
            return;
        }
        try {
            String username = AuthenticationUtil.getFullyAuthenticatedUser();
            NodeRef currentUser = serviceRegistry.getPersonService().getPerson(username);
            String from = drawingUtil.getEmail(currentUser);
//            List<String> toEmails = sendToList.stream().map(this::getEmail).collect(Collectors.toList());
            List<String> toEmails = sendToList;
            List<String> ccEmails = new ArrayList<>();
            ccEmails.add(from);
            List<String> userFullNames = sendToList.stream().map(drawingUtil::getFullUserName).collect(Collectors.toList());
            Map<String, Object> model = createTemplateModel(drawing, comment, userFullNames);
            String subject = model.get("drawingName") + " " + emailSubject;
            logger.debug("noReply: {},toMails: {}, ccMails: {}, emailSubject: {}",NO_REPLY, toEmails.toArray(), ccEmails.toArray(), subject);
            emailService.send(NO_REPLY, toEmails, ccEmails, subject, emailTemplate, model, renditions);
        } catch (Exception e) {
            e.printStackTrace();
            throw new AlfrescoRuntimeException(e.getMessage());
        }
    }

    @Override
    protected void addParameterDefinitions(List<ParameterDefinition> paramList) {
        paramList.add(
                new ParameterDefinitionImpl(
                        SEND_TO_PARAM,
                        DataTypeDefinition.TEXT,
                        false,
                        getParamDisplayLabel(SEND_TO_PARAM),
                        false)
        );
        paramList.add(
                new ParameterDefinitionImpl(
                        COMMENT_PARAM,
                        DataTypeDefinition.TEXT,
                        false,
                        getParamDisplayLabel(COMMENT_PARAM),
                        false)
        );
        paramList.add(
                new ParameterDefinitionImpl(
                        VERSION_PARAM,
                        DataTypeDefinition.TEXT,
                        false,
                        getParamDisplayLabel(VERSION_PARAM),
                        false)
        );
    }

    protected Map<String, Object> createTemplateModel(NodeRef drawing, String comment, List<String> userFullNames) {
        Map<String, Object> model = new HashMap<>();
        if (!StringUtils.isEmpty(comment)) {
            model.put("comment", comment);
        }
        model.put("userFullNames", String.join(",", userFullNames));
        model.put("performer", String.join(",", drawingUtil.getFullUserNameAndEmail(AuthenticationUtil.getFullyAuthenticatedUser())));
        model.put("drawingName", serviceRegistry.getNodeService().getProperty(drawing, ContentModel.PROP_NAME));
        model.put("drawingLink", baseUrl + "/share/page/site/tdm-drawing/folder-details?nodeRef=workspace://SpacesStore/" + drawing.getId());
        return model;
    }

    protected abstract boolean isAllowed(NodeRef drawing);

    protected abstract void updateStatus(NodeRef drawing, List<NodeRef> renditions);

    protected abstract void updateVersion(Action action, NodeRef drawing);
}
