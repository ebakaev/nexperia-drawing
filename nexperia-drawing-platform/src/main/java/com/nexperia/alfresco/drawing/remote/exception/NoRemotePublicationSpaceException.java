package com.nexperia.alfresco.drawing.remote.exception;



/**
 * Exception thrown when remote Alfresco does not have
 * Publication space configured.
 * 
 * @author byaminov
 *
 */
public class NoRemotePublicationSpaceException extends WebScriptClientException {

	private static final long serialVersionUID = 2914183897986608690L;

	public NoRemotePublicationSpaceException(String message) {
		super(message);
	}

}
