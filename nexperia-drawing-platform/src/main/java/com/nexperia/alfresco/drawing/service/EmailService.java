package com.nexperia.alfresco.drawing.service;

import org.alfresco.error.AlfrescoRuntimeException;
import org.alfresco.model.ContentModel;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.repository.ContentReader;
import org.alfresco.service.cmr.repository.NodeRef;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.util.ByteArrayDataSource;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class EmailService {

    private static final Logger logger = LoggerFactory.getLogger(EmailService.class);

    private final JavaMailSender javaMailSender;
    private final TemplateService templateService;
    private final ServiceRegistry serviceRegistry;

    public EmailService(JavaMailSender javaMailSender, TemplateService templateService, ServiceRegistry serviceRegistry) {
        this.javaMailSender = javaMailSender;
        this.templateService = templateService;
        this.serviceRegistry = serviceRegistry;
    }

    public void send(String from, List<String> toList, List<String> ccList, String subject, String templatePath, Map<String, Object> model, List<NodeRef> renditions) {
        if (toList == null || toList.isEmpty()) {
            return;
        }
        String body = generateBody(templatePath, model);
        List<InternetAddress> to = toList.stream().map(this::getAddress).collect(Collectors.toList());
        List<InternetAddress> cc = ccList != null && !ccList.isEmpty() ? ccList.stream().map(this::getAddress).collect(Collectors.toList()) : null;
        send(from, to, cc, subject, body, renditions);
    }

    private String generateBody(String templatePath, Map<String, Object> model) {
        return templateService.processFreeMarkerTemplate(templatePath, model);
    }

    private InternetAddress getAddress(String email) {
        try {
            return new InternetAddress(email);
        } catch (AddressException e) {
            throw new AlfrescoRuntimeException("Invalid email address: " + email);
        }
    }

    private void send(String from, List<InternetAddress> toList, List<InternetAddress> ccList, String subject, String body, List<NodeRef> renditions) {
        try {
            MimeMessage msg = javaMailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(msg, true);
            toList.forEach(ia -> addTo(helper, ia));
            if (ccList != null) {
                ccList.forEach(ia -> cc(helper, ia));
            }
            addAttachments(helper, renditions);
            helper.setText(body, true);
            helper.setSubject(subject);
            helper.setFrom(from);
            javaMailSender.send(msg);
            logger.debug("Email has been sent: from={}, to={}, cc={}, subject={}, body={}", from, toList.toArray(), msg.getAllRecipients(), subject, body);
        } catch (MessagingException e) {
            throw new AlfrescoRuntimeException("Failed to send email: " + e.getMessage());
        }
    }

    private void addAttachments(MimeMessageHelper helper, List<NodeRef> renditions) {
        if (renditions == null || renditions.isEmpty()) {
            return;
        }
        renditions.forEach(nodeRef -> {
            ContentReader contentReader = serviceRegistry.getContentService().getReader(nodeRef, ContentModel.PROP_CONTENT);
            if (contentReader == null) {
                return;
            }
            try (InputStream inputStream = contentReader.getContentInputStream()) {
                String name = (String) serviceRegistry.getNodeService().getProperty(nodeRef, ContentModel.PROP_NAME);
                ByteArrayDataSource source = new ByteArrayDataSource(inputStream, contentReader.getMimetype());
                addAttachment(helper, source, name);
            } catch (IOException e) {
                throw new AlfrescoRuntimeException("Failed to obtain input stream: " + e.getMessage());
            }
        });
    }

    private void addAttachment(MimeMessageHelper helper, ByteArrayDataSource source, String name) {
        try {
            helper.addAttachment(name, source);
        } catch (MessagingException e) {
            throw new AlfrescoRuntimeException("Failed to add attachment: " + e.getMessage());
        }
    }

    private void addTo(MimeMessageHelper helper, InternetAddress ia) {
        try {
            helper.addTo(ia);
        } catch (MessagingException e) {
            throw new AlfrescoRuntimeException("Add to list error: " + e.getMessage());
        }
    }

    private void cc(MimeMessageHelper helper, InternetAddress ia) {
        try {
            helper.addCc(ia);
        } catch (MessagingException e) {
            throw new AlfrescoRuntimeException("Add to cc error: " + e.getMessage());
        }
    }

}
