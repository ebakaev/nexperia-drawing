package com.nexperia.alfresco.drawing.utils;

import com.amazonaws.services.dynamodbv2.xspec.S;
import com.google.gson.JsonObject;
import com.nexperia.alfresco.drawing.model.DrawingModel;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.namespace.QName;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class DrawingUtil {

    ServiceRegistry serviceRegistry;
    NodeService nodeService;
    DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
    DateFormat dateTimeFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    public final static String NODE_REF_SPACE_NAME = "workspace://SpacesStore/";

    public static String incrementVersion(String version, boolean isMajor) {
        int major = Integer.valueOf(version.substring(0, version.lastIndexOf(".")));
        int minor = Integer.valueOf(version.substring(version.lastIndexOf(".") + 1, version.length()));
        if (isMajor) {
            return ++major + ".0";
        } else {
            return major + "." + ++minor;
        }
    }

    public String getName(NodeRef nodeRef) {
        return (String) getProperty(nodeRef, ContentModel.PROP_NAME);
    }

    public String getType(NodeRef nodeRef) {
        String documentType = (String) getProperty(nodeRef, DrawingModel.PROP_DRAWING_TYPE);
        if (StringUtils.isNotBlank(documentType)) {
            return documentType;
        } else {
            NodeRef parent = serviceRegistry.getNodeService().getPrimaryParent(nodeRef).getParentRef();
            return (String) getProperty(parent, DrawingModel.PROP_DRAWING_TYPE);
        }
    }

    public String getMAGCode(NodeRef nodeRef) {
        return (String) getProperty(nodeRef, DrawingModel.PROP_MAG_CODE);
    }

    public String getCreatedFor(NodeRef nodeRef) {
        return (String) getProperty(nodeRef, DrawingModel.PROP_CREATED_FOR);
    }

    public String getStatus(NodeRef nodeRef) {
        return (String) getProperty(nodeRef, DrawingModel.PROP_STATUS);
    }

    public String getPrimaryFormat(NodeRef nodeRef) {
        return (String) getProperty(nodeRef, DrawingModel.PROP_PRIMARY_FORMAT);
    }

    public Object getProperty(NodeRef nodeRef, QName qName) {
        return nodeService.getProperty(nodeRef, qName);
    }

    public void setProperty(NodeRef nodeRef, QName qName, Serializable value) {
        nodeService.setProperty(nodeRef, qName, value);
    }

    public NodeRef getDrawingOfRendition(NodeRef rendition) {
        NodeRef parent = nodeService.getPrimaryParent(rendition).getParentRef();
        return nodeService.getType(parent).equals(DrawingModel.TYPE_DRAWING) ? parent : null;
    }

    public boolean exists(NodeRef nodeRef) {
        return !serviceRegistry.getNodeService().exists(nodeRef);
    }

    public NodeRef findExistingRendition(NodeRef drawing, String fileExtension) {
        return serviceRegistry.getNodeService()
                .getChildAssocs(drawing)
                .stream()
                .map(ChildAssociationRef::getChildRef)
                .filter(this::isRendition)
                .filter(nodeRef -> StringUtils.equalsIgnoreCase(getFileExtension(nodeRef), fileExtension))
                .findFirst()
                .orElse(null);
    }

    public boolean isRendition(NodeRef nodeRef) {
        return serviceRegistry.getNodeService().getType(nodeRef).equals(DrawingModel.TYPE_RENDITION);
    }

    public boolean isDrawing(NodeRef nodeRef) {
        return serviceRegistry.getNodeService().getType(nodeRef).equals(DrawingModel.TYPE_DRAWING);
    }

    public String getFileExtension(String fileName) {
        return FilenameUtils.getExtension(fileName);
    }

    public String getFileExtension(NodeRef nodeRef) {
        String name = (String) serviceRegistry.getNodeService().getProperty(nodeRef, ContentModel.PROP_NAME);
        return getFileExtension(name);
    }

    public void setServiceRegistry(ServiceRegistry serviceRegistry) {
        this.serviceRegistry = serviceRegistry;
    }

    public void setNodeService(NodeService nodeService) {
        this.nodeService = nodeService;
    }

    public List<NodeRef> getRenditionsFromDrawing(NodeRef drawing) {
        return nodeService.getChildAssocs(drawing).stream()
                .filter(childAssociationRef -> DrawingModel.TYPE_RENDITION.equals(nodeService.getType(childAssociationRef.getChildRef())))
                .map(ChildAssociationRef::getChildRef)
                .collect(Collectors.toList());
    }

    public List<NodeRef> getReadyToPublishRenditionsFromDrawing(NodeRef drawing) {
        return nodeService.getChildAssocs(drawing).stream()
                .filter(childAssociationRef ->
                        (DrawingModel.TYPE_RENDITION.equals(nodeService.getType(childAssociationRef.getChildRef()))
                                && DrawingModel.PTW_YES.equals(nodeService.getProperty(childAssociationRef.getChildRef(), DrawingModel.PROP_PUBLISH_TO_WEB))))
                .map(ChildAssociationRef::getChildRef)
                .collect(Collectors.toList());
    }

    public List<NodeRef> getReleasedRenditionsFromDrawing(NodeRef drawing) {
        return nodeService.getChildAssocs(drawing).stream()
                .filter(childAssociationRef ->
                        (DrawingModel.TYPE_RENDITION.equals(nodeService.getType(childAssociationRef.getChildRef()))
                                && DrawingModel.STATUS_RELEASED.equals(nodeService.getProperty(childAssociationRef.getChildRef(), DrawingModel.PROP_STATUS))))
                .map(ChildAssociationRef::getChildRef)
                .collect(Collectors.toList());
    }

    public NodeRef getSpaceLocation(String path) {
        return SearchUtils.getSpaceLocation(path, serviceRegistry);
    }

    public boolean isNotSecret(NodeRef drawing) {
        return DrawingModel.SECURITY_STATUS_COMPANY_PUBLIC.equals((String) getProperty(drawing, DrawingModel.PROP_SECURITY_STATUS));
    }

    public String getFullUserName(NodeRef user) {
        return serviceRegistry.getPersonService().getPerson(user).getFirstName() + " " +
                serviceRegistry.getPersonService().getPerson(user).getLastName();
    }

    public String getFullUserName(String userName) {
        String username = AuthenticationUtil.getFullyAuthenticatedUser();
        NodeRef currentUser = serviceRegistry.getPersonService().getPerson(username);
        StringBuilder sb = new StringBuilder();
        sb.append(serviceRegistry.getPersonService().getPerson(currentUser).getFirstName());
        sb.append(" ");
        sb.append(serviceRegistry.getPersonService().getPerson(currentUser).getLastName());
        return sb.toString();
    }

    public String getFullUserNameAndEmail(String userName) {
        String username = AuthenticationUtil.getFullyAuthenticatedUser();
        NodeRef currentUser = serviceRegistry.getPersonService().getPerson(username);
        StringBuilder sb = new StringBuilder();
        sb.append(serviceRegistry.getPersonService().getPerson(currentUser).getFirstName());
        sb.append(" ");
        sb.append(serviceRegistry.getPersonService().getPerson(currentUser).getLastName());
        sb.append(" (");
        sb.append(userName);
        sb.append(")");
        return sb.toString();
    }

    public List<NodeRef> getRequiredRenditions(NodeRef drawing) {
        List<String> requiredRenditions = (List<String>) serviceRegistry.getNodeService().getProperty(drawing, DrawingModel.PROP_REQUIRED_RENDITIONS);
        if (requiredRenditions == null) {
            return null;
        }
        List<NodeRef> renditions = getRenditionsFromDrawing(drawing);
        return renditions.stream().filter(nodeRef -> requiredRenditions.contains(getFileExtension(nodeRef))).collect(Collectors.toList());
    }

    public List<String> getRequiredRenditionsAsStringList(NodeRef drawing) {
        return (List<String>) serviceRegistry.getNodeService().getProperty(drawing, DrawingModel.PROP_REQUIRED_RENDITIONS);
    }

    public String getRequiredRenditionsAsString(NodeRef drawing) {
        List<String> requiredRenditions = getRequiredRenditionsAsStringList(drawing);
        return requiredRenditions != null ? String.join(",", requiredRenditions) : "";
    }

    public boolean isAllowNextStep(NodeRef drawing) {
        if (serviceRegistry.getNodeService().getProperty(drawing, DrawingModel.PROP_IS_ALLOWED_NEXT_STEP) == null) {
            serviceRegistry.getNodeService().setProperty(drawing, DrawingModel.PROP_IS_ALLOWED_NEXT_STEP, false);
            return false;
        } else {
            return (boolean) serviceRegistry.getNodeService().getProperty(drawing, DrawingModel.PROP_IS_ALLOWED_NEXT_STEP);
        }
    }

    public boolean isRevisedOnce(NodeRef drawing) {
        if (serviceRegistry.getNodeService().getProperty(drawing, DrawingModel.PROP_IS_REVISED_ONCE) == null) {
            serviceRegistry.getNodeService().setProperty(drawing, DrawingModel.PROP_IS_REVISED_ONCE, false);
            return false;
        } else {
            return (boolean) serviceRegistry.getNodeService().getProperty(drawing, DrawingModel.PROP_IS_REVISED_ONCE);
        }
    }

    public boolean isValidEmailAddress(String email) {
        boolean result = true;
        try {
            InternetAddress emailAddr = new InternetAddress(email);
            emailAddr.validate();
        } catch (AddressException ex) {
            result = false;
        }
        return result;
    }

    public List<String> getEmails(Serializable parameterValue) {
        List<String> emailsList = new ArrayList<>();
        if (parameterValue == null) {
            return emailsList;
        }
        String emailsString = (String) parameterValue;
        Matcher m = Pattern.compile("[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+").matcher(emailsString);
        while (m.find()) {
            emailsList.add(m.group());
        }
        return emailsList;
    }

    public String getDescriptiveTitle(NodeRef nodeRef) {
        return (String) getProperty(nodeRef, DrawingModel.PROP_DESCRIPTIVE_TITLE);
    }

    public String getSecurityStatus(NodeRef nodeRef) {
        return (String) getProperty(nodeRef, DrawingModel.PROP_SECURITY_STATUS);
    }

    public String getDrawingVersion(NodeRef nodeRef) {
        return (String) getProperty(nodeRef, DrawingModel.PROP_DRAWING_VERSION);
    }
    public String getReleasedVersions(NodeRef nodeRef) {
        return (String) getProperty(nodeRef, DrawingModel.PROP_RELEASE_VERSIONS);
    }

    public String getRequester(NodeRef nodeRef) {
        return (String) getProperty(nodeRef, DrawingModel.PROP_REQUESTER);
    }

    public String getDeliveryDate(NodeRef nodeRef) {
        Object deliveryDate = getProperty(nodeRef, DrawingModel.PROP_DELIVERY_DATE);
        return deliveryDate != null ? dateFormatter.format(deliveryDate) : "";
    }

    public String getModificationDate(NodeRef nodeRef) {
        Object modifiedDate = getProperty(nodeRef, ContentModel.PROP_MODIFIED);
        return modifiedDate != null ? dateTimeFormatter.format(modifiedDate) : "";
    }

    public String getLastStatusChangedDate(NodeRef nodeRef) {
        Object lastStatusChangedDate = getProperty(nodeRef, DrawingModel.PROP_LAST_STATUS_CHANGED_DATE);
        return lastStatusChangedDate != null ? dateTimeFormatter.format(lastStatusChangedDate) : "";
    }

    public void saveReleaseVersion(NodeRef drawing, NodeRef rendition) {
        String renditionVersion = (String) getProperty(rendition, ContentModel.PROP_VERSION_LABEL);
        String drawingVersion  = getDrawingVersion(drawing);
        String renditionExtension = getFileExtension(rendition);
        String releasedVersions = getReleasedVersions(drawing);
        if (releasedVersions == null) {
            releasedVersions = "{}";
        }
        try {
            JSONObject releasedVersionsJson = new JSONObject(releasedVersions);
            JSONObject releasedVersionsByExtension;
            if (releasedVersionsJson.has(renditionExtension)) {
                releasedVersionsByExtension = (JSONObject) releasedVersionsJson.get(renditionExtension);
            } else {
                releasedVersionsByExtension = new JSONObject();
            }
            releasedVersionsByExtension.put(renditionVersion, drawingVersion);
            releasedVersionsJson.put(renditionExtension, releasedVersionsByExtension);
            setProperty(drawing, DrawingModel.PROP_RELEASE_VERSIONS, releasedVersionsJson.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String getEmail(NodeRef userRef) {
        String email = (String) serviceRegistry.getNodeService().getProperty(userRef, ContentModel.PROP_EMAIL);
        if (StringUtils.isEmpty(email)) {
            email = serviceRegistry.getPersonService().getPerson(userRef).getUserName();
        }
        return email;
    }
}
