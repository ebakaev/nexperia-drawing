package com.nexperia.alfresco.drawing.remote.exception;



/**
 * Exception thrown when an update comes for a document in Publication Space,
 * but new version is not greater than already published one. This can happen
 * in a simple update or during superseding.
 * 
 * @author byaminov
 *
 */
public class NoVersionIncreaseException extends WebScriptClientException {

	private static final long serialVersionUID = 587999735295231302L;

	public NoVersionIncreaseException(String message) {
		super(message);
	}

}
