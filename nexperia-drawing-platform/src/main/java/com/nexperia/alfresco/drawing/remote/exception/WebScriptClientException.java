package com.nexperia.alfresco.drawing.remote.exception;

/**
 * Common parent for exception thrown by remote webscript clients.
 *
 * @author ebakaev
 *
 */
public class WebScriptClientException extends RuntimeException {

    private static final long serialVersionUID = -916785558944801623L;

    public WebScriptClientException() {
        super();
    }

    public WebScriptClientException(String message, Throwable cause) {
        super(message, cause);
    }

    public WebScriptClientException(String message) {
        super(message);
    }

    public WebScriptClientException(Throwable cause) {
        super(cause);
    }

}