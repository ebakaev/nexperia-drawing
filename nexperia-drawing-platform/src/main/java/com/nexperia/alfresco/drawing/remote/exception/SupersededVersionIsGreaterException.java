package com.nexperia.alfresco.drawing.remote.exception;



/**
 * Exception thrown when one of superseded documents has version greater
 * than version of superseding document. It is thrown only in case filename
 * of superseded document differs from name of superseding document. In case they 
 * are equal a {@link NoVersionIncreaseException} is thrown.
 * 
 * @author byaminov
 *
 */
public class SupersededVersionIsGreaterException extends WebScriptClientException {

	private static final long serialVersionUID = -2023067900722054553L;
	
	public SupersededVersionIsGreaterException(String message) {
		super(message);
	}

}
