package com.nexperia.alfresco.drawing.remote.exception;



/**
 * Exception thrown when there is already a published
 * file with the same name.
 * 
 * @author byaminov
 *
 */
public class RemoteFileExistsException extends WebScriptClientException {

	private static final long serialVersionUID = -2745750705485128325L;

	public RemoteFileExistsException(String message) {
		super(message);
	}
}
