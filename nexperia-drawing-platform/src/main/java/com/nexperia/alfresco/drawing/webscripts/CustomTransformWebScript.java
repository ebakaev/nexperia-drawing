package com.nexperia.alfresco.drawing.webscripts;

import com.nexperia.alfresco.drawing.transformer.CustomTransformService;
import org.alfresco.error.AlfrescoRuntimeException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.extensions.webscripts.AbstractWebScript;
import org.springframework.extensions.webscripts.WebScriptRequest;
import org.springframework.extensions.webscripts.WebScriptResponse;

import java.io.IOException;
import java.io.OutputStream;

public class CustomTransformWebScript extends AbstractWebScript {

	private CustomTransformService imageToImage;
	private CustomTransformService imageToPdf;
	private CustomTransformService pdfToImage;

	@Override
	public void execute(WebScriptRequest req, WebScriptResponse res) throws IOException {

		String nodeId = req.getParameter("nodeId");
		String transformTo = req.getParameter("to");
		String type = req.getParameter("type");

		CustomTransformService customTransformService;

		if (StringUtils.equals(type, "image2image")) {
			customTransformService = imageToImage;
		} else if (StringUtils.equals(type, "image2pdf")) {
			customTransformService = imageToPdf;
		} else if (StringUtils.equals(type, "pdf2image")) {
			customTransformService = pdfToImage;
		} else {
			throw new AlfrescoRuntimeException("Invalid transform type: " + type);
		}

		byte[] binary = customTransformService.transform(nodeId, transformTo);
		OutputStream out = res.getOutputStream();
		out.write(binary);
		out.flush();
		out.close();
	}

	public void setImageToImage(CustomTransformService imageToImage) {
		this.imageToImage = imageToImage;
	}

	public void setImageToPdf(CustomTransformService imageToPdf) {
		this.imageToPdf = imageToPdf;
	}

	public void setPdfToImage(CustomTransformService pdfToImage) {
		this.pdfToImage = pdfToImage;
	}
}
