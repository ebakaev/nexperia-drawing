package com.nexperia.alfresco.drawing.transformer;

import com.nexperia.alfresco.drawing.utils.DrawingFileUtils;
import org.alfresco.error.AlfrescoRuntimeException;
import org.alfresco.model.ContentModel;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.repository.ContentReader;
import org.alfresco.service.cmr.repository.ContentService;
import org.alfresco.service.cmr.repository.NodeRef;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.*;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.io.File;
import java.io.InputStream;

public class CustomTransformService {

	private ServiceRegistry serviceRegistry;

	private String transformEndpoint;

	public byte[] transform(NodeRef nodeRef, String targetExtension) {
		ContentService contentService = serviceRegistry.getContentService();
		ContentReader contentReader = contentService.getReader(nodeRef, ContentModel.PROP_CONTENT);
		return transform(contentReader.getContentInputStream(), targetExtension);
	}

	public byte[] transform(String nodeId, String targetExtension) {
		NodeRef nodeRef = new NodeRef(nodeId);
		return transform(nodeRef, targetExtension);
	}

	public byte[] transform(InputStream contentStream, String targetExtension) {
		try {
			File tempFile = DrawingFileUtils.writeToFile(contentStream);
			FileSystemResource fileSystemResource = new FileSystemResource(tempFile);

			MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
			body.add("file", fileSystemResource);
			body.add("targetExtension", targetExtension);

			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.MULTIPART_FORM_DATA);

			ResponseEntity<byte[]> response = new RestTemplate().exchange(transformEndpoint, HttpMethod.POST, new HttpEntity<>(body, headers), byte[].class);
			return response.getBody();
		} catch (Exception e) {
			throw new AlfrescoRuntimeException(e.getMessage());
		}
	}

	public void setServiceRegistry(ServiceRegistry serviceRegistry) {
		this.serviceRegistry = serviceRegistry;
	}

	public void setTransformEndpoint(String transformEndpoint) {
		this.transformEndpoint = transformEndpoint;
	}
}
