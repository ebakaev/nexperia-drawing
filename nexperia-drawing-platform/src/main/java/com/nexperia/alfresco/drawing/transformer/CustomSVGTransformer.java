package com.nexperia.alfresco.drawing.transformer;

import com.nexperia.alfresco.drawing.model.DrawingModel;
import com.nexperia.alfresco.drawing.service.PDFFLYService;
import com.nexperia.alfresco.drawing.utils.DrawingUtil;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.content.transform.AbstractContentTransformer2;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.repository.ContentReader;
import org.alfresco.service.cmr.repository.ContentWriter;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.TransformationOptions;
import org.alfresco.util.TempFileProvider;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

import static org.alfresco.repo.content.MimetypeMap.MIMETYPE_IMAGE_JPEG;
import static org.alfresco.repo.content.MimetypeMap.MIMETYPE_IMAGE_SVG;

public class CustomSVGTransformer extends AbstractContentTransformer2 {

    protected final Logger logger = LoggerFactory.getLogger(getClass());

    private CustomTransformService customTransformService;
    PDFFLYService pdfflyService;
    DrawingUtil drawingUtil;
    ServiceRegistry serviceRegistry;

    @Override
    public boolean isTransformable(String sourceMimetype, String targetMimetype, TransformationOptions options) {
        boolean isTransormable = MIMETYPE_IMAGE_SVG.equals(sourceMimetype) && MIMETYPE_IMAGE_JPEG.equals(targetMimetype);
        if (isTransormable) {
            logger.debug("isTransformable: Source mimetype '{}', target mimetype is '{}'", sourceMimetype, targetMimetype);
        }
        return isTransormable;
    }

    @Override
    protected void transformInternal(ContentReader contentReader, ContentWriter contentWriter, TransformationOptions transformationOptions) throws Exception {
        logger.debug("Searching for JPG or for EPS as a primary format");
        NodeRef drawing = drawingUtil.getDrawingOfRendition(transformationOptions.getSourceNodeRef());
        NodeRef jpgRendition = drawingUtil.findExistingRendition(drawing, DrawingModel.FORMAT_JPG);
        File convertedFile = null;
        if (jpgRendition == null) {
            String primaryFormat = drawingUtil.getPrimaryFormat(drawing);
            if (primaryFormat != null && StringUtils.equalsIgnoreCase(DrawingModel.FORMAT_EPS, primaryFormat)) {
                ContentReader primaryFormatContentReader = serviceRegistry.getContentService().getReader(drawingUtil.findExistingRendition(drawing, DrawingModel.FORMAT_EPS), ContentModel.PROP_CONTENT);
                File contentToTransform = TempFileProvider.createTempFile(primaryFormatContentReader.getContentInputStream(), "_source_", "." + DrawingModel.FORMAT_EPS);
                convertedFile = getConvertedFile(contentToTransform);
            }
        } else {
            ContentReader jpgContentReader = serviceRegistry.getContentService().getReader(jpgRendition, ContentModel.PROP_CONTENT);
            convertedFile = TempFileProvider.createTempFile(jpgContentReader.getContentInputStream(), "_source_", "." + DrawingModel.FORMAT_JPG);
        }

        if (convertedFile != null) {
            contentWriter.putContent(convertedFile);
        } else {
            logger.debug("There's no EPS as primary format or JPG generated from EPS");
        }

//        File contentToTransform = TempFileProvider.createTempFile(contentReader.getContentInputStream(), "_source_.", DrawingModel.FORMAT_SVG);
//        File convertedFile = getConvertedFile(contentToTransform, transformationOptions.getSourceNodeRef());
//        logger.debug("Transformed file to write {}", convertedFile);
//        contentWriter.putContent(convertedFile);
//        logger.debug("SVG to JPG transform done!");
    }

    public File getConvertedFile(File source) throws Exception {
        logger.debug("Using PDFFLY");
        File targetFile = pdfflyService.getConvertedFile(source, "jpg");
        logger.debug("Target file is {}", targetFile);
        return targetFile;
    }

    public void setCustomTransformService(CustomTransformService customTransformService) {
        this.customTransformService = customTransformService;
    }

    public void setPdfflyService(PDFFLYService pdfflyService) {
        this.pdfflyService = pdfflyService;
    }

    public void setDrawingUtil(DrawingUtil drawingUtil) {
        this.drawingUtil = drawingUtil;
    }

    public void setServiceRegistry(ServiceRegistry serviceRegistry) {
        this.serviceRegistry = serviceRegistry;
    }
}