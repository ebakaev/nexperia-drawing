package com.nexperia.alfresco.drawing.webscripts.migration;

import com.nexperia.alfresco.drawing.service.StructureService;
import org.alfresco.model.ContentModel;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.action.Action;
import org.alfresco.service.cmr.repository.NodeRef;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.extensions.webscripts.AbstractWebScript;
import org.springframework.extensions.webscripts.WebScriptRequest;
import org.springframework.extensions.webscripts.WebScriptResponse;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class StructureMigratedDrawingsWebScript extends AbstractWebScript {

    public static final Logger logger = LoggerFactory.getLogger(StructureMigratedDrawingsWebScript.class);

    private final String SPACES_STORE_PREFIX = "workspace://SpacesStore/";

    ServiceRegistry serviceRegistry;
    StructureService structureService;
    String drawingsToMove;

    @Override
    public void execute(WebScriptRequest webScriptRequest, WebScriptResponse webScriptResponse) throws IOException {
        logger.debug("STARTING...");
        moveDrawingsFromFileWithNodeRefs();
        logger.debug("Migration FINISHED!");
    }

    private void moveDrawingsFromFileWithNodeRefs() {
        List<String> allLines = null;
        try {
            allLines = Files.readAllLines(Paths.get(drawingsToMove));
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (allLines != null) {
            int size = allLines.size();
            int i = 1;
            int failed = 0;
            logger.debug("There're '" + size + "' Drawings to process.");
            for (String drawingUUID : allLines) {
                NodeRef sourceDrawingNodeRef = new NodeRef(SPACES_STORE_PREFIX + drawingUUID.trim());
                if (serviceRegistry.getNodeService().exists(sourceDrawingNodeRef)) {

                    String drawingName = (String) serviceRegistry.getNodeService().getProperty(sourceDrawingNodeRef, ContentModel.PROP_NAME);
                    logger.debug("[ {} of {} ] Processing {} [{}]", i++, size, drawingName, sourceDrawingNodeRef);
//                    structureService.applyStructureRulesOnDrawing(sourceDrawingNodeRef);
                    Action action = serviceRegistry.getActionService().createAction("structure-rules-action");
                    action.setExecuteAsynchronously(true);
                    serviceRegistry.getActionService().executeAction(action, sourceDrawingNodeRef);
//                    serviceRegistry.getNodeService().moveNode(sourceDrawingNodeRef, targetSpace, null, null);
                } else {
                    failed++;
                    logger.debug(sourceDrawingNodeRef + " doesn't exists!");
                }
            }
            logger.debug(failed + " count of Drawings were not found.");
        }
    }

    public void setServiceRegistry(ServiceRegistry serviceRegistry) {
        this.serviceRegistry = serviceRegistry;
    }

    public void setDrawingsToMove(String drawingsToMove) {
        this.drawingsToMove = drawingsToMove;
    }

    public void setStructureService(StructureService structureService) {
        this.structureService = structureService;
    }
}
