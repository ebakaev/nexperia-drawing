package com.nexperia.alfresco.drawing.remote.exception;

public class EmptyContentException extends WebScriptClientException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4483341357289160369L;

	public EmptyContentException(String message) {
		super(message);
	}
}
