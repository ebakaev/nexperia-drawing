package com.nexperia.alfresco.drawing.utils;

import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.StoreRef;
import org.alfresco.service.cmr.search.ResultSet;
import org.alfresco.service.cmr.search.SearchService;

public class DrawingSearchUtils {

	private DrawingSearchUtils() {}

	public static NodeRef getFirstResult(SearchService searchService, String searchExpression) {
		NodeRef retValue = null;
		ResultSet resultSet = searchService.query(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE, SearchService.LANGUAGE_LUCENE, searchExpression);
		if (resultSet.length() != 0) {
			retValue = resultSet.getNodeRef(0);
		}
		resultSet.close();
		return retValue;
	}
}
