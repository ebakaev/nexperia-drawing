package com.nexperia.alfresco.drawing.service;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.SdkClientException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.DeleteObjectRequest;
import org.alfresco.model.ContentModel;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.repository.ContentReader;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class S3Service {

    public static final Logger logger = LoggerFactory.getLogger(S3Service.class);

    String bucketName;
    String accessKeyValue;
    String secretKeyValue;
    Regions clientRegion = Regions.EU_WEST_1;
    ServiceRegistry serviceRegistry;
    Boolean serviceEnabled;

    public void releaseRendition(NodeRef rendition) {
        if (serviceEnabled) {
            NodeService nodeService = serviceRegistry.getNodeService();

            String renditionName = (String) nodeService.getProperty(rendition, ContentModel.PROP_NAME);
            ContentReader contentReader = serviceRegistry.getContentService().getReader(rendition, ContentModel.PROP_CONTENT);

            try {
                AWSCredentials credentials = new BasicAWSCredentials(accessKeyValue, secretKeyValue);
                AmazonS3 s3Client = AmazonS3ClientBuilder.standard()
                        .withCredentials(new AWSStaticCredentialsProvider(credentials))
                        .withRegion(clientRegion)
                        .build();
                s3Client.putObject(bucketName, renditionName, contentReader.getContentInputStream(), null);
            } catch (AmazonServiceException e) {
                // The call was transmitted successfully, but Amazon S3 couldn't process
                // it, so it returned an error response.
                e.printStackTrace();
            } catch (SdkClientException e) {
                // Amazon S3 couldn't be contacted for a response, or the client
                // couldn't parse the response from Amazon S3.
                e.printStackTrace();
            }

            logger.debug("{} rendition has been sent to Amazon S3", rendition);
        } else {
            logger.debug("S3 service disabled...");
        }
    }

    public void makeObsoleteRendition(String renditionName) {
        if (serviceEnabled) {
            try {
                AWSCredentials credentials = new BasicAWSCredentials(accessKeyValue, secretKeyValue);
                AmazonS3 s3Client = AmazonS3ClientBuilder.standard()
                        .withCredentials(new AWSStaticCredentialsProvider(credentials))
                        .withRegion(clientRegion)
                        .build();

                s3Client.deleteObject(new DeleteObjectRequest(bucketName, renditionName));
            } catch (AmazonServiceException e) {
                // The call was transmitted successfully, but Amazon S3 couldn't process
                // it, so it returned an error response.
                e.printStackTrace();
            } catch (SdkClientException e) {
                // Amazon S3 couldn't be contacted for a response, or the client
                // couldn't parse the response from Amazon S3.
                e.printStackTrace();
            }

            logger.debug("{} rendition has been removed from Amazon S3", renditionName);
        } else {
            logger.debug("S3 service disabled...");
        }
    }

    public void setBucketName(String bucketName) {
        this.bucketName = bucketName;
    }

    public void setAccessKeyValue(String accessKeyValue) {
        this.accessKeyValue = accessKeyValue;
    }

    public void setSecretKeyValue(String secretKeyValue) {
        this.secretKeyValue = secretKeyValue;
    }

    public void setServiceRegistry(ServiceRegistry serviceRegistry) {
        this.serviceRegistry = serviceRegistry;
    }

    public void setServiceEnabled(Boolean serviceEnabled) {
        this.serviceEnabled = serviceEnabled;
    }
}
