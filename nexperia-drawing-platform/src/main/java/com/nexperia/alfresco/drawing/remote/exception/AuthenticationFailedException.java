package com.nexperia.alfresco.drawing.remote.exception;



/**
 * Exception thrown when request to remote webscript
 * failed because of authentication issues. 
 * 
 * @author byaminov
 *
 */
public class AuthenticationFailedException extends WebScriptClientException {

	private static final long serialVersionUID = 5557662577625711219L;

	public AuthenticationFailedException(String message) {
		super(message);
	}

}
