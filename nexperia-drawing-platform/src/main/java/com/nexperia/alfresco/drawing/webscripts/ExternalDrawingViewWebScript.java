package com.nexperia.alfresco.drawing.webscripts;

import com.nexperia.alfresco.drawing.dto.PreviewResponse;
import com.nexperia.alfresco.drawing.service.DrawingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.extensions.webscripts.AbstractWebScript;
import org.springframework.extensions.webscripts.WebScriptException;
import org.springframework.extensions.webscripts.WebScriptRequest;
import org.springframework.extensions.webscripts.WebScriptResponse;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;

public class ExternalDrawingViewWebScript extends AbstractWebScript {

    public static final Logger logger = LoggerFactory.getLogger(ExternalDrawingViewWebScript.class);

    DrawingService drawingService;

    @Override
    public void execute(WebScriptRequest webScriptRequest, WebScriptResponse webScriptResponse) throws IOException {
        String drawingName = webScriptRequest.getParameter("renditionName");
        String version = webScriptRequest.getParameter("version");
        logger.debug("Remote service requested {} rendition.", drawingName);
        if (drawingName == null) {
            throw new WebScriptException(HttpServletResponse.SC_BAD_REQUEST, "'drawingName' parameter is mandatory'");
        }
        PreviewResponse previewResponse = drawingService.getPreviewByRenditionForExternalTools(drawingName, version);
        if (previewResponse == null) {
            throw new WebScriptException(HttpServletResponse.SC_NOT_FOUND, "Rendition wasn't found.");
        }
        webScriptResponse.setContentType(previewResponse.getContentType());
        OutputStream out = webScriptResponse.getOutputStream();
        out.write(previewResponse.getContent());
        out.flush();
        out.close();
    }

    public void setDrawingService(DrawingService drawingService) {
        this.drawingService = drawingService;
    }
}
