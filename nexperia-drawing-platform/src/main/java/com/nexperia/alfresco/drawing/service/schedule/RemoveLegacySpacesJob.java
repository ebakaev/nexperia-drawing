package com.nexperia.alfresco.drawing.service.schedule;

import org.alfresco.error.AlfrescoRuntimeException;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RemoveLegacySpacesJob implements Job {

    public static final Logger logger = LoggerFactory.getLogger(RemoveLegacySpacesJob.class);

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        JobDataMap jobData = context.getJobDetail().getJobDataMap();
        Object legacyJobsCleanerObj = jobData.get("legacyRemoveExecutor");
        if (legacyJobsCleanerObj instanceof RemoveLegacySpacesExecutor) {
            RemoveLegacySpacesExecutor removeLegacySpacesExecutor = (RemoveLegacySpacesExecutor) legacyJobsCleanerObj;
            removeLegacySpacesExecutor.execute();
        } else {
            throw new AlfrescoRuntimeException("RemoveLegacySpacesJob data must contain valid 'legacyRemoveExecutor' reference");
        }
    }

}
