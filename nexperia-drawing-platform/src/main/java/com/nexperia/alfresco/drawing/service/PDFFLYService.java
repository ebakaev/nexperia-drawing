package com.nexperia.alfresco.drawing.service;

import org.alfresco.model.ContentModel;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.repository.ContentReader;
import org.alfresco.service.cmr.repository.ContentService;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.util.TempFileProvider;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PDFFLYService {

    public static final Logger logger = LoggerFactory.getLogger(PDFFLYService.class);

    private static final String TEMP_FILENAME_PREFIX = "tempresfile_";
    private static final String PDFFLY_COMMAND = "pdffly";

    private String path;
    private boolean isServiceEnabled = true;
    ServiceRegistry serviceRegistry;

    public File getConvertedFile(NodeRef nodeRef, String extension) throws Exception {
        if (isServiceEnabled) {
            File sourceFile = TempFileProvider.createTempFile("convert_source_", String.valueOf(new Date().getTime()));
            ContentService contentService = serviceRegistry.getContentService();
            ContentReader contentReader = contentService.getReader(nodeRef, ContentModel.PROP_CONTENT);
            FileUtils.copyInputStreamToFile(contentReader.getContentInputStream(), sourceFile);
            return getConvertedFile(sourceFile, extension);
        } else {
            logger.debug("PDFFLY service disabled...");
        }
        return null;
    }

    public File getConvertedFile(File sourceFile, String extension) throws Exception {
        if (isServiceEnabled) {
            Runtime rt = Runtime.getRuntime();
            List<String> cmd = new ArrayList<String>();
            cmd.add(path + PDFFLY_COMMAND);
            cmd.add("-i");
            cmd.add(sourceFile.getAbsolutePath().replace("\\", "/"));
            String resFile = sourceFile.getAbsolutePath().substring(0, sourceFile.getAbsolutePath().length() - sourceFile.getName().length()) +
                    TEMP_FILENAME_PREFIX + (new Date()).getTime() + "." + extension;
            cmd.add("-o");
            cmd.add(resFile);
            cmd.add("-f");
            cmd.add(extension);
            String[] cmdArgs = cmd.toArray(new String[0]);
            if (logger.isDebugEnabled()) {
                logger.debug("Command will be executed : " + cmd.toString().replaceAll(",", " "));
            }
            Process proc = rt.exec(cmdArgs);

            StreamGobbler errorGobbler = new
                    StreamGobbler(proc.getErrorStream(), "ERROR");

            // any output?
            StreamGobbler outputGobbler = new
                    StreamGobbler(proc.getInputStream(), "OUTPUT");

            // kick them off
            errorGobbler.start();
            outputGobbler.start();

            // any error???
            int exitVal = proc.waitFor();
            if (logger.isDebugEnabled()) {
                logger.debug("Process exitValue: " + exitVal);
            }
            File convertedFile = new File(resFile);
            if (!convertedFile.exists()) {
                throw new FileNotFoundException(resFile + " was not found");
            }

            // success
            if (logger.isDebugEnabled()) {
                logger.debug("PdfFly executed successfully.");
            }
            return convertedFile;
        } else {
            logger.debug("PDFFLY service disabled...");
        }

        return null;
    }

    private class StreamGobbler extends Thread {
        InputStream is;
        String type;
        OutputStream os;

        StreamGobbler(InputStream is, String type) {
            this(is, type, System.out);
        }

        StreamGobbler(InputStream is, String type, OutputStream redirect) {
            this.is = is;
            this.type = type;
            this.os = redirect;
        }

        public void run() {
            try {
                PrintWriter pw = null;
                if (os != null)
                    pw = new PrintWriter(os);

                InputStreamReader isr = new InputStreamReader(is);
                BufferedReader br = new BufferedReader(isr);
                String line;
                while ((line = br.readLine()) != null) {
                    if (pw != null)
                        pw.println(line);
                }
                if (pw != null)
                    pw.flush();
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }
    }


    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public boolean isServiceEnabled() {
        return isServiceEnabled;
    }

    public void setServiceEnabled(boolean serviceEnabled) {
        isServiceEnabled = serviceEnabled;
    }

    public void setServiceRegistry(ServiceRegistry serviceRegistry) {
        this.serviceRegistry = serviceRegistry;
    }
}
