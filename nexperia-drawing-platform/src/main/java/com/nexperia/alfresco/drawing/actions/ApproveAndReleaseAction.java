package com.nexperia.alfresco.drawing.actions;

import com.nexperia.S3Client;
import com.nexperia.alfresco.drawing.model.DrawingModel;
import com.nexperia.alfresco.drawing.service.EmailService;
import com.nexperia.alfresco.drawing.service.PublishToWebService;
import com.nexperia.alfresco.drawing.utils.DrawingUtil;
import org.alfresco.error.AlfrescoRuntimeException;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.forum.CommentService;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.action.Action;
import org.alfresco.service.cmr.repository.ContentReader;
import org.alfresco.service.cmr.repository.NodeRef;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class ApproveAndReleaseAction extends BaseFlowAction {

    public static final Logger logger = LoggerFactory.getLogger(ApproveAndReleaseAction.class);

    S3Client s3Client;
    PublishToWebService publishService;

    protected ApproveAndReleaseAction(EmailService emailService, ServiceRegistry serviceRegistry, CommentService commentService, String emailSubject, String emailTemplate, String baseUrl, DrawingUtil drawingUtil) {
        super(emailService, serviceRegistry, commentService, emailSubject, emailTemplate, baseUrl, drawingUtil);
    }

    @Override
    protected boolean isAllowed(NodeRef drawing) {
        return serviceRegistry.getNodeService().getProperty(drawing, DrawingModel.PROP_STATUS).equals(DrawingModel.STATUS_DRAFT);
    }

    @Override
    protected void executeImpl(Action action, NodeRef drawing) {
        List<NodeRef> renditions = super.getRenditions(drawing);
        logger.debug("Release renditions: " + renditions + " by NR: " + drawingUtil.getName(drawing));
        if (renditions.size() == 0) {
            throw new AlfrescoRuntimeException("There is no rendition uploaded to approve and release!");
        }
        boolean isAllowNextStep = drawingUtil.isAllowNextStep(drawing);
        boolean isRevisedOnce = drawingUtil.isRevisedOnce(drawing);
        if (isRevisedOnce && !isAllowNextStep) {
            throw new AlfrescoRuntimeException("There is no new rendition to approve!");
        }
        super.executeAction(action, drawing, renditions);
    }

    @Override
    protected void updateStatus(NodeRef drawing, List<NodeRef> renditions) {
        String name = drawingUtil.getName(drawing);
        boolean isNotSecret = drawingUtil.isNotSecret(drawing);
        for (NodeRef rendition : renditions) {
            String renditionName = drawingUtil.getName(rendition);
            String ptw = (String) serviceRegistry.getNodeService().getProperty(rendition, DrawingModel.PROP_PUBLISH_TO_WEB);
            //marks all renditions as drawing become released.
            serviceRegistry.getNodeService().setProperty(rendition, DrawingModel.PROP_STATUS, DrawingModel.STATUS_RELEASED);
            drawingUtil.saveReleaseVersion(drawing, rendition);
            logger.debug("{} status updated to {}", renditionName, DrawingModel.STATUS_RELEASED);
            if (DrawingModel.PTW_YES.equals(ptw)) {
                if (isNotSecret) {
                    ContentReader contentReader = serviceRegistry.getContentService().getReader(rendition, ContentModel.PROP_CONTENT);
                    s3Client.releaseDocument(drawingUtil.getName(rendition), contentReader.getContentInputStream(), drawingUtil.getType(rendition));
                } else {
                    logger.debug("Drawing {} is secret, skipping {} rendition publication.", name, drawingUtil.getName(rendition));
                }
            }
        }
        if (isNotSecret) {
            publishService.publishDrawing(drawing);
        } else {
            logger.debug("Drawing {} is secret, skipping drawing publication.", name);
        }
        serviceRegistry.getNodeService().setProperty(drawing, DrawingModel.PROP_STATUS, DrawingModel.STATUS_RELEASED);
        logger.debug(name + " drawing job is released.");
        serviceRegistry.getNodeService().setProperty(drawing, DrawingModel.PROP_IS_ALLOWED_NEXT_STEP, false);
    }

    @Override
    protected void updateVersion(Action action, NodeRef drawing) {
        //Nothing
    }

    public void setS3Client(S3Client s3Client) {
        this.s3Client = s3Client;
    }

    public void setPublishService(PublishToWebService publishService) {
        this.publishService = publishService;
    }
}
