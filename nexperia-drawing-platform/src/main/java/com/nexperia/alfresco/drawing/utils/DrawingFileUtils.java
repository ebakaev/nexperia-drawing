package com.nexperia.alfresco.drawing.utils;

import org.alfresco.util.TempFileProvider;
import org.apache.commons.io.IOUtils;

import java.io.*;

public class DrawingFileUtils {

	private DrawingFileUtils() {}

	public static File writeToFile(InputStream contentStream) throws IOException {
		File tempFile = TempFileProvider.createTempFile(String.valueOf(System.nanoTime()), null);
		try (OutputStream out = new BufferedOutputStream(new FileOutputStream(tempFile))) {
			IOUtils.copy(contentStream, out);
			return tempFile;
		} finally {
			contentStream.close();
		}
	}
}
