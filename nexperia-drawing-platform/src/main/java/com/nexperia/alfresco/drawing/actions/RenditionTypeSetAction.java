package com.nexperia.alfresco.drawing.actions;

import com.nexperia.alfresco.drawing.model.DrawingModel;
import com.nexperia.alfresco.drawing.service.CreateRenditionService;
import com.nexperia.alfresco.drawing.utils.DrawingUtil;
import org.alfresco.error.AlfrescoRuntimeException;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.action.executer.ActionExecuterAbstractBase;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.action.Action;
import org.alfresco.service.cmr.action.ParameterDefinition;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.stream.Collectors;

public class RenditionTypeSetAction extends ActionExecuterAbstractBase {

    public static final Logger logger = LoggerFactory.getLogger(RenditionTypeSetAction.class);

    private final ServiceRegistry serviceRegistry;
    private final CreateRenditionService createRenditionService;
    DrawingUtil drawingUtil;

    public RenditionTypeSetAction(ServiceRegistry serviceRegistry, CreateRenditionService createRenditionService, DrawingUtil drawingUtil) {
        this.serviceRegistry = serviceRegistry;
        this.createRenditionService = createRenditionService;
        this.drawingUtil = drawingUtil;
    }

    @Override
    protected void executeImpl(Action action, NodeRef nodeRef) {
        logger.debug("Rendition type set action started!");
        if (isNotContent(nodeRef)) {
            logger.debug("RenditionTypeSetAction: Node {} is not a content [{}].", drawingUtil.getName(nodeRef), nodeRef);
            return;
        }
        if (drawingUtil.exists(nodeRef)) {
            logger.debug("RenditionTypeSetAction: Node {} does not exist.", nodeRef);
            return;
        }
        if (isThumbnail(nodeRef)) {
            logger.debug("RenditionTypeSetAction: Node {} is a thumbnail [{}].", drawingUtil.getName(nodeRef), nodeRef);
            return;
        }
        NodeRef drawing = drawingUtil.getDrawingOfRendition(nodeRef);
        if (drawing == null) {
            logger.debug("RenditionTypeSetAction: Parent Node of {} is not a drawing [{}].", drawingUtil.getName(nodeRef), nodeRef);
            return;
        }

        String renditionName = drawingUtil.getName(nodeRef);
        String drawingName = drawingUtil.getName(drawing);
        logger.debug("Checking rendition name {} with drawing {}", renditionName, drawingName);
        checkDrawingAndRenditionNames(nodeRef);
        checkIfDrawingShouldBeRevised(drawing, renditionName);

        logger.debug("Creation Rendition {}", renditionName);
        createRenditionService.create(nodeRef, drawing);
    }

    private void checkIfDrawingShouldBeRevised(NodeRef drawing, String renditionName) {
        String drawingName = drawingUtil.getName(drawing);
        String drawingStatus = drawingUtil.getStatus(drawing);
        String primaryFormat = drawingUtil.getPrimaryFormat(drawing);
        String renditionExtension = FilenameUtils.getExtension(renditionName);
        if (DrawingModel.STATUS_RELEASED.equals(drawingStatus)
                && StringUtils.isNotEmpty(primaryFormat)
                && primaryFormat.equalsIgnoreCase(renditionExtension)) {
            logger.debug("Uploaded the Primary format on Released Drawing '{}'. Running revise action with minor version incrementation.", drawingName);
            List<NodeRef> renditions = serviceRegistry.getNodeService().getChildAssocs(drawing).stream().map(ChildAssociationRef::getChildRef).collect(Collectors.toList());
            serviceRegistry.getNodeService().setProperty(drawing, DrawingModel.PROP_STATUS, DrawingModel.STATUS_DRAFT);
            logger.debug("{} drawing job status updated to {}", drawingName, DrawingModel.STATUS_DRAFT);
            for (NodeRef rendition : renditions) {
                serviceRegistry.getNodeService().setProperty(rendition, DrawingModel.PROP_STATUS, DrawingModel.STATUS_DRAFT);
                String rendName = (String) serviceRegistry.getNodeService().getProperty(rendition, ContentModel.PROP_NAME);
                logger.debug("{} rendition status updated to {}", rendName, DrawingModel.STATUS_DRAFT);
            }

            NodeService nodeService = serviceRegistry.getNodeService();
            String drawingVersion = (String) nodeService.getProperty(drawing, DrawingModel.PROP_DRAWING_VERSION);
            String newDrawingVersion = DrawingUtil.incrementVersion(drawingVersion, false);
            nodeService.setProperty(drawing, DrawingModel.PROP_DRAWING_VERSION, newDrawingVersion);

            logger.debug("{} drawing job is revised.", drawingName);
        }
    }

    private void checkDrawingAndRenditionNames(NodeRef nodeRef) {
        String renditionNameWithoutExtension;
        String ext = drawingUtil.getFileExtension(nodeRef);
        if (drawingUtil.findExistingRendition(drawingUtil.getDrawingOfRendition(nodeRef), ext) != null) {
            renditionNameWithoutExtension = FilenameUtils.getBaseName(drawingUtil.getName(nodeRef).replace("-1.", "."));
        } else {
            renditionNameWithoutExtension = FilenameUtils.getBaseName(drawingUtil.getName(nodeRef));
        }
        NodeRef drawing = drawingUtil.getDrawingOfRendition(nodeRef);
        String drawingName = drawingUtil.getName(drawing);
        if (!renditionNameWithoutExtension.equals(drawingName)) {
            throw new AlfrescoRuntimeException("Incorrect rendition name! Please check it.");
        }
        String drawingStatus = drawingUtil.getStatus(drawing);
        String primaryFormat = drawingUtil.getPrimaryFormat(drawing);
        if (!(DrawingModel.STATUS_RELEASED.equals(drawingStatus) && primaryFormat != null && primaryFormat.equalsIgnoreCase(ext)) && !DrawingModel.STATUS_DRAFT.equals(drawingStatus)) {
            throw new AlfrescoRuntimeException("Upload is denied! Please check drawing status.");
        }
    }

    private boolean isNotContent(NodeRef nodeRef) {
        return !serviceRegistry.getNodeService().getType(nodeRef).equals(ContentModel.TYPE_CONTENT);
    }

    private boolean isThumbnail(NodeRef nodeRef) {
        return serviceRegistry.getNodeService().getType(nodeRef).equals(ContentModel.TYPE_THUMBNAIL);
    }

    @Override
    protected void addParameterDefinitions(List<ParameterDefinition> list) {
        // nothing
    }

}