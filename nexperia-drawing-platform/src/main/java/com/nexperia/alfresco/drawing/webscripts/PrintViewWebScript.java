package com.nexperia.alfresco.drawing.webscripts;

import com.nexperia.alfresco.drawing.dto.DrawingRepresentation;
import com.nexperia.alfresco.drawing.utils.DrawingUtil;
import org.alfresco.service.cmr.repository.NodeRef;
import org.springframework.extensions.webscripts.Cache;
import org.springframework.extensions.webscripts.DeclarativeWebScript;
import org.springframework.extensions.webscripts.Status;
import org.springframework.extensions.webscripts.WebScriptRequest;

import java.util.*;
import java.util.stream.Collectors;

public class PrintViewWebScript extends DeclarativeWebScript {

    DrawingUtil drawingUtil;
    String baseUrl;

    @Override
    protected Map<String, Object> executeImpl(WebScriptRequest req, Status status, Cache cache) {
        Map<String, Object> result = new HashMap<String, Object>();
        List<String> drawingNodeRefs = Arrays.stream(req.getParameter("nodeRefs").split(",")).map(nodeRef -> DrawingUtil.NODE_REF_SPACE_NAME + nodeRef).collect(Collectors.toList());
        List<DrawingRepresentation> drawings = drawingNodeRefs.stream().map(drawing -> getDrawingRepresentation(new NodeRef(drawing))).collect(Collectors.toList());

        result.put("drawings", drawings);
        return result;
    }

    private DrawingRepresentation getDrawingRepresentation(NodeRef drawing) {
        return DrawingRepresentation.builder()
                .nodeRef(drawing.toString())
                .drawingId(drawingUtil.getName(drawing))
                .createdFor(drawingUtil.getCreatedFor(drawing))
                .descriptiveTitle(drawingUtil.getDescriptiveTitle(drawing))
                .drawingType(drawingUtil.getType(drawing))
                .magCode(drawingUtil.getMAGCode(drawing))
                .securityStatus(drawingUtil.getSecurityStatus(drawing))
                .drawingVersion(drawingUtil.getDrawingVersion(drawing))
                .primaryFormat(drawingUtil.getPrimaryFormat(drawing))
                .requester(drawingUtil.getRequester(drawing))
                .deliveryDate(drawingUtil.getDeliveryDate(drawing))
                .requiredRenditions(drawingUtil.getRequiredRenditionsAsString(drawing))
                .modified(drawingUtil.getModificationDate(drawing))
                .lastStatusChangedDate(drawingUtil.getLastStatusChangedDate(drawing))
                .drawingStatus(drawingUtil.getStatus(drawing))
                .baseUrl(baseUrl)
                .build();
    }

    public void setDrawingUtil(DrawingUtil drawingUtil) {
        this.drawingUtil = drawingUtil;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }
}
