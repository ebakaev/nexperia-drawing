package com.nexperia.alfresco.drawing.remote.exception;




/**
 * Exception thrown when response returned from Publication WebScript
 * was invalid or unexpected.
 * 
 * @author byaminov
 *
 */
public class InvalidResponseException extends WebScriptClientException {

	private static final long serialVersionUID = 7133646997748237738L;

	public InvalidResponseException(String message) {
		super(message);
	}

	public InvalidResponseException(String message, Throwable e) {
		super(message, e);
	}

}
