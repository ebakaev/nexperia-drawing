package com.nexperia.alfresco.drawing.webscripts;

import com.nexperia.alfresco.drawing.model.DrawingModel;
import com.nexperia.alfresco.drawing.utils.DrawingUtil;
import com.nexperia.alfresco.drawing.utils.SearchUtils;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.model.FileInfo;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.extensions.webscripts.AbstractWebScript;
import org.springframework.extensions.webscripts.WebScriptException;
import org.springframework.extensions.webscripts.WebScriptRequest;
import org.springframework.extensions.webscripts.WebScriptResponse;

import java.io.IOException;
import java.util.Date;

public class ArchiveWebScript extends AbstractWebScript {

    protected final Logger logger = LoggerFactory.getLogger(getClass());

    ServiceRegistry serviceRegistry;
    DrawingUtil drawingUtil;
    String archiveSpacePath;

    @Override
    public void execute(WebScriptRequest webScriptRequest, WebScriptResponse webScriptResponse) throws IOException {
        NodeService nodeService = serviceRegistry.getNodeService();
        String nrStr = webScriptRequest.getParameter("nodeRef");
        NodeRef drawingNodeRef = new NodeRef(nrStr);
        if (!drawingUtil.isDrawing(drawingNodeRef)) {
            logger.debug("Node with {} nodeRef is not Drawing. Skipping...", drawingNodeRef);
            return;
        }
        if (nodeService.hasAspect(drawingNodeRef, DrawingModel.ASPECT_ARCHIVED)) {
            logger.debug("Drawing with {} nodeRef is already archived. Skipping...", drawingNodeRef);
            return;
        }
        String drawingName = drawingUtil.getName(drawingNodeRef);
        logger.debug(drawingName + " is moving to Archive space");


        //moving node to Archive space
        nodeService.moveNode(drawingNodeRef, getArchiveSpaceLocation(), null, null);

        //marking by this aspect to indicate in Share UI
        nodeService.addAspect(drawingNodeRef, DrawingModel.ASPECT_ARCHIVED, null);
        logger.debug("{} is marked by Archived aspect", drawingName);
        nodeService.setProperty(drawingNodeRef, DrawingModel.PROP_STATUS, DrawingModel.STATUS_ARCHIVED);
        logger.debug("{} status is updated to {}", drawingName, DrawingModel.STATUS_ARCHIVED);
        Date now = new Date();
        nodeService.setProperty(drawingNodeRef, DrawingModel.PROP_LAST_STATUS_CHANGED_DATE, now);
        logger.debug("{}: last status changed is updated to {}", drawingName, now);

        //making renditions obsolete
        for (FileInfo rendition : serviceRegistry.getFileFolderService().listFiles(drawingNodeRef)) {
            NodeRef nr = rendition.getNodeRef();
            if (DrawingModel.TYPE_RENDITION.equals(nodeService.getType(nr))) {
                nodeService.setProperty(nr, DrawingModel.PROP_STATUS, DrawingModel.STATUS_ARCHIVED);
            }
            if (DrawingModel.TYPE_RENDITION.equals(nodeService.getType(nr))
                    && DrawingModel.PTW_YES.equals(nodeService.getProperty(nr, DrawingModel.PROP_PUBLISH_TO_WEB))) {
                logger.debug("{} is market as PTW=Yes, making it obsolete.", rendition.getName());
                //marking by this aspect to indicate in Share UI
                nodeService.addAspect(drawingNodeRef, DrawingModel.ASPECT_ARCHIVED, null);
                nodeService.setProperty(drawingNodeRef, DrawingModel.PROP_STATUS, DrawingModel.STATUS_ARCHIVED);
            }
        }

        try {
            JSONObject tomJsonObj = new JSONObject();
            tomJsonObj.put("msg", "Drawing '" + drawingName + "' was successfully archived.");
            String jsonString = tomJsonObj.toString();
            webScriptResponse.getWriter().write(jsonString);
        } catch (JSONException e) {
            throw new WebScriptException("Unable to serialize JSON response");
        }
    }

    private NodeRef getArchiveSpaceLocation() {
        return SearchUtils.getSpaceLocation(archiveSpacePath, serviceRegistry);
    }

    public void setServiceRegistry(ServiceRegistry serviceRegistry) {
        this.serviceRegistry = serviceRegistry;
    }

    public void setArchiveSpacePath(String archiveSpacePath) {
        this.archiveSpacePath = archiveSpacePath;
    }

    public void setDrawingUtil(DrawingUtil drawingUtil) {
        this.drawingUtil = drawingUtil;
    }
}
