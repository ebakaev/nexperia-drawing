package com.nexperia.alfresco.drawing.remote.customconnector;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.extensions.config.ConfigService;
import org.springframework.extensions.config.RemoteConfigElement;
import org.springframework.extensions.config.RemoteConfigElement.EndpointDescriptor;
import org.springframework.extensions.surf.exception.ConnectorServiceException;
import org.springframework.extensions.surf.exception.CredentialVaultProviderException;
import org.springframework.extensions.surf.exception.WebScriptsPlatformException;
import org.springframework.extensions.surf.util.ReflectionHelper;
import org.springframework.extensions.webscripts.connector.*;

import javax.servlet.http.HttpSession;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * This service was creates because of bug in AuthenticatingConnector for GET request.
 * It doesn't check if input stream is null, so NPE on first invocation is thrown.
 *
 * @author Eduard Bakaev
 */
public class CustomConnectorService extends ConnectorService {

	private ConfigService configService;
	private RemoteConfigElement remoteConfig;
	private ApplicationContext applicationContext;

	/**
	 * Lock to provide protection around Remote config lookup
	 */
	private ReadWriteLock configLock = new ReentrantReadWriteLock();

	/**
	 * Gets the config service.
	 *
	 * @return the config service
	 */
	public ConfigService getConfigService() {
		return this.configService;
	}

	/**
	 * Sets the config service.
	 *
	 * @param configService the new config service
	 */
	public void setConfigService(ConfigService configService) {
		this.configService = configService;
	}

	/**
	 * Sets the Spring application context
	 *
	 * @param applicationContext the Spring application context
	 */
	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}

	/**
	 * Retrieves a Connector for the given endpoint that is scoped
	 * to the given user context.
	 * <p/>
	 * A user context is a means of wrapping the Credentials and
	 * ConnectorSession objects for a given user.  If they are provided,
	 * then context will be drawn from them and stored back.
	 *
	 * @param endpointId  the endpoint id
	 * @param userContext the user context
	 * @param session     the http session (optional, if present will persist connector session)
	 * @return the connector
	 * @throws ConnectorServiceException
	 */
	public Connector getConnector(String endpointId, UserContext userContext, HttpSession session)
			throws ConnectorServiceException {

		if (endpointId == null) {
			throw new IllegalArgumentException("EndpointId cannot be null.");
		}

		// load the endpoint
		EndpointDescriptor endpointDescriptor = this.getRemoteConfig().getEndpointDescriptor(endpointId);
		if (endpointDescriptor == null) {
			throw new ConnectorServiceException(
					"Unable to find endpoint definition for endpoint id: " + endpointId);
		}

		// load the connector
		String connectorId = (String) endpointDescriptor.getConnectorId();
		if (connectorId == null) {
			throw new ConnectorServiceException(
					"The connector id property on the endpoint definition '" + endpointId + "' was empty");
		}
		RemoteConfigElement.ConnectorDescriptor connectorDescriptor = this.getRemoteConfig().getConnectorDescriptor(connectorId);
		if (connectorDescriptor == null) {
			throw new ConnectorServiceException(
					"Unable to find connector definition for connector id: " + connectorId + " on endpoint id: " + endpointId);
		}

		// get the endpoint url
		String url = endpointDescriptor.getEndpointUrl();

		// build the connector
		Connector connector = buildConnector(connectorDescriptor, url);
		if (connector == null) {
			throw new ConnectorServiceException(
					"Unable to construct Connector for class: " + connectorDescriptor.getImplementationClass() + ", connector id: " + connectorId);
		}

		// if an authenticator is configured for the connector, then we
		// will wrap the connector with an AuthenticatingConnector type
		// which will do a re-attempt if our credential fails
		String authId = connectorDescriptor.getAuthenticatorId();
		if (authId != null) {
			RemoteConfigElement.AuthenticatorDescriptor authDescriptor = this.getRemoteConfig().getAuthenticatorDescriptor(authId);
			if (authDescriptor == null) {
				throw new ConnectorServiceException(
						"Unable to find authenticator definition for authenticator id: " + authId + " on connector id: " + connectorId);
			}
			String authClass = authDescriptor.getImplementationClass();
			Authenticator authenticator = buildAuthenticator(authClass);

			// wrap the connector
			connector = new FixedAuthenticatingConnector(connector, authenticator);
		}

		// set credentials onto the connector
		// credentials are either "declared", "user", or "none":
		//  "declared" indicates that pre-set fixed declarative user credentials are to be used
		//  "user" indicates that the current user's credentials should be drawn from the vault and used
		//  "none" means that we don't include any credentials
		RemoteConfigElement.IdentityType identity = endpointDescriptor.getIdentity();
		switch (identity) {
			case DECLARED: {
				Credentials credentials = null;
				if (userContext != null && userContext.getCredentials() != null) {
					// reuse previously vaulted credentials
					credentials = userContext.getCredentials();
				}
				if (credentials == null) {
					// create new credentials for this declared user
					String username = (String) endpointDescriptor.getUsername();
					String password = (String) endpointDescriptor.getPassword();

					credentials = new CredentialsImpl(endpointId);
					credentials.setProperty(Credentials.CREDENTIAL_USERNAME, username);
					credentials.setProperty(Credentials.CREDENTIAL_PASSWORD, password);

					// store credentials in vault if we persisting against a user session
					if (session != null) {
						try {
							CredentialVault vault = getCredentialVault(session, username);
							if (vault != null) {
								vault.store(credentials);
							}
						} catch (CredentialVaultProviderException cvpe) {
							throw new ConnectorServiceException("Unable to acquire credential vault", cvpe);
						}
					}
				}
				connector.setCredentials(credentials);

				break;
			}

			case USER: {
				Credentials credentials = null;

				if (userContext != null) {
					if (userContext.getCredentials() != null) {
						// reuse previously vaulted credentials
						credentials = userContext.getCredentials();
					} else if (endpointDescriptor.getExternalAuth() && userContext.getUserId() != null) {
						// Propagate the user ID if we are using external authentication
						credentials = new CredentialsImpl(endpointId);
						credentials.setProperty(Credentials.CREDENTIAL_USERNAME, userContext.getUserId());
					}
				}

				if (credentials != null) {
					connector.setCredentials(credentials);
				}
			}
		}

		// Establish Connector Session
		ConnectorSession connectorSession = null;
		if (userContext != null && userContext.getConnectorSession() != null) {
			// reuse previously session-bound connector session
			connectorSession = userContext.getConnectorSession();
		}
		if (connectorSession == null) {
			// create a new "temporary" connector session
			// this will not get bound back into the session
			connectorSession = new ConnectorSession(endpointId);
		}
		connector.setConnectorSession(connectorSession);

		return connector;
	}

	/**
	 * Return the RemoteConfigElement instance
	 *
	 * @return RemoteConfigElement
	 */
	public RemoteConfigElement getRemoteConfig() {
		this.configLock.readLock().lock();
		try {
			if (this.remoteConfig == null) {
				this.configLock.readLock().unlock();
				this.configLock.writeLock().lock();
				try {
					// check again as multiple threads could have been waiting on the write lock
					if (this.remoteConfig == null) {
						// retrieve and cache the remote configuration block
						this.remoteConfig = (RemoteConfigElement) this.getConfigService().getConfig("Remote").getConfigElement("remote");
						if (this.remoteConfig == null) {
							throw new WebScriptsPlatformException("The 'Remote' configuration was not found.");
						}
					}
				} finally {
					this.configLock.readLock().lock();
					this.configLock.writeLock().unlock();
				}
			}
		} finally {
			this.configLock.readLock().unlock();
		}
		return this.remoteConfig;
	}

	/**
	 * Internal method for building a Connector.
	 * <p/>
	 * Connectors are not cached.  A new Connector will be constructed each time.
	 *
	 * @param descriptor the descriptor
	 * @param url        the url
	 * @return the connector
	 */
	private Connector buildConnector(RemoteConfigElement.ConnectorDescriptor descriptor, String url) {
		Class[] argTypes = new Class[]{descriptor.getClass(), url.getClass()};
		Object[] args = new Object[]{descriptor, url};
		Connector conn = (Connector) ReflectionHelper.newObject(descriptor.getImplementationClass(), argTypes, args);

		// Set the application context for the Connector object
		// TODO: connectors should be Spring beans, but due to legacy config they are constructed from class names
		if (conn instanceof ApplicationContextAware) {
			((ApplicationContextAware) conn).setApplicationContext(applicationContext);
		}

		return conn;
	}

	/**
	 * Internal method for building an Authenticator.
	 *
	 * @param className the class name
	 * @return the authenticator
	 */
	private Authenticator buildAuthenticator(String className)
			throws ConnectorServiceException {
		Authenticator auth = (Authenticator) ReflectionHelper.newObject(className);
		if (auth == null) {
			throw new ConnectorServiceException("Unable to instantiate Authenticator: " + className);
		}

		// Set the application context for the Authenticator object
		// TODO: Authenticators should be Spring beans, but due to legacy config they are constructed from class names
		if (auth instanceof ApplicationContextAware) {
			((ApplicationContextAware) auth).setApplicationContext(applicationContext);
		}

		return auth;
	}

}
