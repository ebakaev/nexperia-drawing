package com.nexperia.alfresco.drawing.remote.exception;

/**
 * Exception thrown when document parameters are valid,
 * but are not allowed for publication.
 * 
 * @author byaminov
 *
 */
public class PublicationForbiddenException extends IllegalParametersException {

	private static final long serialVersionUID = -243920284903106443L;

	public PublicationForbiddenException(String message) {
		super(message);
	}

}
