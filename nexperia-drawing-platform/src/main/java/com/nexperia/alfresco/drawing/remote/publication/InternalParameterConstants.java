package com.nexperia.alfresco.drawing.remote.publication;

/**
 * Common constants for naming of parameters and
 * result properties in publication webscript and
 * its client. These constants are used mainly for internal
 * serialization and deserialization of parameters.
 * 
 * @author byaminov
 *
 */
public interface InternalParameterConstants extends PublicationParameters {
	
	/* Request parameters */
	public static final String PARAM_COMMAND = "command";
	public static final String PARAM_COMMAND_VALUE_PUBLISH = "publish";
	public static final String PARAM_COMMAND_VALUE_UPDATE = "update";
	public static final String PARAM_NO_CONTENT = "nocontent";
	public static final String PARAM_ID = "id";
	
	public static final String[] PARAM_NAMES = new String[] {PARAM_COMMAND, PARAM_NO_CONTENT, PARAM_ID, PARAM_DRY_RUN};
	public static final String[] PARAM_COMMAND_VALUES = new String[] {PARAM_COMMAND_VALUE_PUBLISH, PARAM_COMMAND_VALUE_UPDATE};

	/* Success result message keys */
	public static final String PREFIX_KEY = "key_";
	public static final String KEY_PUBLISHED_COPY_ID = "publishedCopyId";
	public static final String KEY_NEW_VERSION_WAS_PUBLISHED = "newVersionWasPublished";
	
	/* Failure result message keys */
	public static final String KEY_CODE_ID = "code";
	public static final String KEY_CODE_MEANING = "meaning";
	public static final String KEY_MESSAGE = "message";
	public static final String KEY_MISSING_PROPERTIES = "missingProperties";
}
