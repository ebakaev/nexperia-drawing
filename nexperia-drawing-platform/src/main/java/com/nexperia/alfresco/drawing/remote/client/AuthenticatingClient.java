package com.nexperia.alfresco.drawing.remote.client;

/**
 * Common interface for webscript clients which support
 * authentication of remote publication requests.
 *
 * @author ebakaev
 *
 */
public interface AuthenticatingClient {

    /**
     * Sets credentials to use to connecto to remote Alfresco.
     * You can provide null username and/or password if connector
     * configured in {@link PublicationClient} does not need them.
     *
     * @param username username of a user of remote Alfresco.
     * @param password password of a user of remote Alfresco.
     */
    void setCredentials(String username, String password);

}
