package com.nexperia.alfresco.drawing.service.schedule;

import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.model.FileFolderService;
import org.alfresco.service.cmr.repository.NodeRef;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RemoveLegacySpacesExecutor {

    public static final Logger logger = LoggerFactory.getLogger(RemoveLegacySpacesExecutor.class);

    ServiceRegistry serviceRegistry;
    NodeRef rootFolderToCleanup;

    public void execute() {
        AuthenticationUtil.runAsSystem(new AuthenticationUtil.RunAsWork<String>() {
            @Override
            public String doWork() throws Exception {
                logger.debug("Starting the cleaning...");
                FileFolderService fileFolderService = serviceRegistry.getFileFolderService();
                fileFolderService.listFolders(rootFolderToCleanup).stream().limit(100).forEach(folderToDelete -> {
                    fileFolderService.delete(folderToDelete.getNodeRef());
                    logger.debug("{} successfully deleted...", folderToDelete.getName());
                });

                return null;
            }
        });
    }

    public void setServiceRegistry(ServiceRegistry serviceRegistry) {
        this.serviceRegistry = serviceRegistry;
    }

    public void setRootFolderToCleanup(String rootFolderToCleanup) {
        this.rootFolderToCleanup = new NodeRef(rootFolderToCleanup);
    }
}
