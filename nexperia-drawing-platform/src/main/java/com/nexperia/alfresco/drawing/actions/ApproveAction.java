package com.nexperia.alfresco.drawing.actions;

import com.nexperia.alfresco.drawing.behaviour.RenditionBehaviour;
import com.nexperia.alfresco.drawing.model.DrawingModel;
import com.nexperia.alfresco.drawing.service.EmailService;
import com.nexperia.alfresco.drawing.utils.DrawingUtil;
import org.alfresco.repo.forum.CommentService;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.action.Action;
import org.alfresco.service.cmr.repository.NodeRef;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class ApproveAction extends BaseFlowAction {

	public static final Logger logger = LoggerFactory.getLogger(ApproveAction.class);

	protected ApproveAction(EmailService emailService, ServiceRegistry serviceRegistry, CommentService commentService, String emailSubject, String emailTemplate, String baseUrl, DrawingUtil drawingUtil) {
		super(emailService, serviceRegistry, commentService, emailSubject, emailTemplate, baseUrl, drawingUtil);
	}

	@Override
	protected boolean isAllowed(NodeRef drawing) {
		return serviceRegistry.getNodeService().getProperty(drawing, DrawingModel.PROP_STATUS).equals(DrawingModel.STATUS_PENDING_APPROVAL);
	}

	@Override
	protected void executeImpl(Action action, NodeRef drawing) {
		List<NodeRef> renditions = super.getRenditions(drawing);
		logger.debug("Release renditions: " + renditions + " by NR: " + drawing);
		super.executeAction(action, drawing, renditions);
	}

	@Override
	protected void updateStatus(NodeRef drawing, List<NodeRef> renditions) {
		serviceRegistry.getNodeService().setProperty(drawing, DrawingModel.PROP_STATUS, DrawingModel.STATUS_APPROVED);
	}

	@Override
	protected void updateVersion(Action action, NodeRef drawing) {
		//Nothing
	}
}
