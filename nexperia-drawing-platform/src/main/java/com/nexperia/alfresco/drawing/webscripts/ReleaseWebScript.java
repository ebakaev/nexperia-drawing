package com.nexperia.alfresco.drawing.webscripts;

import com.nexperia.alfresco.drawing.model.DrawingModel;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.model.FileFolderService;
import org.alfresco.service.cmr.model.FileInfo;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.CopyService;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.extensions.webscripts.AbstractWebScript;
import org.springframework.extensions.webscripts.WebScriptException;
import org.springframework.extensions.webscripts.WebScriptRequest;
import org.springframework.extensions.webscripts.WebScriptResponse;

import java.io.IOException;

/*  Most likely redundant WS as we use Action for this */
public class ReleaseWebScript extends AbstractWebScript {

    protected final Logger logger = LoggerFactory.getLogger(getClass());
    ServiceRegistry serviceRegistry;

    String nodeRefOfReleaseSpace;

    @Override
    public void execute(WebScriptRequest webScriptRequest, WebScriptResponse webScriptResponse) throws IOException {
        String nrStr = webScriptRequest.getParameter("nodeRef");
        NodeRef nodeRef = new NodeRef(nrStr);
        FileFolderService fileFolderService = serviceRegistry.getFileFolderService();
        NodeService nodeService = serviceRegistry.getNodeService();
        CopyService copyService = serviceRegistry.getCopyService();

        for (FileInfo rendition : fileFolderService.listFiles(nodeRef)) {
            NodeRef nr = rendition.getNodeRef();
            if (DrawingModel.TYPE_RENDITION.equals(nodeService.getType(nr))
                    && DrawingModel.PTW_YES.equals(nodeService.getProperty(nr, DrawingModel.PROP_PUBLISH_TO_WEB))) {
                logger.debug(rendition.getName() + " is market as PTW=Yes, publishing it");
                ChildAssociationRef originalAssoc = nodeService.getPrimaryParent(nr);

                //Will be an action to publish this rendition and publicationId will be set.
                copyService.copy(nr, new NodeRef(nodeRefOfReleaseSpace), originalAssoc.getTypeQName(), originalAssoc.getQName());

                //marking by this aspect to indicate in Share UI
                nodeService.addAspect(nr, DrawingModel.ASPECT_RELEASED, null);
                nodeService.setProperty(nr, DrawingModel.PROP_STATUS, DrawingModel.STATUS_RELEASED);
            }
        }

        //marking by this aspect to indicate in Share UI
        nodeService.addAspect(nodeRef, DrawingModel.ASPECT_RELEASED, null);
        nodeService.setProperty(nodeRef, DrawingModel.PROP_STATUS, DrawingModel.STATUS_RELEASED);

        try {
            JSONObject tomJsonObj = new JSONObject();
            tomJsonObj.put("msg", "Drawing was successfully released.");
            String jsonString = tomJsonObj.toString();
            webScriptResponse.getWriter().write(jsonString);
        } catch (JSONException e) {
            throw new WebScriptException("Unable to serialize JSON response");
        }
    }

    public void setServiceRegistry(ServiceRegistry serviceRegistry) {
        this.serviceRegistry = serviceRegistry;
    }

    public void setNodeRefOfReleaseSpace(String nodeRefOfReleaseSpace) {
        this.nodeRefOfReleaseSpace = nodeRefOfReleaseSpace;
    }
}
