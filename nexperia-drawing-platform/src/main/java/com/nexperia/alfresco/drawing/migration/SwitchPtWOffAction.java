package com.nexperia.alfresco.drawing.migration;

import com.nexperia.alfresco.drawing.model.DrawingModel;
import com.nexperia.alfresco.drawing.utils.DrawingUtil;
import org.alfresco.repo.action.executer.ActionExecuterAbstractBase;
import org.alfresco.repo.transaction.RetryingTransactionHelper;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.action.Action;
import org.alfresco.service.cmr.action.ParameterDefinition;
import org.alfresco.service.cmr.model.FileFolderService;
import org.alfresco.service.cmr.model.FileInfo;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class SwitchPtWOffAction extends ActionExecuterAbstractBase {

    public static final Logger logger = LoggerFactory.getLogger(SwitchPtWOffAction.class);

    ServiceRegistry serviceRegistry;
    DrawingUtil drawingUtil;
    String drawingsToUpdate;

    private final String DRAWING_SPACE = "draw:drawingSpace";
    private final String SPACES_STORE_PREFIX = "workspace://SpacesStore/";

    @Override
    protected void executeImpl(Action action, NodeRef nodeRef) {
        logger.debug("STARTING Changes...");
        processDrawingsFromFileWithNodeRefs(drawingsToUpdate);
        logger.debug("Update FINISHED!");
    }

    private void processDrawingsFromFileWithNodeRefs(String drawingsToUpdate) {
        try {
            List<String> allLines = Files.readAllLines(Paths.get(drawingsToUpdate));
            int size = allLines.size();
            int i = 1;
            int failed = 0;
            logger.debug("There're '" + size + "' Drawings to process.");
            for (String drawingUUID : allLines) {
                NodeRef sourceDrawingNodeRef = new NodeRef(SPACES_STORE_PREFIX + drawingUUID.trim());
                if (serviceRegistry.getNodeService().exists(sourceDrawingNodeRef)) {
                    String drawingName = drawingUtil.getName(sourceDrawingNodeRef);
                    logger.debug("[ " + i++ + " of  " + size + " ] Processing " + drawingName);
                    processDrawing(sourceDrawingNodeRef);
                } else {
                    failed++;
                    logger.debug(sourceDrawingNodeRef + " doesn't exists!");
                }
            }
            logger.debug(failed + " count of Drawings were not found.");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void processDrawing(NodeRef sourceDrawingNodeRef) {
        FileFolderService fileFolderService = serviceRegistry.getFileFolderService();
        String sourceDrawingName = drawingUtil.getName(sourceDrawingNodeRef);
        logger.debug("Update is processing for " + sourceDrawingName + " [" + sourceDrawingNodeRef + "]");
        if (serviceRegistry.getNodeService().hasAspect(sourceDrawingNodeRef, DrawingModel.ASPECT_MIGRATED)) {
            //We're skipping already updated Drawings
            logger.debug("SKIPPING Drawing as already Updated!: " + sourceDrawingName);
        } else {
            NodeService nodeService = serviceRegistry.getNodeService();
            String type = nodeService.getType(sourceDrawingNodeRef).toPrefixString(serviceRegistry.getNamespaceService());
            if (DRAWING_SPACE.equals(type)) {
                try {
                    serviceRegistry.getRetryingTransactionHelper().doInTransaction(new RetryingTransactionHelper.RetryingTransactionCallback<String>() {
                        public String execute() throws Throwable {
                            List<FileInfo> renditions = fileFolderService.listFiles(sourceDrawingNodeRef);
                            logger.debug("there're " + renditions.size() + " renditions to update...");
                            for (FileInfo fi : renditions) {
                                serviceRegistry.getNodeService().setProperty(fi.getNodeRef(), DrawingModel.PROP_PUBLISH_TO_WEB, "No");
                                logger.debug(fi.getName() + " rendition updated...");
                            }
                            //Mark that this Job was moved to the new env
                            nodeService.addAspect(sourceDrawingNodeRef, DrawingModel.ASPECT_MIGRATED, null);
                            return null;
                        }
                    }, false, true);
                } catch (Exception e) {
                    logger.error(sourceDrawingName + " update FAILED with exception: " + e.getMessage(), e);
                }
            }
        }
    }

    @Override
    protected void addParameterDefinitions(List<ParameterDefinition> list) {

    }

    public void setServiceRegistry(ServiceRegistry serviceRegistry) {
        this.serviceRegistry = serviceRegistry;
    }

    public void setDrawingUtil(DrawingUtil drawingUtil) {
        this.drawingUtil = drawingUtil;
    }

    public void setDrawingsToUpdate(String drawingsToUpdate) {
        this.drawingsToUpdate = drawingsToUpdate;
    }
}
