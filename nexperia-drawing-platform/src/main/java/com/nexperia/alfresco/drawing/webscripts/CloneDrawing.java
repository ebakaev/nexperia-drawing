package com.nexperia.alfresco.drawing.webscripts;

import com.nexperia.alfresco.drawing.model.DrawingModel;
import com.nexperia.alfresco.drawing.service.DrawingService;
import com.nexperia.alfresco.drawing.service.exceptions.DrawingExistsException;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.namespace.QName;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.extensions.webscripts.AbstractWebScript;
import org.springframework.extensions.webscripts.WebScriptException;
import org.springframework.extensions.webscripts.WebScriptRequest;
import org.springframework.extensions.webscripts.WebScriptResponse;

import java.io.IOException;
import java.io.Serializable;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;

import static com.nexperia.alfresco.drawing.model.DrawingModel.DRAWING_MODEL_1_0_URI;

public class CloneDrawing extends AbstractWebScript {

    protected final Logger logger = LoggerFactory.getLogger(getClass());

    DrawingService drawingService;
    ServiceRegistry serviceRegistry;

    @Override
    public void execute(WebScriptRequest webScriptRequest, WebScriptResponse webScriptResponse) throws IOException {
        Map<QName, Serializable> properties = getProperties(webScriptRequest);
        String drawingIds = webScriptRequest.getParameter("drawingIds");
        if (drawingIds != null) {
            drawingIds = drawingIds.trim();
        }
        String countToAutoCreate = webScriptRequest.getParameter("howManyToCreate");
        String descriptiveTitle = (String) properties.get(DrawingModel.PROP_DESCRIPTIVE_TITLE);
        String[] descTitles = descriptiveTitle.split("\\|");
        logger.debug("Start creating following drawings: " + drawingIds);
        AtomicBoolean isRequestReady = new AtomicBoolean(false);
        try {
            List<String> drawingsCreated = new ArrayList<String>();
            if (StringUtils.isEmpty(drawingIds)) {
                if (StringUtils.isNotEmpty(countToAutoCreate)) {
                    int count = Integer.valueOf(countToAutoCreate);
                    if (descTitles.length > 1 && count == descTitles.length) {
                        for (int i = 0; i < count; i++) {
                            properties.put(DrawingModel.PROP_DESCRIPTIVE_TITLE, descTitles[i]);
                            String createdDrawing = drawingService.createDrawing(null, properties);
                            drawingsCreated.add(createdDrawing);
                        }
                    } else {
                        for (int i = 0; i < count; i++) {
                            String createdDrawing = drawingService.createDrawing(null, properties);
                            drawingsCreated.add(createdDrawing);
                        }
                    }
                } else {
                    String createdDrawing = drawingService.createDrawing(null, properties);
                    drawingsCreated.add(createdDrawing);
                }
                drawingIds = String.join(",", drawingsCreated);
            } else {
                String[] drawings = drawingIds.split(",");

                if (descTitles.length > 1 && descTitles.length == drawings.length) {
                    for (int i = 0; i < descTitles.length; i++) {
                        properties.put(DrawingModel.PROP_DESCRIPTIVE_TITLE, descTitles[i]);
                        drawingService.createDrawing(drawings[i].trim(), properties);
                    }
                } else {
                    Arrays.stream(drawings).forEach(drawing -> {
                        try {
                            drawingService.createDrawing(drawing.trim(), properties);
                        } catch (DrawingExistsException drawingExistsException) {
                            try {
                                doResponseWithMessage(webScriptResponse, drawingExistsException.getMessage());
                                isRequestReady.set(true);
                                logger.error(drawingExistsException.getMessage());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            }
        } catch (DrawingExistsException drawingExistsException) {
            doResponseWithMessage(webScriptResponse, drawingExistsException.getMessage());
            isRequestReady.set(true);
            logger.error(drawingExistsException.getMessage());
        }

        if (!isRequestReady.get()) {
            doResponseWithMessage(webScriptResponse, "You're good! Drawing/s is/are created: \n\n" + drawingIds + "\n\n");
        }
    }

    private void doResponseWithMessage(WebScriptResponse webScriptResponse, String message) throws IOException {
        try {
            JSONObject tomJsonObj = new JSONObject();
            tomJsonObj.put("msg", message);
            String jsonString = tomJsonObj.toString();
            webScriptResponse.getWriter().write(jsonString);
        } catch (JSONException e) {
            throw new WebScriptException("Unable to serialize JSON response");
        }
    }

    private Map<QName, Serializable> getProperties(WebScriptRequest webScriptRequest) {
        Map<QName, Serializable> properties = new HashMap<>();
        for (String param : webScriptRequest.getParameterNames()) {
            QName prop = QName.createQName(DRAWING_MODEL_1_0_URI, param);
            if ("requiredRenditions".equals(param)) {
                properties.put(prop, (Serializable) Arrays.asList(webScriptRequest.getParameter(param).split(",")));
            } else {
                properties.put(prop, webScriptRequest.getParameter(param));
            }
        }
        return properties;
    }

    public void setServiceRegistry(ServiceRegistry serviceRegistry) {
        this.serviceRegistry = serviceRegistry;
    }

    public void setDrawingService(DrawingService drawingService) {
        this.drawingService = drawingService;
    }
}
