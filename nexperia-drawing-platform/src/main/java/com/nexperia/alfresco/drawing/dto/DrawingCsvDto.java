package com.nexperia.alfresco.drawing.dto;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvBindByPosition;
import lombok.Builder;
import lombok.Data;

import java.util.Date;

@Data
@Builder
public class DrawingCsvDto {

	@CsvBindByName(column = "name")
	@CsvBindByPosition(position = 0)
	private String name;

	@CsvBindByName(column = "descriptiveTitle")
	@CsvBindByPosition(position = 1)
	private String descriptiveTitle;

	@CsvBindByName(column = "drawingType")
	@CsvBindByPosition(position = 2)
	private String drawingType;

	@CsvBindByName(column = "primaryFormat")
	@CsvBindByPosition(position = 3)
	private String primaryFormat;

	@CsvBindByName(column = "drawingVersion")
	@CsvBindByPosition(position = 4)
	private String drawingVersion;

	@CsvBindByName(column = "createdFor")
	@CsvBindByPosition(position = 5)
	private String createdFor;

	@CsvBindByName(column = "securityStatus")
	@CsvBindByPosition(position = 6)
	private String securityStatus;

	@CsvBindByName(column = "magCode")
	@CsvBindByPosition(position = 7)
	private String magCode;

	@CsvBindByName(column = "requester")
	@CsvBindByPosition(position = 8)
	private String requester;

	@CsvBindByName(column = "lastStatusChangedDate")
	@CsvBindByPosition(position = 9)
	private String lastStatusChangedDate;

	@CsvBindByName(column = "deliveryDate")
	@CsvBindByPosition(position = 10)
	private String deliveryDate;

	@CsvBindByName(column = "requiredRenditions")
	@CsvBindByPosition(position = 11)
	private String requiredRenditions;


}
