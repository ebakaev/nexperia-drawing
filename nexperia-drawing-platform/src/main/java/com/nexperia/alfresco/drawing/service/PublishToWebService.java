package com.nexperia.alfresco.drawing.service;

import com.nexperia.alfresco.drawing.model.DrawingModel;
import com.nexperia.alfresco.drawing.remote.client.PublicationClient;
import com.nexperia.alfresco.drawing.remote.client.PublicationConstants;
import com.nexperia.alfresco.drawing.remote.exception.ConnectionException;
import com.nexperia.alfresco.drawing.utils.DrawingUtil;
import org.alfresco.error.AlfrescoRuntimeException;
import org.alfresco.model.ContentModel;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.repository.ContentIOException;
import org.alfresco.service.cmr.repository.NodeRef;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.RestTemplate;

import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class PublishToWebService {

    public static final Logger logger = LoggerFactory.getLogger(PublishToWebService.class);

    ServiceRegistry serviceRegistry;
    PublicationClient publicationClient;
    DrawingUtil drawingUtil;
    String spiderLibrarySearchUrl = "https://qa.publish-dam.pim.nexperia.com/s/nxp/spc/search?FileName=";
    Boolean serviceEnabled;

    private static String DEFAULT_APPROVER = "jan.peeters@nexperia.com";
    private DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");

    public void publishDrawing(NodeRef drawing) {
        if (serviceEnabled) {
            List<NodeRef> renditionsToPublish = drawingUtil.getReadyToPublishRenditionsFromDrawing(drawing);
            renditionsToPublish.forEach(this::publishRendition);
        } else {
            logger.debug("Publication service disabled. Search Url: {}", spiderLibrarySearchUrl);
        }
    }

    public void publishRendition(NodeRef rendition) {
        try {
            if (serviceEnabled) {
                NodeRef drawing = drawingUtil.getDrawingOfRendition(rendition);
//            String renditionName = drawingUtil.getName(rendition);
                Map<String, String> properties = collectProperties(drawing, rendition);
                InputStream content = serviceRegistry.getContentService().getReader(rendition, ContentModel.PROP_CONTENT).getContentInputStream();
//            boolean updated = false;
//            String publicationId = (String) drawingUtil.getProperty(rendition, DrawingModel.PROP_PUBLICATION_ID);
//            if (StringUtils.isNotEmpty(publicationId)) {
//                try {
//                    boolean result = publicationClient.update(publicationId,
//                            content,
//                            properties, null);
//                    if (!result) {
//                        if (logger.isDebugEnabled()) {
//                            logger.debug("Update was not needed for document " + renditionName + ", as it is already " +
//                                    "published by ID " + publicationId + " with the same metadata");
//                        }
//                    }
//                    updated = true;
//                } catch (NoSuchPublishedIdException e) {
//                    if (logger.isDebugEnabled()) {
//                        logger.debug("Could not update document '" + renditionName + "' by ID '" + publicationId +
//                                "'. It will be republished. Message: " + e.getMessage());
//                    }
//                }
//            }
//            if (!updated) {
                String publicationId = publicationClient.publish(content, properties, null);
                drawingUtil.setProperty(rendition, DrawingModel.PROP_PUBLICATION_ID, publicationId);
//            }
            } else {
                logger.debug("Publication service disabled. Search Url: {}", spiderLibrarySearchUrl);
            }
        } catch (ContentIOException e) {
            throw new RuntimeException("Publication could not be executed. " + e.getMessage(), e);
        } catch (ConnectionException e) {
            throw new AlfrescoRuntimeException("A connection lost please try again.", e);
        } catch (Exception e) {
            String mes = e.getMessage();
            throw new RuntimeException("Publication could not be executed. " + mes, e);
        }
    }

    public void makeRenditionObsolete(NodeRef rendition) {
        if (serviceEnabled) {
            //do obsolete action for a rendition
            NodeRef drawing = drawingUtil.getDrawingOfRendition(rendition);
            Map<String, String> properties = collectProperties(drawing, rendition);
            properties.put(PublicationConstants.DOCUMENT_STATUS, DrawingModel.STATUS_OBSOLETE);
            String publicationId = (String) drawingUtil.getProperty(rendition, DrawingModel.PROP_PUBLICATION_ID);
            InputStream content = serviceRegistry.getContentService().getReader(rendition, ContentModel.PROP_CONTENT).getContentInputStream();
            if (StringUtils.isEmpty(publicationId)) {
                publicationId = findPublicaitonIdInSPIDER(properties.get(PublicationConstants.FILE_NAME));
            }
            if (StringUtils.isNotEmpty(publicationId)) {
                publicationClient.update(publicationId, content, properties);
            } else {
                logger.debug("File wasn't found in SPIDER Library");
            }
        } else {
            logger.debug("Publication service disabled. Search Url: {}", spiderLibrarySearchUrl);
        }
    }

    private String findPublicaitonIdInSPIDER(String renditionName) {
        String requestUrl = spiderLibrarySearchUrl + renditionName;
        logger.debug("Search URL: {}", requestUrl);
        Object[] object = new RestTemplate().getForObject(requestUrl, Object[].class);
        if (object.length > 0) {
            Map<String, String> response = (Map<String, String>) object[0];
            logger.debug("Found publicationID = '{}' for {} rendition in Spider Library", response.get("FileID"), renditionName);
            return response.get("FileID");
        }
        return null;
    }

    private Map<String, String> collectProperties(NodeRef drawing, NodeRef rendition) {
        Map<String, String> map = new HashMap<>();
        map.put(PublicationConstants.PUBLICATION_CHANNELS, "www");
        map.put(PublicationConstants.APPROVER, DEFAULT_APPROVER);
        map.put(PublicationConstants.CREATION_DATE, dateFormatter.format((Date) drawingUtil.getProperty(drawing, ContentModel.PROP_CREATED)));
        map.put(PublicationConstants.CREATOR, (String) drawingUtil.getProperty(drawing, ContentModel.PROP_CREATOR));
        map.put(PublicationConstants.DESCRIPTIVE_TITLE, (String) drawingUtil.getProperty(drawing, DrawingModel.PROP_DESCRIPTIVE_TITLE));
        map.put(PublicationConstants.DOCUMENT_STATUS, DrawingModel.STATUS_RELEASED);
        map.put(PublicationConstants.DOCUMENT_TYPE, (String) drawingUtil.getProperty(drawing, DrawingModel.PROP_DRAWING_TYPE));
        map.put(PublicationConstants.FILE_NAME, drawingUtil.getName(rendition).toLowerCase());
        map.put(PublicationConstants.MAG_NAME_AND_CODE, (String) drawingUtil.getProperty(drawing, DrawingModel.PROP_MAG_CODE));
        map.put(PublicationConstants.PUBLISHED_TO_WEB, "true");
        map.put(PublicationConstants.RELEASED_DATE, dateFormatter.format(new Date()));
        map.put(PublicationConstants.RELEASE_DATE, dateFormatter.format(new Date()));
        map.put(PublicationConstants.RELEASER, serviceRegistry.getAuthenticationService().getCurrentUserName());
        map.put(PublicationConstants.SECURITY_STATUS, (String) drawingUtil.getProperty(drawing, DrawingModel.PROP_SECURITY_STATUS));
        String drawingId = (String) drawingUtil.getProperty(drawing, DrawingModel.PROP_DRAWING_ID);
        map.put(PublicationConstants.TITLE, drawingId == null ? drawingUtil.getName(drawing) : drawingId);
        map.put(PublicationConstants.VERSION, (String) drawingUtil.getProperty(drawing, DrawingModel.PROP_DRAWING_VERSION));
        map.put(PublicationConstants.VERSION_LABEL, (String) drawingUtil.getProperty(rendition, ContentModel.PROP_VERSION_LABEL));
        map.put(PublicationConstants.LANGUAGE_CODE, Locale.US.toString());
        return map;
    }

    public void setServiceRegistry(ServiceRegistry serviceRegistry) {
        this.serviceRegistry = serviceRegistry;
    }

    public void setPublicationClient(PublicationClient publicationClient) {
        this.publicationClient = publicationClient;
    }

    public void setDrawingUtil(DrawingUtil drawingUtil) {
        this.drawingUtil = drawingUtil;
    }

    public void setSpiderLibrarySearchUrl(String spiderLibrarySearchUrl) {
        this.spiderLibrarySearchUrl = spiderLibrarySearchUrl;
    }

    public void setServiceEnabled(Boolean serviceEnabled) {
        this.serviceEnabled = serviceEnabled;
    }
}
