package com.nexperia.alfresco.drawing.model;

import org.alfresco.service.namespace.QName;

import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class DrawingModel {

    public static final String DRAWING_MODEL_1_0_URI = "http://www.nexperia.com/drawing/content-model";

    public static final QName TYPE_DRAWING = QName.createQName(DRAWING_MODEL_1_0_URI, "drawingSpace");
    public static final QName TYPE_RENDITION = QName.createQName(DRAWING_MODEL_1_0_URI, "rendition");

    public static final QName ASPECT_DRAWING_STATUS = QName.createQName(DRAWING_MODEL_1_0_URI, "drawingStatusAspect");
    public static final QName ASPECT_PUBLICATION = QName.createQName(DRAWING_MODEL_1_0_URI, "publicationAspect");

    public static final QName PROP_STATUS = QName.createQName(DRAWING_MODEL_1_0_URI, "drawingStatus");
    public static final QName PROP_PUBLISH_TO_WEB = QName.createQName(DRAWING_MODEL_1_0_URI, "publishToWeb");
    public static final QName PROP_PUBLICATION_ID = QName.createQName(DRAWING_MODEL_1_0_URI, "publicationId");

    //Drawign data aspect
    public static final QName PROP_DRAWING_ID = QName.createQName(DRAWING_MODEL_1_0_URI, "drawingId");
    public static final QName PROP_DESCRIPTIVE_TITLE = QName.createQName(DRAWING_MODEL_1_0_URI, "descriptiveTitle");
    public static final QName PROP_DRAWING_TYPE = QName.createQName(DRAWING_MODEL_1_0_URI, "drawingType");
    public static final QName PROP_PRIMARY_FORMAT = QName.createQName(DRAWING_MODEL_1_0_URI, "primaryFormat");
    public static final QName PROP_DRAWING_VERSION = QName.createQName(DRAWING_MODEL_1_0_URI, "drawingVersion");
    public static final QName PROP_CREATED_FOR = QName.createQName(DRAWING_MODEL_1_0_URI, "createdFor");
    public static final QName PROP_SECURITY_STATUS = QName.createQName(DRAWING_MODEL_1_0_URI, "securityStatus");
    public static final QName PROP_MAG_CODE = QName.createQName(DRAWING_MODEL_1_0_URI, "magCode");
    public static final QName PROP_REQUESTER = QName.createQName(DRAWING_MODEL_1_0_URI, "requester");
    public static final QName PROP_LAST_STATUS_CHANGED_DATE = QName.createQName(DRAWING_MODEL_1_0_URI, "lastStatusChangedDate");
    public static final QName PROP_DELIVERY_DATE = QName.createQName(DRAWING_MODEL_1_0_URI, "deliveryDate");;
    public static final QName PROP_REQUIRED_RENDITIONS = QName.createQName(DRAWING_MODEL_1_0_URI, "requiredRenditions");
    public static final QName PROP_IS_ALLOWED_NEXT_STEP = QName.createQName(DRAWING_MODEL_1_0_URI, "isAllowedNextStep");
    public static final QName PROP_IS_REVISED_ONCE = QName.createQName(DRAWING_MODEL_1_0_URI, "isRevisedOnce");
    public static final QName PROP_RELEASE_VERSIONS = QName.createQName(DRAWING_MODEL_1_0_URI, "releaseVersions");

    public static final QName ASPECT_ARCHIVED = QName.createQName(DRAWING_MODEL_1_0_URI, "archived");
    public static final QName ASPECT_RELEASED = QName.createQName(DRAWING_MODEL_1_0_URI, "released");
    public static final QName ASPECT_MIGRATED = QName.createQName(DRAWING_MODEL_1_0_URI, "migrated");
    public static final QName ASPECT_NEW = QName.createQName(DRAWING_MODEL_1_0_URI, "newAspect");

    public static final String STATUS_ARCHIVED = "Archived";
    public static final String STATUS_RELEASED = "Released";
    public static final String STATUS_DRAFT = "Draft";
    public static final String STATUS_PENDING_APPROVAL = "Pending approval";
    public static final String STATUS_APPROVED = "Approved";
    public static final String STATUS_OBSOLETE = "Obsolete";

    public static final String ACTION_APPROVE = "approve-action";
    public static final String ACTION_SEND_TO_APPROVE = "send-to-approve-action";
    public static final String ACTION_REJECT = "reject-action";
    public static final String ACTION_RELEASE = "release-action";
    public static final String ACTION_APPROVE_AND_RELEASE = "approve-release-action";
    public static final String ACTION_REVISE = "revise-action";

    public static final String PTW_YES = "Yes";
    public static final String PTW_NO = "no";

    public static final String DRAWING_TYPE_PACKING = "Packing";

    public static final String DRAWING_TYPE_BLANK_PINNING_DIAGRAM = "Blank pinning diagram";
    public static final String DRAWING_TYPE_FOOTPRINT = "Footprint";
    public static final String DRAWING_TYPE_OUTLINE_3D = "Outline 3d";
    public static final String DRAWING_TYPE_MINIMIZED_OUTLINE = "Minimized outline";
    public static final String DRAWING_TYPE_PACKAGE_OUTLINE = "Package outline";
    public static final String DRAWING_TYPE_SIMPLIFIED_OUTLINE = "Simplified outline";

    public static final String DRAWING_CREATED_FOR_AN = "AN";
    public static final String DRAWING_CREATED_FOR_UM = "UM";
    public static final String DRAWING_CREATED_FOR_TN = "TN";

    public static final String DRAWING_MAG_R03 = "R03";
    public static final String DRAWING_MAG_R04 = "R04";
    public static final String DRAWING_MAG_R07 = "R07";
    public static final String DRAWING_MAG_R17 = "R17";
    public static final String DRAWING_MAG_R18 = "R18";
    public static final String DRAWING_MAG_R19 = "R19";
    public static final String DRAWING_MAG_R73 = "R73";
    public static final String DRAWING_MAG_R34 = "R34";
    public static final String DRAWING_MAG_RE1 = "RE1";
    public static final String DRAWING_MAG_RE2 = "RE2";
    public static final String DRAWING_MAG_R33 = "R33";
    public static final String DRAWING_MAG_R71 = "R71";
    public static final String DRAWING_MAG_RP3 = "RP3";
    public static final String DRAWING_MAG_RP8 = "RP8";
    public static final String DRAWING_MAG_RY7 = "RY7";


    public static final String FORMAT_EPS = "EPS";
    public static final String FORMAT_SVG = "SVG";
    public static final String FORMAT_JPG = "JPG";

    public static final Map<String, String> DRAWING_MAG_CODE_MAP = Stream.of(new String[][]{
            {"R03", "Product/BL10 Analog & Logic ICs/R03 Signal Chain"},
            {"R04", "Product/BL10 Analog & Logic ICs/R04 Power"},
            {"R07", "Product/BL10 Analog & Logic ICs/R07 - Analog & Logic"},
            {"R17", "Product/BL27 IGBT & Modules/R17 - IGBT"},
            {"R18", "Product/BL27 IGBT & Modules/R18 - Power Module"},
            {"R19", "Product/BL29 Power & Signal conversion/R19 - Power & Signal"},
            {"R34", "Product/BL24 MOS Discretes/R34 - Small signal mosfets"},
            {"R73", "Product/BL24 MOS Discretes/R73 - Auto MOS"},
            {"RE1", "Product/BL24 MOS Discretes/RE1 - Power MOS"},
            {"RE2", "Product/BL24 MOS Discretes/RE2 - GaN Power MOS"},
            {"R33", "Product/BL25 Bipolar discretes/R33 - Small Signal Bipolar Discretes"},
            {"R71", "Product/BL25 Bipolar discretes/R71 - Protection & Filtering"},
            {"RP3", "Product/BL25 Bipolar discretes/RP3 - SiC Discretes"},
            {"RP8", "Product/BL25 Bipolar discretes/RP8 - Power Bipolar Discretes"},
            {"RY7", "Product/BLMA SSC Marketing/RY7 - Digital Marketing"},
            {"Other", "Product/BLMA SSC Marketing/Other"},
    }).collect(Collectors.toMap(data -> data[0], data -> data[1]));

    public static final String SECURITY_STATUS_COMPANY_PUBLIC = "Company Public";


}
