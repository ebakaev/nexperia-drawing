package com.nexperia.alfresco.drawing.utils;

import org.alfresco.error.AlfrescoRuntimeException;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.StoreRef;
import org.alfresco.service.cmr.search.*;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

public class SearchUtils {

	public static NodeRef getSpaceLocation(String path, ServiceRegistry serviceRegistry) {
		return getSingleNodeByPath(serviceRegistry, path);
	}

	public static NodeRef getReportsNode(ServiceRegistry serviceRegistry) {
		String path = "/app:company_home/st:sites/cm:tdm-drawing/cm:documentLibrary/cm:Reports";
		return getSingleNodeByPath(serviceRegistry, path);
	}

	private static NodeRef getSingleNodeByPath(ServiceRegistry serviceRegistry, String path) {
		String query = "PATH:'" + path + "'";
		List<NodeRef> nodeRefs = findNodesByFTSQuery(query, serviceRegistry, SearchUtils::getUnlimitedSearch);
		if (nodeRefs.isEmpty()) {
			throw new AlfrescoRuntimeException("Cannot find a node by path: " + path);
		}
		return nodeRefs.get(0);
	}

	public static List<NodeRef> findAll(String query, ServiceRegistry serviceRegistry) {
		return findNodesByFTSQuery(query, serviceRegistry, SearchUtils::getUnlimitedSearch);
	}

	private static List<NodeRef> findNodesByFTSQuery(String query, ServiceRegistry serviceRegistry, Function<String, SearchParameters> searchParams) {
		ResultSet resultSet = null;
		try {
			resultSet = serviceRegistry.getSearchService().query(searchParams.apply(query));
			List<NodeRef> result = new ArrayList<>();
			for (ResultSetRow resultSetRow : resultSet) {
				result.add(resultSetRow.getNodeRef());
			}
			return result;
		} finally {
			if (resultSet != null) {
				resultSet.close();
			}
		}
	}

	private static SearchParameters getUnlimitedSearch(String query) {
		final SearchParameters params = new SearchParameters();
		params.addStore(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE);
		params.setLanguage(SearchService.LANGUAGE_FTS_ALFRESCO);
		params.setQuery(query);
		params.setLimitBy(LimitBy.UNLIMITED);
		params.setLimit(0);
		params.setMaxPermissionChecks(Integer.MAX_VALUE);
		params.setMaxPermissionCheckTimeMillis(Long.MAX_VALUE);
		params.setMaxItems(-1);
		return params;
	}
}
