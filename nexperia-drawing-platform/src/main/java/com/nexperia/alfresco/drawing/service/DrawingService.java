package com.nexperia.alfresco.drawing.service;

import com.nexperia.alfresco.drawing.dto.PreviewResponse;
import com.nexperia.alfresco.drawing.model.DrawingModel;
import com.nexperia.alfresco.drawing.model.legacy.LegacyDrawingModel;
import com.nexperia.alfresco.drawing.service.exceptions.DrawingExistsException;
import com.nexperia.alfresco.drawing.utils.DrawingSearchUtils;
import com.nexperia.alfresco.drawing.utils.DrawingUtil;
import com.nexperia.alfresco.drawing.utils.SearchUtils;
import org.alfresco.error.AlfrescoRuntimeException;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.activities.ActivityType;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.activities.ActivityService;
import org.alfresco.service.cmr.repository.*;
import org.alfresco.service.cmr.search.SearchService;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.namespace.QName;
import org.alfresco.transform.client.model.Mimetype;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.extensions.webscripts.WebScriptRequest;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.*;
import java.util.function.BiPredicate;
import java.util.stream.Collectors;

import static com.nexperia.alfresco.drawing.model.DrawingModel.DRAWING_MODEL_1_0_URI;

public class DrawingService {

    public static final Logger logger = LoggerFactory.getLogger(DrawingService.class);

    private ServiceRegistry serviceRegistry;
    private String defaultTargetNr;
    private SearchService searchService;
    private DrawNameSequentialIdentifierStrategy sequentialIdentifierStrategy;
    private ActivityService activityService;
    private DrawingUtil drawingUtil;
    private CreateRenditionService createRenditionService;
    private StructureService structureService;

    private List<String> renditionsForPreview;

    public String createDrawing(String drawingName, Map<QName, Serializable> properties) throws DrawingExistsException {
        if (StringUtils.isEmpty(drawingName)) {
            String query = String.format("TYPE:\"%1$s\"", LegacyDrawingModel.TYPE_DRAWINGS_ROOT_SPACE.toString());
            NodeRef drawingsRoot = DrawingSearchUtils.getFirstResult(searchService, query);

            drawingName = sequentialIdentifierStrategy.setNextIdentifier(drawingsRoot);

            int counter = 0;
            while (counter < 100) {
                NodeRef nodeWithDuplicateName = DrawingSearchUtils.getFirstResult(searchService, "TYPE:\"draw:drawingSpace\" && @cm\\:name:\"" + drawingName + "\"");
                if (nodeWithDuplicateName == null) break;
                drawingName = sequentialIdentifierStrategy.setNextIdentifier(drawingsRoot);
                counter++;
            }
        }

        String query = String.format("TYPE:\"%1$s\" AND @cm\\:name:%2$s", DrawingModel.TYPE_DRAWING.toPrefixString(serviceRegistry.getNamespaceService()), drawingName);
        NodeRef existingDrawing = DrawingSearchUtils.getFirstResult(searchService, query);
        if (existingDrawing != null) {
            throw new DrawingExistsException("Drawing with '" + drawingName + "' drawingID already exists!");
        }

        properties.put(ContentModel.PROP_NAME, drawingName);
        properties.put(DrawingModel.PROP_DRAWING_ID, drawingName);

        String drawingType = (String) properties.get(DrawingModel.PROP_DRAWING_TYPE);
        String magCode = (String) properties.get(DrawingModel.PROP_MAG_CODE);
        String createdFor = (String) properties.get(DrawingModel.PROP_CREATED_FOR);
        NodeRef targetSpaceNr = structureService.getTargetSpace(drawingName, drawingType, magCode, createdFor);
        createNode(targetSpaceNr, drawingName, properties);
        return drawingName;
    }

    private void createNode(NodeRef parent, String nodeName, Map<QName, Serializable> properties) {
        boolean isMarkAsNew = false;
        if (parent == null) {
            parent = getSpaceLocation();
            isMarkAsNew = true;
        }

        NodeRef drawing = serviceRegistry
                .getNodeService()
                .createNode(
                        parent,
                        ContentModel.ASSOC_CONTAINS,
                        QName.createQName(NamespaceService.CONTENT_MODEL_1_0_URI, nodeName),
                        DrawingModel.TYPE_DRAWING,
                        properties).getChildRef();

        if (isMarkAsNew) {
            serviceRegistry.getNodeService().addAspect(drawing, DrawingModel.ASPECT_NEW, null);
        }
        activityService.postActivity(ActivityType.FOLDER_ADDED, "tdm-drawing", "siteService", drawing);
    }

    public String getDrawingId(String nodeId) {
        NodeRef nodeRef = new NodeRef(nodeId);
        return (String) serviceRegistry.getNodeService().getProperty(nodeRef, ContentModel.PROP_NAME);
    }

    public PreviewResponse getPreviewByRendition(String drawingId) {
        NodeRef drawing = DrawingSearchUtils.getFirstResult(searchService, "TYPE:\"draw:drawingSpace\" && @cm\\:name:\"" + drawingId + "\"");

        if (drawing == null) {
            return getDefaultImage();
        }

        NodeService nodeService = serviceRegistry.getNodeService();

        List<NodeRef> children = nodeService.getChildAssocs(drawing).stream().map(ChildAssociationRef::getChildRef).collect(Collectors.toList());

        NodeRef previewCandidate = renditionsForPreview.stream().map(fileExt -> nodeByFileExtension(children, fileExt)).filter(Objects::nonNull).findFirst().orElse(null);

        if (previewCandidate != null) {
            ContentService contentService = serviceRegistry.getContentService();
            ContentReader contentReader = contentService.getReader(previewCandidate, ContentModel.PROP_CONTENT);
            try (InputStream inputStream = contentReader.getContentInputStream()) {
                String contentType = serviceRegistry.getMimetypeService().guessMimetype((String) nodeService.getProperty(previewCandidate, ContentModel.PROP_NAME));
                return new PreviewResponse(inputStream.readAllBytes(), contentType);
            } catch (IOException e) {
                throw new AlfrescoRuntimeException(e.getMessage());
            }
        }

        return getDefaultImage();
    }

    private PreviewResponse getDefaultImage() {
        ClassPathResource defaultAsset = new ClassPathResource("/assets/Nexperia_X_RGB.svg");
        try (InputStream inputStream = defaultAsset.getInputStream()) {
            return new PreviewResponse(inputStream.readAllBytes(), Mimetype.MIMETYPE_IMAGE_SVG);
        } catch (IOException e) {
            throw new AlfrescoRuntimeException(e.getMessage());
        }
    }

    private NodeRef nodeByFileExtension(List<NodeRef> nodes, String fileExtension) {
        return nodes.stream().filter(nodeRef -> fileExtensionPredicate.test(fileExtension, nodeRef)).findFirst().orElse(null);
    }

    private final BiPredicate<String, NodeRef> fileExtensionPredicate = (String ext, NodeRef node) -> {
        NodeService nodeService = serviceRegistry.getNodeService();
        String fileName = (String) nodeService.getProperty(node, ContentModel.PROP_NAME);
        return StringUtils.endsWithIgnoreCase(fileName, "." + ext);
    };

    public PreviewResponse getPreviewByRenditionForExternalTools(String drawingName, String version) {
        String fileExtension = drawingUtil.getFileExtension(drawingName);
        NodeRef drawing = DrawingSearchUtils.getFirstResult(searchService, "TYPE:\"draw:drawingSpace\" && @cm\\:name:\"" + FilenameUtils.getBaseName(drawingName) + "\"");
        if (drawing == null) {
            logger.debug("Drawing for '{}' wasn't found!", drawingName);
            return null;
        }
        NodeRef renditionForPreview = drawingUtil.findExistingRendition(drawing, fileExtension);
        if (renditionForPreview == null) {
            logger.debug("'{}' rendition wasn't found! It has to be generated", drawingName);
            String primaryFormat = (String) serviceRegistry.getNodeService().getProperty(drawing, DrawingModel.PROP_PRIMARY_FORMAT);
            NodeRef primaryFormatRendition = drawingUtil.findExistingRendition(drawing, primaryFormat.toLowerCase());
            if (primaryFormatRendition != null) {
                renditionForPreview = createRenditionService.createRendition(primaryFormatRendition, fileExtension);
            }
        }
        if (renditionForPreview != null) {
            NodeService nodeService = serviceRegistry.getNodeService();
            if (StringUtils.isNotEmpty(version) && serviceRegistry.getVersionService().getVersionHistory(renditionForPreview).getVersion(version) != null) {
                renditionForPreview = serviceRegistry.getVersionService().getVersionHistory(renditionForPreview).getVersion(version).getFrozenStateNodeRef();
            }
            ContentService contentService = serviceRegistry.getContentService();
            ContentReader contentReader = contentService.getReader(renditionForPreview, ContentModel.PROP_CONTENT);
            try (InputStream inputStream = contentReader.getContentInputStream()) {
                String contentType = serviceRegistry.getMimetypeService().guessMimetype((String) nodeService.getProperty(renditionForPreview, ContentModel.PROP_NAME));
                return new PreviewResponse(inputStream.readAllBytes(), contentType);
            } catch (IOException e) {
                throw new AlfrescoRuntimeException(e.getMessage());
            }
        }
        return null;
    }

    private NodeRef getSpaceLocation() {
        return SearchUtils.getSpaceLocation(defaultTargetNr, serviceRegistry);
    }

    public void setServiceRegistry(ServiceRegistry serviceRegistry) {
        this.serviceRegistry = serviceRegistry;
    }

    public void setDefaultTargetNr(String defaultTargetNr) {
        this.defaultTargetNr = defaultTargetNr;
    }

    public void setSearchService(SearchService searchService) {
        this.searchService = searchService;
    }

    public void setSequentialIdentifierStrategy(DrawNameSequentialIdentifierStrategy sequentialIdentifierStrategy) {
        this.sequentialIdentifierStrategy = sequentialIdentifierStrategy;
    }

    public void setRenditionsForPreview(List<String> renditionsForPreview) {
        this.renditionsForPreview = renditionsForPreview;
    }

    public void setActivityService(ActivityService activityService) {
        this.activityService = activityService;
    }

    public void setDrawingUtil(DrawingUtil drawingUtil) {
        this.drawingUtil = drawingUtil;
    }

    public void setCreateRenditionService(CreateRenditionService createRenditionService) {
        this.createRenditionService = createRenditionService;
    }

    public void setStructureService(StructureService structureService) {
        this.structureService = structureService;
    }
}
