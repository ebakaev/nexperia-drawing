package com.nexperia.alfresco.drawing.service;

import com.nexperia.alfresco.drawing.model.legacy.LegacyDrawingModel;
import com.nexperia.alfresco.drawing.utils.DrawingSearchUtils;
import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.cmr.search.SearchService;
import org.alfresco.service.namespace.QName;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.StringTokenizer;

public class DrawNameSequentialIdentifierStrategy {

	public static final QName IDENTIFIER_SET_QNAME = QName.createQName("http://www.nxp.com/model/common-core/1.0", "identifierSet");
	public static final QName SEQUENTIAL_ROOT_ASPECT_NAME = QName.createQName("http://www.nxp.com/model/common-core/1.0", "sequentialIdentifierRoot");
	public static final QName COUNTER_PROPERTY_QNAME = QName.createQName("http://www.nxp.com/model/common-core/1.0", "lastIdentifier");
	public static final QName IDENTIFIER_QNAME = QName.createQName("http://www.nxp.com/model/common-core/1.0", "identifier");

	private static final int SEQUENTIAL_INCREMENT = 1;
	private static final String IDENTIFIER_SEQUENCE_INITIAL = "aaa-000000";
	private static final String DELIMETER = "-";
	private static final String LETTERS = "abcdefghijklmnopqrstuvwxyz";

	private NodeService nodeService;
	private SearchService searchService;

	public static final QName SEQUENTIALLY_NUMBERED_QNAME = QName.createQName("http://www.nxp.com/drawing/content-model", "drawingNameSequentiallyIdentified");

	private static  final Log log = LogFactory.getLog(DrawNameSequentialIdentifierStrategy.class);


	public String setNextIdentifier(NodeRef currentNode) {
		// Finds the first parent occurrence of a node who is the marker "nxp:sequentialIdentifierRoot"
		String lastIdentifier;
		String newIdentifier = null;
		boolean lastIdentifierFound = false;
		NodeRef initialNode = currentNode;

		while (currentNode != null) {
			if (nodeService.hasAspect(currentNode, SEQUENTIAL_ROOT_ASPECT_NAME)) {
				lastIdentifier = (String) nodeService.getProperty(currentNode, COUNTER_PROPERTY_QNAME);
				lastIdentifierFound = true;
				log.debug("Successfully found last identifier : " + lastIdentifier + " on space: " + nodeService.getProperty(currentNode, ContentModel.PROP_NAME));
				if (lastIdentifier == null) {
					lastIdentifier = IDENTIFIER_SEQUENCE_INITIAL;
				}
				newIdentifier = generateIdentifier(lastIdentifier);
				nodeService.setProperty(currentNode, COUNTER_PROPERTY_QNAME, newIdentifier);
				break;
			} else {
				ChildAssociationRef parentAssoc = nodeService.getPrimaryParent(currentNode);
				if (parentAssoc != null)
					currentNode = parentAssoc.getParentRef();
				else
					currentNode = null;
			}
		}
		if (!lastIdentifierFound) {
			log.error("Last identifier was not found for object " + nodeService.getProperty(initialNode, ContentModel.PROP_NAME) + "Failing");
			return null;
		}

		// Actually sets the id
		nodeService.setProperty(initialNode, IDENTIFIER_QNAME, newIdentifier);

		log.debug("Successfully set identifier: " + newIdentifier + " on document " + nodeService.getProperty(initialNode, ContentModel.PROP_NAME) + " property " + IDENTIFIER_QNAME);

		return (newIdentifier);

	}

	private String generateIdentifier(String lastIdentifier) {
		StringTokenizer tolenizer = new StringTokenizer(lastIdentifier, DELIMETER);
		String letters = tolenizer.nextToken();
		String resultLetters = letters;
		int digits = Integer.parseInt(tolenizer.nextToken());
		String resultDigits;
		digits += SEQUENTIAL_INCREMENT;
		if (digits >= 1000000) {
			resultDigits = "000000";
			char first = letters.charAt(0);
			char second = letters.charAt(1);
			char last = letters.charAt(2);
			if (LETTERS.indexOf(last) == 25) {
				last = 'a';
				if (LETTERS.indexOf(second) == 25) {
					second = 'a';
					if (LETTERS.indexOf(first) == 25) {
						first = 'a';
						second = 'a';
						last = 'a';
					} else {
						first = LETTERS.charAt(LETTERS.indexOf(second) + 1);
					}
				} else {
					second = LETTERS.charAt(LETTERS.indexOf(second) + 1);
				}
			} else {
				last = LETTERS.charAt(LETTERS.indexOf(last) + 1);
			}
			resultLetters = first + String.valueOf(second) + last;
		} else {
			resultDigits = String.valueOf(digits);
		}

		String resultIdentifier = resultLetters + DELIMETER + StringUtils.leftPad(resultDigits, 6, "0");

		if (!checkNewIdentifier(resultIdentifier)) {
			resultIdentifier = generateIdentifier(resultIdentifier);
		}

		return resultIdentifier;
	}

	private boolean checkNewIdentifier(String resultIdentifier) {
		NodeRef drawsRoot =
				DrawingSearchUtils.getFirstResult(
						searchService,
						String.format("+TYPE:\"%1$s\" +@cm\\:name:\"%2$s\"", LegacyDrawingModel.TYPE_DRAWINGS_SPACE.toString(), resultIdentifier)
				);
		return drawsRoot == null;
	}

	public Boolean canGenerateIdentifier(NodeRef node) {
		Boolean identifierSet = (Boolean) nodeService.getProperty(node, IDENTIFIER_SET_QNAME);
		identifierSet = (identifierSet == null) ? Boolean.FALSE : identifierSet;
		return !identifierSet;
	}

	public QName getIdentifierStrategyQName() {
		return SEQUENTIALLY_NUMBERED_QNAME;
	}

	public void setNodeService(NodeService nodeService) {
		this.nodeService = nodeService;
	}

	public void setSearchService(SearchService searchService) {
		this.searchService = searchService;
	}

}
