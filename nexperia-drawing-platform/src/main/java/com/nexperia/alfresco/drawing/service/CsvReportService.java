package com.nexperia.alfresco.drawing.service;

import com.opencsv.CSVWriter;
import com.opencsv.bean.*;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.io.FileWriter;
import java.io.Writer;
import java.util.List;

@Slf4j
public abstract class CsvReportService<T> {

	void createCsvFile(String csvPath, Class<T> type, List<T> data) {
		try (Writer writer = new FileWriter(csvPath)) {
			CustomMappingStrategy<T> mappingStrategy = new CustomMappingStrategy<>();
			mappingStrategy.setType(type);

			StatefulBeanToCsv<T> toCsv = new StatefulBeanToCsvBuilder<T>(writer)
					.withSeparator(CSVWriter.DEFAULT_SEPARATOR)
					.withMappingStrategy(mappingStrategy)
					.build();

			toCsv.write(data);
		} catch (Exception e) {
			throw new RuntimeException(String.format("Failed to generate CSV file: %s", e.getMessage()));
		}
	}

	@SuppressWarnings("rawtypes")
	static class CustomMappingStrategy<T> extends ColumnPositionMappingStrategy<T> {
		@Override
		public String[] generateHeader(T bean) throws CsvRequiredFieldEmptyException {
			final int numColumns = getFieldMap().values().size();
			super.generateHeader(bean);

			String[] header = new String[numColumns];

			BeanField beanField;
			for (int i = 0; i < numColumns; i++) {
				beanField = findField(i);
				String columnHeaderName = extractHeaderName(beanField);
				header[i] = columnHeaderName;
			}
			return header;
		}

		private String extractHeaderName(final BeanField beanField) {
			if (beanField == null || beanField.getField() == null || beanField.getField().getDeclaredAnnotationsByType(CsvBindByName.class).length == 0) {
				return StringUtils.EMPTY;
			}

			final CsvBindByName bindByNameAnnotation = beanField.getField().getDeclaredAnnotationsByType(CsvBindByName.class)[0];
			return bindByNameAnnotation.column();
		}
	}
}
