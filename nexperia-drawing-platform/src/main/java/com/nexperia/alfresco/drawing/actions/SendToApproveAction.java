package com.nexperia.alfresco.drawing.actions;

import com.nexperia.alfresco.drawing.model.DrawingModel;
import com.nexperia.alfresco.drawing.service.EmailService;
import com.nexperia.alfresco.drawing.utils.DrawingUtil;
import org.alfresco.error.AlfrescoRuntimeException;
import org.alfresco.repo.forum.CommentService;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.action.Action;
import org.alfresco.service.cmr.repository.NodeRef;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class SendToApproveAction extends BaseFlowAction {

	private static final Logger logger = LoggerFactory.getLogger(SendToApproveAction.class);

	protected SendToApproveAction(EmailService emailService, ServiceRegistry serviceRegistry, CommentService commentService, String emailSubject, String emailTemplate, String baseUrl, DrawingUtil drawingUtil) {
		super(emailService, serviceRegistry, commentService, emailSubject, emailTemplate, baseUrl, drawingUtil);
	}

	@Override
	protected boolean isAllowed(NodeRef nodeRef) {
		return DrawingModel.TYPE_DRAWING.equals(serviceRegistry.getNodeService().getType(nodeRef));
	}

	@SuppressWarnings("unchecked")
	@Override
	protected void executeImpl(Action action, NodeRef drawing) {
		List<NodeRef> renditions = super.getRenditions(drawing);
		logger.debug("Release renditions: " + renditions + " by NR: " + drawing);
		if (renditions.size() == 0) {
			throw new AlfrescoRuntimeException("There is no rendition to approve!");
		}
		boolean isAllowNextStep = drawingUtil.isAllowNextStep(drawing);
		boolean isRevisedOnce = drawingUtil.isRevisedOnce(drawing);
		if (isRevisedOnce && !isAllowNextStep) {
			throw new AlfrescoRuntimeException("There is no new rendition to approve!");
		}
		super.executeAction(action, drawing, renditions);
	}

	@Override
	protected void updateStatus(NodeRef drawing, List<NodeRef> renditions) {
		serviceRegistry.getNodeService().setProperty(drawing, DrawingModel.PROP_STATUS, DrawingModel.STATUS_PENDING_APPROVAL);
	}

	@Override
	protected void updateVersion(Action action, NodeRef drawing) {
		//Nothing
	}
}
