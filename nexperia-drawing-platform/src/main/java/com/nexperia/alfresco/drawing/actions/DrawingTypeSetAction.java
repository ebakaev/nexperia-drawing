package com.nexperia.alfresco.drawing.actions;

import com.nexperia.alfresco.drawing.model.DrawingModel;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.action.executer.ActionExecuterAbstractBase;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.action.Action;
import org.alfresco.service.cmr.action.ParameterDefinition;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;

import java.util.List;

public class DrawingTypeSetAction extends ActionExecuterAbstractBase {

    ServiceRegistry serviceRegistry;

    @Override
    protected void executeImpl(Action action, NodeRef nodeRef) {
        QName type = serviceRegistry.getNodeService().getType(nodeRef);
        if (ContentModel.TYPE_FOLDER.equals(type)) {
            serviceRegistry.getNodeService().setType(nodeRef, DrawingModel.TYPE_DRAWING);
        }
    }

    @Override
    protected void addParameterDefinitions(List<ParameterDefinition> list) {
        // nothing
    }

    public void setServiceRegistry(ServiceRegistry serviceRegistry) {
        this.serviceRegistry = serviceRegistry;
    }
}
