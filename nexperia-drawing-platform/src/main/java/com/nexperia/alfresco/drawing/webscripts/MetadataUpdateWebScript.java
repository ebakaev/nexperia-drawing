package com.nexperia.alfresco.drawing.webscripts;

import com.nexperia.alfresco.drawing.dto.DrawingRepresentation;
import com.nexperia.alfresco.drawing.model.DrawingModel;
import com.nexperia.alfresco.drawing.utils.DrawingUtil;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.namespace.QName;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.extensions.webscripts.Cache;
import org.springframework.extensions.webscripts.DeclarativeWebScript;
import org.springframework.extensions.webscripts.Status;
import org.springframework.extensions.webscripts.WebScriptRequest;

import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MetadataUpdateWebScript extends DeclarativeWebScript {

    protected final Logger logger = LoggerFactory.getLogger(getClass());

    ServiceRegistry serviceRegistry;
    DrawingUtil drawingUtil;
    String baseUrl;

    private final String POST_METHOD = "POST";
    private final String GET_METHOD = "GET";

    @Override
    protected Map<String, Object> executeImpl(WebScriptRequest req, Status status, Cache cache) {
        Map<String, Object> result = new HashMap<String, Object>();
        String httpMethod = req.getParameter("nodeRefs") == null ? POST_METHOD : GET_METHOD;
        List<DrawingRepresentation> drawings = new ArrayList<DrawingRepresentation>();
        List<String> failed = new ArrayList<>();
        List<String> success = new ArrayList<>();
        if (GET_METHOD.equals(httpMethod)) {
            List<String> drawingNodeRefs = Arrays.stream(req.getParameter("nodeRefs").split(",")).collect(Collectors.toList());
            drawings = drawingNodeRefs.stream().map(drawing -> getDrawingRepresentation(new NodeRef(drawing), "false")).collect(Collectors.toList());
        } else {
            NodeService nodeService = serviceRegistry.getNodeService();
            List<String> drawingRefs = Arrays.stream(req.getParameterNames()).filter(str -> str.contains("_desctitle")).collect(Collectors.toList());
            drawingRefs.replaceAll(descTitle -> descTitle.replace("_desctitle", ""));
            for (String drawingRef : drawingRefs) {
                NodeRef drawing = new NodeRef(DrawingUtil.NODE_REF_SPACE_NAME + drawingRef);
                String drawingName = drawingUtil.getName(drawing);
                Map<QName, Serializable> properties = Arrays.stream(req.getParameterNames())
                        .filter(str -> str.contains(drawingRef))
                        .collect(Collectors.toMap(
                                data -> (QName) DRAWING_PROPERTIES_MAP.get(data.replace(drawingRef, "")),
                                data -> data.contains("_desctitle") ? ((String) req.getParameter(data)).trim() : (Serializable) req.getParameter(data)));
                logger.debug("Updating metadata for {} Drawing...", drawingName);
                properties.values().removeIf(value -> (value.equals("undefined") || value.equals("Not defined") || value.equals("")));
                if (properties.get(DrawingModel.PROP_REQUESTER) != null && !drawingUtil.isValidEmailAddress((String) properties.get(DrawingModel.PROP_REQUESTER))) {
                    drawings.add(getDrawingRepresentation(drawing, "false"));
                    failed.add(drawingName);
                    continue;
                }
                String requiredRenditions = (String) properties.get(DrawingModel.PROP_REQUIRED_RENDITIONS);
                if (StringUtils.isNotEmpty(requiredRenditions)) {
                    ArrayList<String> renditions = new ArrayList<>();
                    String[] renditionList = requiredRenditions.split(",");
                    Collections.addAll(renditions, renditionList);
                    properties.put(DrawingModel.PROP_REQUIRED_RENDITIONS, renditions);
                }
                Map<QName, Serializable> allProperties = nodeService.getProperties(drawing);
                allProperties.putAll(properties);
                nodeService.setProperties(drawing, allProperties);
                drawings.add(getDrawingRepresentation(drawing, "true"));
                success.add(drawingName);
            }
        }

        result.put("drawings", drawings);
        result.put("failed", String.join(",", failed));
        result.put("success", String.join(",", success));
        return result;
    }

    private DrawingRepresentation getDrawingRepresentation(NodeRef drawing, String success) {
        return DrawingRepresentation.builder()
                .nodeRef(drawing.toString().replace(DrawingUtil.NODE_REF_SPACE_NAME, ""))
                .drawingId(drawingUtil.getName(drawing))
                .createdFor(drawingUtil.getCreatedFor(drawing))
                .descriptiveTitle(drawingUtil.getDescriptiveTitle(drawing))
                .drawingType(drawingUtil.getType(drawing))
                .magCode(drawingUtil.getMAGCode(drawing))
                .securityStatus(drawingUtil.getSecurityStatus(drawing))
                .drawingVersion(drawingUtil.getDrawingVersion(drawing))
                .primaryFormat(drawingUtil.getPrimaryFormat(drawing))
                .requester(drawingUtil.getRequester(drawing))
                .deliveryDate(drawingUtil.getDeliveryDate(drawing))
                .requiredRenditions(drawingUtil.getRequiredRenditionsAsString(drawing))
                .drawingStatus(drawingUtil.getStatus(drawing))
                .success(success)
                .baseUrl(baseUrl)
                .build();
    }

    public void setServiceRegistry(ServiceRegistry serviceRegistry) {
        this.serviceRegistry = serviceRegistry;
    }

    public void setDrawingUtil(DrawingUtil drawingUtil) {
        this.drawingUtil = drawingUtil;
    }

    public static final Map<String, QName> DRAWING_PROPERTIES_MAP = Stream.of(new Object[][]{
            {"_desctitle", DrawingModel.PROP_DESCRIPTIVE_TITLE},
            {"_drawtype", DrawingModel.PROP_DRAWING_TYPE},
            {"_cf", DrawingModel.PROP_CREATED_FOR},
            {"_ss", DrawingModel.PROP_SECURITY_STATUS},
            {"_mc", DrawingModel.PROP_MAG_CODE},
            {"_req", DrawingModel.PROP_REQUESTER},
            {"_dd", DrawingModel.PROP_DELIVERY_DATE},
            {"_rr", DrawingModel.PROP_REQUIRED_RENDITIONS},
    }).collect(Collectors.toMap(data -> (String) data[0], data -> (QName) data[1]));

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }
}
