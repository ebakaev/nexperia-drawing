package com.nexperia.alfresco.drawing.actions;

import com.nexperia.alfresco.drawing.model.DrawingModel;
import com.nexperia.alfresco.drawing.service.EmailService;
import com.nexperia.alfresco.drawing.utils.DrawingUtil;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.forum.CommentService;
import org.alfresco.repo.policy.BehaviourFilter;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.action.Action;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class ReviseAction extends BaseFlowAction {

    public static final String NAME = "revise-action";

    public static final Logger logger = LoggerFactory.getLogger(ReviseAction.class);

    BehaviourFilter policyFilter;

    protected ReviseAction(EmailService emailService, ServiceRegistry serviceRegistry, CommentService commentService, String emailSubject, String emailTemplate, String baseUrl, DrawingUtil drawingUtil) {
        super(emailService, serviceRegistry, commentService, emailSubject, emailTemplate, baseUrl, drawingUtil);
    }

    @Override
    protected boolean isAllowed(NodeRef nodeRef) {
        return DrawingModel.TYPE_DRAWING.equals(serviceRegistry.getNodeService().getType(nodeRef)) && DrawingModel.STATUS_RELEASED.equals(serviceRegistry.getNodeService().getProperty(nodeRef, DrawingModel.PROP_STATUS));
    }

    @Override
    protected void executeImpl(Action action, NodeRef nodeRef) {
        super.executeAction(action, nodeRef, getRenditions(nodeRef));
    }

    @Override
    protected void updateStatus(NodeRef drawing, List<NodeRef> renditions) {
        String name = (String) serviceRegistry.getNodeService().getProperty(drawing, ContentModel.PROP_NAME);
        for (NodeRef rendition : renditions) {
            serviceRegistry.getNodeService().setProperty(rendition, DrawingModel.PROP_STATUS, DrawingModel.STATUS_DRAFT);
            String renditionName = (String) serviceRegistry.getNodeService().getProperty(rendition, ContentModel.PROP_NAME);
            logger.debug("{} rendition status updated to {}", renditionName, DrawingModel.STATUS_DRAFT);
        }
        serviceRegistry.getNodeService().setProperty(drawing, DrawingModel.PROP_STATUS, DrawingModel.STATUS_DRAFT);
        logger.debug("{} drawing job status updated to {}", name, DrawingModel.STATUS_DRAFT);
        serviceRegistry.getNodeService().setProperty(drawing, DrawingModel.PROP_IS_REVISED_ONCE, true);
        logger.debug(name + " drawing job is revised.");
    }

    @Override
    protected void updateVersion(Action action, NodeRef drawing) {
        NodeService nodeService = serviceRegistry.getNodeService();
        boolean isMajor = action.getParameterValue(VERSION_PARAM).equals("major");
        String drawingVersion = (String) nodeService.getProperty(drawing, DrawingModel.PROP_DRAWING_VERSION);
        String newDrawingVersion = DrawingUtil.incrementVersion(drawingVersion, isMajor);
        nodeService.setProperty(drawing, DrawingModel.PROP_DRAWING_VERSION, newDrawingVersion);
    }

    public void setPolicyFilter(BehaviourFilter policyFilter) {
        this.policyFilter = policyFilter;
    }
}
