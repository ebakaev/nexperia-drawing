package com.nexperia.alfresco.drawing.remote.client;

import com.nexperia.alfresco.drawing.remote.exception.WebScriptClientException;

import java.io.InputStream;
import java.util.Map;

public interface PublicationClient extends AuthenticatingClient {

    /**
     * Publishes a document to remote Publication space.
     *
     * @param content input stream with document content.
     * @param metadata mapping between document meta data names and values.
     * @return ID of published document in case of successful publication.
     * @throws WebScriptClientException if other problems prevented from updating the published document.
     * You can catch WebScriptClientException inheritors to get more specific information about what happened.
     */
    String publish(InputStream content, Map<String, String> metadata) throws WebScriptClientException;

    /**
     * Publishes a document to remote Publication space.
     *
     * @param content input stream with document content.
     * @param metadata mapping between document meta data names and values.
     * @param publicationParameters additional parameters used during publication.
     * For parameter keys see {@link PublicationParameters}.
     * @return ID of published document in case of successful publication.
     * @throws WebScriptClientException if other problems prevented from updating the published document.
     * You can catch WebScriptClientException inheritors to get more specific information about what happened.
     */
    String publish(InputStream content, Map<String, String> metadata, Map<String, String> publicationParameters)
            throws WebScriptClientException;

    /**
     * Updates a published document in remote Publication space.
     *
     * @param id ID of the document to update.
     * @param content input stream with updated content. Can be null if content should not be updated.
     * @param metadata mapping between document meta data names and values.
     * @return True if published document was updated. False if document was not updated, because
     * properties and content provided are equal to those of already published document.
     * @throws WebScriptClientException if other problems prevented from updating the published document.
     * You can catch WebScriptClientException inheritors to get more specific information about what happened.
     */
    boolean update(String id, InputStream content, Map<String, String> metadata) throws WebScriptClientException;

    /**
     * Updates a published document in remote Publication space.
     *
     * @param id ID of the document to update.
     * @param content input stream with updated content. Can be null if content should not be updated.
     * @param metadata mapping between document meta data names and values.
     * @param publicationParameters additional parameters used during publication.
     * For parameter keys see {@link PublicationParameters}.
     * @return True if published document was updated. False if document was not updated, because
     * properties and content provided are equal to those of already published document.
     * @throws WebScriptClientException if other problems prevented from updating the published document.
     * You can catch WebScriptClientException inheritors to get more specific information about what happened.
     */
    boolean update(String id, InputStream content, Map<String, String> metadata, Map<String, String> publicationParameters)
            throws WebScriptClientException;


}
