package com.nexperia.alfresco.drawing.webscripts;

import com.nexperia.alfresco.drawing.utils.DrawingUtil;
import org.alfresco.model.ContentModel;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.extensions.webscripts.AbstractWebScript;
import org.springframework.extensions.webscripts.WebScriptException;
import org.springframework.extensions.webscripts.WebScriptRequest;
import org.springframework.extensions.webscripts.WebScriptResponse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DownloadWebScript extends AbstractWebScript {

    final String EXTENSION_TO_DOWNLOAD = ".svg";

    ServiceRegistry serviceRegistry;
    DrawingUtil drawingUtil;

    @Override
    public void execute(WebScriptRequest webScriptRequest, WebScriptResponse webScriptResponse) throws IOException {
        String nodeRefs = webScriptRequest.getParameter("nodeRefs");
        List<String> renditionUrls = getRenditionDownloadUrls(nodeRefs);

        try {
            JSONObject tomJsonObj = new JSONObject();
            tomJsonObj.put("renditions", renditionUrls);
            String jsonString = tomJsonObj.toString();
            webScriptResponse.getWriter().write(jsonString);
        } catch (JSONException e) {
            throw new WebScriptException("Unable to serialize JSON response");
        }
    }

    private List<String> getRenditionDownloadUrls(String nodeRefs) {
        List<String> renditionUrlPaths = new ArrayList<>();
        String[] drawings = nodeRefs.split(",");
        for (String drawing : drawings) {
            NodeRef rendition = getSVGRendition(new NodeRef(drawing));
            if (rendition != null) {
//                String url = String.format("slingshot/node/content/workspace/SpacesStore/%s/%s?a=true",rendition.getId(), drawingUtil.getName(rendition));
                String url = rendition.getId() + "/" + drawingUtil.getName(rendition);
                renditionUrlPaths.add(url);
            }
        }

        return renditionUrlPaths;
    }

    protected NodeRef getSVGRendition(NodeRef drawing) {
        NodeService nodeService = serviceRegistry.getNodeService();
        return nodeService.getChildAssocs(drawing).stream().map(ChildAssociationRef::getChildRef).filter(drawingNr -> ((String) nodeService.getProperty(drawingNr, ContentModel.PROP_NAME)).endsWith(EXTENSION_TO_DOWNLOAD)).findFirst().orElse(null);
    }

    public void setServiceRegistry(ServiceRegistry serviceRegistry) {
        this.serviceRegistry = serviceRegistry;
    }

    public void setDrawingUtil(DrawingUtil drawingUtil) {
        this.drawingUtil = drawingUtil;
    }
}
