package com.nexperia.alfresco.drawing.actions;

import com.nexperia.alfresco.drawing.service.StructureService;
import org.alfresco.repo.action.executer.ActionExecuterAbstractBase;
import org.alfresco.service.cmr.action.Action;
import org.alfresco.service.cmr.action.ParameterDefinition;
import org.alfresco.service.cmr.repository.NodeRef;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class StructureRulesAction extends ActionExecuterAbstractBase {

    public static final Logger logger = LoggerFactory.getLogger(StructureRulesAction.class);

    StructureService structureService;

    @Override
    protected void executeImpl(Action action, NodeRef drawing) {
        structureService.applyStructureRulesOnDrawing(drawing);
    }

    @Override
    protected void addParameterDefinitions(List<ParameterDefinition> list) {

    }

    public void setStructureService(StructureService structureService) {
        this.structureService = structureService;
    }
}
