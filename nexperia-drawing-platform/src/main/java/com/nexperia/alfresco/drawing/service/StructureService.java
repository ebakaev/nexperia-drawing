package com.nexperia.alfresco.drawing.service;

import com.nexperia.alfresco.drawing.model.DrawingModel;
import com.nexperia.alfresco.drawing.utils.DrawingUtil;
import org.alfresco.error.AlfrescoRuntimeException;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.transaction.RetryingTransactionHelper;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.model.FileInfo;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.StoreRef;
import org.alfresco.util.ISO9075;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class StructureService {

    public static final Logger logger = LoggerFactory.getLogger(StructureService.class);

    ServiceRegistry serviceRegistry;
    DrawingUtil drawingUtil;

    public void applyStructureRulesOnDrawing(NodeRef drawing) {
        String drawingName = drawingUtil.getName(drawing);
        logger.debug("Running action for {} [{}]", drawingName, drawing);
        if (serviceRegistry.getNodeService().getType(drawing).equals(DrawingModel.TYPE_DRAWING)) {
            String drawingType = (String) drawingUtil.getProperty(drawing, DrawingModel.PROP_DRAWING_TYPE);
            String drawingMagCode = (String) drawingUtil.getProperty(drawing, DrawingModel.PROP_MAG_CODE);
            String drawingCreatedFor = (String) drawingUtil.getProperty(drawing, DrawingModel.PROP_CREATED_FOR);
            NodeRef targetSpace = getTargetSpace(drawingName, drawingType, drawingMagCode, drawingCreatedFor);
            if (targetSpace != null) {
                try {
                    serviceRegistry.getNodeService().moveNode(drawing, targetSpace, null, null);
                    logger.debug("{} [{}] moved successfully...", drawingName, drawing);
                } catch (Exception e) {
                    logger.debug("{} [\t{}\t] FAILED with message: {}", drawingName, drawing, e.getMessage());
                    e.printStackTrace();
                }
                serviceRegistry.getNodeService().removeAspect(drawing, DrawingModel.ASPECT_NEW);
            } else {
                logger.debug("{} [{}] : Target space is null... It means Drawing doesn't match criteria and remain in Archive.", drawingName, drawing);
            }
        } else {
            logger.debug("{} ins't draw:drawingType", drawingUtil.getName(drawing));
        }
    }

    private NodeRef getTargetSpaceByCreatedFor(String drawingCreatedFor) {
        String subFolderName = "Applications/" + drawingCreatedFor;
        return createFolderStructureByPath("", subFolderName);
    }

    private boolean checkForCreatedFor(String drawingCreatedFor) {
        //Check the value of “Create for” start with “AN”or “UM” or “TN”
        //Check if a “Created for” folder exists under applications, if not create folder
        return StringUtils.startsWith(drawingCreatedFor, DrawingModel.DRAWING_CREATED_FOR_AN)
                || StringUtils.startsWith(drawingCreatedFor, DrawingModel.DRAWING_CREATED_FOR_TN)
                || StringUtils.startsWith(drawingCreatedFor, DrawingModel.DRAWING_CREATED_FOR_UM);
    }

    public NodeRef getTargetSpace(String drawingName, String drawingType, String magCode, String createdFor) {
        NodeRef targetSpace = null;
        if (DrawingModel.DRAWING_TYPE_PACKING.equals(drawingType)) {
            targetSpace = getTargetSpaceByDrawingType(drawingType, drawingName);
        } else if (checkForOtherDrawingTypes(drawingType)) {
            targetSpace = getTargetSpaceByDrawingType(drawingType, drawingName);
        } else if (checkForCreatedFor(createdFor)) {
            targetSpace = getTargetSpaceByCreatedFor(createdFor);
        } else if (checkForMagCode(magCode)) {
            targetSpace = getTargetSpaceByMagCode(magCode, drawingName);
        }
        return targetSpace;
    }

    private NodeRef getTargetSpaceByMagCode(String drawingMagCode, String drawingName) {
        //Check if there is a folder with the first MAG code and 3 characters of the job, if not create it
        String subFolderName = DrawingModel.DRAWING_MAG_CODE_MAP.get(drawingMagCode) + "/" + drawingName.substring(0, 3);
        return createFolderStructureByPath("", subFolderName);
    }

    private boolean checkForMagCode(String drawingMagCode) {
        return DrawingModel.DRAWING_MAG_CODE_MAP.containsKey(drawingMagCode);
    }

    private boolean checkForOtherDrawingTypes(String drawingType) {
        return DrawingModel.DRAWING_TYPE_BLANK_PINNING_DIAGRAM.equals(drawingType)
                || DrawingModel.DRAWING_TYPE_FOOTPRINT.equals(drawingType)
                || DrawingModel.DRAWING_TYPE_MINIMIZED_OUTLINE.equals(drawingType)
                || DrawingModel.DRAWING_TYPE_PACKAGE_OUTLINE.equals(drawingType)
                || DrawingModel.DRAWING_TYPE_SIMPLIFIED_OUTLINE.equals(drawingType)
                || DrawingModel.DRAWING_TYPE_OUTLINE_3D.equals(drawingType);
    }

    private NodeRef getTargetSpaceByDrawingType(String drawingType, String drawingName) {
        String subFolderName = "";
        if (DrawingModel.DRAWING_TYPE_PACKING.equals(drawingType)) {
            subFolderName = "Packing";
        } else if (checkForOtherDrawingTypes(drawingType)) {
            if (!drawingName.contains("_")) {
                subFolderName = "Packages/" + drawingName;
            } else {
                subFolderName = "Packages/" + drawingName.substring(0, drawingName.indexOf("_"));
            }
        }
        if (StringUtils.isNotEmpty(subFolderName)) {
            return createFolderStructureByPath("", subFolderName);
        }
        return null;
    }

    private synchronized NodeRef createFolderStructureByPath(String rootFolderName, String subFolderName) {
        NodeRef resultFolder = null;
        String siteShortName = "tdm-drawing";
        String folderPathQuery = getFolderPathQuery(rootFolderName, siteShortName); //Change the query accordingly if not using sites
        List<NodeRef> selectedNodes = serviceRegistry.getSearchService().selectNodes(serviceRegistry.getNodeService().getRootNode(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE),
                folderPathQuery, null, serviceRegistry.getNamespaceService(), false);
        if (selectedNodes != null && !selectedNodes.isEmpty()) {
            NodeRef rootFolderRef = selectedNodes.get(0);
            if (rootFolderRef != null && serviceRegistry.getFileFolderService().exists(rootFolderRef)) {
                if (subFolderName.contains("/")) {
                    String[] pathTokens = subFolderName.split("/");
                    resultFolder = rootFolderRef;
                    for (String subFolder : pathTokens) {
                        RetryingTransactionHelper retryingTransactionHelper = serviceRegistry.getRetryingTransactionHelper();
                        retryingTransactionHelper.setMaxRetries(3);
                        try {
                            resultFolder = retryingTransactionHelper.doInTransaction(new CreateSpaceCommand(resultFolder, subFolder), false, true);
                        } catch (AlfrescoRuntimeException e) {
                            throw new AlfrescoRuntimeException(String.format("=== Failed to create subFolder '%s'", subFolder), e);
                        } catch (IllegalStateException e) {
                            throw new AlfrescoRuntimeException(String.format("=== Failed to create subFolder '%s' due to illegal state, see below", subFolder), e);
                        }
//                        NodeRef folderForCreate = containsFolder(resultFolder, subFolder);
//                        if (folderForCreate == null) {
//                            try {
//                                resultFolder = serviceRegistry.getFileFolderService().create(resultFolder, subFolder, ContentModel.TYPE_FOLDER).getNodeRef();
//                            } catch (FileExistsException existsException) {
//
//                            }
//                        } else {
//                            resultFolder = folderForCreate;
//                        }
                    }
                } else {
                    NodeRef folderForCreate = containsFolder(rootFolderRef, subFolderName);
                    if (folderForCreate == null) {
                        resultFolder = serviceRegistry.getFileFolderService().create(rootFolderRef, subFolderName.trim(), ContentModel.TYPE_FOLDER).getNodeRef();
                    } else {
                        resultFolder = folderForCreate;
                    }
                }
            } else {
                // handle and notify message as needed. ROOT FOLDR NOT FOUND
                logger.debug("ROOT FOLDR NOT FOUND");
            }
        } else {
            // handle and notify message as needed.
            logger.debug("FOUND something!");
        }
        return resultFolder;
    }

    private String getFolderPathQuery(String folderPath, String siteShortName) {
        StringBuilder queryBuilder = new StringBuilder("/app:company_home/st:sites/");
        queryBuilder.append("cm:").append(ISO9075.encode(siteShortName))
                .append("/cm:documentLibrary");
        if (null != folderPath && !folderPath.isEmpty()) {
            String[] pathTokens = folderPath.split("/");
            if (!"Document Library".equals(folderPath)) {
                for (String pathToken : pathTokens) {
                    queryBuilder.append("/cm:").append(ISO9075.encode(pathToken));
                }
            }
        }
        return queryBuilder.toString();
    }

    private NodeRef containsFolder(NodeRef space, String folderName) {
        List<FileInfo> folders = serviceRegistry.getFileFolderService().listFolders(space);
        for (FileInfo fi : folders) {
            if (fi.getName().equalsIgnoreCase(folderName.trim())) {
                return fi.getNodeRef();
            }
        }
//        return serviceRegistry.getFileFolderService().listFolders(space).stream().filter(fileInfo -> fileInfo.getName().equals(folderName)).
        return null;
    }


    protected class CreateSpaceCommand implements RetryingTransactionHelper.RetryingTransactionCallback<NodeRef> {

        NodeRef resultFolder;
        String subFolder;

        public CreateSpaceCommand(NodeRef resultFolder, String subFolder) {
            this.resultFolder = resultFolder;
            this.subFolder = subFolder;
        }

        @Override
        public NodeRef execute() throws Throwable {
            NodeRef folderForCreate = containsFolder(resultFolder, subFolder);
            if (folderForCreate == null) {
                resultFolder = serviceRegistry.getFileFolderService().create(resultFolder, subFolder.trim(), ContentModel.TYPE_FOLDER).getNodeRef();
            } else {
                resultFolder = folderForCreate;
            }
            return resultFolder;
        }
    }


    public void setServiceRegistry(ServiceRegistry serviceRegistry) {
        this.serviceRegistry = serviceRegistry;
    }

    public void setDrawingUtil(DrawingUtil drawingUtil) {
        this.drawingUtil = drawingUtil;
    }
}
