package com.nexperia.alfresco.drawing.service;

import com.nexperia.alfresco.drawing.dto.DrawingCsvDto;
import com.nexperia.alfresco.drawing.dto.DrawingSearchDto;
import com.nexperia.alfresco.drawing.model.DrawingModel;
import com.nexperia.alfresco.drawing.utils.SearchUtils;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.alfresco.error.AlfrescoRuntimeException;
import org.alfresco.model.ContentModel;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.repository.ContentWriter;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.namespace.QName;
import org.alfresco.util.TempFileProvider;
import org.apache.commons.lang3.StringUtils;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@AllArgsConstructor
@Slf4j
public class DrawingReportService extends CsvReportService<DrawingCsvDto> {

	private final ServiceRegistry serviceRegistry;

	@SuppressWarnings("unchecked")
	public NodeRef createReport(DrawingSearchDto drawingSearchDto) {
		String query = buildQuery(drawingSearchDto);
		List<NodeRef> drawings = SearchUtils.findAll(query, serviceRegistry);
		log.debug("There are {} Drawings found.", drawings.size());
		AtomicInteger count = new AtomicInteger(0);
		String pattern = "yyyy-MM-dd";
		String patternWithTime = "yyyy-MM-dd hh:mm";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
		SimpleDateFormat withTimeDateFormat = new SimpleDateFormat(patternWithTime);
		List<DrawingCsvDto> csvDtoList = drawings.stream().map(nodeRef -> {
			NodeService nodeService = serviceRegistry.getNodeService();
			count.addAndGet(1);
			if (count.get() % 500 == 0) {
				log.debug("{} Drawings precessed...", count.get());
			}
			Date lastStatusChangedDate = (Date) nodeService.getProperty(nodeRef, DrawingModel.PROP_LAST_STATUS_CHANGED_DATE);
			String lscDate = "Not defined";
			if (lastStatusChangedDate != null) {
				lscDate = withTimeDateFormat.format(lastStatusChangedDate);
			}
			Date deliveryDate = (Date) nodeService.getProperty(nodeRef, DrawingModel.PROP_DELIVERY_DATE);
			String dDate = "Not defined";
			if (deliveryDate != null) {
				dDate = simpleDateFormat.format(deliveryDate);
			}
			return DrawingCsvDto.builder()
					.name((String) nodeService.getProperty(nodeRef, ContentModel.PROP_NAME))
					.descriptiveTitle((String) nodeService.getProperty(nodeRef, DrawingModel.PROP_DESCRIPTIVE_TITLE))
					.magCode((String) nodeService.getProperty(nodeRef, DrawingModel.PROP_MAG_CODE))
					.primaryFormat((String) nodeService.getProperty(nodeRef, DrawingModel.PROP_PRIMARY_FORMAT))
					.drawingType((String) nodeService.getProperty(nodeRef, DrawingModel.PROP_DRAWING_TYPE))
					.createdFor((String) nodeService.getProperty(nodeRef, DrawingModel.PROP_CREATED_FOR))
					.securityStatus((String) nodeService.getProperty(nodeRef, DrawingModel.PROP_SECURITY_STATUS))
					.requester((String) nodeService.getProperty(nodeRef, DrawingModel.PROP_REQUESTER))
					.deliveryDate(dDate)
					.lastStatusChangedDate(lscDate)
					.drawingVersion((String) nodeService.getProperty(nodeRef, DrawingModel.PROP_DRAWING_VERSION))
					.requiredRenditions(getRequiredRenditions(nodeRef))
					.build();
		}).collect(Collectors.toList());
		log.debug("CSV report file is generated!");
		File tempFile = TempFileProvider.createTempFile("report-" + System.nanoTime(), ".csv");
		createCsvFile(tempFile.getAbsolutePath(), DrawingCsvDto.class, csvDtoList);
		return createReportNode(tempFile);
	}

	private String getRequiredRenditions(NodeRef nodeRef) {
		List<String> rr = (List<String>) serviceRegistry.getNodeService().getProperty(nodeRef, DrawingModel.PROP_REQUIRED_RENDITIONS);
		return rr == null ? "" : String.join(",", rr);
	}

	private NodeRef createReportNode(File tempFile) {
		Map<QName, Serializable> targetProps = new HashMap<>();
		targetProps.put(ContentModel.PROP_NAME, tempFile.getName());
		NodeRef parent = SearchUtils.getReportsNode(serviceRegistry);
		NodeRef report = serviceRegistry.getNodeService()
				.createNode(parent,
						ContentModel.ASSOC_CONTAINS,
						QName.createQName(NamespaceService.CONTENT_MODEL_1_0_URI, tempFile.getName()),
						ContentModel.TYPE_CONTENT,
						targetProps).getChildRef();
		try (InputStream inputStream = new BufferedInputStream(new FileInputStream(tempFile))) {
			ContentWriter contentWriter = serviceRegistry.getContentService().getWriter(report, ContentModel.PROP_CONTENT, true);
			contentWriter.guessMimetype(tempFile.getName());
			contentWriter.putContent(inputStream);
			return report;
		} catch (IOException e) {
			throw new AlfrescoRuntimeException("Failed to create CSV file inside Reports folder: " + e.getMessage());
		} finally {
			log.debug("Temp file {} is deleted {}", tempFile.getName(), tempFile.delete());
		}
	}

	private String buildQuery(DrawingSearchDto drawingSearchDto) {
		String name = drawingSearchDto.getName();
		String magCode = drawingSearchDto.getMagCode();
		String primaryFormat = drawingSearchDto.getPrimaryFormat();
		String descriptiveTitle = drawingSearchDto.getDescriptiveTitle();
		String drawingType = drawingSearchDto.getDrawingType();
		String createdFor = drawingSearchDto.getCreatedFor();
		String securityStatus = drawingSearchDto.getSecurityStatus();
		String requester = drawingSearchDto.getRequester();
		String deliveryDate = drawingSearchDto.getDeliveryDate();
		String requiredRenditions = drawingSearchDto.getRequiredRenditions();
		String version = drawingSearchDto.getDrawingVersion();

		StringBuilder queryBuilder = new StringBuilder();
		queryBuilder.append("TYPE:'draw:drawingSpace'");
		if (!StringUtils.isEmpty(name)) {
			queryBuilder.append(" AND ");
			queryBuilder.append("cm:name: '*").append(name).append("*'");
		}
		if (!StringUtils.isEmpty(magCode)) {
			queryBuilder.append(" AND ");
			queryBuilder.append("draw:magCode: '").append(magCode).append("'");
		}
		if (!StringUtils.isEmpty(primaryFormat)) {
			queryBuilder.append(" AND ");
			queryBuilder.append("draw:primaryFormat: '").append(primaryFormat).append("'");
		}
		if (!StringUtils.isEmpty(descriptiveTitle)) {
			queryBuilder.append(" AND ");
			queryBuilder.append("draw:descriptiveTitle: '*").append(descriptiveTitle).append("*'");
		}
		if (!StringUtils.isEmpty(drawingType)) {
			queryBuilder.append(" AND ");
			queryBuilder.append("draw:drawingType: '").append(drawingType).append("'");
		}
		if (!StringUtils.isEmpty(createdFor)) {
			queryBuilder.append(" AND ");
			queryBuilder.append("draw:createdFor: '*").append(createdFor).append("*'");
		}
		if (!StringUtils.isEmpty(securityStatus)) {
			queryBuilder.append(" AND ");
			queryBuilder.append("draw:securityStatus: '").append(securityStatus).append("'");
		}
		if (!StringUtils.isEmpty(requester)) {
			queryBuilder.append(" AND ");
			queryBuilder.append("draw:requester: '*").append(requester).append("*'");
		}
		if (!StringUtils.isEmpty(deliveryDate)) {
			queryBuilder.append(" AND ");
			queryBuilder.append("draw:deliveryDate: '[MIN TO ").append(deliveryDate).append("]'");
		}
		if (!StringUtils.isEmpty(requiredRenditions)) {
			queryBuilder.append(" AND ");
			String[] tokens = requiredRenditions.split(",");
			queryBuilder.append("(");
			for (int i = 0; i < tokens.length; i++) {
				queryBuilder.append(" draw:requiredRenditions: '").append(tokens[i]).append("'");
				if (i != tokens.length - 1) {
					queryBuilder.append(" OR ");
				}
			}
			queryBuilder.append(")");
		}
		if (!StringUtils.isEmpty(version)) {
			queryBuilder.append(" AND ");
			queryBuilder.append("draw:drawingVersion: '").append(version).append("'");
		}
		return queryBuilder.toString();
	}


}
